import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { grey400, grey600, grey800, amber600 } from 'material-ui/styles/colors';

import App from './components/App';
import { injectGlobal } from 'styled-components';

const muiTheme = getMuiTheme(darkBaseTheme, {
	palette: {
		textColor: grey400
	},
	table: {
		backgroundColor: 'rgba(255, 255, 255, 0)'
	},
	tableHeaderColumn: {
		textColor: '#696A6D'
	},
	tableRow: {
		textColor: '#AAABAD',
		borderColor: 'rgba(255, 255, 255, 0.13)',
		hoverColor: 'rgba(0, 0, 0, 0.15)',
		selectedColor: 'rgba(0, 0, 0, 0.3)',
		stripeColor: grey800
	},
	bottomNavigation: {
		selectedColor: '#f7b100',
		unselectedColor: grey400
	},
	toggle: {
		thumbOnColor: amber600,
		trackOnColor: grey400,
		thumbOffColor: grey400,
		trackOffColor: grey600
	},
	flatButton: {
		primaryTextColor: amber600
	},
	textField: {
		focusColor: amber600
	}
});

//for chrome autofill
injectGlobal`
	input:-webkit-autofill {
	    -webkit-box-shadow: 0 0 0 30px #383838 inset;
	}
	input:-webkit-autofill {
	    -webkit-text-fill-color: #BDBDBD !important;
	}
	.tableFontSize {
		font-size: 15px;
	}
`;

render(
	<BrowserRouter>
		<MuiThemeProvider muiTheme={muiTheme}>
			<App />
		</MuiThemeProvider>
	</BrowserRouter>,
	document.getElementById('root')
);
