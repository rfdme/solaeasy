module.exports = {
  websocketURL: process.env.REACT_APP_WEBSOCKET_URL || 'http://localhost:8082',
  proxyTarget: process.env.PROXY_TARGET || 'http://localhost:8081'
}