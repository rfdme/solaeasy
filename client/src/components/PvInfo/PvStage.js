import React from 'react';
import styled from 'styled-components';
import { black, grey400, amber500 } from 'material-ui/styles/colors';
import { sprintf } from 'sprintf-js';
import { TitleContainer, Title, TitleIcon, TitleLabel, Hr } from '../Layout';
import IconSvg from '../../svg/main_icon4.svg';

import checkIcon from './Check.svg';

const Stage = ({ index, text, checked, last, final, handleClick }) => {
  return (
    <Step
      color={last ? black : grey400}
      bgColor={last ? amber500 : null}
      border={final ? '0' : '1px'}
      clickable={checked || last ? true : false}
      onClick={() => (checked || last ? handleClick(index) : null)}
    >
      {checked ? <Checked src={checkIcon} /> : <Unchecked />}
      {sprintf('%s. %s', index + 1, text)}
    </Step>
  );
};

const PvStage = ({ stage, handleClick }) => {
  const stages = [
    '簽訂租約',
    '取得併聯審查意見書',
    '取得同意備案函',
    '簽訂工程合約',
    '完工併聯',
    '取得銀行貸款撥款',
    '能源局設備登記',
    '建置完成'
  ];
  return (
    <MainBlock>
      <TitleContainer>
        <Title>
          <TitleIcon src={IconSvg} />
          <TitleLabel>建置時程</TitleLabel>
        </Title>
      </TitleContainer>
      <Hr style={{ marginBottom: '15px' }} />
      {stages.map((x, i) => (
        <Stage
          key={i}
          index={i}
          text={x}
          checked={i < stage ? true : false}
          last={i === stage ? true : false}
          final={i === stages.length - 1 ? true : false}
          handleClick={handleClick}
        />
      ))}
    </MainBlock>
  );
};

const MainBlock = styled.div`
  display: flex;
  flex-direction: column;
`;

const Step = styled.div`
  height: 30px;
  padding: 2em 1em 1em 1em;
  border-bottom: ${props => props.border} solid ${grey400};
  color: ${props => props.color};
  background-color: ${props => props.bgColor};
  cursor: ${props => (props.clickable ? 'pointer' : null)};
`;

const Checked = styled.img`
  margin-right: 1em;
  width: 1em;
`;
const Unchecked = styled.span`
  display: inline-block;
  margin-right: 1em;
  width: 1em;
`;

export default PvStage;
