import React from 'react';
import styled from 'styled-components';
import { TitleContainer, Title, TitleIcon, TitleLabel, Hr } from '../Layout';
import IconSvg from '../../svg/main_icon4.svg';
import LineSvg from '../../svg/line_step.png';
import DefaultPoint from '../../svg/icon_menu_default.svg';
import YellowPoint from '../../svg/icon_menu_yellow.svg';
import YellowHoverPoint from '../../svg/icon_menu_yellow_hover.svg';

const PvStage2 = ({ stage, handleClick }) => {
  let width = 1366 - 250 - 100;
  if (window.innerWidth <= 1280) {
    width = 1280 - 250 - 100;
  }
  if (window.innerWidth <= 1024) {
    width = 1024 - 200 - 50;
  }

  //console.log('width', window.innerWidth, width);

  const Width = width;
  const StepCount = 7;
  const MarginLeft = 50;
  const MarginRight = 50;
  const MarginTop = 100;
  const StepMarginTop = 200;
  const LineWidth = Width - MarginLeft - MarginRight;
  const StepWidth = LineWidth / (StepCount - 1);

  const stages = [
    '已簽訂租約',
    '台電併聯審查核可',
    '已簽訂工程合約',
    '取得同意備案函',
    '已併聯掛表',
    '完成設備登記',
    '開始正式售電'
  ];

  let points = [];
  let steps = [];
  const currentStep = stage - 1;
  for (let i = 0; i < StepCount; i++) {
    if (i < currentStep) {
      points.push(
        <Point
          key={i}
          src={YellowPoint}
          top={MarginTop}
          x={i * StepWidth + MarginLeft}
          style={{
            width: '20px',
            height: '20px'
          }}
        />
      );
    } else if (i === currentStep) {
      points.push(
        <Point
          key={i}
          src={YellowHoverPoint}
          top={MarginTop}
          x={i * StepWidth + MarginLeft}
          style={{
            width: '60px',
            height: '60px'
          }}
        />
      );
    } else {
      points.push(
        <Point
          key={i}
          src={DefaultPoint}
          top={MarginTop}
          x={i * StepWidth + MarginLeft}
          style={{
            width: '20px',
            height: '20px'
          }}
        />
      );
    }

    steps.push(
      <Step
        key={i}
        top={StepMarginTop}
        x={i * StepWidth + MarginLeft}
        style={
          i === currentStep
            ? {
                color: '#ffffff',
                fontSize: '18px',
                marginTop: '-5px',
                width: '150px'
              }
            : null
        }
        onClick={i <= currentStep ? () => handleClick(i) : null}
        clickable={i <= currentStep ? true : false}
      >
        <StepLabel>Step {i + 1}</StepLabel>
        {stages[i]}
      </Step>
    );
  }
  return (
    <MainBlock>
      <TitleContainer>
        <Title>
          <TitleIcon src={IconSvg} />
          <TitleLabel>建置時程</TitleLabel>
        </Title>
      </TitleContainer>
      <Hr />
      <Container>
        <DefalutLine top={MarginTop} left={MarginLeft} width={LineWidth} />
        <Line
          src={LineSvg}
          top={MarginTop}
          left={MarginLeft}
          x={currentStep > 0 ? currentStep * StepWidth : 0}
        />
        {points}
        {steps}
      </Container>
    </MainBlock>
  );
};

const Container = styled.div`
  position: relative;
  width: 100%;
  height: 300px;
`;

const DefalutLine = styled.div`
  position: absolute;
  top: ${props => props.top + 'px'};
  left: ${props => props.left + 'px'};
  width: ${props => props.width + 'px'};
  height: 5px;
  background-color: #313235;
  transform: translateY(-50%);
`;

const Line = styled.img`
  position: absolute;
  top: ${props => props.top + 'px'};
  left: ${props => props.left + 'px'};
  width: ${props => props.x + 'px'};
  height: 5px;
  transform: translateY(-50%);
`;

const Point = styled.img`
  position: absolute;
  top: ${props => props.top + 'px'};
  left: ${props => props.x + 'px'};
  transform: translate(-50%, -50%);
`;

const Step = styled.div`
  position: absolute;
  width: 120px;
  top: ${props => props.top + 'px'};
  left: ${props => props.x + 'px'};
  transform: translateX(-50%);
  color: #696a6d;
  font-size: 15px;
  text-align: center;
  cursor: ${props => (props.clickable ? 'pointer' : null)};
`;

const StepLabel = styled.div`
  margin-bottom: 15px;
`;

const MainBlock = styled.div`
  display: flex;
  flex-direction: column;
`;

export default PvStage2;
