import React, { Component } from 'react';
import styled from 'styled-components';
import { compose, withProps } from 'recompose';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from 'react-google-maps';
import IconButton from 'material-ui/IconButton';
import MyLocationIcon from 'material-ui/svg-icons/maps/my-location';
import { TitleContainer, Title, TitleIcon, TitleLabel, Hr } from '../Layout';
import IconSvg from '../../svg/main_icon5.svg';

const MyMapComponent = compose(
  withProps({
    googleMapURL: 'https://maps.google.com/maps/api/js?key=AIzaSyCQ3UTayHzAcoPXJUM3Ag_-FBxII1o6fNg',
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `350px` }} />,
    mapElement: <div style={{ height: `100%` }} />
  }),
  withScriptjs,
  withGoogleMap
)(props => (
  <GoogleMap
    defaultZoom={7}
    defaultCenter={{ lat: 23.6, lng: 121 }}
    mapTypeId="satellite"
    options={{
      mapTypeControl: false,
      fullscreenControl: false,
      streetViewControl: false,
      gestureHandling: 'cooperative'
    }}
    center={props.position}
    zoom={19}
  >
    {props.isMarkerShown && <Marker position={props.position} />}
    <div>
      <MyReset>
        <IconButton
          style={{
            padding: '0',
            width: '28px',
            height: '28px'
          }}
          iconStyle={{ color: 'dimgray' }}
          onClick={() => {
            props.reset();
          }}
        >
          <MyLocationIcon />
        </IconButton>
      </MyReset>
    </div>
  </GoogleMap>
));

class PvMap extends Component {
  constructor(props) {
    super(props);
    this.forceUpdateHandler = this.forceUpdateHandler.bind(this);
    this.state = {
      key: 0
    };
  }

  forceUpdateHandler() {
    this.setState({ key: this.state.key + 1 });
  }
  render() {
    return (
      <MainBlock>
        <TitleContainer>
          <Title>
            <TitleIcon src={IconSvg} />
            <TitleLabel>電廠位置</TitleLabel>
          </Title>
        </TitleContainer>
        <Hr style={{ marginBottom: '15px' }} />
        {this.props.position ? (
          <MyMapComponent key={this.state.key} {...this.props} reset={this.forceUpdateHandler} />
        ) : (
          <ComingSoon>建置中</ComingSoon>
        )}
      </MainBlock>
    );
  }
}

const MainBlock = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
`;

const MyReset = styled.div`
  display: inline-block;
  position: relative;
  top: -109px;
  left: 312px;
  background: whitesmoke;
`;

const ComingSoon = styled.div`
  color: #aaabad;
`;

export default PvMap;
