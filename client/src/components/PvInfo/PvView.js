import React from 'react';
import styled from 'styled-components';

import { TitleContainer, Title, TitleIcon, TitleLabel, Hr } from '../Layout';
import IconSvg from '../../svg/main_icon6.svg';

const PvView = ({ imgUrl }) => (
  <MainBlock>
    <TitleContainer>
      <Title>
        <TitleIcon src={IconSvg} />
        <TitleLabel>電廠實景</TitleLabel>
      </Title>
    </TitleContainer>
    <Hr style={{ marginBottom: '15px' }} />
    {imgUrl ? (
      <Content>
        <MainView>
          <Img src={imgUrl} alt="PV View" />
        </MainView>
        {/*
      <OtherView>
        <MockImg1>TODO</MockImg1>
        <MockImg2>TODO</MockImg2>
        <MockImg3>TODO</MockImg3>
      </OtherView>
      */}
      </Content>
    ) : (
      <ComingSoon>建置中</ComingSoon>
    )}
  </MainBlock>
);

const MainBlock = styled.div`
  flex: 1;
`;

const Content = styled.div`
  display: flex;
  max-height: 350px;
  justify-content: center;
  align-items: center;
`;

const MainView = styled.div`
  height: 350px;
`;
/*
const OtherView = styled.div`
  flex: 1;
  color: white;
  display: flex;
  flex-direction: column;
`;

const MockImg1 = styled.div`
  width: 150px;
  flex: 1;
  border: 1px solid white;
  margin-bottom: 0.5em;
`;
const MockImg2 = styled.div`
  width: 150px;
  flex: 1;
  border: 1px solid white;
`;
const MockImg3 = styled.div`
  width: 150px;
  flex: 1;
  border: 1px solid white;
  margin-top: 0.5em;
`;
*/
const Img = styled.img`
  max-height: 100%;
  @media screen and (max-width: 1024px) {
    max-width: 100%;
  }
`;

const ComingSoon = styled.div`
  color: #aaabad;
`;

export default PvView;
