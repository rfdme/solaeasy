import React from 'react';
import styled from 'styled-components';
import {
  TitleContainer,
  Title,
  TitleIcon,
  TitleLabel,
  TitleRight,
  Hr
} from '../Layout';
import IconSvg from '../../svg/main_icon3.svg';
import SttwSvg from '../../svg/icon_sttw.svg';
import { pvStatusCodes } from '../Utils';
import BigNumber from 'bignumber.js';
const BN = BigNumber.clone({ DECIMAL_PLACES: 16, ROUNDING_MODE: 1 });

const PvData = ({ data }) => {
  return (
    <div>
      <TitleContainer>
        <Title>
          <TitleIcon src={IconSvg} />
          <TitleLabel>{data.id + ' ' + data.name}</TitleLabel>
        </Title>
        <TitleRight>
          <SttwIcon src={SttwSvg} /> <Sttw>{data.price}</Sttw>
        </TitleRight>
      </TitleContainer>
      <Hr />
      <Row>
        <Col>
          <Label>電廠代號</Label>
          <Value>{data.id}</Value>
        </Col>
        <Gap />
        <Col>
          <Label>專案名稱</Label>
          <Value>{data.name}</Value>
        </Col>
        <Gap />
        <Col>
          <Label>專案代號</Label>
          <Value>{data.projectCode}</Value>
        </Col>
        <Gap />
        <Col>
          <Label>裝置容量</Label>
          <Value>{data.capacity}瓩</Value>
        </Col>
      </Row>
      <Row>
        <Col>
          <Label>狀態</Label>
          <Value>{pvStatusCodes(data.status)}</Value>
        </Col>
        <Gap />
        <Col>
          <Label>躉售電價</Label>
          <Value>{data.sellPrice}</Value>
        </Col>
        <Gap />
        <Col span={2}>
          <Label>地號</Label>
          <Value>{data.location}</Value>
        </Col>
        <Gap />
      </Row>
      <Row>
        <Col>
          <Label>發行量</Label>
          <Value>{BN(data.ownedCapital / 10).toFormat()}</Value>
        </Col>
        <Gap />
        <Col>
          <Label>發行價</Label>
          <Value>{data.initialPrice}元</Value>
        </Col>
        <Gap />
        <Col span={2}>
          <Label>發行者</Label>
          <Value>{data.owner}</Value>
        </Col>
        <Gap />
      </Row>
    </div>
  );
};

const SttwIcon = styled.img`
  width: 45px;
  margin-top: -20px;
  margin-right: 10px;
`;

const Sttw = styled.span`
  font-size: 50px;
  color: #2bbc8f;
  font-family: Montserrat;
`;

const Row = styled.div`
  display: flex;
  padding-bottom: 0.5em;
  margin-top: 1em;
`;

const Gap = styled.div`
  width: 20px;
`;

const EmptyCol = styled.div`
  flex: 1;
  display: flex;
  font-size: 15px;
`;

const Col = styled(EmptyCol)`
  flex: ${props => (props.span ? props.span : 1)};
  border-bottom: 1px solid #37373e;
  padding-bottom: 1em;
  line-height: 1.2;
`;

const Label = styled.div`
  width: 6em;
  color: #696a6d;
`;

const Value = styled.div`
  flex: 1;
  color: #aaabad;
  text-align: right;
`;

export default PvData;
