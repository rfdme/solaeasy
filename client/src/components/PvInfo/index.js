import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import { amber600 } from 'material-ui/styles/colors';
import styled from 'styled-components';
import axios from 'axios';

import PvChart from './PvChart';
import PvData from './PvData';
import { PvList } from '../SideBar';
import PvMap from './PvMap';
import PvStage2 from './PvStage2';
import PvView from './PvView';
import Footer from '../Footer';
import {
  Page,
  HalfPaper,
  SideBar,
  Content,
  Column,
  ColumnGap
} from '../Layout';

class Home extends Component {
  constructor(props) {
    super(props);

    this.handleSelectedPV = this.handleSelectedPV.bind(this);
    this.handleLoadPvData = this.handleLoadPvData.bind(this);
    this.handleLoadPowerData = this.handleLoadPowerData.bind(this);
    this.handleLoadPvImg = this.handleLoadPvImg.bind(this);
    this.handleLoadPvStage = this.handleLoadPvStage.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);

    this.state = {
      pvData: null,
      powerData: {},
      pvImg: null,
      pvCoord: null,
      isMarkerShown: false,
      pvStage: [],
      open: false
    };
  }

  handleSelectedPV(id) {
    this.handleLoadPvData(id);
    this.handleLoadPowerData(id);
    this.handleLoadPvImg(id);
    this.handleLoadPvStage(id);
  }

  handleLoadPvData(id) {
    return axios
      .get(`/api/v1/pv/project/` + id)
      .then(res => {
        if (res.data.status === true) {
          this.setState({
            pvData: res.data.result.pvData,
            pvCoord: {
              lat: res.data.result.pvData.coordLat,
              lng: res.data.result.pvData.coordLng
            },
            isMarkerShown: true
          });
        } else {
          // TODO: handle error_code, msg
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  handleLoadPowerData(id) {
    return axios
      .get(`/api/v1/pv/values/` + id)
      .then(res => {
        if (res.data.status === true) {
          this.setState({ powerData: res.data.result.powerData });
        } else {
          // TODO: handle error_code, msg
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  handleLoadPvImg(id) {
    return axios
      .get(`/api/v1/pv/img/` + id)
      .then(res => {
        if (res.data.status === true) {
          this.setState({ pvImg: res.data.result.pvImg });
        } else {
          // TODO: handle error_code, msg
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  handleLoadPvStage(id) {
    return axios
      .get(`/api/v1/pv/stage/` + id)
      .then(res => {
        if (res.data.status === true) {
          this.setState({ pvStage: res.data.result.pvStage });
        } else {
          // TODO: handle error_code, msg
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  handleOpen(stage) {
    let content = '尚無說明';
    let file = null;
    for (let i = 0; i < this.state.pvStage.length; i++) {
      let data = this.state.pvStage[i];
      if (data.stage === stage) {
        content = data.content;
        file = data.file;
      }
    }
    this.setState({ open: true, stageContent: content, stageFile: file });
  }

  handleClose() {
    this.setState({ open: false });
  }

  render() {
    const {
      pvData,
      powerData,
      pvImg,
      pvCoord,
      isMarkerShown,
      stageContent,
      stageFile
    } = this.state;

    const actions = [
      <FlatButton
        label="確認"
        primary={true}
        keyboardFocused={true}
        onClick={this.handleClose}
      />
    ];

    return (
      <Page>
        <div className="page-wrap">
          <div className="container">
            <Column>
              <SideBar>
                <PvList
                  auth={this.props.auth}
                  onSelect={this.handleSelectedPV}
                />
              </SideBar>
              <Content>
                {pvData ? (
                  <HalfPaper style={{ paddingBottom: '28px' }}>
                    <PvData data={pvData} />
                    <PvStage2
                      stage={pvData.stage}
                      handleClick={this.handleOpen}
                    />
                    <PvChart powerData={powerData} />
                    <Column>
                      <ColumnMap>
                        <PvMap
                          position={pvCoord}
                          isMarkerShown={isMarkerShown}
                        />
                      </ColumnMap>
                      <ColumnGap />
                      <ColumnImg>
                        <PvView imgUrl={pvImg} />
                      </ColumnImg>
                    </Column>
                  </HalfPaper>
                ) : null}
              </Content>
            </Column>
          </div>
        </div>
        <Dialog
          title="時程說明"
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
        >
          <StageContent>{stageContent}</StageContent>
          {stageFile ? (
            <StageLink href={'/api/v1/files/' + stageFile} target="_blank">
              查看建置相關合約
            </StageLink>
          ) : null}
        </Dialog>
        <Footer />
      </Page>
    );
  }
}

const ColumnMap = styled.div`
  width: 350px;
  height: auto;
`;

const ColumnImg = styled.div`
  flex: 1;
`;

const StageContent = styled.div`
  margin-bottom: 1em;
`;

const StageLink = styled.a`
  color: ${amber600};
`;

export default Home;
