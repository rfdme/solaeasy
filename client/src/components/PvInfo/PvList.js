import React, { Component } from 'react';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';

class PvList extends Component {
	constructor(props) {
		super(props);

		this.handleChange = this.handleChange.bind(this);

		this.state = {
			selectedId: props.selectedId
		};
	}

	handleChange(event, index, value) {
		this.setState({ selectedId: value });
		this.props.onSelect(value);
	}

	render() {
		const { data, selectedId } = this.props;

		return (
			<div>
				<DropDownMenu
					value={selectedId}
					onChange={this.handleChange}
					style={{ width: '100%' }}
				>
					{data.map((x, i) => (
						<MenuItem
							key={i}
							value={x.id}
							primaryText={x.id + ' ' + x.name}
						/>
					))}
				</DropDownMenu>
			</div>
		);
	}
}

export default PvList;
