import React from 'react';
import { yellow100, amber500 } from 'material-ui/styles/colors';

import { Bar } from 'react-chartjs-2';
import { TitleContainer, Title, TitleIcon, TitleLabel, Hr } from '../Layout';
import IconSvg from '../../svg/main_icon1.svg';

const PvChart = ({ powerData }) => {
	const data = {
		labels: powerData.labels,
		datasets: [
			{
				label: '發電量(瓩)',
				backgroundColor: yellow100,
				borderColor: amber500,
				borderWidth: 1,
				hoverBackgroundColor: amber500,
				hoverBorderColor: yellow100,
				data: powerData.values
			}
		]
	};

	return (
		<div>
			<TitleContainer>
				<Title>
					<TitleIcon src={IconSvg} />
					<TitleLabel>發電數據</TitleLabel>
				</Title>
			</TitleContainer>
			<Hr />
			<Bar
				data={data}
				width={100}
				height={250}
				options={{
					maintainAspectRatio: false
				}}
			/>
		</div>
	);
};

export default PvChart;
