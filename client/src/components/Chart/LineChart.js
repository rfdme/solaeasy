import React from 'react';
import styled from 'styled-components';
import {
  Stage,
  Layer,
  Group,
  Rect,
  Text,
  Line,
  Circle,
  RegularPolygon
} from 'react-konva';
import ContainerDimensions from 'react-container-dimensions';

import labelAlgorithm from '../Utils/labelAlgorithm';
import { strip } from '../Utils';

const paddingLeft = 20;
const hintHeight = 20;

const xAxisHeight = 30;
const xAxisHorizontal = 20;
const xLabelOffsetX = -15;
const xLabelOffsetY = 5;

const yAxisBaseWidth = 10;
const yLabelOffsetX = 5;
const yLabelOffsetY = -15;

const bgColor = 'rgba(255, 255, 255, 0)';
const lineColor = '#f7b100';
const fillColor = '#60512B';

const charWidth = 6.4;

const example = {
  labels: [
    '00:00',
    '01:00',
    '02:00',
    '03:00',
    '04:00',
    '05:00',
    '06:00',
    '07:00',
    '08:00',
    '09:00',
    '10:00',
    '11:00',
    '12:00',
    '13:00',
    '14:00',
    '15:00',
    '16:00',
    '17:00',
    '18:00',
    '19:00'
  ],
  values: [
    10.0,
    10.1,
    9.9,
    10.2,
    9.8,
    10.3,
    9.7,
    10.4,
    9.6,
    10,
    10.0,
    10.1,
    9.9,
    10.2,
    9.8,
    10.3,
    9.7,
    10.4,
    9.6,
    10
  ],
  tickLabels: [
    '00:00',
    '02:00',
    '04:00',
    '06:00',
    '08:00',
    '10:00',
    '12:00',
    '14:00',
    '16:00',
    '18:00'
  ]
};

class LineChart extends React.Component {
  constructor(props) {
    super(props);

    this.handleMouseMove = this.handleMouseMove.bind(this);

    this.state = {
      width: props.width,
      height: props.height,
      circleX: null,
      circleY: null,
      hintLabel: '',
      hintValue: '',
      labels: props.labels ? props.labels : example.labels,
      values: props.values ? props.values : example.values,
      tickLabels: props.tickLabels ? props.tickLabels : example.tickLabels,
      yAxisWidth: this.getAxisYWidth(
        props.labels ? props.labels : example.labels,
        props.values ? props.values : example.values
      ),
      hintWidth1: 0,
      hintWidth2: 0
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.labels && nextProps.values) {
      this.setState({
        width: nextProps.width,
        height: nextProps.height,
        labels: nextProps.labels,
        values: nextProps.values,
        tickLabels: nextProps.tickLabels,
        yAxisWidth: this.getAxisYWidth(nextProps.labels, nextProps.values)
      });
    }
  }

  getAxisYWidth(labels, values) {
    let length1 = 0;
    let length2 = 0;

    for (let i in labels) {
      const v = labels[i];
      length1 = Math.max(length1, v.toString().length * charWidth);
    }

    for (let i in values) {
      const v = values[i];
      length2 = Math.max(length2, v.toString().length * charWidth);
    }

    return Math.max(yAxisBaseWidth, length1 + length2);
  }

  getAxisX() {
    const max = this.state.labels.length;
    const min = 0;
    const tick = 1;

    return { max, min, tick };
  }

  getAxisY() {
    const max = Math.max(...this.state.values);
    const min = Math.min(...this.state.values);
    const result = labelAlgorithm(min, max, 5);

    return {
      max: result.niceMaximum,
      min: result.niceMinimum,
      tick: result.tickSpacing
    };
  }

  draw({ w, h }) {
    return (
      <Stage width={w} height={h}>
        <Layer>
          <Rect //底
            width={w}
            height={h}
            fill={bgColor}
          />
        </Layer>
        {this.drawChart({
          x: paddingLeft,
          y: 0,
          w: w - paddingLeft,
          h: h
        })}
      </Stage>
    );
  }

  valueToAxisX(max, min, length, v) {
    return length / (max - min) * (v - min);
  }

  valueToAxisY(max, min, length, v) {
    return length - length / (max - min) * (v - min);
  }

  drawAxisX({ w, h }) {
    //繪出刻度線&值
    const { max, min, tick } = this.getAxisX();
    let lines = [];
    let currentTick = min;
    let currentValue = this.state.labels[currentTick];
    let k = 0;
    while (currentTick <= max) {
      if (this.state.tickLabels.indexOf(currentValue) > -1) {
        if (currentTick !== max) {
          lines.push(
            <Text
              key={k++}
              x={this.valueToAxisX(max, min, w, currentTick) + xLabelOffsetX}
              y={xLabelOffsetY}
              text={currentValue}
              fill={'#999999'}
            />
          );
        }
        lines.push(
          <Line
            key={k++}
            points={[
              this.valueToAxisX(max, min, w, currentTick),
              xAxisHorizontal,
              this.valueToAxisX(max, min, w, currentTick),
              h
            ]}
            stroke={'#999999'}
            strokeWidth={1}
          />
        );
      }
      currentTick += tick;
      currentValue = this.state.labels[currentTick];
    }
    return (
      <Group>
        <Rect //底
          width={w}
          height={h}
          fill={bgColor}
        />
        <Line
          points={[0, xAxisHorizontal, w, xAxisHorizontal]}
          stroke={'#999999'}
          strokeWidth={1}
        />
        {lines}
      </Group>
    );
  }

  drawAxisY({ w, h }) {
    //繪出刻度線&值
    const { max, min, tick } = this.getAxisY();
    let lines = [];
    let currentTick = min;
    let k = 0;
    while (currentTick < max) {
      if (strip(currentTick) > min && strip(currentTick) < max) {
        lines.push(
          <Text
            key={k++}
            x={yLabelOffsetX}
            y={this.valueToAxisY(max, min, h, currentTick) + yLabelOffsetY}
            text={strip(currentTick).toString()}
            fill={'#999999'}
          />
        );
        lines.push(
          <Line
            key={k++}
            points={[
              1,
              this.valueToAxisY(max, min, h, currentTick),
              10,
              this.valueToAxisY(max, min, h, currentTick)
            ]}
            stroke={'#999999'}
            strokeWidth={1}
          />
        );
      }
      currentTick += tick;
    }

    return (
      <Group>
        <Rect //底
          width={w}
          height={h}
          fill={bgColor}
        />
        <Line
          points={[0, -(xAxisHeight - xAxisHorizontal), 0, h]}
          stroke={'#999999'}
          strokeWidth={1}
        />
        {lines}
      </Group>
    );
  }

  drawLinePanel({ w, h }) {
    const axisX = this.getAxisX();
    const axisY = this.getAxisY();
    let linePoints = [];
    let fillPoints = [0, h];
    let x, y;
    for (let i = 0; i < this.state.values.length; i++) {
      x = this.valueToAxisX(axisX.max, axisX.min, w, i);
      y = this.valueToAxisY(axisY.max, axisY.min, h, this.state.values[i]);
      linePoints.push(x);
      linePoints.push(y);
      fillPoints.push(x);
      fillPoints.push(y);
    }

    fillPoints.push(x);
    fillPoints.push(h);

    //繪出線圖
    return (
      <Group>
        <Rect //底
          width={w}
          height={h}
          fill={bgColor}
        />
        <Line //折線填滿
          points={fillPoints}
          strokeWidth={0}
          fill={fillColor}
          closed={true}
        />
        <Line //折線
          points={linePoints}
          stroke={lineColor}
          strokeWidth={2}
          lineCap={'round'}
          lineJoin={'round'}
        />
      </Group>
    );
  }

  drawDashLine({ w, h }) {
    if (this.state.circleX === null || this.state.circleY === null) {
      return null;
    }

    return (
      <Group>
        <Line //垂直虛線
          points={[
            this.state.circleX,
            -(xAxisHeight - xAxisHorizontal),
            this.state.circleX,
            h
          ]}
          stroke={lineColor}
          strokeWidth={1}
          dash={[2, 2]}
        />
        <Line //水平虛線
          points={[0, this.state.circleY, w, this.state.circleY]}
          stroke={lineColor}
          strokeWidth={1}
          dash={[2, 2]}
        />
        <Circle //圓形標點
          x={this.state.circleX}
          y={this.state.circleY}
          radius={5}
          fill={lineColor}
          stroke="white"
          strokeWidth={1}
        />
      </Group>
    );
  }

  drawHint({ w, h }) {
    if (this.state.circleX === null || this.state.circleY === null) {
      return null;
    }

    return (
      <Group>
        <Rect //底
          width={w}
          height={h}
          fill={bgColor}
        />
        <RegularPolygon //箭頭
          x={this.state.circleX}
          y={3}
          sides={5}
          radius={10}
          fill={lineColor}
        />
        <Group //值標籤
          x={-20}
        >
          <Rect //標籤, TODO: 自動計算標籤大小
            x={this.state.circleX}
            width={5 + this.state.hintWidth1 + 5 + this.state.hintWidth2 + 5}
            height={20}
            fill={lineColor}
          />
          <Text
            x={this.state.circleX + 5}
            y={5}
            text={this.state.hintLabel}
            fill="white"
          />
          <Text
            x={this.state.circleX + 5 + this.state.hintWidth1 + 5}
            y={5}
            text={this.state.hintValue}
            fill="black"
          />
        </Group>
      </Group>
    );
  }

  drawChart({ x, y, w, h }) {
    const yAxisHeight = h - xAxisHeight - hintHeight;
    const xAxisWidth = w - this.state.yAxisWidth;
    const hintWidth = w - this.state.yAxisWidth;
    const LinePanelWidth = w - this.state.yAxisWidth;
    const LinePanelHeight = h - xAxisHeight - hintHeight;
    return (
      <Layer
        x={x}
        y={y}
        onMouseMove={e =>
          this.handleMouseMove(e, LinePanelWidth, LinePanelHeight)
        }
      >
        <Group //X軸
          x={0}
          y={0}
        >
          {this.drawAxisX({ w: xAxisWidth, h: xAxisHeight })}
        </Group>
        <Group //線圖區
          x={0}
          y={xAxisHeight}
        >
          {this.drawLinePanel({
            w: LinePanelWidth,
            h: LinePanelHeight
          })}
        </Group>
        <Group //Y軸
          x={xAxisWidth}
          y={xAxisHeight}
        >
          {this.drawAxisY({
            w: this.state.yAxisWidth,
            h: yAxisHeight
          })}
        </Group>
        <Group //輔助線
          x={0}
          y={xAxisHeight}
        >
          {this.drawDashLine({
            w: LinePanelWidth,
            h: LinePanelHeight
          })}
        </Group>
        <Group //值標籤區
          x={0}
          y={xAxisHeight + LinePanelHeight}
        >
          {this.drawHint({ w: hintWidth, h: hintHeight })}
        </Group>
      </Layer>
    );
  }

  xAxisToNearIndex(max, min, length, x) {
    let index = x / length * (max - min);
    index = Math.round(index, 1);
    if (index < min) {
      index = min;
    } else if (index > max - 1) {
      index = max - 1;
    }

    return index;
  }

  handleMouseMove(e, w, h) {
    if (this.state.labels.length === 0 || this.state.values.length === 0) {
      return;
    }

    const axisX = this.getAxisX();
    const axisY = this.getAxisY();

    const index = this.xAxisToNearIndex(
      axisX.max,
      axisX.min,
      w,
      e.evt.layerX - paddingLeft
    );

    if (index < this.state.values.length) {
      const x = this.valueToAxisX(axisX.max, axisX.min, w, index);
      const y = this.valueToAxisY(
        axisY.max,
        axisY.min,
        h,
        this.state.values[index]
      );

      const label = this.state.labels[index];
      const value = this.state.values[index];

      this.setState({
        circleX: x,
        circleY: y,
        hintLabel: label,
        hintValue: value,
        hintWidth1: label.toString().length * charWidth,
        hintWidth2: value.toString().length * charWidth
      });
    }
  }

  render() {
    return (
      <MyDiv height={this.state.height} width={this.state.width}>
        <ContainerDimensions>
          {({ width, height }) => {
            return this.draw({ w: width, h: height });
          }}
        </ContainerDimensions>
      </MyDiv>
    );
  }
}

const MyDiv = styled.div`
  position: relative;
  height: ${props => props.height};
  width: ${props => props.width};
`;

export default LineChart;
