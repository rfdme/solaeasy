import React, { Component } from 'react';
import axios from 'axios';
import styled from 'styled-components';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import { black, amber600, red500 /*, white*/ } from 'material-ui/styles/colors';

import Footer from '../Footer';

class ResetPwd extends Component {
  constructor(props) {
    super(props);

    this.handleConfirm = this.handleConfirm.bind(this);

    this.state = {
      reseted: false,
      message: ''
    };
  }

  handleConfirm() {
    const email = this.refs.email.input.value;
    axios
      .post('/api/v1/auth/resetPwd', { email })
      .then(res => {
        if (res.data.status === true) {
          this.setState({
            message: '重設成功, 3秒後自動重回登入頁',
            reseted: true
          });
          setTimeout(() => this.props.route.history.push('/Home/Login'), 3000);
        } else {
          this.setState({ message: res.data.msg });
        }
      })
      .catch(err => {
        console.error(err);
      });
  }

  //TODO: 輸入驗證提示
  render() {
    return (
      <MainBlock>
        <div className="page-wrap">
          <Block>
            <Title>重設密碼</Title>
            <label>帳號:</label>
            <TextField name="email" type="email" ref="email" fullWidth />
            <br />
            <label style={{ color: red500 }}>{this.state.message}</label>
            <RaisedButton
              fullWidth
              backgroundColor={amber600}
              labelColor={black}
              onClick={this.handleConfirm}
              style={{ marginTop: '1em' }}
              disabled={this.state.reseted}
            >
              <span style={{ fontSize: '1.5em' }}>確　認</span>
            </RaisedButton>
          </Block>
        </div>
        <Footer />
      </MainBlock>
    );
  }
}

const MainBlock = styled.div`
  flex: 1;
  overflow-y: auto;
`;

const Title = styled.div`
  width: 100%;
  font-size: 2em;
  color: ${amber600};
  text-align: center;
  margin-bottom: 1em;
`;

const Block = styled.div`
  width: 400px;
  position: relative;
  left: 50%;
  transform: translateX(-50%);
  margin: 4em 0;
  padding: 2em;
  border-radius: 0.28571429em;
  border: 1px solid rgba(34, 36, 38, 0.15);
  box-shadow: 0 2px 4px 0 rgba(34, 36, 38, 0.12), 0 2px 10px 0 rgba(34, 36, 38, 0.15);
  background-color: rgba(255, 255, 255, 0.05);
`;

export default ResetPwd;
