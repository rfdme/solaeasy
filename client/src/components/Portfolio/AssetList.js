import React, { Component } from 'react';
import styled from 'styled-components';
import axios from 'axios';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from 'material-ui/Table';
//import moment from 'moment';
import { TitleContainer, Title, TitleIcon, TitleLabel, TitleRight, Hr } from '../Layout';
import IconSvg from '../../svg/main_icon10.svg';
import ListIcon1 from '../../svg/icon_filter_01.svg';
import ListIcon2 from '../../svg/icon_filter_01_hover.svg';
import GraphIcon1 from '../../svg/icon_filter_02.svg';
import GraphIcon2 from '../../svg/icon_filter_02_hover.svg';
//import PowerIcon from '../../svg/icon_list_power.svg';

import PvItem from '../Dashboard/Balance/PvItem';

import { pvStatusCodes } from '../Utils';

import BigNumber from 'bignumber.js';
const BN = BigNumber.clone({ DECIMAL_PLACES: 16, ROUNDING_MODE: 1 });

class AssetList extends Component {
  constructor(props) {
    super(props);

    this.selectDisplayType = this.selectDisplayType.bind(this);
    this.loadUserPv = this.loadUserPv.bind(this);

    this.state = {
      displayType: 'list',
      userPv: null
    };
  }

  selectDisplayType(t) {
    if (t === 'list') {
      this.setState({ displayType: 'list' });
    } else if (this.state.userPv === null) {
      this.loadUserPv();
    } else {
      this.setState({ displayType: 'graph' });
    }
  }

  loadUserPv() {
    axios
      .get(`/api/v1/dashboard/userPv/` + this.props.auth.userId)
      .then(res => {
        if (res.data.status === true) {
          this.setState({
            userPv: res.data.result.userPv,
            displayType: 'graph'
          });
        } else {
          // TODO: handle error_code, msg
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  render() {
    let assetRow = [];
    if (this.state.displayType === 'list') {
      for (let i = 0; i < this.props.assets.pvList.length; i++) {
        let x = this.props.assets.pvList[i];

        /*
        let remaindAmount = BN(x.totalAmount);
        if (x.lastRepayDate) {
          //當日應縮小比例
          const totalDays = moment(x.validityDateEnd, 'YYYY-MM-DD').diff(
            moment(x.validityDateBegin, 'YYYY-MM-DD'),
            'days'
          );
          const remainDays = moment(x.validityDateEnd, 'YYYY-MM-DD').diff(
            moment(x.lastRepayDate, 'YYYY-MM-DD'),
            'days'
          );
          remaindAmount = BN(x.totalAmount)
            .times(remainDays)
            .div(totalDays);
        }
        */
        if (x.holdAmount > 0) {
          assetRow.push(
            <TableRow key={i}>
              <TableRowColumn>
                {/*
                <EtherscanButton
                  onClick={() => {
                    this.props.openEtherscan(x.id);
                  }}
                >
                  <EtherscanIcon src={PowerIcon} />
                  <span>{x.id}</span>
                </EtherscanButton>
              */}
                {x.id}
              </TableRowColumn>
              <TableRowColumn> {x.name} </TableRowColumn>
              <TableRowColumn>{pvStatusCodes(x.status)}</TableRowColumn>
              <TableRowColumn>{BN(x.capacity).toFormat()} kWp</TableRowColumn>
              <TableRowColumn>{BN(x.totalAmount).toFormat(1)}</TableRowColumn>
              <TableRowColumn>{x.initialPrice}</TableRowColumn>
              <TableRowColumn>{BN(x.holdAmount).toFormat(1)}</TableRowColumn>
              {/*<TableRowColumn>{x.price}</TableRowColumn>*/}
              <TableRowColumn>
                {BN(x.holdAmount)
                  .times(x.initialPrice)
                  .toFormat(0)}
                元
              </TableRowColumn>
            </TableRow>
          );
        }
      }
    } else {
      for (let i = 0; i < this.state.userPv.length; i++) {
        const pv = this.state.userPv[i];
        assetRow.push(<PvItem key={i} data={pv} />);
      }
    }
    return (
      <div style={{ paddingBottom: '28px' }}>
        <TitleContainer>
          <Title>
            <TitleIcon src={IconSvg} />
            <TitleLabel>資產清單</TitleLabel>
          </Title>
          <TitleRight>
            <SelectButton
              selected={this.state.displayType === 'list'}
              onClick={() => this.selectDisplayType('list')}
            >
              <ButtonIcon src={this.state.displayType === 'list' ? ListIcon2 : ListIcon1} />
              <span>{'列表顯示'}</span>
            </SelectButton>
            <SelectButton
              selected={this.state.displayType === 'graph'}
              onClick={() => this.selectDisplayType('graph')}
            >
              <ButtonIcon src={this.state.displayType === 'graph' ? GraphIcon2 : GraphIcon1} />
              <span>{'圖表顯示'}</span>
            </SelectButton>
          </TitleRight>
        </TitleContainer>
        <Hr style={{ marginBottom: '15px' }} />
        {this.state.displayType === 'list' ? (
          <Table>
            <TableHeader displaySelectAll={false} adjustForCheckbox={false} enableSelectAll={false}>
              <TableRow>
                <TableHeaderColumn>電廠代號</TableHeaderColumn>
                <TableHeaderColumn>專案名稱</TableHeaderColumn>
                <TableHeaderColumn>狀態</TableHeaderColumn>
                <TableHeaderColumn>裝置容量</TableHeaderColumn>
                <TableHeaderColumn>發行量</TableHeaderColumn>
                <TableHeaderColumn>發行價</TableHeaderColumn>
                <TableHeaderColumn>持有量</TableHeaderColumn>
                {/*<TableHeaderColumn>現價</TableHeaderColumn>*/}
                <TableHeaderColumn>價值</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false} showRowHover={true}>
              {assetRow}
              <TableRow>
                <TableRowColumn>
                  {/*
                  <EtherscanButton
                    onClick={() => {
                      this.props.openEtherscan('STTW');
                    }}
                  >
                    <EtherscanIcon src={PowerIcon} />
                    <span>餘額</span>
                  </EtherscanButton>
                */}
                  餘額
                </TableRowColumn>
                <TableRowColumn colSpan={6} />
                <TableRowColumn>{BN(this.props.assets.sttw).toFormat(0) + ' 元'}</TableRowColumn>
              </TableRow>
              <TableRow>
                <TableRowColumn colSpan={6} />
                <TableRowColumn>總計</TableRowColumn>
                <TableRowColumn>
                  {this.props.assets.pvList
                    .reduce((v, x) => {
                      return v.plus(BN(x.holdAmount).times(x.price));
                    }, BN(this.props.assets.sttw))
                    .toFormat(0) + ' 元'}
                </TableRowColumn>
              </TableRow>
              {!this.props.assets.unrealizedSttw ? null : (
                <TableRow>
                  <TableRowColumn colSpan={6} />
                  <TableRowColumn>未實現收益</TableRowColumn>
                  <TableRowColumn>{this.props.assets.unrealizedSttw.toFormat(0) + ' 元'}</TableRowColumn>
                </TableRow>
              )}
            </TableBody>
          </Table>
        ) : (
          <div
            style={{
              display: 'flex',
              flexWrap: 'wrap',
              alignItems: 'center',
              padding: '0 50px'
            }}
          >
            {assetRow}
          </div>
        )}
      </div>
    );
  }
}

const ButtonIcon = styled.img`
  width: 12px;
  height: 12px;
  margin-right: 5px;
`;

const SelectButton = styled.div`
  color: ${props => (props.selected ? '#f7b100' : '#5a5a5a')};
  border: 1px solid ${props => (props.selected ? '#f7b100' : '#5a5a5a')};
  border-radius: 30px;
  padding: 3px 1em 3px 1em;
  margin-right: 0.5em;
  font-size: 14px;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  cursor: pointer;
`;
/*
const EtherscanButton = styled.div`
  width: 90px;
  height: 30px;
  color: #f7b100;
  border: 1px solid #f7b100;
  border-radius: 3px;
  padding-left: 10px;
  font-size: 15px;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  cursor: pointer;
  @media screen and (max-width: 1024px) {
    width: 70px;
    font-size: 12px;
  }
`;

const EtherscanIcon = styled(ButtonIcon)`
  width: 15px;
  height: 15px;
  @media screen and (max-width: 1024px) {
    width: 12px;
    height: 12px;
  }
`;
*/
export default AssetList;
