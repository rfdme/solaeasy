import React from 'react';
import { TitleContainer, Title, TitleIcon, TitleLabel, Hr } from '../Layout';
import IconSvg from '../../svg/main_icon1.svg';

import { LineChart } from '../Chart';

import BigNumber from 'bignumber.js';
const BN = BigNumber.clone({ DECIMAL_PLACES: 16, ROUNDING_MODE: 1 });

class ProfitChart extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			labels: [],
			values: [],
			tickLabels: []
		};
	}

	componentDidMount() {
		this.refresh(this.props);
	}

	componentWillReceiveProps(nextProps) {
		this.refresh(nextProps);
	}

	refresh(props) {
		if (!props.data) {
			return;
		}

		const tick = Math.round(props.data.length / 5, 1);

		let labels = [];
		let values = [];
		let tickLabels = [];

		let accumulated = BN(0);
		for (let i = 0; i < props.data.length; i++) {
			const d = props.data[i];
			labels.push(d.date);
			accumulated = accumulated.plus(d.value);

			values.push(accumulated.toFixed(1));
			if (i % tick === 0) {
				tickLabels.push(d.date);
			}
		}

		this.setState({ labels, values, tickLabels });
	}

	render() {
		return (
			<div>
				<TitleContainer>
					<Title>
						<TitleIcon src={IconSvg} />
						<TitleLabel>{this.props.title}</TitleLabel>
					</Title>
				</TitleContainer>
				<Hr style={{ marginBottom: '15px' }} />
				{this.state.values.length > 0 ? (
					<LineChart
						height={'50vh'}
						width={'100%'}
						labels={this.state.labels}
						values={this.state.values}
						tickLabels={this.state.tickLabels}
					/>
				) : (
					<div>
						<label>尚無資料</label>
					</div>
				)}
			</div>
		);
	}
}

export default ProfitChart;
