import React from 'react';
import { Pie } from 'react-chartjs-2';
import { TitleContainer, Title, TitleIcon, TitleLabel, Hr } from '../Layout';
import IconSvg from '../../svg/main_icon11.svg';

import BigNumber from 'bignumber.js';
const BN = BigNumber.clone({ DECIMAL_PLACES: 16, ROUNDING_MODE: 1 });

const PieChart = ({ assets }) => {
	let data = {
		labels: [],
		datasets: [
			{
				data: [],
				backgroundColor: [
					'#FF6384',
					'#36A2EB',
					'#FFCE56',
					'#ef9a9a',
					'#9c27b0',
					'#18ffff',
					'#26a69a',
					'#80cbc4',
					'#8c9eff'
				],
				hoverBackgroundColor: [
					'#FF6384',
					'#36A2EB',
					'#FFCE56',
					'#ef9a9a',
					'#9c27b0',
					'#18ffff',
					'#26a69a',
					'#80cbc4',
					'#8c9eff'
				]
			}
		]
	};

	assets.pvList.forEach((x, i) => {
		if (x.holdAmount > 0) {
			data.labels.push(x.id);
			data.datasets[0].data.push(
				BN(x.holdAmount)
					.times(x.price)
					.toString()
			);
		}
	});

	if (assets.sttw > 0) {
		data.labels.push('餘額');
		data.datasets[0].data.push(assets.sttw);
	}

	if (assets.unrealizedSttw) {
		data.labels.push('未實現收益');
		data.datasets[0].data.push(assets.unrealizedSttw);
	}

	return (
		<div style={{ paddingBottom: '28px' }}>
			<TitleContainer>
				<Title>
					<TitleIcon src={IconSvg} />
					<TitleLabel>資產分布</TitleLabel>
				</Title>
			</TitleContainer>
			<Hr style={{ marginBottom: '15px' }} />
			{data.labels.length > 0 ? (
				<Pie data={data} width={300} />
			) : (
				<div>
					<label>尚無資料</label>
				</div>
			)}
		</div>
	);
};

export default PieChart;
