import React, { Component } from 'react';
import axios from 'axios';

import AssetList from './AssetList.js';
import PieChart from './PieChart.js';
import ValueChart from './ValueChart.js';
import ProfitChart from './ProfitChart.js';
import Footer from '../Footer';

import { Page, Paper, Column, ColumnGap } from '../Layout';

import BigNumber from 'bignumber.js';
const BN = BigNumber.clone({ DECIMAL_PLACES: 16, ROUNDING_MODE: 1 });

class Portfolio extends Component {
	constructor(props) {
		super(props);

		this.openEtherscan = this.openEtherscan.bind(this);

		this.state = {
			assets: {
				pvList: [],
				sttw: 0
			},
			valueHistory: null,
			unrealizedProfit: null,
			realizedProfit: null
		};
	}

	componentDidMount() {
		if (!this.props.auth.token) {
			this.props.route.history.push('/Home');
			return;
		}

		if (this.props.mock.enable) {
			this.loadMockAssets(this.props.auth.token);
		} else {
			this.loadAssets(this.props.auth.token, this.props.auth.userId);
			this.loadValueHistory(
				this.props.auth.token,
				this.props.auth.userId
			);
			this.loadUnrealizedProfit(
				this.props.auth.token,
				this.props.auth.userId
			);
			this.loadRealizedProfit(
				this.props.auth.token,
				this.props.auth.userId
			);
		}
	}

	componentWillReceiveProps(nextProps) {
		if (!nextProps.auth.token) {
			nextProps.route.history.push('/Home');
		}
	}

	loadMockAssets(token) {
		const { sttw, assets, eventBySimulate } = this.props.mock.getData();

		let ids = [];
		Object.keys(assets).map(id => ids.push(id));

		return axios
			.post(
				`/api/v1/assets/forMock`,
				{ ids },
				{
					headers: { Authorization: token }
				}
			)
			.then(res => {
				let assetInfo = res.data.assets;
				assetInfo.sttw = sttw;

				let unrealizedProfit = eventBySimulate.reduce(
					(sum, item) => sum.plus(item.profit),
					BN(0)
				);
				assetInfo.unrealizedSttw = unrealizedProfit;

				assetInfo.pvList.map(x => (x.holdAmount = assets[x.id]));

				this.setState({ assets: assetInfo });
			})
			.catch(error => {
				console.error(error);
			});
	}

	loadAssets(token, user) {
		return axios
			.get(`/api/v1/assets/` + user, {
				headers: {
					Authorization: token
				}
			})
			.then(res => {
                if(res.data.status === true) {
                    this.setState({ assets: res.data.result.assets });
                } else {
                    // TODO: handle error_code, msg
                }

			})
			.catch(error => {
				console.error(error);
			});
	}

	loadValueHistory(token, user) {
		return axios
			.get(`/api/v1/assets/valueHistory/` + user, {
				headers: {
					Authorization: token
				}
			})
			.then(res => {
                if(res.data.status === true) {
                    this.setState({ valueHistory: res.data.result.valueHistory });
                } else {
                    // TODO: handle error_code, msg
                }
			})
			.catch(error => {
				console.error(error);
			});
	}

	loadUnrealizedProfit(token, user) {
		return axios
			.get(`/api/v1/assets/unrealizedProfit/` + user, {
				headers: {
					Authorization: token
				}
			})
			.then(res => {
                if(res.data.status === true) {
                    this.setState({ unrealizedProfit: res.data.result.unrealizedProfit });
                } else {
                    // TODO: handle error_code, msg
                }
			})
			.catch(error => {
				console.error(error);
			});
	}

	loadRealizedProfit(token, user) {
		return axios
			.get(`/api/v1/assets/realizedProfit/` + user, {
				headers: {
					Authorization: token
				}
			})
			.then(res => {
                if(res.data.status === true) {
                    this.setState({ realizedProfit: res.data.result.realizedProfit });
                } else {
                    // TODO: handle error_code, msg
                }
			})
			.catch(error => {
				console.error(error);
			});
	}

	openEtherscan(token) {
		return axios
			.post(
				`/api/v1/assets/etherscanLink`,
				{
					user: this.props.auth.userId,
					token: token
				},
				{
					headers: {
						Authorization: this.props.auth.token
					}
				}
			)
			.then(res => {
                if(res.data.status === true) {
                    var win = window.open(res.data.url, '_blank');
				    win.focus();
                } else {
                    // TODO: handle error_code, msg
                }
			})
			.catch(error => {
				console.error(error);
			});
	}

	render() {
		return (
			<Page>
				<div className="page-wrap container">
					<Paper>
						<AssetList
							auth={this.props.auth}
							assets={this.state.assets}
							openEtherscan={this.openEtherscan}
						/>
					</Paper>
					<Column>
						<Paper>
							<PieChart assets={this.state.assets} />
						</Paper>
						<ColumnGap />
						<Paper>
							<ValueChart data={this.state.valueHistory} />
						</Paper>
					</Column>
					<Column>
						<Paper>
							<ProfitChart
								title={'預估累積收益'}
								data={this.state.unrealizedProfit}
							/>
						</Paper>
						<ColumnGap />
						<Paper>
							<ProfitChart
								title={'已實現累積收益'}
								data={this.state.realizedProfit}
							/>
						</Paper>
					</Column>
				</div>
				<Footer />
			</Page>
		);
	}
}

export default Portfolio;
