import React from 'react';
import styled from 'styled-components';
import {
	SideBarPaper,
	TitleContainer,
	Title,
	TitleIcon,
	TitleLabel
} from '../Layout';
import IconSvg from '../../svg/main_icon7.svg';
import DefaultPoint from '../../svg/icon_indicator.svg';
import YellowPoint from '../../svg/icon_indicator_hover.svg';

const DetailMenu = ({ menu, defaultIndex, onSelect }) => {
	let menuItems = [];
	for (let i = 0; i < menu.length; i++) {
		const selected = defaultIndex === i;
		menuItems.push(
			<MenuItem key={i} selected={selected} onClick={() => onSelect(i)}>
				<MenuIcon src={selected ? YellowPoint : DefaultPoint} />
				<MenuLabel selected={selected}>{menu[i]}</MenuLabel>
			</MenuItem>
		);
	}

	return (
		<SideBarPaper>
			<TitleContainer style={{ height: '30px', padding: '1em' }}>
				<Title>
					<TitleIcon src={IconSvg} />
					<TitleLabel>明細查詢</TitleLabel>
				</Title>
			</TitleContainer>
			{menuItems}
		</SideBarPaper>
	);
};

const MenuItem = styled.div`
	color: #f7b100;
	background-color: ${props => (props.selected ? '#26262b' : null)};
	padding: 20px 0 20px 25px;
	display: flex;
	align-items: center;
	cursor: pointer;
`;

const MenuIcon = styled.img`
	margin-right: 10px;
	width: 10px;
`;

const MenuLabel = styled.span`
	color: ${props => (props.selected ? '#f7b100' : '#aaabad')};
`;

export default DetailMenu;
