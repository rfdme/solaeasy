import React, { Component } from 'react';
import styled from 'styled-components';
import axios from 'axios';
import { amber600 } from 'material-ui/styles/colors';

import { SideBarPaper, TitleContainer, Title, TitleIcon, TitleLabel, SideBarHr } from '../Layout';

import IconSvg from '../../svg/main_icon2.svg';
import PointIcon from '../../svg/icon_menu_point.svg';
import GreenIcon from '../../svg/icon_menu_green.svg';
import GreenHoverIcon from '../../svg/icon_menu_green_hover.svg';
import RedIcon from '../../svg/icon_menu_red.svg';
import RedHoverIcon from '../../svg/icon_menu_red_hover.svg';

import BigNumber from 'bignumber.js';
const BN = BigNumber.clone({ DECIMAL_PLACES: 16, ROUNDING_MODE: 1 });

class PvList extends Component {
  constructor(props) {
    super(props);

    this.handleLoad = this.handleLoad.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.state = {
      selectedLabel: this.props.auth && this.props.auth.userId ? 'owned' : 'all',
      selectedId: null,
      data: []
    };
  }

  componentDidMount() {
    this.handleLoad(this.state.selectedLabel);
  }

  handleLoad(label) {
    if (this.props.auth && this.props.auth.userId && label === 'owned') {
      axios
        .get(`/api/v1/dashboard/userPv/` + this.props.auth.userId)
        .then(res => {
          if (res.data.status === true) {
            this.setState({ data: res.data.result.userPv });
            if (res.data.result.userPv.length > 0) {
              this.handleChange(res.data.result.userPv[0].id);
            }
          } else {
            // TODO: handle error_code, msg
          }
        })
        .catch(error => {
          console.error(error);
        });
    } else {
      axios
        .get(`/api/v1/dashboard/allPv`)
        .then(res => {
          if (res.data.status === true) {
            this.setState({ data: res.data.result.allPv });
            if (res.data.result.allPv.length > 0) {
              this.handleChange(res.data.result.allPv[0].id);
            }
          } else {
            // TODO: handle error_code, msg
          }
        })
        .catch(error => {
          console.error(error);
        });
    }
  }

  handleChange(value) {
    this.setState({ selectedId: value });
    this.props.onSelect(value);
  }

  selectLabel = label => {
    this.setState({ selectedLabel: label, selectedId: null });
    this.handleLoad(label);
  };

  render() {
    const allLabelColor = this.state.selectedLabel === 'all' ? null : '#696A6D';
    const ownedLabelColor = this.state.selectedLabel === 'owned' ? null : '#696A6D';

    let items = [];

    for (let i = 0; i < this.state.data.length; i++) {
      const x = this.state.data[i];
      let diff;
      let diffPercent;
      if (x.price == null || x.open == null) {
        diff = BN(0);
        diffPercent = '0.00%';
      } else {
        diff = BN(x.price).minus(x.open);
        diffPercent =
          diff
            .times(100)
            .div(x.open)
            .toFixed(2) + '%';
      }
      const sign = diff.gt(0) ? '+' : '';
      if (this.state.selectedId === x.id) {
        items.push(
          <SelectedItem key={i} onClick={() => this.handleChange(x.id)}>
            <SelectedItemIcon src={diff.lt(0) ? GreenHoverIcon : RedHoverIcon} />
            <ItemContent>
              <Name style={{ color: amber600 }}>{x.id + ' ' + x.name}</Name>
              <Detail>
                <DetailLabel>
                  <DetailLabel1>
                    <Point src={PointIcon} />
                    目前漲幅
                  </DetailLabel1>
                  <DetailLabel2>
                    <Point src={PointIcon} />
                    目前價格
                  </DetailLabel2>
                </DetailLabel>
                <DetailValueLarge>
                  <DetailValue1>{sign + diff.toString() + ' (' + diffPercent + ')'}</DetailValue1>
                  <DetailValue2>{x.price}</DetailValue2>
                </DetailValueLarge>
              </Detail>
            </ItemContent>
          </SelectedItem>
        );
      } else {
        items.push(
          <Item key={i} onClick={() => this.handleChange(x.id)}>
            <ItemIcon src={diff < 0 ? GreenIcon : RedIcon} />
            <ItemContent>
              <Name>{x.id + ' ' + x.name}</Name>
              <Detail>
                <DetailValueSmall>
                  <DetailValue1>{sign + diff.toString() + ' (' + diffPercent + ')'}</DetailValue1>
                  <DetailValue2>{x.price}</DetailValue2>
                </DetailValueSmall>
              </Detail>
            </ItemContent>
          </Item>
        );
      }
    }

    return (
      <SideBarPaper>
        <TitleContainer style={{ height: '30px', padding: '1em' }}>
          <Title>
            <TitleIcon src={IconSvg} />
            {this.props.auth && this.props.auth.userId ? (
              <MyTitleLabel
                style={{
                  color: ownedLabelColor,
                  cursor: 'pointer'
                }}
                onClick={() => this.selectLabel('owned')}
              >
                已持有
              </MyTitleLabel>
            ) : null}
            {this.props.auth && this.props.auth.userId ? (
              <span
                style={{
                  color: '#696A6D',
                  margin: '0 0.5em'
                }}
              >
                {'/'}
              </span>
            ) : null}
            <MyTitleLabel
              style={{ color: allLabelColor, cursor: 'pointer' }}
              onClick={() => this.selectLabel('all')}
            >
              所有電廠
            </MyTitleLabel>
          </Title>
        </TitleContainer>
        <SideBarHr />
        <ItemContainer>{items}</ItemContainer>
      </SideBarPaper>
    );
  }
}

const MyTitleLabel = styled(TitleLabel)`
  @media screen and (max-width: 1024px) {
    font-size: 16px;
  }
`;

const ItemContainer = styled.div`
  height: 100%;
  overflow-x: auto;
`;

const SelectedItem = styled.div`
  background-color: #26262b;
  height: 80px;
  padding: 10px;
  display: flex;
  align-items: flex-start;
`;

const Item = styled.div`
  height: 55px;
  padding: 10px;
  border-bottom: 1px solid rgba(35, 36, 41, 1);
  display: flex;
  align-items: flex-start;
  cursor: pointer;
`;

const ItemIcon = styled.img`
  vertical-align: middle;
  margin-right: 5px;
  width: 22px;
`;

const SelectedItemIcon = styled.img`
  vertical-align: middle;
  margin: -6px -3px 0 -5px;
  width: 35px;
`;

const ItemContent = styled.div`
  flex: 1;
`;

const Name = styled.div`
  font-size: 15px;
  color: #aaabad;
`;

const Detail = styled.div`
  margin-top: 0.5em;
`;

const Point = styled.img`
  width: 8px;
  height: 8px;
  margin-right: 2px;
`;
const DetailLabel = styled.div`
  display: flex;
  margin-bottom: 1em;
  color: #5a5a5a;
  font-size: 12px;
`;
const DetailLabel1 = styled.div`
  flex: 1;
`;
const DetailLabel2 = styled.div`
  flex: 1;
  text-align: right;
`;
const DetailValueLarge = styled.div`
  display: flex;
  color: #ffffff;
  font-size: 14px;
`;
const DetailValueSmall = styled.div`
  display: flex;
  color: #5a5a5a;
  font-size: 12px;
`;
const DetailValue1 = styled.div`
  flex: 1;
`;
const DetailValue2 = styled.div`
  flex: 1;
  text-align: right;
`;

export default PvList;
