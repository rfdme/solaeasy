import React, { Component } from 'react';
import styled from 'styled-components';
import { sprintf } from 'sprintf-js';
import axios from 'axios';
import { SideBarPaper, TitleContainer, Title, TitleIcon, TitleLabel } from '../Layout';
import IconSvg from '../../svg/main_icon7.svg';
import YellowPoint from '../../svg/icon_indicator_hover.svg';

import BigNumber from 'bignumber.js';
const BN = BigNumber.clone({ DECIMAL_PLACES: 16, ROUNDING_MODE: 1 });

class EventDetail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      eventPV: [],
      eventSimulate: [],
      eventBill: [],
      eventRepay: []
    };
  }

  componentDidMount() {
    this.reload(this.props.date);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.date !== this.props.date) {
      this.reload(nextProps.date);
    }
  }

  reload(date) {
    this.loadEventPV(date);

    if (this.props.auth.token) {
      this.loadEventSimulate(date, this.props.auth.token, this.props.auth.userId);
      this.loadEventBill(date, this.props.auth.token, this.props.auth.userId);
      this.loadEventRepay(date, this.props.auth.token, this.props.auth.userId);
    }
  }

  loadEventPV(date) {
    return axios
      .get(sprintf(`/api/v1/event/pv?begin=%s&end=%s`, date, date))
      .then(res => {
        if (res.data.status === true) {
          this.setState({ eventPV: res.data.result.events });
        } else {
          // TODO: handle error_code, msg
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  loadEventSimulate(date, token, user) {
    if (this.props.mock.enable) {
      const { eventBySimulate } = this.props.mock.getData();
      const todayEventSimulate = eventBySimulate.filter(s => s.date === date);
      this.setState({
        eventSimulate: todayEventSimulate
      });
    } else {
      return axios
        .get(sprintf(`/api/v1/event/simulateDetail?date=%s&user=%s`, date, user), {
          headers: {
            Authorization: token
          }
        })
        .then(res => {
          if (res.data.status === true) {
            this.setState({ eventSimulate: res.data.result.events });
          } else {
            // TODO: handle error_code, msg
          }
        })
        .catch(error => {
          console.error(error);
        });
    }
  }

  loadEventBill(date, token, user) {
    return axios
      .get(sprintf(`/api/v1/event/billDetail?date=%s&user=%s`, date, user), {
        headers: {
          Authorization: token
        }
      })
      .then(res => {
        if (res.data.status === true) {
          this.setState({ eventBill: res.data.result.events });
        } else {
          // TODO: handle error_code, msg
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  loadEventRepay(date, token, user) {
    return axios
      .get(sprintf(`/api/v1/event/repayDetail?date=%s&user=%s`, date, user), {
        headers: {
          Authorization: token
        }
      })
      .then(res => {
        if (res.data.status === true) {
          this.setState({ eventRepay: res.data.result.events });
        } else {
          // TODO: handle error_code, msg
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  render() {
    let k = 0;
    let eventPV = [];
    this.state.eventPV.forEach((x, i) => {
      eventPV.push(<div key={k++}>{sprintf('%s: %s', x.pvId, x.name)}</div>);
    });

    let eventSimulate = [];
    let sumSimulate = BN(0);
    this.state.eventSimulate.forEach((x, i) => {
      eventSimulate.push(<div key={k++}>{sprintf('%s: %s元', x.pvId, BN(x.profit).toFormat(1))}</div>);
      sumSimulate = sumSimulate.plus(x.profit);
    });
    if (!sumSimulate.eq(0)) {
      eventSimulate.push(<div key={k++}>{sprintf('總計: %s元', sumSimulate.toFormat(1))}</div>);
    }

    let eventBill = [];
    let sumBill = BN(0);
    this.state.eventBill.forEach((x, i) => {
      eventBill.push(
        <div key={k++}>
          {sprintf(
            //'%s (%s~%s): %s元',
            '%s: %s元',
            x.pvId,
            //x.rangeBegin.slice(0, 7),
            //x.rangeEnd.slice(0, 7),
            BN(x.profit).toFormat(1)
          )}
        </div>
      );
      sumBill = sumBill.plus(x.profit);
    });
    if (!sumBill.eq(0)) {
      eventBill.push(<div key={k++}>{sprintf('總計: %s元', sumBill.toFormat(1))}</div>);
    }

    let eventRepay = [];
    let sumRepay = BN(0);
    this.state.eventRepay.forEach((x, i) => {
      eventRepay.push(<div key={k++}>{sprintf('%s: %s元', x.pvId, BN(x.repay).toFormat(1))}</div>);
      sumRepay = sumRepay.plus(x.repay);
    });
    if (!sumRepay.eq(0)) {
      eventRepay.push(<div key={k++}>{sprintf('總計: %s元', sumRepay.toFormat(1))}</div>);
    }

    return (
      <SideBarPaper>
        <TitleContainer style={{ height: '30px', padding: '1em' }}>
          <Title>
            <TitleIcon src={IconSvg} />
            <TitleLabel>事件內容</TitleLabel>
          </Title>
        </TitleContainer>
        <SelectedDate>
          <DateIcon src={YellowPoint} />
          {this.props.date}
        </SelectedDate>
        <Data>
          {eventPV.length > 0 ? (
            <DataBlock>
              <DataTitle color={'#5793F3'}>電廠事件:</DataTitle>
              <div>{eventPV}</div>
            </DataBlock>
          ) : null}
          {eventSimulate.length > 0 ? (
            <DataBlock>
              <DataTitle color={'#2bbc8f'}>模擬收益:</DataTitle>
              <div>{eventSimulate}</div>
            </DataBlock>
          ) : null}
          {eventBill.length > 0 ? (
            <DataBlock>
              <DataTitle color={'#f7b100'}>實際收益:</DataTitle>
              <div>{eventBill}</div>
            </DataBlock>
          ) : null}
          {eventRepay.length > 0 ? (
            <DataBlock>
              <DataTitle color={'#dc415c'}>返回本金:</DataTitle>
              <div>{eventRepay}</div>
            </DataBlock>
          ) : null}
        </Data>
      </SideBarPaper>
    );
  }
}

const SelectedDate = styled.div`
  color: #f7b100;
  background-color: #26262b;
  padding: 20px;
  display: flex;
  align-items: center;
`;

const DateIcon = styled.img`
  margin-right: 10px;
  width: 10px;
`;

const Data = styled.div`
  flex: 1;
  overflow-y: auto;
  padding: 20px 0px 20px 20px;
  font-size: 15px;
  @media screen and (max-width: 1024px) {
    font-size: 12px;
  }
`;

const DataBlock = styled.div`
  color: #aaabad;
  margin-bottom: 1em;
`;

const DataTitle = styled.div`
  color: ${props => props.color};
`;

export default EventDetail;
