import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import ArrowDropDownIcon from 'material-ui/svg-icons/navigation/arrow-drop-down';
//import Toggle from 'material-ui/Toggle';
import FontAwesome from 'react-fontawesome';

import logoIcon from '../../svg/logo.svg';
import dashboardIcon from '../../svg/menu_icon_1.svg';
import pvIcon from '../../svg/menu_icon_2.svg';
//import exchangeIcon from '../../svg/menu_icon_3.svg';
import eventBookIcon from '../../svg/menu_icon_4.svg';
import detailIcon from '../../svg/menu_icon_5.svg';
import portfolioIcon from '../../svg/menu_icon_6.svg';
//import donationIcon from '../../svg/menu_icon_8.svg';
import adminIcon from '../../svg/menu_icon_admin.svg';

// The Header creates links that can be used to navigate
// between routes.
class Header extends Component {
  constructor(props) {
    super(props);
    this.handleOnItemClick = this.handleOnItemClick.bind(this);
    this.signOut = this.signOut.bind(this);
    this.handleClose = this.handleClose.bind(this);

    this.state = {
      open: false,
      selected: this.props.route.location.pathname
    };
  }

  handleOnItemClick(event, child) {
    switch (child.props.primaryText) {
      case '登出':
        this.signOut();
        break;
      default:
        this.props.auth.changeUser(child.props.userId, child.props.primaryText);
        break;
    }
  }

  signOut() {
    this.props.auth.signOut();
    this.setState({ open: true });
  }

  handleClose() {
    this.setState({ open: false });
  }

  isSelected = path => {
    return this.props.route.location.pathname === path;
  };

  render() {
    const { auth /*, mock*/ } = this.props;
    const actions = [<FlatButton label="確認" primary={true} onClick={this.handleClose} />];

    let anotherUser = [];
    if (auth.userList) {
      for (let i = 0; i < auth.userList.length; i++) {
        const user = auth.userList[i];
        if (auth.userId !== user.id) {
          anotherUser.push(<MenuItem primaryText={user.name} userId={user.id} />);
        }
      }
    }
    return (
      <div>
        <Dialog
          title="提示訊息"
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
        >
          親愛的用戶，已將您成功登出了。
        </Dialog>
        <HeaderBar>
          <LogoLink href="/">
            <MyLogo src={logoIcon} />
          </LogoLink>
          <HeaderRight>
            <Link
              to="/Home"
              style={{
                display: 'flex',
                alignItems: 'center'
              }}
            >
              <MyLink selected={this.isSelected('/Home')}>
                <Icon src={dashboardIcon} />
                <Label>首頁</Label>
              </MyLink>
            </Link>
            <Link
              to="/Home/PvInfo"
              style={{
                display: 'flex',
                alignItems: 'center'
              }}
            >
              <MyLink href="/Home/PvInfo" selected={this.isSelected('/Home/PvInfo')}>
                <Icon src={pvIcon} />
                <Label>電廠資料</Label>
              </MyLink>
            </Link>
            {/*}
            <Link
              to="/Home/Exchange"
              style={{
                display: 'flex',
                alignItems: 'center'
              }}
            >
              <MyLink selected={this.isSelected('/Home/Exchange')}>
                <Icon src={exchangeIcon} />
                <Label>交易中心</Label>
              </MyLink>
            </Link>
          */}
            <Link
              to="/Home/EventBook"
              style={{
                display: 'flex',
                alignItems: 'center'
              }}
            >
              <MyLink selected={this.isSelected('/Home/EventBook')}>
                <Icon src={eventBookIcon} />
                <Label>事件日曆</Label>
              </MyLink>
            </Link>
            {auth.token ? (
              <Link
                to="/Home/Detail"
                style={{
                  display: 'flex',
                  alignItems: 'center'
                }}
              >
                <MyLink selected={this.isSelected('/Home/Detail')}>
                  <Icon src={detailIcon} />
                  <Label>明細查詢</Label>
                </MyLink>
              </Link>
            ) : null}
            {auth.token ? (
              <Link
                to="/Home/Portfolio"
                style={{
                  display: 'flex',
                  alignItems: 'center'
                }}
              >
                <MyLink selected={this.isSelected('/Home/Portfolio')}>
                  <Icon src={portfolioIcon} />
                  <Label>資產總覽</Label>
                </MyLink>
              </Link>
            ) : null}
            {/*
            <Link
              to="/Home/Donation"
              style={{
                display: 'flex',
                alignItems: 'center'
              }}
            >
              <MyLink
                selected={this.isSelected('/Home/Donation')}
              >
                <Icon src={donationIcon} />
                <Label>NGO捐贈</Label>
              </MyLink>
            </Link>
          */}
            {auth.token ? (
              <Link
                to="/Home/Account"
                style={{
                  display: 'flex',
                  alignItems: 'center'
                }}
              >
                <MyLink selected={this.isSelected('/Home/Account')}>
                  <Icon src={adminIcon} />
                  {/*<FontAwesome
                  name="user-o"
                  style={{
                    verticalAlign: 'middle',
                    marginRight: '5px',
                    color: '#f1f1f1'
                  }}
                />*/}
                  <Label>{auth.username}</Label>
                </MyLink>
              </Link>
            ) : (
              <Link
                to="/Home/Login"
                style={{
                  display: 'flex',
                  alignItems: 'center'
                }}
              >
                <MyLink selected={this.isSelected('/Home/Login')}>
                  <FontAwesome
                    name="sign-in"
                    style={{
                      color: '#aaabad',
                      verticalAlign: 'middle',
                      marginRight: '5px'
                    }}
                  />
                  <Label>登入</Label>
                </MyLink>
              </Link>
            )}
            {auth.token ? (
              <IconMenu
                onItemClick={this.handleOnItemClick}
                iconButtonElement={
                  <IconButton>
                    <ArrowDropDownIcon color="#aaabad" />
                  </IconButton>
                }
                anchorOrigin={{
                  horizontal: 'right',
                  vertical: 'top'
                }}
                targetOrigin={{
                  horizontal: 'right',
                  vertical: 'top'
                }}
                width={'120px'}
              >
                <MenuItem primaryText="登出" />
                {anotherUser}
                {/*}
                <Toggle
                  toggled={mock.enable}
                  onToggle={mock.toggle}
                  label="模擬"
                  style={{
                    width: '100px',
                    paddingLeft: '18px'
                  }}
                />
                */}
              </IconMenu>
            ) : null}
          </HeaderRight>
        </HeaderBar>
      </div>
    );
  }
}

const HeaderBar = styled.div`
  height: 54px;
  background-color: #333539;
  font-size: 15px;
`;

const HeaderRight = styled.div`
  height: 54px;
  display: flex;
  justify-content: flex-end;
  align-items: stretch;
`;

const LogoLink = styled.a`
  float: left;
  height: auto;
  width: 118px;
  margin: 5px 10px 5px 20px;
`;

const MyLogo = styled.img`
  height: auto;
  width: 190px;
  margin: 5px;
`;

const MyLink = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  padding: 10px;
  color: #aaabad;//${props => (props.selected ? '#f7b100' : '#aaabad')};
  &:hover {
    background-color: #151515;
  }

  background-color: ${props => (props.selected ? '#151515' : null)};
`;

const Icon = styled.img`
  transition: fill 0.25s;
  width: 20px;
  height: 20px;
  margin-right: 5px;

  ${MyLink}:hover & {
    //TODO: change icon
  }

  @media screen and (max-width: 1024px) {
    display: none;
  }
`;

const Label = styled.span`
  display: flex;
  align-items: center;
  line-height: 1.2;

  ${MyLink}:hover & {
    //color: #f7b100;
  }
`;

export default Header;
