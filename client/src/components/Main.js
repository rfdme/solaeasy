import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Sola from './Sola';
import Home from './Home';
import { LineChart } from './Chart';
import Robot from './Robot';
import Test from './Test';

// The Main component renders one of the three provided
// Routes (provided that one matches). Both the /roster
// and /schedule routes will match any pathname that starts
// with /roster or /schedule. The / route will only match
// when the pathname is exactly the string "/"
const Main = props => (
  <main>
    <Switch>
      <Route exact path="/" component={Sola} />
      <Route path="/Home" render={routeProps => <Home route={routeProps} {...props} />} />
      <Route path="/Chart" render={routeProps => <LineChart height={'50vh'} width={'50vw'} />} />
      <Route path="/Robot/:interval" render={routeProps => <Robot route={routeProps} {...props} />} />
      <Route path="/Test" render={routeProps => <Test />} />
    </Switch>
  </main>
);

export default Main;
