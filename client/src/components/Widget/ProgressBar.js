import React from 'react';
import styled from 'styled-components';

const ProgressBar = ({ value }) => {
  let v = value;
  if (value < 0) {
    v = 0;
  } else if (value > 1) {
    v = 1;
  }
  const percentage = (v * 100).toFixed(2) + '%';
  return (
    <BarContainer>
      <Bar width={percentage} />
    </BarContainer>
  );
};

const BarContainer = styled.div`
  background-color: #1f1f22;
  border-radius: 3px;
  padding: 3px;
`;
const Bar = styled.div`
  background-color: #f7b100;
  width: ${props => props.width};
  height: 4px;
  border-radius: 3px;
`;

export default ProgressBar;
