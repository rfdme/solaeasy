import React from 'react';
import styled from 'styled-components';
import ActionDone from 'material-ui/svg-icons/action/done';

const Checkbox = ({ label, checked, color, onCheck }) => {
	const showColor = checked ? color : 'grey';
	return (
		<Border color={showColor} onClick={() => onCheck()}>
			{checked ? (
				<ActionDone
					style={{
						verticalAlign: 'middle',
						width: '1em',
						height: '1em',
						marginRight: '0.5em',
						color: { showColor }
					}}
				/>
			) : (
				<Unchecked />
			)}
			<span
				style={{
					verticalAlign: 'middle'
				}}
			>
				{label}
			</span>
		</Border>
	);
};

const Border = styled.div`
	color: ${props => props.color};
	border: 1px solid ${props => props.color};
	border-radius: 30px;
	padding: 1px 1.5em 1px 0.5em;
	margin-right: 0.5em;
	font-size: 14px;
	cursor: pointer;
`;

const Unchecked = styled.span`
	display: inline-block;
	margin-right: 0.5em;
	width: 1em;
`;

export default Checkbox;
