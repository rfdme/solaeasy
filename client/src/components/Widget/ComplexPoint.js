import React from 'react';
import styled from 'styled-components';
import { amber400 } from 'material-ui/styles/colors';

const size = 20;
const ComplexPoint = ({ x, y, color, onClick }) => {
	const c1 = 9;
	const c2 = 5;
	const r = size / 2;
	return (
		<Point x={x} y={y} onClick={onClick}>
			<svg id="svg" width={size} height={size}>
				<circle
					cx={r}
					cy={r}
					r={c1}
					stroke={amber400}
					fillOpacity="0"
				/>
				<circle cx={r} cy={r} r={c2} fill={amber400} />
			</svg>
		</Point>
	);
};

const Point = styled.div`
	position: absolute;
	top: ${props => props.y + 'px'};
	left: ${props => props.x + 'px'};
	width: ${size + 'px'};
	height: ${size + 'px'};
	transform: translate(-50%, -50%);
	cursor: pointer;
`;

export default ComplexPoint;
