import React from 'react';

import { amber600, grey600 } from 'material-ui/styles/colors';
import styled from 'styled-components';

const Pagination = ({ page, showPageCount, totalPageCount, onPage }) => {
	let pages = [];

	const showBegin = page - Math.floor(showPageCount / 2);
	const showEnd = page + Math.floor(showPageCount / 2);

	const paddingRight = showBegin < 0 ? Math.abs(showBegin) : 0;
	const paddingLeft =
		showEnd - (totalPageCount - 1) > 0 ? showEnd - (totalPageCount - 1) : 0;
	for (let i = 0; i < totalPageCount; i++) {
		if (i >= showBegin - paddingLeft && i <= showEnd + paddingRight)
			pages.push(
				<Circle
					key={i + 1}
					onClick={() => {
						onPage(i);
					}}
					color={i === page ? amber600 : grey600}
				/>
			);
	}

	if (totalPageCount > 1) {
		return <Paging>{pages}</Paging>;
	} else {
		return null;
	}
};

const Paging = styled.div`
	width: 100%;
	text-align: center;
`;

const Circle = styled.div`
	width: 15px;
	height: 15px;
	margin: 0 10px;
	background-color: ${props => props.color};
	border-radius: 100%;
	display: inline-block;
	cursor: pointer;
`;

export default Pagination;
