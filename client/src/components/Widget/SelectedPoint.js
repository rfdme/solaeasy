import React from 'react';
import styled from 'styled-components';

const size = 55;
const SelectedPoint = ({ x, y, color }) => {
	const c1 = 55 / 2;
	const c2 = 47 / 2;
	const c3 = 37 / 2;
	const c4 = 21 / 2;
	const c5 = 7 / 2;
	const r = c1;
	//const color = '#DC415C';
	return (
		<Point x={x} y={y}>
			<svg id="svg" width={size} height={size}>
				<circle cx={r} cy={r} r={c1} fill={color} fillOpacity="0.1" />
				<circle cx={r} cy={r} r={c2} fill={color} fillOpacity="0.27" />
				<circle cx={r} cy={r} r={c3} fill={color} />
				<circle cx={r} cy={r} r={c4} fill="white" />
				<circle cx={r} cy={r} r={c5} fill={color} />
			</svg>
		</Point>
	);
};

const Point = styled.div`
	position: absolute;
	top: ${props => props.y + 'px'};
	left: ${props => props.x + 'px'};
	width: ${size + 'px'};
	height: ${size + 'px'};
	transform: translate(-50%, -50%);
`;

export default SelectedPoint;
