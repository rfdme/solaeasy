import React, { Component } from 'react';
import styled from 'styled-components';
import bg from './bg.jpg';

class Sola extends Component {
	constructor(props) {
		super(props);

		this.handleEnter = this.handleEnter.bind(this);
	}

	handleEnter() {
		this.props.history.push('/Home');
	}

	render() {
		return (
			<SolaPanel>
				<LogoContainer>
					<Logo
						class="Logo__SVG-gfWwwV eIrngU"
						xmlns="http://www.w3.org/2000/svg"
						viewBox="0 0 300 300"
					>
						<path d="M63.8,255.12c0,4.22,3.28,5.86,7,5.86,2.44,0,6.17-.71,6.17-4,0-3.46-4.8-4-9.41-5.24s-9.46-3.15-9.46-9c0-6.53,6.13-9.59,11.85-9.59,6.61,0,12.7,2.88,12.7,10.21H75.92c-.22-3.77-2.88-4.8-6.26-4.8-2.22,0-4.8.93-4.8,3.6,0,2.44,1.51,2.8,9.46,4.79,2.35.53,9.41,2.09,9.41,9.24,0,5.82-4.53,10.17-13.19,10.17-7,0-13.63-3.46-13.5-11.28Z" />
						<path d="M124.26,233.19c9.77,0,15.58,7.32,15.58,16.74,0,9.15-5.82,16.47-15.58,16.47s-15.58-7.32-15.58-16.47c0-9.41,5.82-16.74,15.58-16.74m0,27.35c6.26,0,8.61-5.33,8.61-10.61,0-5.55-2.35-10.87-8.61-10.87s-8.61,5.33-8.61,10.87c0,5.29,2.35,10.61,8.61,10.61" />
						<polygon points="166.78 233.94 173.75 233.94 173.75 259.79 189.2 259.79 189.2 265.65 166.78 265.65 166.78 233.94" />
						<polygon points="231.1 233.94 223.95 233.94 211.92 265.65 218.98 265.65 221.46 258.59 221.47 258.59 223.3 253.39 223.28 253.39 227.41 241.76 227.5 241.76 233.29 258.59 233.32 258.59 235.72 265.65 242.95 265.65 231.1 233.94" />
						<polygon points="150 67.52 204.52 90.11 204.52 56.18 150 33.6 95.48 56.18 95.48 122.64 150 145.22 150 129.54 173.18 139.15 173.18 163.71 150 173.31 150 173.31 95.48 150.74 95.48 184.66 150 207.24 150 207.24 204.52 184.66 204.52 118.2 150 95.62 150 111.29 150 111.29 126.83 101.69 126.83 77.12 150 67.52" />
					</Logo>
				</LogoContainer>
				<Enter onClick={this.handleEnter}>Enter</Enter>
			</SolaPanel>
		);
	}
}

const SolaPanel = styled.div`
	position: relative;
	height: 100vh;
	width: 100vw;
	background-image: url(${bg});
	background-size: cover;
	background-position: center;
	background-repeat: no-repeat;
	display: flex;
	flex-direction: column;
	&::before {
		content: '';
		position: absolute;
		top: 0;
		left: 0;
		bottom: 0;
		right: 0;
		background-color: rgba(0, 0, 0, 0.2);
	}
`;

const LogoContainer = styled.div`
	position: relative;
	width: 300px;
	height: 300px;
	margin-left: auto;
	margin-right: auto;
	padding-top: 200px;
`;

const Logo = styled.svg`
	fill: white;
`;

const Enter = styled.button`
	position: relative;
	width: 270px;
	height: 65px;
	margin-left: auto;
	margin-right: auto;
	margin-top: 220px;
	font-family: -apple-system, 'Lato', sans-serif;
	font-size: 1.25em;
	color: white;
	background-color: #f7b100;
	border: 0;

	&:hover {
		box-shadow: inset 0 0 0 999px rgba(0, 0, 0, 0.125);
	}
`;

export default Sola;
