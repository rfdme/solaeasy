import React, { Component } from 'react';
import styled from 'styled-components';
import axios from 'axios';
import ImageDehaze from 'material-ui/svg-icons/image/dehaze';

import Assets from './Assets';
import PvItem from './PvItem';
import { Pagination } from '../../Widget';
import { TitleContainer, Title, TitleIcon, TitleLabel, Hr } from '../../Layout';
import IconSvg from '../../../svg/main_icon1.svg';
import LeftSvg from '../../../svg/main_arrow_left.svg';
import RightSvg from '../../../svg/main_arrow_right.svg';

class Balance extends Component {
	constructor(props) {
		super(props);

		let countForPage = 4;

		if (window.innerWidth <= 1280) {
			countForPage = 3;
		}

		this.state = {
			toggle: true,
			userPv: [],
			paging: 0,
			countForPage: countForPage
		};
	}

	componentDidMount() {
		const { token } = this.props.auth;

		if (token) {
			this.loadUserPv();
		}
	}

	loadUserPv = () => {
		axios
			.get(`/api/v1/dashboard/userPv/` + this.props.auth.userId)
			.then(res => {
                if(res.data.status === true) {
                    this.setState({ userPv: res.data.result.userPv });
                } else {
                     // TODO: handle error_code, msg
                }
			})
			.catch(error => {
				console.error(error);
			});
	};

	handleToggle = () => {
		this.setState({ toggle: !this.state.toggle });
	};

	handlePage = page => {
		this.setState({ paging: page });
	};

	previousPage = () => {
		if (this.state.paging > 0) {
			this.setState({ paging: this.state.paging - 1 });
		}
	};

	nextPage = () => {
		const totalPageCount = this.getPageCount();
		if (this.state.paging < totalPageCount - 1) {
			this.setState({ paging: this.state.paging + 1 });
		}
	};

	getPageCount = () => {
		return Math.ceil(this.state.userPv.length / this.state.countForPage);
	};

	render() {
		const totalPageCount = this.getPageCount();
		const begin = this.state.paging * this.state.countForPage;
		const end = (this.state.paging + 1) * this.state.countForPage;

		let pvs = [];
		if (this.state.toggle) {
			this.state.userPv.forEach((x, i) => {
				if (begin <= i && i < end) {
					const pv = this.state.userPv[i];
					pvs.push(<PvItem key={i} data={pv} />);
				}
			});
			let emptyCount = this.state.countForPage - pvs.length;
			for (let i = 0; i < emptyCount; i++) {
				pvs.push(<EmptyItem key={'empty' + i} />);
			}
		} else {
			for (let i = 0; i < this.state.userPv.length; i++) {
				const pv = this.state.userPv[i];
				pvs.push(<PvItem key={i} data={pv} />);
			}
		}
		return (
			<div>
				<TitleContainer>
					<Title>
						<TitleIcon src={IconSvg} />
						<TitleLabel>持倉盈虧</TitleLabel>
					</Title>
				</TitleContainer>
				<Hr />
				<div
					style={{
						display: 'flex',
						flexWrap: 'wrap',
						alignItems: 'center'
					}}
				>
					<Assets data={this.state.userPv} />
					<Arrow
						src={LeftSvg}
						onClick={this.previousPage}
						style={{
							visibility: this.state.toggle ? null : 'hidden'
						}}
					/>
					{pvs}
					<Arrow
						src={RightSvg}
						onClick={this.nextPage}
						style={{
							visibility: this.state.toggle ? null : 'hidden'
						}}
					/>
					<Toggle>
						<div style={{ width: '25%' }}>{/*for layout*/}</div>
						<div onClick={this.handleToggle}>
							<ImageDehaze
								style={{
									verticalAlign: 'middle',
									color: '#3a3b3f'
								}}
							/>
							<span
								style={{
									verticalAlign: 'middle'
								}}
							>
								{this.state.toggle ? '展開' : '收合'}
							</span>
						</div>
						{this.state.toggle ? (
							<div style={{ width: '25%' }}>
								<div style={{ float: 'right' }}>
									<Pagination
										page={this.state.paging}
										totalPageCount={totalPageCount}
										showPageCount={5}
										onPage={this.handlePage}
									/>
								</div>
							</div>
						) : (
							<div style={{ width: '25%' }}>{/*for layout*/}</div>
						)}
					</Toggle>
				</div>
			</div>
		);
	}
}

const Toggle = styled.div`
	font-size: 20px;
	width: 100%;
	color: #3a3b3f;
	text-align: center;
	padding: 0.5em;
	display: flex;
	justify-content: space-between;
	cursor: pointer;
`;

const Arrow = styled.img`
	width: 34px;
	height: 34px;
	cursor: pointer;
`;

const EmptyItem = styled.div`
	width: 240px;
`;

export default Balance;
