import React, { Component } from 'react';
import styled from 'styled-components';

import { PercentageCircle } from '../../Widget';
import IconSvg from '../../../svg/icon_power.svg';

import BigNumber from 'bignumber.js';
const BN = BigNumber.clone({ DECIMAL_PLACES: 16, ROUNDING_MODE: 1 });

class PvItem extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { data } = this.props;
    const v1 = data.amount ? data.amount / data.totalAmount : 0;
    const v3 = (data.price - data.open) / data.price;
    return (
      <Item>
        <Circle>
          <PercentageCircle value1={v1} value2={data.price} value3={v3} />
        </Circle>
        <Row1>
          <Icon src={IconSvg} />
          <Name>{`${data.id} ${data.name}`}</Name>
        </Row1>
        <Row2>
          <OwnedAmount>{BN(data.amount ? data.amount : 0).toFormat(1)}</OwnedAmount>
          <span>{' / '}</span>
          {BN(data.totalAmount).toFormat(1)}
        </Row2>
      </Item>
    );
  }
}

const Item = styled.div`
  color: #aaabad;
  font-size: 20px;
  width: 240px;
  text-align: center;
  @media screen and (max-width: 1280px) {
    width: 223px;
    font-size: 16px;
  }
`;

const Circle = styled.div`
  display: inline-block;
`;

const Row1 = styled.div`
  margin: 1.5em 0 1em 0;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Icon = styled.img`
  width: 30px;
  height: 30px;
  margin-right: 5px;
`;

const Name = styled.span`
  border-bottom: 1px solid #37373e;
  padding-bottom: 5px;
  margin-bottom: 5px;
`;
const Row2 = styled.div`
  font-family: Montserrat;
  text-align: center;
`;
const OwnedAmount = styled.span`
  font-family: Montserrat;
  color: #f7b100;
`;

export default PvItem;
