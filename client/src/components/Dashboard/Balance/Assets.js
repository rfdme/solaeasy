import React from 'react';
import styled from 'styled-components';

import { ProgressBar } from '../../Widget';

import BigNumber from 'bignumber.js';
const BN = BigNumber.clone({ DECIMAL_PLACES: 16, ROUNDING_MODE: 1 });

const Assets = props => {
	const { data } = props;
	let lastTotal = data.reduce((total, d) => total + d.amount * d.open, 0);
	let total = data.reduce((total, d) => total + d.amount * d.price, 0);
	let sortData = data
		.concat()
		.sort((a, b) => b.amount * a.price - a.amount * a.price);
	const rows = [];
	for (let i = 0; i < sortData.length; i++) {
		let d = sortData[i];
		let percent = d.amount * d.price / total;
		rows.push(
			<Row key={i}>
				<Item>
					{`${d.id} / `}
					<Amount>{BN(d.amount).toFormat(1)}</Amount>
					<Percent>
						{BN(percent.toString())
							.times(100)
							.toFormat(0) + '%'}
					</Percent>
				</Item>
				<ProgressBar value={percent} />
			</Row>
		);
	}

	return (
		<div style={{ color: '#696A6D' }}>
			<div>所有資產</div>
			<List>
				{rows.length > 0 ? (
					rows
				) : (
					<div>
						<label>尚無資料</label>
					</div>
				)}
			</List>
			<Summary>
				<SummaryItem>
					<span>總資產</span>
					<Value color={'#F7B100'}>
						{BN(total.toString()).toFormat(1)}
					</Value>
				</SummaryItem>
				<SummaryItem>
					<span>今日盈虧</span>
					<Value color={total >= lastTotal ? '#DC415C' : '#2BBC8F'}>
						{total >= lastTotal ? '+' : ''}
						{BN(total.toString())
							.minus(lastTotal.toString())
							.toFormat(1)}
					</Value>
				</SummaryItem>
				<SummaryItem>
					<span>{/*總盈虧*/}</span>
					<Value color={'#DC415C'}>{/*TODO*/}</Value>
				</SummaryItem>
			</Summary>
		</div>
	);
};

const List = styled.div`
	width: 270px;
	height: 200px;
	overflow: auto;
	@media screen and (max-width: 1280px) {
		width: 240px;
	}
`;

const Row = styled.div`
	width: 242px;
	@media screen and (max-width: 1280px) {
		width: 230px;
	}
`;

const Item = styled.div`
	margin: 0.5em 0 15px 0;
	color: #aaabad;
	font-size: 15px;
`;
const Amount = styled.span`
	color: #f7b100;
	font-size: 15px;
`;
const Percent = styled.span`
	color: #aaabad;
	float: right;
`;

const Summary = styled.div`
	width: 242px;
	margin-top: 1em;
	@media screen and (max-width: 1280px) {
		width: 230px;
	}
`;

const SummaryItem = styled.div`
	margin-bottom: 0.5em;
`;

const Value = styled.span`
	float: right;
	color: ${props => props.color};
	font-family: Montserrat;
	font-size: 20px;
`;

export default Assets;
