import React from 'react';
import Footer from '../Footer';
import Balance from './Balance';
import Timeline from './Timeline';
import TradingView from './TradingView';
import HotPv from './HotPv';
import Summary from './Summary';

import { Page, Paper, Column, ColumnGap } from '../Layout';

const Dashboard = ({ auth }) => {
  return (
    <Page>
      <div className="page-wrap container">
        <Paper>
          <Summary />
        </Paper>
        {auth.userId ? (
          <Paper>
            <Balance auth={auth} />
          </Paper>
        ) : null}
        <Paper>
          <Timeline auth={auth} />
        </Paper>
        <Column>
          <Paper>
            <TradingView auth={auth} />
          </Paper>
          <ColumnGap />
          <Paper>
            <HotPv auth={auth} />
          </Paper>
        </Column>
      </div>
      <Footer />
    </Page>
  );
};

export default Dashboard;
