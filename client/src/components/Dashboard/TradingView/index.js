import React, { Component } from 'react';
import axios from 'axios';
import {
	Table,
	TableBody,
	TableHeader,
	TableHeaderColumn,
	TableRow,
	TableRowColumn
} from 'material-ui/Table';
import { Pagination } from '../../Widget';
import { TitleContainer, Title, TitleIcon, TitleLabel, Hr } from '../../Layout';
import IconSvg from '../../../svg/main_icon7.svg';

import BigNumber from 'bignumber.js';
const BN = BigNumber.clone({ DECIMAL_PLACES: 16, ROUNDING_MODE: 1 });

class TradingView extends Component {
	constructor(props) {
		super(props);

		this.state = {
			selectedLabel: this.props.auth.userId ? 'user' : 'market',
			data: [],
			paging: 0
		};
	}

	componentDidMount() {
		this.loadData(this.state.selectedLabel);
	}

	loadData = label => {
		this.setState({ data: [] });
		if (label === 'user') {
			axios
				.get(`/api/v1/dashboard/lastTrades/` + this.props.auth.userId)
				.then(res => {
                    if(res.data.status === true) {
                        const { lastTrades } = res.data.result;
					    this.setState({ data: lastTrades });
                    } else {
                        // TODO: handle error_code, msg
                    }
				})
				.catch(error => {
					console.error(error);
				});
		} else {
			axios
				.get(`/api/v1/dashboard/lastMarketTrades`)
				.then(res => {
                    if(res.data.status === true) {
                        const { lastTrades } = res.data.result;
					this.setState({ data: lastTrades });
                    } else {
                        // TODO: handle error_code, msg
                    }
				})
				.catch(error => {
					console.error(error);
				});
		}
	};

	selectLabel = label => {
		this.setState({ selectedLabel: label });
		this.loadData(label);
	};

	handlePage = page => {
		this.setState({ paging: page });
	};

	render() {
		const userLabelColor =
			this.state.selectedLabel === 'user' ? null : '#696A6D';
		const marketLabelColor =
			this.state.selectedLabel === 'market' ? null : '#696A6D';

		const rowCountForPage = 5;
		const totalPageCount = Math.ceil(
			this.state.data.length / rowCountForPage
		);
		const begin = this.state.paging * rowCountForPage;
		const end = (this.state.paging + 1) * rowCountForPage;
		let rows = [];
		this.state.data.forEach((x, i) => {
			if (begin <= i && i < end) {
				rows.push(
					<TableRow key={i}>
						<TableRowColumn>
							<span className="tableFontSize">
								{x.pvId + ' ' + x.name}
							</span>
						</TableRowColumn>
						{this.state.selectedLabel === 'user' ? (
							<TableRowColumn>
								<span className="tableFontSize"> {x.side}</span>{' '}
							</TableRowColumn>
						) : null}
						<TableRowColumn>
							<span className="tableFontSize"> {x.price}</span>{' '}
						</TableRowColumn>
						<TableRowColumn>
							<span className="tableFontSize">
								{BN(x.amount).toFixed(1)}
							</span>
						</TableRowColumn>
						<TableRowColumn>
							<span className="tableFontSize">
								{BN(x.amount)
									.times(x.price)
									.toFixed(1)}
							</span>
						</TableRowColumn>
					</TableRow>
				);
			}
		});
		return (
			<div style={{ paddingBottom: '14px' }}>
				<TitleContainer>
					<Title>
						<TitleIcon src={IconSvg} />
						{this.props.auth.userId ? (
							<TitleLabel
								style={{
									color: userLabelColor,
									cursor: 'pointer'
								}}
								onClick={() => this.selectLabel('user')}
							>
								您最近的成交紀錄
							</TitleLabel>
						) : null}
						{this.props.auth.userId ? (
							<span
								style={{
									color: '#696A6D',
									margin: '0 0.5em'
								}}
							>
								{'/'}
							</span>
						) : null}
						<TitleLabel
							style={{
								color: marketLabelColor,
								cursor: 'pointer'
							}}
							onClick={() => this.selectLabel('market')}
						>
							市場近期交易
						</TitleLabel>
					</Title>
				</TitleContainer>
				<Hr />
				<Table
					fixedHeader={false}
					style={{ tableLayout: 'auto' }}
					height={'320px'}
				>
					<TableHeader
						displaySelectAll={false}
						adjustForCheckbox={false}
						enableSelectAll={false}
					>
						<TableRow>
							<TableHeaderColumn>
								<span className="tableFontSize">電廠</span>
							</TableHeaderColumn>
							{this.state.selectedLabel === 'user' ? (
								<TableHeaderColumn>
									<span className="tableFontSize">買賣</span>
								</TableHeaderColumn>
							) : null}
							<TableHeaderColumn>
								<span className="tableFontSize">價格</span>
							</TableHeaderColumn>
							<TableHeaderColumn>
								<span className="tableFontSize">數量</span>
							</TableHeaderColumn>
							<TableHeaderColumn>
								<span className="tableFontSize">金額</span>
							</TableHeaderColumn>
						</TableRow>
					</TableHeader>
					<TableBody displayRowCheckbox={false} showRowHover={true}>
						{rows}
					</TableBody>
				</Table>
				<Pagination
					page={this.state.paging}
					totalPageCount={totalPageCount}
					showPageCount={5}
					onPage={this.handlePage}
				/>
			</div>
		);
	}
}

export default TradingView;
