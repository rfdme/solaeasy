import React, { Component } from 'react';
import axios from 'axios';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from 'material-ui/Table';
import { ProgressBar, Pagination } from '../../Widget';
import { TitleContainer, Title, TitleIcon, TitleLabel, Hr } from '../../Layout';
import IconSvg from '../../../svg/main_icon2.svg';
import InfoSvg from '../../../svg/icon_info.svg';

class HotPv extends Component {
  constructor(props) {
    super(props);

    this.state = {
      allAchievementRate: {},
      selectedLabel: this.props.auth.userId ? 'favorite' : 'hot',
      data: [],
      paging: 0
    };
  }

  componentDidMount() {
    this.loadAchieveRate();
  }

  loadData = label => {
    this.setState({ data: [] });
    if (label === 'favorite') {
      axios
        .get(`/api/v1/dashboard/favoritePv/` + this.props.auth.userId)
        .then(res => {
          if (res.data.status === true) {
            const { favoritePv } = res.data.result;
            this.setState({ data: favoritePv });
          } else {
            // TODO: handle error_code, msg
          }
        })
        .catch(error => {
          console.error(error);
        });
    } else {
      axios
        .get(`/api/v1/dashboard/hotPv`)
        .then(res => {
          if (res.data.status === true) {
            const { hotPv } = res.data.result;
            this.setState({ data: hotPv });
          } else {
            // TODO: handle error_code, msg
          }
        })
        .catch(error => {
          console.error(error);
        });
    }
  };

  loadAchieveRate() {
    axios
      .get(`/api/v1/pv/achievementRate`)
      .then(res => {
        if (res.data.status === true) {
          let { allAchievementRate } = this.state;
          for (let i = 0; i < res.data.result.data.length; i++) {
            const a = res.data.result.data[i];
            allAchievementRate[a.id] = parseFloat(a.achievementRate);
          }

          this.setState({ allAchievementRate: allAchievementRate });
          this.loadData(this.state.selectedLabel);
        } else {
          // TODO: handle error_code, msg
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  selectLabel = label => {
    this.setState({ selectedLabel: label });
    this.loadData(label);
  };

  handlePage = page => {
    this.setState({ paging: page });
  };

  render() {
    const favoriteLabelColor =
      this.state.selectedLabel === 'favorite' ? null : '#696A6D';
    const hotLabelColor = this.state.selectedLabel === 'hot' ? null : '#696A6D';

    const rowCountForPage = 5;
    const totalPageCount = Math.ceil(this.state.data.length / rowCountForPage);
    const begin = this.state.paging * rowCountForPage;
    const end = (this.state.paging + 1) * rowCountForPage;
    let rows = [];
    this.state.data.forEach((x, i) => {
      if (begin <= i && i < end) {
        const r =
          this.state.allAchievementRate[x.pvId] !== undefined
            ? this.state.allAchievementRate[x.pvId]
            : 0;
        const percent = (r * 100).toFixed(1) + '%';
        rows.push(
          <TableRow key={i}>
            <TableRowColumn>
              <span className="tableFontSize">{x.pvId + ' ' + x.name}</span>
            </TableRowColumn>
            <TableRowColumn>
              <span className="tableFontSize">{x.price}</span>
            </TableRowColumn>
            <TableRowColumn>
              <ProgressBar value={r} />
            </TableRowColumn>
            <TableRowColumn>
              <span className="tableFontSize">{percent}</span>
            </TableRowColumn>
          </TableRow>
        );
      }
    });
    return (
      <div style={{ paddingBottom: '14px' }}>
        <TitleContainer>
          <Title>
            <TitleIcon src={IconSvg} />
            {this.props.auth.userId ? (
              <TitleLabel
                style={{
                  color: favoriteLabelColor,
                  cursor: 'pointer'
                }}
                onClick={() => this.selectLabel('favorite')}
              >
                您持有的電廠
              </TitleLabel>
            ) : null}
            {this.props.auth.userId ? (
              <span
                style={{
                  color: '#696A6D',
                  margin: '0 0.5em'
                }}
              >
                {'/'}
              </span>
            ) : null}
            <TitleLabel
              style={{ color: hotLabelColor, cursor: 'pointer' }}
              onClick={() => this.selectLabel('hot')}
            >
              熱門電廠
            </TitleLabel>
          </Title>
        </TitleContainer>
        <Hr />
        <Table
          fixedHeader={false}
          style={{ tableLayout: 'auto' }}
          height={'320px'}
        >
          <TableHeader
            displaySelectAll={false}
            adjustForCheckbox={false}
            enableSelectAll={false}
          >
            <TableRow>
              <TableHeaderColumn>
                <span className="tableFontSize">電廠</span>
              </TableHeaderColumn>
              <TableHeaderColumn>
                <span className="tableFontSize">價格</span>
              </TableHeaderColumn>
              <TableHeaderColumn>
                <span className="tableFontSize">發電達成率</span>
              </TableHeaderColumn>
              <TableHeaderColumn>
                <img
                  alt=""
                  src={InfoSvg}
                  style={{ width: '20px', height: '20px' }}
                />
              </TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false} showRowHover={true}>
            {rows}
          </TableBody>
        </Table>
        <Pagination
          page={this.state.paging}
          totalPageCount={totalPageCount}
          showPageCount={5}
          onPage={this.handlePage}
        />
      </div>
    );
  }
}

export default HotPv;
