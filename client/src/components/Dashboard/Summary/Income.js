import React from 'react';
import styled from 'styled-components';
import { LittleTitle } from '../../Layout';
import BG from '../../../svg/bk_color.svg';

const Income = ({ today, total }) => {
	return (
		<Div>
			<LittleTitle>收入</LittleTitle>
			<Picture>
				<img src={BG} alt="" />
				<Value1>{(parseFloat(today) / 10000).toFixed(1)}</Value1>
				<Value2>
					<span
						style={{
							color: '#f7b100',
							fontSize: '20px',
							marginRight: '10px'
						}}
					>
						{(parseFloat(total) / 10000).toFixed(1)}
					</span>
					<span style={{ color: '#696a6d' }}>萬</span>
				</Value2>
			</Picture>
			<Today>Today</Today>
			<Total>Total</Total>
		</Div>
	);
};

const Div = styled.div`
	width: 300px;
	position: relative;
`;

const Today = styled.div`
	position: absolute;
	top: 60px;
	left: 20px;
	color: #696a6d;
	font-family: Montserrat;
`;

const Total = styled(Today)`
	top: 267px;
`;

const Picture = styled.div`
	position: absolute;
	top: 60px;
	left: 40px;
	font-family: Montserrat;
	width: 200px;
`;

const Value1 = styled.div`
	position: absolute;
	top: 60px;
	color: #f2eb4d;
	font-size: 65px;
	&::after {
		content: '萬';
		font-size: 20px;
		color: #696a6d;
		vertical-align: 110%;
	}
	width: 200px;
	display: flex;
	justify-content: center;
	padding-left: 8px;
`;

const Value2 = styled.div`
	position: absolute;
	top: 203px;
	width: 200px;
	display: flex;
	justify-content: center;
	padding-left: 15px;
	margin-top: 3px;
`;

export default Income;
