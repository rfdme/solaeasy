import React from 'react';
import styled from 'styled-components';
import { LittleTitle } from '../../Layout';
import BG from '../../../svg/bk_tree.svg';

const CO2 = ({ today, total }) => {
	return (
		<Div>
			<LittleTitle>
				<CO>CO</CO>減量
			</LittleTitle>
			<Picture>
				<img src={BG} alt="" />
				<Value1>{parseInt(today, 10)}</Value1>
				<Value2>
					<span
						style={{
							color: '#f7b100',
							fontSize: '20px',
							marginRight: '10px'
						}}
					>
						{parseInt(total / 1000, 10)}
					</span>
					<span style={{ color: '#696a6d' }}>噸</span>
				</Value2>
			</Picture>
			<Today>Today</Today>
			<Total>Total</Total>
		</Div>
	);
};

const Div = styled.div`
	width: 300px;
	position: relative;
`;

const CO = styled.span`
	margin-right: 5px;
	&::after {
		content: '2';
		font-size: 5px;
		vertical-align: 5%;
	}
`;

const Today = styled.div`
	position: absolute;
	top: 60px;
	left: 20px;
	color: #696a6d;
	font-family: Montserrat;
`;

const Total = styled(Today)`
	top: 267px;
`;

const Picture = styled.div`
	position: absolute;
	top: 60px;
	left: 40px;
	font-family: Montserrat;
	margin-top: 3px;
	width: 200px;
`;

const Value1 = styled.div`
	position: absolute;
	top: 45px;
	color: #ffffff;
	font-size: 48px;
	&::after {
		content: 'kg';
		font-size: 20px;
		color: #ffffff;
		vertical-align: 110%;
	}
	width: 201px;
	display: flex;
	justify-content: center;
	padding-left: 15px;
`;

const Value2 = styled.div`
	position: absolute;
	top: 203px;
	width: 200px;
	display: flex;
	justify-content: center;
	padding-left: 15px;
`;

export default CO2;
