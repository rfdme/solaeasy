import React from 'react';
import styled from 'styled-components';
import CounterCircle from './CounterCircle';
import { LittleTitle } from '../../Layout';

const PvCounter = ({ actived, total }) => {
	return (
		<Div>
			<LittleTitle>案場數量</LittleTitle>
			<Today>Today</Today>
			<Content>
				<CounterCircle
					numerator={actived}
					denominator={total}
					label={'建置中'}
					unit={'座'}
				/>
				<Value>
					<span style={{ color: '#f7b100' }}>{actived}</span>
					<span style={{ color: '#696a6d', margin: '0 10px' }}>
						{'/'}
					</span>
					<span style={{ color: '#ffffff' }}>{total}</span>
				</Value>
			</Content>
		</Div>
	);
};

const Div = styled.div`
	width: 300px;
	position: relative;
`;

const Content = styled.div`
	width: 212px;
	padding-left: 40px;
	@media screen and (max-width: 1024px) {
		padding-left: 20px;
	}
`;

const Today = styled.div`
	position: absolute;
	top: 60px;
	left: 20px;
	color: #696a6d;
	font-family: Montserrat;
	@media screen and (max-width: 1024px) {
		left: 2px;
	}
`;

const Value = styled.div`
	font-family: Montserrat;
	font-size: 20px;
	text-align: center;
`;

export default PvCounter;
