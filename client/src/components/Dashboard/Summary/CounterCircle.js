import React from 'react';
import styled from 'styled-components';

const CounterCircle = ({ numerator, denominator, label, unit }) => {
  const size = 106;
  const r = 96;
  const tr = 86;

  var c = Math.PI * (r * 2);
  const value = denominator > 0 ? numerator / denominator : 0;
  var offset = (1 - value) * c;
  return (
    <div style={{ width: size * 2, height: size * 2 }}>
      <Content width={size * 2} height={size * 2}>
        <svg id="svg" width={size * 2} height={size * 2}>
          <Circle r={r} cx={size} cy={size} fill="transparent" />
          <BarBg r={r} cx={size} cy={size} fill="transparent" strokeDasharray={c} strokeDashoffset={offset} />
          <Bar r={r} cx={size} cy={size} fill="transparent" strokeDasharray={c} strokeDashoffset={offset} />
        </svg>
        <Numerator tr={tr} unit={unit}>
          {numerator}
        </Numerator>
        <Value tr={tr}>
          <span style={{ color: '#696a6d' }}>{label}</span>
          {'  '}
          <span style={{ color: '#ffffff' }}>{denominator - numerator}</span>
        </Value>
      </Content>
    </div>
  );
};

const Circle = styled.circle`
  stroke-dashoffset: 0;
  transition: stroke-dashoffset 1s linear;
  stroke: #1f1f22;
  stroke-width: 8px;
  stroke-linecap: round;
`;

const BarBg = styled.circle`
  transition: stroke-dashoffset 1s linear;
  stroke: #3c3325;
  stroke-width: 15px;
  stroke-linecap: round;
  transform: rotate(-90deg);
  transform-origin: 50% 50%;
`;

const Bar = styled.circle`
  transition: stroke-dashoffset 1s linear;
  stroke: #f7b100;
  stroke-width: 8px;
  stroke-linecap: round;
  transform: rotate(-90deg);
  transform-origin: 50% 50%;
`;

const Content = styled.div`
  width: ${props => props.width + 'px'};
  height: ${props => props.height + 'px'};
  display: block;
  margin: 2em auto;
  //box-shadow: 0 0 1em black;
  border-radius: 100%;
  position: relative;
`;

const Numerator = styled.div`
  color: #f7b100;
  position: absolute;
  display: block;
  height: ${props => props.tr * 2 + 'px'};
  width: ${props => props.tr * 2 + 'px'};
  left: 53%;
  top: 40%;
  //box-shadow: inset 0 0 1em black;
  margin-top: ${props => -props.tr + 'px'};
  margin-left: ${props => -props.tr + 'px'};
  border-radius: 100%;
  line-height: ${props => props.tr * 2 + 'px'};
  font-size: 65px;
  font-family: Montserrat;
  text-shadow: 0 0 0.5em black;
  text-align: center;
  &::after {
    content: '座';
    font-size: 14px;
    color: #696a6d;
    vertical-align: 20%;
  }
`;
const Value = styled.div`
  position: absolute;
  display: block;
  height: 100px;
  width: ${props => props.tr * 2 + 'px'};
  left: 52%;
  top: 70%;
  margin-top: ${props => -props.tr + 'px'};
  margin-left: ${props => -props.tr + 'px'};
  line-height: ${props => props.tr * 2 + 'px'};
  font-size: 16px;
  font-family: Montserrat;
  text-align: center;
`;

export default CounterCircle;
