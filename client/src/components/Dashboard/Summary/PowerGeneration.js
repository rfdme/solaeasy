import React from 'react';
import styled from 'styled-components';
import { LittleTitle } from '../../Layout';
import BG from '../../../svg/bk_power.svg';

const PowerGeneration = ({ today, total }) => {
  return (
    <Div>
      <LittleTitle>發電量</LittleTitle>
      <Picture1>
        <Img src={BG} />
        <Value1>{parseFloat(today, 10).toFixed(1)}</Value1>
        <Label1>Today</Label1>
        <Label2>kWh</Label2>
      </Picture1>
      <Picture2>
        <Img src={BG} />
        <Value2>{parseFloat(total, 10).toFixed(1)}</Value2>
        <Label1>Total</Label1>
        <Label2>kWh</Label2>
      </Picture2>
    </Div>
  );
};

const Div = styled.div`
  width: 300px;
  position: relative;
`;

const Picture1 = styled.div`
  position: absolute;
  top: 60px;
  left: 20px;
  font-family: Montserrat;
  width: 246px;
  @media screen and (max-width: 1024px) {
    left: 0px;
  }
`;

const Picture2 = styled(Picture1)`
  top: 180px;
  left: 20px;
  @media screen and (max-width: 1024px) {
    left: 0px;
  }
`;

const Img = styled.img`
  position: absolute;
  top: 10px;
`;

const Value1 = styled.div`
  position: absolute;
  top: 20px;
  left: 15px;
  color: #ffffff;
  font-size: 48px;
`;

const Value2 = styled(Value1)`
  top: 30px;
  left: 20px;
  font-size: 28px;
`;

const Label1 = styled.div`
  position: absolute;
  top: 90px;
  left: 5px;
  color: #696a6d;
`;

const Label2 = styled(Label1)`
  left: 200px;
`;

export default PowerGeneration;
