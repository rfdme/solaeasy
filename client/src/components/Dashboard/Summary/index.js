import React, { Component } from 'react';
import axios from 'axios';

import {
  TitleContainer,
  Title,
  TitleIcon,
  TitleLabel,
  Hr,
  Column
} from '../../Layout';
import IconSvg from '../../../svg/main_icon1.svg';

import PvCounter from './PvCounter';
import PowerGeneration from './PowerGeneration';
import CO2 from './CO2';
import Income from './Income';

class Summary extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activedPvCount: 0,
      totalPvCount: 0,
      today: {
        values: 0,
        co2: 0,
        income: 0
      },
      total: {
        values: 0,
        co2: 0,
        income: 0
      }
    };
  }

  componentDidMount() {
    this.loadRunning();
    this.loadSummaryToday();
    this.loadSummary();
  }

  loadRunning() {
    axios
      .get(`/api/v1/pv/running`)
      .then(res => {
        if (res.data.status === true) {
          this.setState({
            activedPvCount: res.data.result.running,
            totalPvCount: res.data.result.total
          });
        } else {
          // TODO: handle error_code, msg
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  loadSummaryToday() {
    axios
      .get(`/api/v1/pv/summaryToday`)
      .then(res => {
        if (res.data.status === true) {
          this.setState({ today: res.data.result });
        } else {
          // TODO: handle error_code, msg
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  loadSummary() {
    axios
      .get(`/api/v1/pv/summary`)
      .then(res => {
        if (res.data.status === true) {
          this.setState({ total: res.data.result });
        } else {
          // TODO: handle error_code, msg
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  render() {
    return (
      <div style={{ paddingBottom: '28px' }}>
        <TitleContainer>
          <Title>
            <TitleIcon src={IconSvg} />
            <TitleLabel>電廠資訊</TitleLabel>
          </Title>
        </TitleContainer>
        <Hr style={{ marginBottom: '15px' }} />
        <Column
          style={{
            justifyContent: 'space-around'
          }}
        >
          <PvCounter
            actived={this.state.activedPvCount}
            total={this.state.totalPvCount}
          />
          <PowerGeneration
            today={this.state.today.values}
            total={this.state.total.values}
          />
          <CO2 today={this.state.today.co2} total={this.state.total.co2} />
          <Income
            today={this.state.today.income}
            total={this.state.total.income}
          />
        </Column>
      </div>
    );
  }
}

export default Summary;
