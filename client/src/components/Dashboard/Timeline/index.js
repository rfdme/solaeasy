import React, { Component } from 'react';
import styled from 'styled-components';
import moment from 'moment';
import axios from 'axios';
import { amber400 } from 'material-ui/styles/colors';

import Tip from './Tip';
import { TitleContainer, Title, TitleIcon, TitleLabel, TitleRight, Hr } from '../../Layout';
import { Checkbox, SelectedPoint, ComplexPoint } from '../../Widget';
import IconSvg from '../../../svg/main_icon1.svg';
import InfoSvg from '../../../svg/icon_info.svg';

const SpaceHeight = 200;
const LineBase = SpaceHeight / 2;
const LineOffset = 25;
const DateWidth = 30;
const PointSize = 13;
const DateInterval = 7;

class Timeline extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedYear: 2019,
      dateEvents: [],
      tipEvents: [],
      tipPositionX: null,
      tipDate: null,
      tipSelected: null,
      checkList: {
        pv: true,
        buy: true,
        sell: true,
        bill: true,
        repay: true,
        cashout: true,
        cashin: true
      }
    };
  }

  componentDidMount() {
    this.loadTimeline(this.state.selectedYear, this.props.auth.userId);
  }

  loadTimeline = (year, userId) => {
    let dateBegin = moment(year, 'YYYY').startOf('year');
    let dateEnd = moment(year, 'YYYY').endOf('year');
    axios
      .post(`/api/v1/dashboard/timeline`, {
        dateBegin: dateBegin.format('YYYY-MM-DD'),
        dateEnd: dateEnd.format('YYYY-MM-DD'),
        userId: userId
      })
      .then(res => {
        if (res.data.status === true) {
          const { timeline } = res.data.result;
          let dateEvents = {};
          for (let i = 0; i < timeline.length; i++) {
            const event = timeline[i];
            const date = moment(event.date, 'YYYY-MM-DD').format('YYYY-MM-DD');
            if (!dateEvents[date]) {
              dateEvents[date] = [];
            }
            dateEvents[date].push(event);
          }
          this.setState({ dateEvents: dateEvents });

          const element = document.getElementById('today');
          const container = document.getElementById('container');
          if (element && container) {
            const scrollTo = element.offsetLeft - (DateWidth * 6 * 7) / 2;
            setTimeout(() => {
              container.scrollLeft = scrollTo > 0 ? scrollTo : 0;
            }, 1000);
          }
        } else {
          // TODO: handle error_code, msg
        }
      })
      .catch(error => {
        console.error(error);
      });
  };

  handleSelectPoint = (x, date) => {
    axios
      .post(`/api/v1/dashboard/dateEvent`, {
        date: date,
        userId: this.props.auth.userId,
        checkPv: this.state.checkList.pv,
        checkBuy: this.state.checkList.buy,
        checkSell: this.state.checkList.sell,
        checkBill: this.state.checkList.bill,
        checkRepay: this.state.checkList.repay,
        checkCashout: this.state.checkList.cashout,
        checkCashin: this.state.checkList.cashin
      })
      .then(res => {
        if (res.data.status === true) {
          const { dateEvent } = res.data.result;
          this.setState({
            tipEvents: dateEvent,
            tipPositionX: x,
            tipDate: date,
            tipSelected: 0
          });
        } else {
          // TODO: handle error_code, msg
        }
      })
      .catch(error => {
        console.error(error);
      });
  };

  getEventColor(e) {
    switch (e) {
      case 'pv':
        return amber400;
      case 'cashin':
        return '#9500E3';
      case 'cashout':
        return '#D844D2';
      case 'repay':
        return '#BCBC2B';
      case 'bill':
        return '#2BA0BC';
      case 'buy':
        return '#DC415C';
      case 'sell':
        return '#2BBC8F';
      default:
        return 'black';
    }
  }

  handleCheckFilter = e => {
    let { checkList } = this.state;
    checkList[e] = !checkList[e];
    this.setState({
      checkList: checkList,
      tipPositionX: null,
      tipDate: null
    });
  };

  getFilterEvents = events => {
    let filterEvents = events.slice();
    let index = events.length - 1;
    while (index >= 0) {
      if (this.state.checkList[filterEvents[index].event] === false) {
        filterEvents.splice(index, 1);
      }

      index -= 1;
    }
    return filterEvents;
  };

  render() {
    let DateLines = [];
    let start = moment(this.state.selectedYear, 'YYYY').startOf('year');

    let k = 0;
    for (let end = moment(start).add(1, 'years'), i = 0; start.isBefore(end); start.add(1, 'days'), i++) {
      if (moment().format('M/D') === start.format('M/D')) {
        DateLines.push(
          <DateLine
            id="today"
            key={k++}
            x={DateWidth * i}
            ref={elem => {
              this.today = elem;
            }}
          >
            {i % DateInterval === 0 ? start.format('M/D') : ''}
          </DateLine>
        );
      } else {
        DateLines.push(
          <DateLine key={k++} x={DateWidth * i}>
            {i % DateInterval === 0 ? start.format('M/D') : ''}
          </DateLine>
        );
      }
      const events = this.state.dateEvents[start.format('YYYY-MM-DD')];
      if (!events) {
        continue;
      }
      const filterEvents = this.getFilterEvents(events);
      if (filterEvents.length > 0) {
        const date = start.format('YYYY-MM-DD');
        if (filterEvents.length > 1) {
          DateLines.push(
            <ComplexPoint
              key={k++}
              x={DateWidth * i + PointSize / 2}
              y={LineBase + LineOffset}
              onClick={() => {
                this.handleSelectPoint(DateWidth * i, date);
              }}
            />
          );
        } else {
          const color = this.getEventColor(filterEvents[0].event);
          if (date === this.state.tipDate) {
            DateLines.push(
              <SelectedPoint
                key={k++}
                x={DateWidth * i + PointSize / 2}
                y={LineBase + LineOffset}
                color={color}
              />
            );
          } else {
            DateLines.push(
              <Point
                key={k++}
                x={DateWidth * i}
                color={color}
                onClick={() => this.handleSelectPoint(DateWidth * i, date)}
              />
            );
          }
        }
      }
    }

    return (
      <div style={{ paddingBottom: '28px' }}>
        <TitleContainer>
          <Title>
            <TitleIcon src={IconSvg} />
            <TitleLabel>{this.props.auth.userId ? '你的SOLA歷史線' : 'SOLA歷史線'}</TitleLabel>
          </Title>
          {this.props.auth.userId ? (
            <TitleRight>
              <Checkbox
                label={'公告'}
                checked={this.state.checkList['pv']}
                color={this.getEventColor('pv')}
                onCheck={() => this.handleCheckFilter('pv')}
              />
              <Checkbox
                label={'買入'}
                checked={this.state.checkList['buy']}
                color={this.getEventColor('buy')}
                onCheck={() => this.handleCheckFilter('buy')}
              />
              <Checkbox
                label={'賣出'}
                checked={this.state.checkList['sell']}
                color={this.getEventColor('sell')}
                onCheck={() => this.handleCheckFilter('sell')}
              />
              <Checkbox
                label={'分潤'}
                checked={this.state.checkList['bill']}
                color={this.getEventColor('bill')}
                onCheck={() => this.handleCheckFilter('bill')}
              />
              {/*
              <Checkbox
                label={'還本'}
                checked={this.state.checkList['repay']}
                color={this.getEventColor('repay')}
                onCheck={() => this.handleCheckFilter('repay')}
              />*/}
              <Checkbox
                label={'出金'}
                checked={this.state.checkList['cashout']}
                color={this.getEventColor('cashout')}
                onCheck={() => this.handleCheckFilter('cashout')}
              />
              <Checkbox
                label={'入金'}
                checked={this.state.checkList['cashin']}
                color={this.getEventColor('cashin')}
                onCheck={() => this.handleCheckFilter('cashin')}
              />
              <img alt="" src={InfoSvg} style={{ width: '20px', height: '20px' }} />
            </TitleRight>
          ) : null}
        </TitleContainer>
        <Hr />
        <Container id="container">
          <Space>
            {this.state.tipPositionX !== null ? (
              <Tip
                auth={this.props.auth}
                x={this.state.tipPositionX}
                height={LineBase}
                offsetX={PointSize / 2}
                offsetY={LineOffset}
                pointSize={PointSize}
                data={this.state.tipEvents}
                date={this.state.tipDate}
              />
            ) : null}
            {DateLines}
          </Space>
        </Container>
      </div>
    );
  }
}

const Container = styled.div`
  overflow-x: auto;
  background-color: #232429;
  padding: 20px;
`;

const Space = styled.div`
  width: ${365 * DateWidth + 'px'};
  height: ${SpaceHeight + 'px'};
  position: relative;
`;

const DateLine = styled.div`
  position: absolute;
  top: ${LineBase + LineOffset + 'px'};
  left: ${props => props.x + 'px'};
  width: ${DateWidth + 'px'};
  padding-top: 2em;
  border-top: 1px solid grey;
  color: lightgrey;
  font-size: 8px;
`;

const Point = styled.div`
  position: absolute;
  top: ${LineBase + LineOffset + 'px'};
  left: ${props => props.x + 'px'};
  width: ${PointSize + 'px'};
  height: ${PointSize + 'px'};
  transform: translateY(-50%);
  background-color: ${props => props.color};
  border-radius: 100%;
  cursor: pointer;
`;

export default Timeline;
