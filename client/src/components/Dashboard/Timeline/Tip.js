import React, { Component } from 'react';
import styled from 'styled-components';
import axios from 'axios';
import { sprintf } from 'sprintf-js';
import NavigationExpandLess from 'material-ui/svg-icons/navigation/expand-less';
import NavigationExpandMore from 'material-ui/svg-icons/navigation/expand-more';

import { amber400 } from 'material-ui/styles/colors';

import BigNumber from 'bignumber.js';
const BN = BigNumber.clone({ DECIMAL_PLACES: 16, ROUNDING_MODE: 1 });

class Tip extends Component {
	constructor(props) {
		super(props);

		this.state = {
			data: [],
			selectedPoint: 0,
			selectedEvent: null,
			selectedColor: null,
			eventData: null,
			page: 0
		};
	}

	componentDidMount() {
		this.init(this.props.data);
	}

	componentWillReceiveProps(nextProps) {
		this.init(nextProps.data);
	}

	getColor = event => {
		switch (event) {
			case 'pv':
				return amber400;
			case 'cashin':
				return '#9500E3';
			case 'cashout':
				return '#D844D2';
			case 'repay':
				return '#BCBC2B';
			case 'bill':
				return '#2BA0BC';
			case 'buy':
				return '#DC415C';
			case 'sell':
				return '#2BBC8F';
			default:
				return 'black';
		}
	};

	init = data => {
		this.setState({
			data: data,
			selectedPoint: 0,
			selectedEvent: data[0].event,
			selectedColor: this.getColor(data[0].event),
			eventData: null,
			page: 0
		});

		this.loadContent(data[0]);
	};

	handleSelectPoint = (i, c, e) => {
		this.setState({
			selectedPoint: i,
			selectedColor: c,
			selectedEvent: e,
			eventData: null,
			page: 0
		});
		this.loadContent(this.state.data[i]);
	};

	loadContent = d => {
		axios
			.post(`/api/v1/dashboard/eventData/` + d.event, {
				date: d.date,
				userId: this.props.auth.userId
			})
			.then(res => {
                if(res.data.status === true) {
                    const { eventData } = res.data.result;
                    this.setState({
                        eventData: eventData
                    });
                } else {
                    // TODO: handle error_code, msg
                }
			})
			.catch(error => {
				console.error(error);
			});
	};

	previousPage = () => {
		const curPage = this.state.page;
		if (curPage > 0) {
			this.setState({ page: curPage - 1 });
		}
	};

	nextPage = () => {
		const curPage = this.state.page;
		if (curPage < this.state.eventData.length - 1) {
			this.setState({ page: curPage + 1 });
		}
	};

	parseEventContent = (e, d) => {
		switch (e) {
			case 'pv':
				return sprintf('%s %s %s', d.pvId, d.pvName, d.name);
			case 'cashin':
				return sprintf('入金 %s 元', BN(d.amount).toFormat(0));
			case 'cashout':
				return sprintf('出金 %s 元', BN(d.amount).toFormat(0));
			case 'repay':
				return sprintf('返還本金 %s 元', BN(d.repay).toFormat(0));
			case 'bill':
				return sprintf('發電收益 %s 元', BN(d.profit).toFormat(0));
			case 'buy':
				return sprintf(
					'%s 買入 %s %s 價格 %s 數量 %s',
					d.datetime.substring(11, 19),
					d.pvId,
					d.name,
					d.price,
					d.amount
				);
			case 'sell':
				return sprintf(
					'%s 賣出 %s %s 價格 %s 數量 %s',
					d.datetime.substring(11, 19),
					d.pvId,
					d.name,
					d.price,
					d.amount
				);
			default:
				return null;
		}
	};

	render() {
		let points = [];
		for (let i = 0; i < this.state.data.length; i++) {
			const color = this.getColor(this.state.data[i].event);
			points.push(
				<Point
					key={i}
					size={this.props.pointSize}
					color={color}
					selected={i === this.state.selectedPoint}
					onClick={() =>
						this.handleSelectPoint(
							i,
							color,
							this.state.data[i].event
						)
					}
				/>
			);
		}

		const showColor = this.state.selectedColor;

		return (
			<Container
				x={this.props.x}
				height={this.props.height}
				offsetX={this.props.offsetX}
				offsetY={this.props.offsetY}
			>
				<Line color={showColor} />
				<PointContainer>
					{points.length > 1 ? points : null}
				</PointContainer>
				<Text color={showColor}>
					<div>
						<div>{this.props.date}</div>
						{this.state.eventData
							? this.parseEventContent(
									this.state.selectedEvent,
									this.state.eventData[this.state.page]
								)
							: null}
					</div>
					{this.state.eventData && this.state.eventData.length > 1 ? (
						<Paging>
							<NavigationExpandLess
								style={{ color: showColor, cursor: 'pointer' }}
								onClick={this.previousPage}
							/>
							<div>{`${this.state.page + 1}/${
								this.state.eventData.length
							}`}</div>
							<NavigationExpandMore
								style={{ color: showColor, cursor: 'pointer' }}
								onClick={this.nextPage}
							/>
						</Paging>
					) : null}
				</Text>
			</Container>
		);
	}
}

const Container = styled.div`
	position: absolute;
	height: ${props => props.height + 'px'};
	width: 300px;
	top: ${props => props.offsetY + 'px'};
	left: ${props => props.x + 'px'};
	margin-left: ${props => props.offsetX + 'px'};
`;

const PointContainer = styled.div`
	position: absolute;
	top: -20px;
	height: 20px;
`;

const Point = styled.div`
	float: left;
	width: ${props => props.size + 'px'};
	height: ${props => props.size + 'px'};
	background-color: ${props => props.color};
	border-radius: 100%;
	margin-right: 5px;
	opacity: ${props => (props.selected ? 1 : 0.2)};
	cursor: pointer;
`;

const Line = styled.div`
	position: absolute;
	height: 100%;
	border-left: 1px solid ${props => props.color};
`;

const Text = styled.div`
	position: absolute;
	color: ${props => props.color};
	padding-left: 3px;
	top: -3px;
	display: flex;
`;

const Paging = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
`;

export default Tip;
