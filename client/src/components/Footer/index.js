import React from 'react';
import styled from 'styled-components';

import logo from './logo.svg';

const Footer = () => {
	return (
		<MyDiv className="site-footer">
			<MyHr />
			<MyContent>
				<MyText>Powered by</MyText>
				<MyLogo src={logo} />
			</MyContent>
		</MyDiv>
	);
};

const MyDiv = styled.div`
	min-height: 70px;
	height: 70px;
	display: flex;
	flex-direction: column;
`;

const MyHr = styled.hr`
	height: 1px;
	margin: 15px 0 0 0;
	opacity: 0.13;
	border: none;
	border-top: 1px solid #ffffff;
`;

const MyContent = styled.div`
	flex: 1;
	display: flex;
	justify-content: center;
	align-items: center;
`;

const MyText = styled.div`
	font-size: 20px;
	color: lightgrey;
`;

const MyLogo = styled.img`
	padding: 10px;
`;

export default Footer;
