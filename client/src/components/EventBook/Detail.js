import React, { Component } from 'react';
import styled from 'styled-components';
import { sprintf } from 'sprintf-js';
import axios from 'axios';
import {
	grey400,
	white,
	blue600,
	green600,
	yellow600,
	red600
} from 'material-ui/styles/colors';

import BigNumber from 'bignumber.js';
const BN = BigNumber.clone({ DECIMAL_PLACES: 16, ROUNDING_MODE: 1 });

class Detail extends Component {
	constructor(props) {
		super(props);

		this.state = {
			eventPV: [],
			eventSimulate: [],
			eventBill: [],
			eventRepay: []
		};
	}

	componentDidMount() {
		this.reload(this.props.date);
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.date !== this.props.date) {
			this.reload(nextProps.date);
		}
	}

	reload(date) {
		this.loadEventPV(date);

		if (this.props.auth.token) {
			this.loadEventSimulate(
				date,
				this.props.auth.token,
				this.props.auth.userId
			);
			this.loadEventBill(
				date,
				this.props.auth.token,
				this.props.auth.userId
			);
			this.loadEventRepay(
				date,
				this.props.auth.token,
				this.props.auth.userId
			);
		}
	}

	loadEventPV(date) {
		return axios
			.get(sprintf(`/api/v1/event/pv?begin=%s&end=%s`, date, date))
			.then(res => {
                if(res.data.status === true) {
                    this.setState({ eventPV: res.data.result.events });
                } else {
                    // TODO: handle error_code, msg
                }
			})
			.catch(error => {
				console.error(error);
			});
	}

	loadEventSimulate(date, token, user) {
		if (this.props.mock.enable) {
			const { eventBySimulate } = this.props.mock.getData();
			const todayEventSimulate = eventBySimulate.filter(
				s => s.date === date
			);
			this.setState({
				eventSimulate: todayEventSimulate
			});
		} else {
			return axios
				.get(
					sprintf(
						`/api/v1/event/simulateDetail?date=%s&user=%s`,
						date,
						user
					),
					{
						headers: {
							Authorization: token
						}
					}
				)
				.then(res => {
                    if(res.data.status === true) {
                        this.setState({ eventSimulate: res.data.result.events });
                    } else {
                        // TODO: handle error_code, msg
                    }
				})
				.catch(error => {
					console.error(error);
				});
		}
	}

	loadEventBill(date, token, user) {
		return axios
			.get(
				sprintf(`/api/v1/event/billDetail?date=%s&user=%s`, date, user),
				{
					headers: {
						Authorization: token
					}
				}
			)
			.then(res => {
                if(res.data.status === true) {
                    this.setState({ eventBill: res.data.result.events });
                } else {
                    // TODO: handle error_code, msg
                }
			})
			.catch(error => {
				console.error(error);
			});
	}

	loadEventRepay(date, token, user) {
		return axios
			.get(
				sprintf(
					`/api/v1/event/repayDetail?date=%s&user=%s`,
					date,
					user
				),
				{
					headers: {
						Authorization: token
					}
				}
			)
			.then(res => {
                if(res.data.status === true) {
                    this.setState({ eventRepay: res.data.result.events });
                } else {
                    // TODO: handle error_code, msg
                }
			})
			.catch(error => {
				console.error(error);
			});
	}

	render() {
		let k = 0;
		let eventPV = [];
		this.state.eventPV.forEach((x, i) => {
			eventPV.push(
				<div key={k++}>{sprintf('%s: %s', x.pvId, x.name)}</div>
			);
		});

		let eventSimulate = [];
		let sumSimulate = BN(0);
		this.state.eventSimulate.forEach((x, i) => {
			eventSimulate.push(
				<div key={k++}>
					{sprintf('%s: %s元', x.pvId, BN(x.profit).toFormat(1))}
				</div>
			);
			sumSimulate = sumSimulate.plus(x.profit);
		});
		if (!sumSimulate.eq(0)) {
			eventSimulate.push(
				<div key={k++}>
					{sprintf('總計: %s元', sumSimulate.toFormat(1))}
				</div>
			);
		}

		let eventBill = [];
		let sumBill = BN(0);
		this.state.eventBill.forEach((x, i) => {
			eventBill.push(
				<div key={k++}>
					{sprintf(
						'%s (%s~%s): %s元',
						x.pvId,
						x.rangeBegin.slice(0, 7),
						x.rangeEnd.slice(0, 7),
						BN(x.profit).toFormat(1)
					)}
				</div>
			);
			sumBill = sumBill.plus(x.profit);
		});
		if (!sumBill.eq(0)) {
			eventBill.push(
				<div key={k++}>
					{sprintf('總計: %s元', sumBill.toFormat(1))}
				</div>
			);
		}

		let eventRepay = [];
		let sumRepay = BN(0);
		this.state.eventRepay.forEach((x, i) => {
			eventRepay.push(
				<div key={k++}>
					{sprintf('%s: %s元', x.pvId, BN(x.repay).toFormat(1))}
				</div>
			);
			sumRepay = sumRepay.plus(x.repay);
		});
		if (!sumRepay.eq(0)) {
			eventRepay.push(
				<div key={k++}>
					{sprintf('總計: %s元', sumRepay.toFormat(1))}
				</div>
			);
		}

		return (
			<MainBlock>
				<Define>
					<Item>
						<Mask color={blue600} />電廠重要公告訊息
					</Item>
					<Item>
						<Mask color={green600} />依據每日發電量試算
					</Item>
					<Item>
						<Mask color={yellow600} />依據實際電費單配發
					</Item>
					<Item>
						<Mask color={red600} />依據電廠起始日還本
					</Item>
				</Define>
				<Date>{this.props.date}</Date>
				<Data>
					{eventPV.length > 0 ? (
						<DataBlock>
							<BlockTitle color={blue600}>電廠事件:</BlockTitle>
							<div>{eventPV}</div>
						</DataBlock>
					) : null}
					{eventSimulate.length > 0 ? (
						<DataBlock>
							<BlockTitle color={green600}>模擬收益:</BlockTitle>
							<div>{eventSimulate}</div>
						</DataBlock>
					) : null}
					{eventBill.length > 0 ? (
						<DataBlock>
							<BlockTitle color={yellow600}>實際收益:</BlockTitle>
							<div>{eventBill}</div>
						</DataBlock>
					) : null}
					{eventRepay.length > 0 ? (
						<DataBlock>
							<BlockTitle color={red600}>返回本金:</BlockTitle>
							<div>{eventRepay}</div>
						</DataBlock>
					) : null}
				</Data>
			</MainBlock>
		);
	}
}

const MainBlock = styled.div`
	width: 200px;
	display: flex;
	flex-direction: column;
	color: ${grey400};
`;

const Define = styled.div`
	padding: 0.5em;
	margin: 1px;
	font-size: 14px;
`;

const Item = styled.div`
	display: flex;
	align-items: center;
`;

const Mask = styled.div`
	float: left;
	width: 15px;
	height: 4px;
	margin-right: 0.5em;
	background-color: ${props => props.color};
	opacity: 0.6;
`;

const Date = styled.div`
	height: 20px;
	padding: 0.5em;
	margin: 1px;
	border-radius: 2px;
	border: 1px solid rgba(0, 0, 0, 0);
	background-color: rgba(255, 255, 255, 0.05);
	color: ${white};
`;

const Data = styled.div`
	flex: 1;
	min-height: 500px;
	max-height: 700px;
	overflow-y: auto;
	padding: 0.5em;
	margin: 1px;
	border-radius: 2px;
	border: 1px solid rgba(0, 0, 0, 0);
	background-color: rgba(255, 255, 255, 0.05);
	color: ${white};
`;

const BlockTitle = styled.div`
	font-size: 14px;
	color: ${props => props.color};
`;

const DataBlock = styled.div`
	font-size: 12px;
	margin-bottom: 1em;
	color: ${grey400};
`;

export default Detail;
