import React, { Component } from 'react';
import moment from 'moment';
import { sprintf } from 'sprintf-js';
import styled from 'styled-components';
import axios from 'axios';

import Calendar from './Calendar';
import DateSelector from './DateSelector';
import { EventDetail } from '../SideBar';
import Footer from '../Footer';
import {
  Page,
  SideBar,
  HalfPaper,
  Column,
  Content,
  TitleContainer,
  Title,
  TitleIcon,
  TitleLabel,
  TitleRight,
  Hr
} from '../Layout';
import IconSvg from '../../svg/menu_icon_4_hover.svg';

class EventBook extends Component {
  constructor(props) {
    super(props);

    this.handleDateSelect = this.handleDateSelect.bind(this);
    this.handleDateClick = this.handleDateClick.bind(this);

    let date = moment().format('YYYY-MM-DD');
    if (this.props.mock.enable) {
      date = this.props.mock.date;
    }

    this.state = {
      today: date,
      selectedDate: date,
      clickedDate: date,
      eventPV: [],
      eventSimulate: [],
      eventBill: [],
      eventRepay: []
    };
  }

  componentDidMount() {
    //如何修正被呼叫兩次的問題
    this.reload();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.selectedDate !== this.state.selectedDate) {
      this.reload();
    }
  }

  getCalendar() {
    let beginDate = moment(this.state.selectedDate, 'YYYY-MM-DD').startOf(
      'month'
    );
    let endDate = moment(this.state.selectedDate, 'YYYY-MM-DD').endOf('month');

    const prevMonthDayCount = beginDate.isoWeekday() - 1;
    const nextMonthDayCount = 7 - endDate.isoWeekday();

    beginDate.subtract(prevMonthDayCount, 'days');
    endDate.add(nextMonthDayCount, 'days');

    return { beginDate, endDate };
  }

  reload() {
    const { beginDate, endDate } = this.getCalendar();

    this.loadEventPV(
      beginDate.format('YYYY-MM-DD'),
      endDate.format('YYYY-MM-DD')
    );

    if (this.props.auth.token) {
      this.loadEventSimulate(
        beginDate.format('YYYY-MM-DD'),
        endDate.format('YYYY-MM-DD'),
        this.props.auth.token,
        this.props.auth.userId
      );
      this.loadEventBill(
        beginDate.format('YYYY-MM-DD'),
        endDate.format('YYYY-MM-DD'),
        this.props.auth.token,
        this.props.auth.userId
      );
      this.loadEventRepay(
        beginDate.format('YYYY-MM-DD'),
        endDate.format('YYYY-MM-DD'),
        this.props.auth.token,
        this.props.auth.userId
      );
    }
  }

  loadEventPV(beginDate, endDate) {
    return axios
      .get(sprintf(`/api/v1/event/pv?begin=%s&end=%s`, beginDate, endDate))
      .then(res => {
        if (res.data.status === true) {
          this.setState({ eventPV: res.data.result.events });
        } else {
          // TODO: handle error_code, msg
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  loadEventSimulate(beginDate, endDate, token, user) {
    if (this.props.mock.enable) {
      const { eventBySimulate } = this.props.mock.getData();
      this.setState({
        eventSimulate: eventBySimulate
      });
    } else {
      return axios
        .get(
          sprintf(
            `/api/v1/event/simulate?begin=%s&end=%s&user=%s`,
            beginDate,
            endDate,
            user
          ),
          {
            headers: {
              Authorization: token
            }
          }
        )
        .then(res => {
          if (res.data.status === true) {
            this.setState({ eventSimulate: res.data.result.events });
          } else {
            // TODO: handle error_code, msg
          }
        })
        .catch(error => {
          console.error(error);
        });
    }
  }

  loadEventBill(beginDate, endDate, token, user) {
    if (this.props.mock.enable) {
      this.setState({
        eventBill: []
      });
    } else {
      return axios
        .get(
          sprintf(
            `/api/v1/event/bill?begin=%s&end=%s&user=%s`,
            beginDate,
            endDate,
            user
          ),
          {
            headers: {
              Authorization: token
            }
          }
        )
        .then(res => {
          if (res.data.status === true) {
            this.setState({ eventBill: res.data.result.events });
          } else {
            // TODO: handle error_code, msg
          }
        })
        .catch(error => {
          console.error(error);
        });
    }
  }

  loadEventRepay(beginDate, endDate, token, user) {
    if (this.props.mock.enable) {
      this.setState({
        eventRepay: []
      });
    } else {
      return axios
        .get(
          sprintf(
            `/api/v1/event/repay?begin=%s&end=%s&user=%s`,
            beginDate,
            endDate,
            user
          ),
          {
            headers: {
              Authorization: token
            }
          }
        )
        .then(res => {
          if (res.data.status === true) {
            this.setState({ eventRepay: res.data.result.events });
          } else {
            // TODO: handle error_code, msg
          }
        })
        .catch(error => {
          console.error(error);
        });
    }
  }

  handleDateSelect(date) {
    this.setState({ selectedDate: date, clickedDate: date });
  }

  handleDateClick(date) {
    this.setState({ clickedDate: date });
  }

  render() {
    const {
      today,
      selectedDate,
      clickedDate,
      eventPV,
      eventSimulate,
      eventBill,
      eventRepay
    } = this.state;

    const { beginDate, endDate } = this.getCalendar();
    return (
      <Page>
        <div className="page-wrap container">
          <Column>
            <MySideBar>
              <EventDetail date={clickedDate} {...this.props} />
            </MySideBar>
            <Content>
              <HalfPaper style={{ paddingBottom: '28px' }}>
                <TitleContainer>
                  <Title>
                    <TitleIcon src={IconSvg} />
                    <TitleLabel>事件日曆</TitleLabel>
                  </Title>
                  <TitleRight style={{ width: '80%' }}>
                    <Tip color={'#5793F3'}>
                      <TipPoint color={'#5793F3'} />
                      電廠重要公告訊息
                    </Tip>
                    <TipGap />
                    <Tip color={'#2bbc8f'}>
                      <TipPoint color={'#2bbc8f'} />
                      依據每日發電量試算
                    </Tip>
                    <TipGap />
                    <Tip color={'#f7b100'}>
                      <TipPoint color={'#f7b100'} />
                      依據實際電費單配發
                    </Tip>
                    <TipGap />
                    {/*}
                    <Tip color={'#dc415c'}>
                      <TipPoint color={'#dc415c'} />依據電廠起始日還本
                    </Tip>
                  */}
                  </TitleRight>
                </TitleContainer>
                <Hr />
                <DateSelector
                  initDate={selectedDate}
                  onSelect={this.handleDateSelect}
                />
                <Calendar
                  today={today}
                  anchorDate={selectedDate}
                  selectedDate={clickedDate}
                  beginDate={beginDate}
                  endDate={endDate}
                  eventPV={eventPV}
                  eventSimulate={eventSimulate}
                  eventBill={eventBill}
                  eventRepay={eventRepay}
                  onClick={this.handleDateClick}
                />
              </HalfPaper>
            </Content>
          </Column>
        </div>
        <Footer />
      </Page>
    );
  }
}

const MySideBar = styled(SideBar)`
  width: 200px;
  @media screen and (max-width: 1024px) {
    width: 150px;
  }
`;

const Tip = styled.span`
  color: ${props => props.color};
  display: flex;
  align-items: center;
  @media screen and (max-width: 1024px) {
    font-size: 14px;
  }
`;

const TipGap = styled.div`
  width: 1em;
`;

const TipPoint = styled.div`
  width: 15px;
  height: 15px;
  background-color: ${props => props.color};
  border-radius: 100%;
  margin-right: 5px;
  @media screen and (max-width: 1024px) {
    width: 14px;
    height: 14px;
  }
`;

export default EventBook;
