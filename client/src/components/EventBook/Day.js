import React, { Component } from 'react';
import { sprintf } from 'sprintf-js';
import styled from 'styled-components';
import { grey600, amber600 } from 'material-ui/styles/colors';

import BigNumber from 'bignumber.js';
const BN = BigNumber.clone({ DECIMAL_PLACES: 16, ROUNDING_MODE: 1 });

class Day extends Component {
	constructor(props) {
		super(props);

		this.handleClick = this.handleClick.bind(this);
	}

	handleClick() {
		this.props.onClick(this.props.day);
	}

	render() {
		const {
			day,
			eventPV,
			eventSimulate,
			eventBill,
			eventRepay,
			selected,
			isToday,
			sameMonth
		} = this.props;

		let events = [];
		let k = 0;
		eventPV.forEach((x, i) => {
			if (x.date === day) {
				events.push(
					<Event key={k++}>
						<EventMask color={'#5793F3'}>
							{sprintf('%s: %s', x.pvId, x.name)}
						</EventMask>
					</Event>
				);
			}
		});

		eventSimulate.forEach((x, i) => {
			if (x.date === day) {
				events.push(
					<Event key={k++}>
						<EventMask color={'#2bbc8f'}>
							{sprintf(
								'預估收益 %s 元',
								BN(x.profit.toString()).toFormat(1)
							)}
						</EventMask>
					</Event>
				);
			}
		});

		eventBill.forEach((x, i) => {
			if (x.date === day) {
				events.push(
					<Event key={k++}>
						<EventMask color={'#f7b100'}>
							{sprintf(
								'實際收益 %s 元',
								BN(x.profit.toString()).toFormat(1)
							)}
						</EventMask>
					</Event>
				);
			}
		});

		eventRepay.forEach((x, i) => {
			if (x.date === day) {
				events.push(
					<Event key={k++}>
						<EventMask color={'#dc415c'}>
							{sprintf(
								'返回本金 %s 元',
								BN(x.repay.toString()).toFormat(1)
							)}
						</EventMask>
					</Event>
				);
			}
		});

		return (
			<MyDay
				onClick={this.handleClick}
				selected={selected}
				sameMonth={sameMonth}
			>
				<Date isToday={isToday} sameMonth={sameMonth}>
					{day}
				</Date>
				{events}
			</MyDay>
		);
	}
}

const MyDay = styled.div`
	flex: 1;
	min-height: 100px;
	padding: 0.5em;
	margin: 1px;
	border-radius: 2px;
	border: 1px solid
		${props => (props.selected ? '#f7b100' : 'rgba(0, 0, 0, 0)')};
	background-color: ${props =>
		props.sameMonth ? '#1e1e22' : 'rgba(0, 0, 0, 0.1)'};
	overflow: hidden;
	cursor: pointer;
`;

const Date = styled.div`
	font-size: 12px;
	text-align: right;
	color: ${props =>
		props.isToday ? amber600 : props.sameMonth ? '#696a6d' : grey600};
`;

const EventMask = styled.div`
	position: absolute;
	top: 0;
	left: 0;
	height: 100%;
	min-width: 106px;
	padding: 0px 10px;
	border-radius: 50px;
	background-color: ${props => props.color};
	font-size: 12px;
	color: #ffffff;
	overflow: hidden;
	white-space: nowrap;
`;

const Event = styled.div`
	position: relative;
	width: 100%;
	height: 20px;
	margin-top: 3px;
	overflow: hidden;
`;

export default Day;
