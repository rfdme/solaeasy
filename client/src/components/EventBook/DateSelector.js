import React, { Component } from 'react';
import moment from 'moment';
import DatePicker from 'material-ui/DatePicker';
import FlatButton from 'material-ui/FlatButton';
import { TitleIcon, Column, ColumnGap } from '../Layout';
import IconSvg from '../../svg/menu_icon_4.svg';

class DateSelector extends Component {
	constructor(props) {
		super(props);

		this.handlePrevMonth = this.handlePrevMonth.bind(this);
		this.handleNextMonth = this.handleNextMonth.bind(this);
		this.handleSelectDate = this.handleSelectDate.bind(this);
	}

	componentDidMount() {}

	handlePrevMonth() {
		let d = moment(this.props.initDate, 'YYYY-MM-DD');
		d.subtract(1, 'months');
		this.props.onSelect(d.format('YYYY-MM-DD'));
	}

	handleNextMonth() {
		let d = moment(this.props.initDate, 'YYYY-MM-DD');
		d.add(1, 'months');
		this.props.onSelect(d.format('YYYY-MM-DD'));
	}

	handleSelectDate(event, date) {
		this.props.onSelect(moment(date).format('YYYY-MM-DD'));
	}

	render() {
		return (
			<Column
				style={{
					alignItems: 'center',
					justifyContent: 'center'
				}}
			>
				<FlatButton
					label="上個月"
					onClick={this.handlePrevMonth}
					style={{ color: '#696A6D' }}
				/>
				<ColumnGap />
				<div style={{ display: 'flex', alignItems: 'center' }}>
					<TitleIcon src={IconSvg} />
					<DatePicker
						hintText="Portrait Dialog"
						autoOk={true}
						value={new Date(this.props.initDate)}
						onChange={this.handleSelectDate}
						formatDate={date => moment(date).format('YYYY-MM-DD')}
						textFieldStyle={{ width: '5.5em' }}
					/>
				</div>
				<ColumnGap />
				<FlatButton
					label="下個月"
					onClick={this.handleNextMonth}
					style={{ color: '#696A6D' }}
				/>
			</Column>
		);
	}
}

export default DateSelector;
