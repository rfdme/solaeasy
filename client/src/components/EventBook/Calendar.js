import React from 'react';
import styled from 'styled-components';

import Day from './Day';

const Calendar = ({
	today,
	anchorDate,
	selectedDate,
	beginDate,
	endDate,
	eventPV,
	eventSimulate,
	eventBill,
	eventRepay,
	onClick
}) => {
	const count = endDate.diff(beginDate, 'days') + 1;
	let i = 0;
	let lastWeek = -1;
	let weeks = [];
	let days;
	let day = beginDate;

	weeks.push(
		<WeekTitle key={-1}>
			<Weekday>週一</Weekday>
			<Weekday>週二</Weekday>
			<Weekday>週三</Weekday>
			<Weekday>週四</Weekday>
			<Weekday>週五</Weekday>
			<Weekday>週六</Weekday>
			<Weekday>週日</Weekday>
		</WeekTitle>
	);
	while (i < count) {
		const curWeek = Math.floor(i / 7);
		if (curWeek !== lastWeek) {
			lastWeek = curWeek;
			days = [];
			weeks.push(<Week key={curWeek}>{days}</Week>);
		}

		days.push(
			<Day
				key={i}
				day={day.format('YYYY-MM-DD')}
				eventPV={eventPV}
				eventSimulate={eventSimulate}
				eventBill={eventBill}
				eventRepay={eventRepay}
				selected={day.format('YYYY-MM-DD') === selectedDate}
				isToday={day.format('YYYY-MM-DD') === today}
				sameMonth={day.format('YYYY-MM') === anchorDate.slice(0, 7)}
				onClick={onClick}
			/>
		);
		day.add(1, 'days');
		i++;
	}

	return <MyCalendar>{weeks}</MyCalendar>;
};

const MyCalendar = styled.div``;

const WeekTitle = styled.div`
	display: flex;
`;

const Week = styled.div`
	display: flex;
`;

const Weekday = styled.div`
	flex: 1;
	padding: 0.5em;
	text-align: center;
	color: #696a6d;
`;

export default Calendar;
