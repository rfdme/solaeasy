import React, { Component } from 'react';
import {
	Table,
	TableBody,
	TableHeader,
	TableHeaderColumn,
	TableRow,
	TableRowColumn
} from 'material-ui/Table';
import { red400, green400 } from 'material-ui/styles/colors';
import { TitleContainer, Title, TitleIcon, TitleLabel, Hr } from '../Layout';
import IconSvg from '../../svg/main_icon7.svg';

import TablePaging from '../TablePaging';

import BigNumber from 'bignumber.js';
const BN = BigNumber.clone({ DECIMAL_PLACES: 16, ROUNDING_MODE: 1 });

class MyTradeList extends Component {
	constructor(props) {
		super(props);

		this.handlePage = this.handlePage.bind(this);

		this.state = {
			paging: 0
		};
	}

	handlePage(page) {
		this.setState({ paging: page });
	}

	render() {
		const rowCountForPage = 5;
		const totalPageCount = Math.ceil(
			this.props.data.length / rowCountForPage
		);
		const begin = this.state.paging * rowCountForPage;
		const end = (this.state.paging + 1) * rowCountForPage;
		let rows = [];
		this.props.data.forEach((x, i) => {
			if (begin <= i && i < end) {
				rows.push(
					<TableRow
						key={i}
						style={
							x.side === '賣'
								? { color: green400 }
								: { color: red400 }
						}
					>
						<TableRowColumn>
							{x.datetime.substring(10)}
						</TableRowColumn>
						<TableRowColumn> {x.pvId} </TableRowColumn>
						<TableRowColumn> {x.side} </TableRowColumn>
						<TableRowColumn> {x.price} </TableRowColumn>
						<TableRowColumn> {x.amount} </TableRowColumn>
						<TableRowColumn>
							{BN(x.fee)
								.div(2)
								.toFormat()}元
						</TableRowColumn>
						<TableRowColumn>
							{BN(x.amount)
								.times(x.price)
								.toFormat()}
							元
						</TableRowColumn>
					</TableRow>
				);
			}
		});
		return (
			<div>
				<TitleContainer>
					<Title>
						<TitleIcon src={IconSvg} />
						<TitleLabel>我的成交明細</TitleLabel>
					</Title>
				</TitleContainer>
				<Hr />
				<Table fixedHeader={false} style={{ tableLayout: 'auto' }}>
					<TableHeader
						displaySelectAll={false}
						adjustForCheckbox={false}
						enableSelectAll={false}
					>
						<TableRow>
							<TableHeaderColumn>時間</TableHeaderColumn>
							<TableHeaderColumn>代碼</TableHeaderColumn>
							<TableHeaderColumn>買賣</TableHeaderColumn>
							<TableHeaderColumn>成交價格</TableHeaderColumn>
							<TableHeaderColumn>成交數量</TableHeaderColumn>
							<TableHeaderColumn>手續費</TableHeaderColumn>
							<TableHeaderColumn>成交金額</TableHeaderColumn>
						</TableRow>
					</TableHeader>
					<TableBody displayRowCheckbox={false} showRowHover={true}>
						{rows}
					</TableBody>
				</Table>
				<TablePaging
					page={this.state.paging}
					totalPageCount={totalPageCount}
					showPageCount={5}
					onPage={this.handlePage}
				/>
			</div>
		);
	}
}

export default MyTradeList;
