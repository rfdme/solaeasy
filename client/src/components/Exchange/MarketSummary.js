import React from 'react';
import {
	Table,
	TableBody,
	TableHeader,
	TableHeaderColumn,
	TableRow,
	TableRowColumn
} from 'material-ui/Table';
import { TitleContainer, Title, TitleIcon, TitleLabel, Hr } from '../Layout';
import IconSvg from '../../svg/main_icon7.svg';

import BigNumber from 'bignumber.js';
const BN = BigNumber.clone({ DECIMAL_PLACES: 16, ROUNDING_MODE: 1 });

const MarketSummary = ({ data }) => (
	<div>
		<TitleContainer>
			<Title>
				<TitleIcon src={IconSvg} />
				<TitleLabel>市場概況</TitleLabel>
			</Title>
		</TitleContainer>
		<Hr />
		<Table
			fixedHeader={true}
			style={{ tableLayout: 'auto' }}
			height={'220px'}
		>
			<TableHeader
				displaySelectAll={false}
				adjustForCheckbox={false}
				enableSelectAll={false}
			>
				<TableRow>
					<TableHeaderColumn>代碼</TableHeaderColumn>
					<TableHeaderColumn>價格</TableHeaderColumn>
					<TableHeaderColumn>成交量</TableHeaderColumn>
				</TableRow>
			</TableHeader>
			<TableBody displayRowCheckbox={false} showRowHover={true}>
				{data.map((x, i) => (
					<TableRow key={i}>
						<TableRowColumn> {x.pvId} </TableRowColumn>
						<TableRowColumn> {x.price} </TableRowColumn>
						<TableRowColumn>
							{BN(x.volume).toFixed(1)}
						</TableRowColumn>
					</TableRow>
				))}
			</TableBody>
		</Table>
	</div>
);

export default MarketSummary;
