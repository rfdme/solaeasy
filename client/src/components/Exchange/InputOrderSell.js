import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { white, green400 } from 'material-ui/styles/colors';
import styled from 'styled-components';
import {
	TitleContainer,
	Title,
	TitleIcon,
	TitleLabel,
	TitleRight,
	Hr,
	Column,
	ColumnGap,
	Content
} from '../Layout';
import IconSvg from '../../svg/main_icon8.svg';

import BN from 'bignumber.js';
BN.config({ DECIMAL_PLACES: 16 });
const SBN = BN.clone({ DECIMAL_PLACES: 6, ROUNDING_MODE: 1 });

class InputOrderSell extends Component {
	constructor(props) {
		super(props);

		this.handlePriceChange = this.handlePriceChange.bind(this);
		this.handleAmountChange = this.handleAmountChange.bind(this);
		this.handleOrderSell = this.handleOrderSell.bind(this);

		this.state = {
			price: 0, //props.defaultPrice,
			amount: 0
		};
	}

	componentWillReceiveProps(nextProps) {
		/*
		if (this.state.price !== nextProps.defaultPrice) {
			this.setState({ price: nextProps.defaultPrice });
		}*/
	}

	formatValue(value) {
		try {
			const bn = SBN(value, 10);
			if (bn.lt(0)) {
				return bn.abs().toString(10);
			} else if (bn.eq(value)) {
				return value;
			} else {
				return bn.toString(10);
			}
		} catch (e) {
			return '';
		}
	}

	handlePriceChange(event) {
		this.setState({ price: this.formatValue(event.target.value) });
	}

	handleAmountChange(event) {
		this.setState({ amount: this.formatValue(event.target.value) });
	}

	handleOrderSell(event) {
		event.preventDefault();

		let price;
		try {
			price = SBN(this.state.price, 10);
		} catch (e) {
			this.setState({ price: 0 });
			price = SBN(0);
		}

		let amount;
		try {
			amount = SBN(this.state.amount, 10);
		} catch (e) {
			this.setState({ amount: 0 });
			amount = SBN(0);
		}

		this.props.callback(this.props.pvId, price, amount);
	}

	render() {
		const { price, amount } = this.state;
		let total, fee;
		try {
			total = BN(price).times(amount);
			fee = total.times(0.015);
		} catch (e) {
			total = BN(0);
			fee = BN(0);
		}
		return (
			<div>
				<form>
					<TitleContainer>
						<Title>
							<TitleIcon src={IconSvg} />
							<TitleLabel>賣出</TitleLabel>
						</Title>
						<TitleRight>
							<RemainLabel>{`可用${
								this.props.pvId
							}:`}</RemainLabel>
							<RemainValue>
								{`${BN(this.props.remained).toFixed(0)} 個`}
							</RemainValue>
						</TitleRight>
					</TitleContainer>
					<Hr />
					<Row>
						<Column>
							<Content>
								<label>價格:</label>
								<TextField
									name="price"
									type="number"
									value={price}
									onChange={this.handlePriceChange}
									fullWidth
								/>
							</Content>
							<ColumnGap />
							<Content>
								<label>數量:</label>
								<TextField
									name="amount"
									type="number"
									value={amount}
									onChange={this.handleAmountChange}
									fullWidth
								/>
							</Content>
						</Column>
					</Row>
					<Row>
						<Column>
							<Content>
								<label>金額:</label>
								<div
									style={{
										width: '100%',
										borderBottom: '1px solid grey',
										color: 'lightgrey',
										margin: '10px 0',
										paddingBottom: '6px'
									}}
								>
									{total.toString(10)}
								</div>
							</Content>
							<ColumnGap />
							<Content>
								<label>手續費:</label>
								<div
									style={{
										width: '100%',
										borderBottom: '1px solid grey',
										color: 'lightgrey',
										margin: '10px 0',
										paddingBottom: '6px'
									}}
								>
									{fee.toString(10)}
								</div>
							</Content>
						</Column>
					</Row>
					<RaisedButton
						label={'賣出' + this.props.pvId}
						onClick={this.handleOrderSell}
						fullWidth={true}
						backgroundColor={green400}
						labelColor={white}
					/>
				</form>
			</div>
		);
	}
}

const Row = styled.div`
	margin-top: 13px;
`;

const RemainLabel = styled.span`
	color: #aaabad;
	line-height: 1.2;
`;

const RemainValue = styled.span`
	color: #2bbc8f;
	line-height: 1.2;
`;

export default InputOrderSell;
