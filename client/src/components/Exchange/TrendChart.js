import React from 'react';
import styled from 'styled-components';
import moment from 'moment';

import {
	TitleContainer,
	Title,
	TitleIcon,
	TitleLabel,
	TitleRight,
	Hr
} from '../Layout';
import IconSvg from '../../svg/main_icon3.svg';
import SttwSvg from '../../svg/icon_sttw.svg';

import { LineChart } from '../Chart';

class TrendChart extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			firstValue: props.firstValue,
			labels: [],
			values: [],
			tickLabels: []
		};
	}

	componentDidMount() {
		this.refresh(this.props);
	}

	componentWillReceiveProps(nextProps) {
		this.refresh(nextProps);
	}

	//值域: 00:00~19:50
	//顯示: 00:10~20:00, 00:00為前一日收盤價
	refresh(props) {
		if (!props.data) {
			return;
		}
		let labels = [];
		let values = [];
		let tickLabels = [];
		let temp = {};
		for (let i in props.data) {
			const d = props.data[i];
			temp[d.time] = d.price;
		}
		let time = moment('00:00', 'HH:mm');
		let lastValue = props.firstValue;
		values.push(lastValue);
		labels.push(time.format('HH:mm'));
		tickLabels.push(time.format('HH:mm'));

		let timeForLabel = moment('00:00', 'HH:mm');
		for (let i = 1; i <= 120; i++) {
			const t = time.format('HH:mm');

			timeForLabel.add(10, 'minutes');
			const tLabel = timeForLabel.format('HH:mm');

			labels.push(tLabel);
			if (i % 12 === 0) {
				tickLabels.push(tLabel);
			}
			if (t in temp) {
				lastValue = temp[t];
			}
			if (moment().isAfter(time)) {
				values.push(lastValue);
			}
			time.add(10, 'minutes');
		}

		this.setState({ labels, values, tickLabels });
	}

	render() {
		return (
			<MyDiv>
				<TitleContainer>
					<Title>
						<TitleIcon src={IconSvg} />
						<TitleLabel>價格走勢</TitleLabel>
					</Title>
					{this.props.price ? (
						<TitleRight>
							<SttwIcon src={SttwSvg} />
							<Sttw>{this.props.price}</Sttw>
						</TitleRight>
					) : null}
				</TitleContainer>
				<Hr style={{ marginBottom: '15px' }} />
				<div
					style={{
						backgroundColor: '#1e1f23'
					}}
				>
					<LineChart
						height={this.props.height}
						width={this.props.width}
						labels={this.state.labels}
						values={this.state.values}
						tickLabels={this.state.tickLabels}
					/>
				</div>
			</MyDiv>
		);
	}
}

const MyDiv = styled.div`
	position: relative;
	height: ${props => props.height};
	width: ${props => props.width};
`;

const SttwIcon = styled.img`
	width: 45px;
	margin-top: -20px;
	margin-right: 10px;
`;

const Sttw = styled.span`
	font-size: 50px;
	color: #2bbc8f;
	font-family: Montserrat;
`;

export default TrendChart;
