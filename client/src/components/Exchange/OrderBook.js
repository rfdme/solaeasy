import React from 'react';
import {
	Table,
	TableBody,
	TableHeader,
	TableHeaderColumn,
	TableRow,
	TableRowColumn
} from 'material-ui/Table';
import { red400, green400 } from 'material-ui/styles/colors';
import { TitleContainer, Title, TitleIcon, TitleLabel, Hr } from '../Layout';
import IconSvg from '../../svg/main_icon7.svg';

import { strip } from '../Utils';

const OrderBook = ({ buys, sells, onSelect }) => {
	let sortedBuys = buys.sort((a, b) => b.price - a.price);
	let sortedSells = sells.sort((a, b) => a.price - b.price);

	let rows = [];
	let k = 0;
	for (let i = 4; i >= 0; i--) {
		if (i < sortedSells.length) {
			let x = sortedSells[i];
			rows.push(
				<TableRow
					key={k++}
					style={{ color: green400 }}
					/*onClick={() => onSelect(x.price)}*/
				>
					<TableRowColumn> {x.price} </TableRowColumn>
					<TableRowColumn> {x.amount} </TableRowColumn>
					<TableRowColumn>{strip(x.price * x.amount)}</TableRowColumn>
				</TableRow>
			);
		} else {
			rows.push(
				<TableRow key={k++} style={{ color: green400 }}>
					<TableRowColumn> -- </TableRowColumn>
					<TableRowColumn> -- </TableRowColumn>
					<TableRowColumn> -- </TableRowColumn>
				</TableRow>
			);
		}
	}
	for (let i = 0; i < 5; i++) {
		if (i < sortedBuys.length) {
			let x = sortedBuys[i];
			rows.push(
				<TableRow
					key={k++}
					style={{ color: red400 }}
					/*onClick={() => onSelect(x.price)}*/
				>
					<TableRowColumn> {x.price} </TableRowColumn>
					<TableRowColumn> {x.amount} </TableRowColumn>
					<TableRowColumn>{strip(x.price * x.amount)}</TableRowColumn>
				</TableRow>
			);
		} else {
			rows.push(
				<TableRow key={k++} style={{ color: red400 }}>
					<TableRowColumn> -- </TableRowColumn>
					<TableRowColumn> -- </TableRowColumn>
					<TableRowColumn> -- </TableRowColumn>
				</TableRow>
			);
		}
	}
	const OnRowSelection = row => {
		if (row <= 4) {
			let index = 4 - row;
			if (index < sortedSells.length) {
				onSelect(sortedSells[index].price);
			}
		} else {
			let index = row - 5;
			if (index < sortedBuys.length) {
				onSelect(sortedBuys[index].price);
			}
		}
	};

	return (
		<div>
			<TitleContainer>
				<Title>
					<TitleIcon src={IconSvg} />
					<TitleLabel>五檔報價</TitleLabel>
				</Title>
			</TitleContainer>
			<Hr />
			<Table
				onRowSelection={OnRowSelection}
				fixedHeader={true}
				height={'510px'}
			>
				<TableHeader
					displaySelectAll={false}
					adjustForCheckbox={false}
					enableSelectAll={false}
				>
					<TableRow>
						<TableHeaderColumn>價格</TableHeaderColumn>
						<TableHeaderColumn>數量</TableHeaderColumn>
						<TableHeaderColumn>金額</TableHeaderColumn>
					</TableRow>
				</TableHeader>
				<TableBody displayRowCheckbox={false} showRowHover={true}>
					{rows}
				</TableBody>
			</Table>
		</div>
	);
};

export default OrderBook;
