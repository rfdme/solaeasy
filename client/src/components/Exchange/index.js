import React, { Component } from 'react';
import { sprintf } from 'sprintf-js';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

import MarketSummary from './MarketSummary';
import MyOrderList from './MyOrderList';
import MyTradeList from './MyTradeList';
import OrderBook from './OrderBook';
import { PvList } from '../SideBar';
import TradeHistory from './TradeHistory';
import InputOrderBuy from './InputOrderBuy';
import InputOrderSell from './InputOrderSell';
import TrendChart from './TrendChart';
import Footer from '../Footer';
import {
  Page,
  HalfPaper,
  SideBar,
  Content,
  Column,
  ColumnGap
} from '../Layout';

import url from '../../config';
const io = require('socket.io-client');

class Exchange extends Component {
  constructor(props) {
    super(props);

    this.init = this.init.bind(this);
    this.handleSelectedPV = this.handleSelectedPV.bind(this);
    this.handleOrderBuy = this.handleOrderBuy.bind(this);
    this.handleOrderSell = this.handleOrderSell.bind(this);
    this.handleOrderSelect = this.handleOrderSelect.bind(this);
    this.handleCloseDialog = this.handleCloseDialog.bind(this);
    this.handleCancelOrder = this.handleCancelOrder.bind(this);

    let token = props.auth.token ? props.auth.token.replace('Bearer ', '') : '';
    this.state = {
      socket: io(url.websocketURL, {
        autoConnect: false,
        query: {
          token: token
        }
      }),
      userId: props.auth.userId,
      pvSelectedId: '',
      lastPrice: null,
      marketSummary: [],
      myTrades: [],
      myOrders: [],
      orderBookBuy: [],
      orderBookSell: [],
      tradeHistory: [],
      lastDayPrice: null,
      trendData: null,
      remainedSttw: 0,
      remainedSntw: 0,
      //defaultPriceBuy: 0,
      //defaultAmountBuy: 0,
      //defaultPriceSell: 0,
      //defaultAmountSell: 0,
      isDialogOpen: false,
      dialogData: {
        title: '',
        message: ''
      }
    };
  }

  componentDidMount() {
    this.state.socket.on('error', function(data) {
      console.log('socket on error...', data);
    });

    this.state.socket.on('connect', () => {
      console.log('connect');
    });

    this.state.socket.on('disconnect', () => console.log('disconnect'));

    //應優化成部分更新
    this.state.socket.on('updateMarketSummary', data => {
      this.setState({ marketSummary: data });
    });

    //應優化成部分更新
    this.state.socket.on('updateTradeHistory', data => {
      this.sortedByDatetime(data);
      if (data.length > 0) {
        this.setState({
          tradeHistory: data,
          lastPrice: data[0].price
        });
      } else {
        this.setState({ tradeHistory: data });
      }
    });

    //應優化成部分更新
    this.state.socket.on('updateTrend', result => {
      if (result.pvId === this.state.pvSelectedId) {
        this.setState({ trendData: result.data });
      }
    });

    this.state.socket.on('syncOrderBookBuy', result => {
      if (result.pvId === this.state.pvSelectedId) {
        this.setState({ orderBookBuy: result.data });
      }
    });

    this.state.socket.on('syncOrderBookSell', result => {
      if (result.pvId === this.state.pvSelectedId) {
        this.setState({ orderBookSell: result.data });
      }
    });

    if (this.state.userId) {
      this.state.socket.on('syncTrades', data => {
        if (this.checkNeedToUpdateMyOrderAndTrade(data)) {
          this.initMyOrders();
          this.initMyTrades();
          this.initMySttw();
          this.initMySntw(this.state.pvSelectedId);
        }
      });
    }

    this.state.socket.on('reset', () => {
      this.init();
    });

    this.state.socket.connect();
    this.init();
  }

  componentWillUnmount() {
    this.state.socket.disconnect();
  }

  checkNeedToUpdateMyOrderAndTrade(data) {
    for (let i = 0; i < this.state.myOrders.length; i++) {
      let order = this.state.myOrders[i];
      if (order.side === '買') {
        if (data.buys.indexOf(order.id) !== -1) {
          return true;
        }
      } else {
        if (data.sells.indexOf(order.id) !== -1) {
          return true;
        }
      }
    }

    return false;
  }

  init() {
    this.initMarketSummary();
    if (this.state.userId) {
      this.initMyTrades();
      this.initMyOrders();
      this.initMySttw();
    }
  }

  initMarketSummary() {
    this.state.socket.emit('initMarketSummary', data => {
      this.setState({ marketSummary: data });
    });
  }

  sortedByDatetime(data) {
    data.sort((a, b) => {
      if (a.datetime > b.datetime) {
        return -1;
      } else if (a.datetime < b.datetime) {
        return 1;
      } else {
        return 0;
      }
    });
  }

  initMyTrades() {
    this.state.socket.emit('initMyTrades', this.state.userId, data => {
      this.sortedByDatetime(data);
      this.setState({ myTrades: data });
    });
  }

  initMyOrders() {
    this.state.socket.emit('initMyOrders', this.state.userId, data => {
      this.sortedByDatetime(data);
      this.setState({ myOrders: data });
    });
  }

  initMySttw() {
    this.state.socket.emit('initMySttw', this.state.userId, data => {
      this.setState({
        remainedSttw: data ? data.sttw : 0
      });
    });
  }

  initMySntw(id) {
    this.state.socket.emit('initMySntw', this.state.userId, id, data => {
      this.setState({
        remainedSntw: data ? data.sntw : 0
      });
    });
  }

  initTradeHistory(id) {
    this.state.socket.emit('initTradeHistory', id, data => {
      this.sortedByDatetime(data);
      if (data.length > 0) {
        console.log('tradeHistory', data);
        this.setState({
          tradeHistory: data,
          lastPrice: data[0].price
        });
      } else {
        this.setState({ tradeHistory: data });
      }
    });
  }

  initOrderBookBuy(id) {
    this.state.socket.emit('initOrderBookBuy', id, data => {
      this.setState({ orderBookBuy: data });
    });
  }

  initOrderBookSell(id) {
    this.state.socket.emit('initOrderBookSell', id, data => {
      this.setState({ orderBookSell: data });
    });
  }

  initLastDayPrice(id) {
    this.state.socket.emit('getLastDayPrice', id, data => {
      if (data) {
        this.setState({ lastDayPrice: data.close });
      }
    });
  }

  initTrendData(id) {
    this.state.socket.emit('getTrendData', id, data => {
      this.setState({ trendData: data });
    });
  }

  handleSelectedPV(id) {
    this.setState({ pvSelectedId: id, lastPrice: null });
    this.initTradeHistory(id);
    this.initOrderBookBuy(id);
    this.initOrderBookSell(id);
    this.initMySntw(id);
    this.initLastDayPrice(id);
    this.initTrendData(id);
  }

  isValidPriceAndAmount(price, amount) {
    if (price.lte(0)) {
      this.openDialog('價格須大於0');
      return false;
    }

    if (amount.lte(0)) {
      this.openDialog('數量須大於0');
      return false;
    }

    return true;
  }

  handleOrderBuy(pvId, price, amount) {
    if (!this.state.userId) {
      this.openDialog('請先登入');
      return;
    }

    if (!this.isValidPriceAndAmount(price, amount)) {
      return;
    }

    if (price.times(amount).gt(this.state.remainedSttw)) {
      this.openDialog('餘額不足');
      return;
    }

    this.state.socket.emit(
      'orderBuy',
      {
        pvId: pvId,
        userId: this.state.userId,
        price: price,
        amount: amount
      },
      result => {
        if (result === '委託成功') {
          this.openDialog(
            sprintf(
              '委託成功:<br/>以價格 %s 元，買進 %s 個%s',
              price,
              amount,
              pvId
            )
          );
          this.initMyOrders();
          this.initMySttw();
        } else {
          this.openDialog(result);
        }
      }
    );
  }

  handleOrderSell(pvId, price, amount) {
    if (!this.state.userId) {
      this.openDialog('請先登入');
      return;
    }

    if (!this.isValidPriceAndAmount(price, amount)) {
      return;
    }

    if (amount.gt(this.state.remainedSntw)) {
      this.openDialog(pvId + '餘額不足');
      return;
    }

    this.state.socket.emit(
      'orderSell',
      {
        pvId: pvId,
        userId: this.state.userId,
        price: price,
        amount: amount
      },
      result => {
        if (result === '委託成功') {
          this.openDialog(
            sprintf(
              '委託成功:<br/>以價格 %s 元，賣出 %s 個%s',
              price,
              amount,
              pvId
            )
          );
          this.initMyOrders();
          this.initMySntw(pvId);
        } else {
          this.openDialog(result);
        }
      }
    );
  }

  handleOrderSelect(price) {
    //const { remainedSttw, remainedSntw } = this.state;
    //let canBuyAmount = Math.trunc(remainedSttw / price);
    //this.setState({
    //defaultPriceBuy: price,
    //defaultAmountBuy: canBuyAmount,
    //defaultPriceSell: price
    //defaultAmountSell: remainedSntw
    //});
  }

  handleCloseDialog() {
    this.setState({ isDialogOpen: false });
  }

  openDialog(msg) {
    this.setState({
      isDialogOpen: true,
      dialogData: {
        title: '提示訊息',
        message: msg
      }
    });
  }

  handleCancelOrder(side, orderId, pvId, price, amount) {
    if (side === '買') {
      this.state.socket.emit(
        'cancelOrderBuy',
        { orderId: orderId, userId: this.state.userId },
        result => {
          if (result === '取消委託成功') {
            this.openDialog(
              sprintf(
                '已取消委託:<br/>以價格 %s 元，買進 %s 個%s',
                price,
                amount,
                pvId
              )
            );
            this.initMyOrders();
            this.initMySttw();
          } else {
            this.openDialog(result);
          }
        }
      );
    } else if (side === '賣') {
      this.state.socket.emit(
        'cancelOrderSell',
        { orderId: orderId, userId: this.state.userId },
        result => {
          if (result === '取消委託成功') {
            this.openDialog(
              sprintf(
                '已取消委託:<br/>以價格 %s 元，賣出 %s 個%s',
                price,
                amount,
                pvId
              )
            );
            this.initMyOrders();
            this.initMySntw(pvId);
          } else {
            this.openDialog(result);
          }
        }
      );
    }
  }

  render() {
    const {
      userId,
      pvSelectedId,
      marketSummary,
      myTrades,
      myOrders,
      orderBookBuy,
      orderBookSell,
      tradeHistory,
      lastDayPrice,
      lastPrice,
      trendData,
      remainedSttw,
      remainedSntw,
      //defaultPriceBuy,
      //defaultAmountBuy,
      //defaultPriceSell,
      //defaultAmountSell,
      isDialogOpen,
      dialogData
    } = this.state;

    const msgActions = [
      <FlatButton
        label="確認"
        primary={true}
        keyboardFocused={true}
        onClick={this.handleCloseDialog}
      />
    ];

    return (
      <Page>
        <div className="page-wrap container">
          <Dialog
            title={dialogData.title}
            actions={msgActions}
            modal={false}
            open={isDialogOpen}
            onRequestClose={this.handleCloseDialog}
            autoScrollBodyContent={true}
          >
            <div
              dangerouslySetInnerHTML={{
                __html: dialogData.message
              }}
            />
          </Dialog>
          <Column>
            <SideBar>
              <PvList auth={this.props.auth} onSelect={this.handleSelectedPV} />
            </SideBar>
            <Content>
              <HalfPaper style={{ paddingBottom: '28px' }}>
                {lastDayPrice ? (
                  <TrendChart
                    height={'260px'}
                    firstValue={lastDayPrice}
                    data={trendData}
                    price={lastPrice}
                  />
                ) : null}
                <Column>
                  <div style={{ flex: 1 }}>
                    <OrderBook
                      buys={orderBookBuy}
                      sells={orderBookSell}
                      onSelect={this.handleOrderSelect}
                    />
                  </div>
                  <ColumnGap />
                  <div
                    style={{
                      flex: 2
                    }}
                  >
                    <Column>
                      <div
                        style={{
                          height: '350px',
                          flex: 1
                        }}
                      >
                        <MarketSummary data={marketSummary} />
                      </div>
                      <ColumnGap />
                      <div
                        style={{
                          height: '350px',
                          flex: 1
                        }}
                      >
                        <TradeHistory data={tradeHistory} />
                      </div>
                    </Column>
                    <Column>
                      <div
                        style={{
                          flex: 1
                        }}
                      >
                        <InputOrderSell
                          pvId={pvSelectedId}
                          remained={remainedSntw}
                          callback={this.handleOrderSell}
                          //defaultPrice={defaultPriceSell}
                        />
                      </div>
                      <ColumnGap />
                      <div
                        style={{
                          flex: 1
                        }}
                      >
                        <InputOrderBuy
                          pvId={pvSelectedId}
                          remained={remainedSttw}
                          callback={this.handleOrderBuy}
                          //defaultPrice={defaultPriceBuy}
                        />
                      </div>
                    </Column>
                  </div>
                </Column>
                {userId > 0 ? (
                  <MyOrderList
                    data={myOrders}
                    onCancel={this.handleCancelOrder}
                  />
                ) : null}
                {userId > 0 ? <MyTradeList data={myTrades} /> : null}
              </HalfPaper>
            </Content>
          </Column>
        </div>
        <Footer />
      </Page>
    );
  }
}

export default Exchange;
