import React, { Component } from 'react';
import { sprintf } from 'sprintf-js';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

import MarketSummary from './MarketSummary';
import MyTradeList from './MyTradeList';
import OrderBook from './OrderBook';
import { PvList } from '../SideBar';
import TradeHistory from './TradeHistory';
import InputOrderBuy from './InputOrderBuy';
import InputOrderSell from './InputOrderSell';
import TrendChart from './TrendChart';
import Footer from '../Footer';
import {
	Page,
	HalfPaper,
	SideBar,
	Content,
	Column,
	ColumnGap
} from '../Layout';

import url from '../../config';
const io = require('socket.io-client');

class MockExchange extends Component {
	constructor(props) {
		super(props);

		this.init = this.init.bind(this);
		this.initMock = this.initMock.bind(this);
		this.handleSelectedPV = this.handleSelectedPV.bind(this);
		this.handleMockBuy = this.handleMockBuy.bind(this);
		this.handleMockSell = this.handleMockSell.bind(this);
		this.handleOrderSelect = this.handleOrderSelect.bind(this);
		this.handleCloseDialog = this.handleCloseDialog.bind(this);

		let token = props.auth.token
			? props.auth.token.replace('Bearer ', '')
			: '';
		this.state = {
			socket: io(url.websocketURL, {
				autoConnect: false,
				query: {
					token: token
				}
			}),
			userId: props.auth.userId,
			pvSelectedId: '',
			marketSummary: [],
			myTrades: [],
			myAssets: [],
			orderBookBuy: [],
			orderBookSell: [],
			tradeHistory: [],
			lastDayPrice: 10,
			trendData: null,
			remainedSttw: 0,
			remainedSntw: 0,
			//defaultPriceBuy: 0,
			//defaultAmountBuy: 0,
			//defaultPriceSell: 0,
			//defaultAmountSell: 0,
			isDialogOpen: false,
			dialogData: {
				title: '',
				message: ''
			}
		};
	}

	componentDidMount() {
		this.state.socket.connect();

		this.state.socket.on('connect', () => {
			console.log('connect');
		});

		this.state.socket.on('disconnect', () => console.log('disconnect'));

		//應優化成部分更新
		this.state.socket.on('updateMarketSummary', data => {
			this.setState({ marketSummary: data });
		});

		//應優化成部分更新
		this.state.socket.on('updateTradeHistory', data => {
			this.sortedByDatetime(data);
			this.setState({ tradeHistory: data });
		});

		//應優化成部分更新
		this.state.socket.on('updateTrend', result => {
			if (result.pvId === this.state.pvSelectedId) {
				this.setState({ trendData: result.data });
			}
		});

		this.state.socket.on('syncOrderBookBuy', result => {
			if (result.pvId === this.state.pvSelectedId) {
				this.setState({ orderBookBuy: result.data });
			}
		});

		this.state.socket.on('syncOrderBookSell', result => {
			if (result.pvId === this.state.pvSelectedId) {
				this.setState({ orderBookSell: result.data });
			}
		});

		this.init();
	}

	componentWillUnmount() {
		this.state.socket.disconnect();
	}

	init() {
		this.initMarketSummary();
		if (this.state.userId) {
			this.initMock();
		}
	}

	initMock() {
		const { sttw, assets, trades } = this.props.mock.getData();
		this.initMyTrades(trades);
		this.initMyAssets(assets);
		this.initMySttw(sttw.toString());
		if (this.state.pvSelectedId) {
			this.initMySntw(this.state.pvSelectedId);
		}
	}

	initMarketSummary() {
		this.state.socket.emit('initMarketSummary', data => {
			this.setState({ marketSummary: data });
		});
	}

	sortedByDatetime(data) {
		data.sort((a, b) => {
			if (a.datetime > b.datetime) {
				return -1;
			} else if (a.datetime < b.datetime) {
				return 1;
			} else {
				return 0;
			}
		});
	}

	initMyTrades(data) {
		this.sortedByDatetime(data);
		this.setState({ myTrades: data });
	}

	initMyAssets(data) {
		this.setState({ myAssets: data });
	}

	initMySttw(sttw) {
		this.setState({
			remainedSttw: sttw
		});
	}

	initMySntw(id) {
		let data = this.state.myAssets[id];
		this.setState({
			remainedSntw: data ? data : 0
		});
	}

	initTradeHistory(id) {
		this.state.socket.emit('initTradeHistory', id, data => {
			this.sortedByDatetime(data);
			this.setState({ tradeHistory: data });
		});
	}

	initOrderBookBuy(id) {
		this.state.socket.emit('initOrderBookBuy', id, data => {
			this.setState({ orderBookBuy: data });
		});
	}

	initOrderBookSell(id) {
		this.state.socket.emit('initOrderBookSell', id, data => {
			this.setState({ orderBookSell: data });
		});
	}

	initLastDayPrice(id) {
		this.state.socket.emit('getLastDayPrice', id, data => {
			if (data) {
				this.setState({ lastDayPrice: data.close });
			} else {
				this.setState({ lastDayPrice: 10 });
			}
		});
	}

	initTrendData(id) {
		this.state.socket.emit('getTrendData', id, data => {
			this.setState({ trendData: data });
		});
	}

	handleSelectedPV(id) {
		this.setState({ pvSelectedId: id });
		this.initTradeHistory(id);
		this.initOrderBookBuy(id);
		this.initOrderBookSell(id);
		this.initMySntw(id);
		this.initLastDayPrice(id);
		this.initTrendData(id);
	}

	isValidPriceAndAmount(price, amount) {
		if (price.lte(0)) {
			this.openDialog('價格須大於0');
			return false;
		}

		if (amount.lte(0)) {
			this.openDialog('數量須大於0');
			return false;
		}

		return true;
	}

	handleMockBuy(pvId, price, amount) {
		if (!this.state.userId) {
			this.openDialog('請先登入');
			return;
		}

		if (!this.isValidPriceAndAmount(price, amount)) {
			return;
		}

		if (price.times(amount).gt(this.state.remainedSttw)) {
			this.openDialog('餘額不足');
			return;
		}

		this.props.mock.buy(pvId, price.toString(), amount.toString());

		this.openDialog(
			sprintf(
				'模擬交易成功:<br/>以價格 %s 元，買進 %s 個%s',
				price,
				amount,
				pvId
			)
		);

		setTimeout(() => {
			this.initMock();
		}, 0);
	}

	handleMockSell(pvId, price, amount) {
		if (!this.state.userId) {
			this.openDialog('請先登入');
			return;
		}

		if (!this.isValidPriceAndAmount(price, amount)) {
			return;
		}

		if (amount.gt(this.state.remainedSntw)) {
			this.openDialog(pvId + '餘額不足');
			return;
		}

		this.props.mock.sell(pvId, price.toString(), amount.toString());

		this.openDialog(
			sprintf(
				'模擬交易成功:<br/>以價格 %s 元，賣出 %s 個%s',
				price,
				amount,
				pvId
			)
		);

		setTimeout(() => {
			this.initMock();
		}, 0);
	}

	handleOrderSelect(price) {
		//const { remainedSttw, remainedSntw } = this.state;
		//let canBuyAmount = Math.trunc(remainedSttw / price);
		//this.setState({
		//defaultPriceBuy: price,
		//defaultAmountBuy: canBuyAmount,
		//defaultPriceSell: price
		//defaultAmountSell: remainedSntw
		//});
	}

	handleCloseDialog() {
		this.setState({ isDialogOpen: false });
	}

	openDialog(msg) {
		this.setState({
			isDialogOpen: true,
			dialogData: {
				title: '提示訊息',
				message: msg
			}
		});
	}

	render() {
		const {
			userId,
			pvSelectedId,
			marketSummary,
			myTrades,
			orderBookBuy,
			orderBookSell,
			tradeHistory,
			lastDayPrice,
			trendData,
			remainedSttw,
			remainedSntw,
			//defaultPriceBuy,
			//defaultAmountBuy,
			//defaultPriceSell,
			//defaultAmountSell,
			isDialogOpen,
			dialogData
		} = this.state;

		const msgActions = [
			<FlatButton
				label="確認"
				primary={true}
				keyboardFocused={true}
				onClick={this.handleCloseDialog}
			/>
		];

		return (
			<Page>
				<div className="page-wrap container">
					<Dialog
						title={dialogData.title}
						actions={msgActions}
						modal={false}
						open={isDialogOpen}
						onRequestClose={this.handleCloseDialog}
						autoScrollBodyContent={true}
					>
						<div
							dangerouslySetInnerHTML={{
								__html: dialogData.message
							}}
						/>
					</Dialog>
					<Column>
						<SideBar>
							<PvList
								auth={this.props.auth}
								onSelect={this.handleSelectedPV}
							/>
						</SideBar>
						<Content>
							<HalfPaper style={{ paddingBottom: '28px' }}>
								<TrendChart
									height={'270px'}
									firstValue={lastDayPrice}
									data={trendData}
								/>
								<Column>
									<div style={{ flex: 1 }}>
										<OrderBook
											height={'300px'}
											buys={orderBookBuy}
											sells={orderBookSell}
											onSelect={this.handleOrderSelect}
										/>
									</div>
									<ColumnGap />
									<div
										style={{
											flex: 2
										}}
									>
										<Column>
											<div
												style={{
													height: '350px',
													flex: 1
												}}
											>
												<MarketSummary
													data={marketSummary}
												/>
											</div>
											<ColumnGap />
											<div
												style={{
													height: '350px',
													flex: 1
												}}
											>
												<TradeHistory
													data={tradeHistory}
												/>
											</div>
										</Column>
										<Column>
											<div
												style={{
													flex: 1
												}}
											>
												<InputOrderSell
													pvId={pvSelectedId}
													remained={remainedSntw}
													callback={
														this.handleMockSell
													}
													//defaultPrice={defaultPriceSell}
												/>
											</div>
											<ColumnGap />
											<div
												style={{
													flex: 1
												}}
											>
												<InputOrderBuy
													pvId={pvSelectedId}
													remained={remainedSttw}
													callback={
														this.handleMockBuy
													}
													//defaultPrice={defaultPriceBuy}
												/>
											</div>
										</Column>
									</div>
								</Column>
								{userId ? (
									<MyTradeList data={myTrades} />
								) : null}
							</HalfPaper>
						</Content>
					</Column>
				</div>
				<Footer />
			</Page>
		);
	}
}

export default MockExchange;
