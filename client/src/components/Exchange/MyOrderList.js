import React, { Component } from 'react';
import {
	Table,
	TableBody,
	TableHeader,
	TableHeaderColumn,
	TableRow,
	TableRowColumn
} from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';
import { red400, green400, black, amber600 } from 'material-ui/styles/colors';
import { TitleContainer, Title, TitleIcon, TitleLabel, Hr } from '../Layout';
import IconSvg from '../../svg/main_icon7.svg';

import TablePaging from '../TablePaging';

import BigNumber from 'bignumber.js';
const BN = BigNumber.clone({ DECIMAL_PLACES: 16, ROUNDING_MODE: 1 });

const rowCountForPage = 5;

class MyOrderList extends Component {
	constructor(props) {
		super(props);

		this.handlePage = this.handlePage.bind(this);

		this.state = {
			paging: 0
		};
	}

	componentWillReceiveProps(nextProps) {
		//避免頁數減少時, 頁數錯誤
		const totalPageCount = Math.ceil(
			nextProps.data.length / rowCountForPage
		);
		if (totalPageCount > 0 && this.state.paging >= totalPageCount) {
			this.setState({ paging: totalPageCount - 1 });
		}
	}

	handlePage(page) {
		this.setState({ paging: page });
	}

	render() {
		const totalPageCount = Math.ceil(
			this.props.data.length / rowCountForPage
		);
		const begin = this.state.paging * rowCountForPage;
		const end = (this.state.paging + 1) * rowCountForPage;
		let rows = [];
		this.props.data.forEach((x, i) => {
			if (begin <= i && i < end) {
				let status;
				switch (x.status) {
					case 0:
						status = (
							<RaisedButton
								label="取消"
								onClick={() => {
									this.props.onCancel(
										x.side,
										x.id,
										x.pvId,
										x.price,
										x.amount
									);
								}}
								backgroundColor={amber600}
								labelColor={black}
							/>
						);
						break;
					case 1:
						status = '已取消';
						break;
					default:
						status = '未知';
						break;
				}
				rows.push(
					<TableRow
						key={i}
						style={
							x.side === '賣'
								? { color: green400 }
								: { color: red400 }
						}
					>
						<TableRowColumn>
							{x.datetime.substring(10)}
						</TableRowColumn>
						<TableRowColumn> {x.pvId} </TableRowColumn>
						<TableRowColumn> {x.side} </TableRowColumn>
						<TableRowColumn> {x.price} </TableRowColumn>
						<TableRowColumn> {x.filled} </TableRowColumn>
						<TableRowColumn> {x.amount} </TableRowColumn>
						<TableRowColumn>
							{BN(x.amount)
								.times(x.price)
								.toFormat()}
							元
						</TableRowColumn>
						<TableRowColumn> {status}</TableRowColumn>
					</TableRow>
				);
			}
		});

		return (
			<div>
				<TitleContainer>
					<Title>
						<TitleIcon src={IconSvg} />
						<TitleLabel>我的委託清單</TitleLabel>
					</Title>
				</TitleContainer>
				<Hr />
				<Table fixedHeader={false} style={{ tableLayout: 'auto' }}>
					<TableHeader
						displaySelectAll={false}
						adjustForCheckbox={false}
						enableSelectAll={false}
					>
						<TableRow>
							<TableHeaderColumn>時間</TableHeaderColumn>
							<TableHeaderColumn>代碼</TableHeaderColumn>
							<TableHeaderColumn>買賣</TableHeaderColumn>
							<TableHeaderColumn>成交價格</TableHeaderColumn>
							<TableHeaderColumn>成交數量</TableHeaderColumn>
							<TableHeaderColumn>委託數量</TableHeaderColumn>
							<TableHeaderColumn>總金額</TableHeaderColumn>
							<TableHeaderColumn>狀態</TableHeaderColumn>
						</TableRow>
					</TableHeader>
					<TableBody displayRowCheckbox={false} showRowHover={true}>
						{rows}
					</TableBody>
				</Table>
				<TablePaging
					page={this.state.paging}
					totalPageCount={totalPageCount}
					showPageCount={5}
					onPage={this.handlePage}
				/>
			</div>
		);
	}
}

export default MyOrderList;
