import React from 'react';
import {
	Table,
	TableBody,
	TableHeader,
	TableHeaderColumn,
	TableRow,
	TableRowColumn
} from 'material-ui/Table';
import { red400, green400 } from 'material-ui/styles/colors';
import { TitleContainer, Title, TitleIcon, TitleLabel, Hr } from '../Layout';
import IconSvg from '../../svg/main_icon7.svg';

const TradeHistory = ({ data }) => (
	<div>
		<TitleContainer>
			<Title>
				<TitleIcon src={IconSvg} />
				<TitleLabel>最新成交</TitleLabel>
			</Title>
		</TitleContainer>
		<Hr />
		<Table
			fixedHeader={true}
			style={{ tableLayout: 'auto' }}
			height={'220px'}
		>
			<TableHeader
				displaySelectAll={false}
				adjustForCheckbox={false}
				enableSelectAll={false}
			>
				<TableRow>
					<TableHeaderColumn>時間</TableHeaderColumn>
					<TableHeaderColumn>價格</TableHeaderColumn>
					<TableHeaderColumn>數量</TableHeaderColumn>
				</TableRow>
			</TableHeader>
			<TableBody displayRowCheckbox={false} showRowHover={true}>
				{data.map((x, i) => (
					<TableRow
						key={i}
						style={
							x.orderTimeBuy > x.orderTimeSell
								? { color: red400 }
								: { color: green400 }
						}
					>
						<TableRowColumn>
							{x.datetime.substring(10)}
						</TableRowColumn>
						<TableRowColumn> {x.price} </TableRowColumn>
						<TableRowColumn> {x.amount} </TableRowColumn>
					</TableRow>
				))}
			</TableBody>
		</Table>
	</div>
);

export default TradeHistory;
