import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import styled from 'styled-components';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import { black, amber600, red500 /*, white*/ } from 'material-ui/styles/colors';

import Footer from '../Footer';

class Login extends Component {
  constructor(props) {
    super(props);

    this.handleUsernameChange = this.handleUsernameChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleSignIn = this.handleSignIn.bind(this);
    this.signInApp = this.signInApp.bind(this);
    this.handleCloseDialog = this.handleCloseDialog.bind(this);

    this.state = {
      username: '',
      password: '',
      isDialogOpen: false,
      dialogData: {
        title: '',
        message: ''
      },
      message: ''
    };
  }

  componentDidMount() {
    if (this.props.auth.token) {
      this.props.route.history.push('/Home');
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.token) {
      nextProps.route.history.push('/Home');
    }
  }

  handleUsernameChange(event) {
    this.setState({ username: event.target.value });
  }

  handlePasswordChange(event) {
    this.setState({ password: event.target.value });
  }

  handleSignIn(event) {
    const email = this.refs.email.input.value;
    const pwd = this.refs.password.input.value;
    this.doSignIn(email, pwd);
  }

  doSignIn(email, password) {
    axios
      .post('/api/v1/auth/local', {
        email: email,
        pwd: password
      })
      .then(res => {
        if (res.data.status === true) {
          if (res.data.result.users.length === 0) {
            this.setState({
              message: '此帳號目前沒有任何權限'
            });
            return;
          }
          if (res.data.result.manager) {
            this.signInApp(res.data.result);
            this.setState({
              message: '帳號密碼正確'
            });
          } else {
            this.setState({ message: res.data.msg });
          }
        } else {
          this.setState({ message: res.data.msg });
        }
      })
      .catch(err => {
        console.error(err);
      });
  }

  signInApp(data) {
    this.props.auth.signIn(data);
    this.props.route.history.goBack();
  }

  handleCloseDialog() {
    this.setState({ isDialogOpen: false });
  }

  openDialog(msg) {
    this.setState({
      isDialogOpen: true,
      dialogData: {
        title: '提示訊息',
        message: msg
      }
    });
  }

  //TODO: 輸入驗證提示
  render() {
    const { isDialogOpen, dialogData } = this.state;
    const msgActions = [
      <FlatButton label="確認" primary={true} keyboardFocused={true} onClick={this.handleCloseDialog} />
    ];
    return (
      <MainBlock>
        <div className="page-wrap">
          <Block>
            <Title>登　入</Title>
            <label>帳號:</label>
            <TextField name="email" type="email" ref="email" fullWidth onChange={this.handleUsernameChange} />
            <br />
            <label>密碼:</label>
            <TextField
              name="password"
              type="password"
              ref="password"
              fullWidth
              onChange={this.handlePasswordChange}
            />
            <label style={{ color: red500 }}>{this.state.message}</label>
            <RaisedButton
              fullWidth
              backgroundColor={amber600}
              labelColor={black}
              onClick={this.handleSignIn}
              style={{ marginTop: '1em' }}
            >
              <span style={{ fontSize: '1.5em' }}>登　入</span>
            </RaisedButton>
            <Link to="/Home/ResetPwd">
              <MyLink>
                <Label>忘記密碼?</Label>
              </MyLink>
            </Link>
          </Block>
          <Dialog
            title={dialogData.title}
            actions={msgActions}
            modal={false}
            open={isDialogOpen}
            onRequestClose={this.handleCloseDialog}
            autoScrollBodyContent={true}
          >
            <div>{dialogData.message}</div>
          </Dialog>
        </div>
        <Footer />
      </MainBlock>
    );
  }
}

const MainBlock = styled.div`
  flex: 1;
  overflow-y: auto;
`;

const Title = styled.div`
  width: 100%;
  font-size: 2em;
  color: ${amber600};
  text-align: center;
  margin-bottom: 1em;
`;

const Block = styled.div`
  width: 400px;
  position: relative;
  left: 50%;
  transform: translateX(-50%);
  margin: 4em 0;
  padding: 2em;
  border-radius: 0.28571429em;
  border: 1px solid rgba(34, 36, 38, 0.15);
  box-shadow: 0 2px 4px 0 rgba(34, 36, 38, 0.12), 0 2px 10px 0 rgba(34, 36, 38, 0.15);
  background-color: rgba(255, 255, 255, 0.05);
`;

const MyLink = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 10px;
  color: #aaabad;
  &:hover {
    background-color: #151515;
  }

  background-color: ${props => (props.selected ? '#151515' : null)};
`;

const Label = styled.span`
  line-height: 1.2;

  ${MyLink}:hover & {
    color: #f7b100;
  }
`;

export default Login;
