import React, { Component } from 'react';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from 'material-ui/Table';
//import RaisedButton from 'material-ui/RaisedButton';
//import { amber600, black } from 'material-ui/styles/colors';

import BlockDialog from './BlockDialog';
import TablePaging from '../TablePaging';

import BigNumber from 'bignumber.js';
const BN = BigNumber.clone({ DECIMAL_PLACES: 16, ROUNDING_MODE: 1 });

class CashinHistory extends Component {
  constructor(props) {
    super(props);

    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handlePage = this.handlePage.bind(this);

    this.state = {
      open: false,
      data: null,
      paging: 0
    };
  }

  handleOpen(data) {
    this.setState({ open: true, data: data });
  }

  handleClose() {
    this.setState({ open: false });
  }

  handlePage(page) {
    this.setState({ paging: page });
  }

  render() {
    const rowCountForPage = this.props.rowCount;
    const totalPageCount = Math.ceil(this.props.data.length / rowCountForPage);
    const begin = this.state.paging * rowCountForPage;
    const end = (this.state.paging + 1) * rowCountForPage;
    let rows = [];
    this.props.data.forEach((x, i) => {
      if (begin <= i && i < end) {
        /*
        let blockStatus;
        if (x.blockInfo) {
          blockStatus = (
            <RaisedButton
              label="區塊紀錄"
              onClick={() => {
                this.handleOpen(x);
              }}
              backgroundColor={amber600}
              labelColor={black}
            />
          );
        } else {
          blockStatus = '區塊同步中';
        }
        */
        rows.push(
          <TableRow key={i}>
            <TableRowColumn> {x.date} </TableRowColumn>
            <TableRowColumn>{BN(x.amount).toFormat()} 元</TableRowColumn>
            {/*<TableRowColumn>{blockStatus}</TableRowColumn>*/}
          </TableRow>
        );
      }
    });
    return (
      <div className="container">
        <TablePaging
          page={this.state.paging}
          totalPageCount={totalPageCount}
          showPageCount={5}
          onPage={this.handlePage}
        />
        <Table fixedHeader={false} style={{ tableLayout: 'auto' }}>
          <TableHeader displaySelectAll={false} adjustForCheckbox={false} enableSelectAll={false}>
            <TableRow>
              <TableHeaderColumn>日期</TableHeaderColumn>
              <TableHeaderColumn>金額</TableHeaderColumn>
              {/*<TableHeaderColumn>區塊鏈資料</TableHeaderColumn>*/}
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false} showRowHover={true}>
            {rows}
          </TableBody>
        </Table>
        <TablePaging
          page={this.state.paging}
          totalPageCount={totalPageCount}
          showPageCount={5}
          onPage={this.handlePage}
        />
        <BlockDialog open={this.state.open} data={this.state.data} onClose={this.handleClose} />
      </div>
    );
  }
}

export default CashinHistory;
