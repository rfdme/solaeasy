import React, { Component } from 'react';
import axios from 'axios';
import styled from 'styled-components';
import { DetailMenu } from '../SideBar';
import {
  Page,
  SideBar,
  HalfPaper,
  Column,
  Content,
  TitleContainer,
  Title,
  TitleIcon,
  TitleLabel,
  TitleRight,
  Hr
} from '../Layout';
import IconSvg from '../../svg/main_icon7.svg';

import ProfitHistory from './ProfitHistory';
import OrderHistory from './OrderHistory';
import TradeHistory from './TradeHistory';
import CashinHistory from './CashinHistory';
import CashoutHistory from './CashoutHistory';
import RepayHistory from './RepayHistory';

import Footer from '../Footer';

class Detail extends Component {
  constructor(props) {
    super(props);

    this.handleMenuSelect = this.handleMenuSelect.bind(this);

    this.state = {
      selectedIndex: 0,
      orderList: [],
      tradeList: [],
      profitList: [],
      cashinList: [],
      cashoutList: [],
      repayList: [],
      tableRowCount: 10
    };
  }

  componentDidMount() {
    const { token } = this.props.auth;

    if (!token) {
      this.props.route.history.push('/Home');
    } else {
      if (this.props.mock.enable) {
      } else {
        this.handleMenuSelect(0);
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.auth.token) {
      nextProps.route.history.push('/Home');
    }
  }

  loadProfit() {
    if (this.props.mock.enable) {
      this.setState({ profitList: [], selectedIndex: 0 });
    } else {
      return axios
        .get(`/api/v1/detail/profit/` + this.props.auth.userId, {
          headers: {
            Authorization: this.props.auth.token
          }
        })
        .then(res => {
          if (res.data.status === true) {
            this.setState({
              profitList: res.data.result.profitList,
              selectedIndex: 0
            });
          } else {
            // TODO: handle error_code, msg
          }
        })
        .catch(error => {
          console.error(error);
        });
    }
  }

  loadOrder() {
    if (this.props.mock.enable) {
      this.setState({ orderList: [], selectedIndex: 1 });
    } else {
      return axios
        .get(`/api/v1/detail/order/` + this.props.auth.userId, {
          headers: {
            Authorization: this.props.auth.token
          }
        })
        .then(res => {
          if (res.data.status === true) {
            this.setState({
              orderList: res.data.result.orderList,
              selectedIndex: 1
            });
          } else {
            // TODO: handle error_code, msg
          }
        })
        .catch(error => {
          console.error(error);
        });
    }
  }

  loadTrade() {
    if (this.props.mock.enable) {
      this.setState({ tradeList: [], selectedIndex: 2 });
    } else {
      return axios
        .get(`/api/v1/detail/trade/` + this.props.auth.userId, {
          headers: {
            Authorization: this.props.auth.token
          }
        })
        .then(res => {
          if (res.data.status === true) {
            this.setState({
              tradeList: res.data.result.tradeList,
              selectedIndex: 2
            });
          } else {
            // TODO: handle error_code, msg
          }
        })
        .catch(error => {
          console.error(error);
        });
    }
  }

  loadCashin() {
    if (this.props.mock.enable) {
      this.setState({ cashinList: [], selectedIndex: 3 });
    } else {
      return axios
        .get(`/api/v1/detail/cashin/` + this.props.auth.userId, {
          headers: {
            Authorization: this.props.auth.token
          }
        })
        .then(res => {
          if (res.data.status === true) {
            this.setState({
              cashinList: res.data.result.cashinList,
              selectedIndex: 3
            });
          } else {
            // TODO: handle error_code, msg
          }
        })
        .catch(error => {
          console.error(error);
        });
    }
  }

  loadCashout() {
    if (this.props.mock.enable) {
      this.setState({ cashoutList: [], selectedIndex: 4 });
    } else {
      return axios
        .get(`/api/v1/detail/cashout/` + this.props.auth.userId, {
          headers: {
            Authorization: this.props.auth.token
          }
        })
        .then(res => {
          if (res.data.status === true) {
            this.setState({
              cashoutList: res.data.result.cashoutList,
              selectedIndex: 4
            });
          } else {
            // TODO: handle error_code, msg
          }
        })
        .catch(error => {
          console.error(error);
        });
    }
  }

  loadRepay() {
    if (this.props.mock.enable) {
      this.setState({ repayList: [], selectedIndex: 5 });
    } else {
      return axios
        .get(`/api/v1/detail/repay/` + this.props.auth.userId, {
          headers: {
            Authorization: this.props.auth.token
          }
        })
        .then(res => {
          if (res.data.status === true) {
            this.setState({
              repayList: res.data.result.repayList,
              selectedIndex: 5
            });
          } else {
            // TODO: handle error_code, msg
          }
        })
        .catch(error => {
          console.error(error);
        });
    }
  }

  handleMenuSelect(index) {
    switch (index) {
      case 0:
        this.loadProfit();
        break;
      case 1:
        this.loadOrder();
        break;
      case 2:
        this.loadTrade();
        break;
      case 3:
        this.loadCashin();
        break;
      case 4:
        this.loadCashout();
        break;
      case 5:
        this.loadRepay();
        break;
      default:
        break;
    }
  }

  selectTable(index) {
    switch (index) {
      case 0:
        return <ProfitHistory data={this.state.profitList} rowCount={this.state.tableRowCount} />;
      case 1:
        return <OrderHistory data={this.state.orderList} rowCount={this.state.tableRowCount} />;
      case 2:
        return <TradeHistory data={this.state.tradeList} rowCount={this.state.tableRowCount} />;
      case 3:
        return <CashinHistory data={this.state.cashinList} rowCount={this.state.tableRowCount} />;
      case 4:
        return <CashoutHistory data={this.state.cashoutList} rowCount={this.state.tableRowCount} />;
      case 5:
        return <RepayHistory data={this.state.repayList} rowCount={this.state.tableRowCount} />;
      default:
        return null;
    }
  }

  selectRowCount(c) {
    this.setState({ tableRowCount: c });
  }

  render() {
    const menu = [
      '收益記錄',
      '交易記錄',
      '成交記錄',
      '存款記錄',
      '提款記錄'
      /*'還本記錄'*/
    ];

    let selectButtons = [];
    const tableRowCounts = [10, 15, 20, 25];
    for (let i = 0; i < tableRowCounts.length; i++) {
      const count = tableRowCounts[i];
      selectButtons.push(
        <SelectButton
          key={i}
          selected={this.state.tableRowCount === count}
          onClick={() => this.selectRowCount(count)}
        >
          {count}
        </SelectButton>
      );
    }

    return (
      <Page>
        <div className="page-wrap container">
          <Column>
            <MySideBar>
              <DetailMenu
                menu={menu}
                defaultIndex={this.state.selectedIndex}
                onSelect={this.handleMenuSelect}
              />
            </MySideBar>
            <Content>
              <HalfPaper
                style={{
                  paddingBottom: '28px',
                  minHeight: '462px'
                }}
              >
                <TitleContainer>
                  <Title>
                    <TitleIcon src={IconSvg} />
                    <TitleLabel>{menu[this.state.selectedIndex]}</TitleLabel>
                  </Title>
                  <TitleRight>{selectButtons}</TitleRight>
                </TitleContainer>
                <Hr />
                <div style={{ marginTop: '15px' }}>{this.selectTable(this.state.selectedIndex)}</div>
              </HalfPaper>
            </Content>
          </Column>
        </div>
        <Footer />
      </Page>
    );
  }
}

const MySideBar = styled(SideBar)`
  width: 200px;
  @media screen and (max-width: 1024px) {
    width: 150px;
  }
`;

const SelectButton = styled.div`
  color: ${props => (props.selected ? '#f7b100' : '#5a5a5a')};
  border: 1px solid ${props => (props.selected ? '#f7b100' : '#5a5a5a')};
  border-radius: 30px;
  padding: 1px 1em 1px 1em;
  margin-right: 0.5em;
  font-size: 14px;
  cursor: pointer;
`;

export default Detail;
