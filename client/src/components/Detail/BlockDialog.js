import React, { Component } from 'react';
import { Table, TableBody, TableRow, TableRowColumn } from 'material-ui/Table';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

class BlockDialog extends Component {
	constructor(props) {
		super(props);

		this.handleClose = this.handleClose.bind(this);

		this.state = {
			open: props.open,
			data: props.data
		};
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			open: nextProps.open,
			data: nextProps.data
		});
	}

	handleClose() {
		this.props.onClose();
		this.setState({ open: false });
	}

	render() {
		const actions = [
			<FlatButton
				label="確認"
				primary={true}
				keyboardFocused={true}
				onClick={this.handleClose}
			/>
		];
		return (
			<Dialog
				title="區塊鏈資料"
				actions={actions}
				modal={false}
				open={this.state.open}
				onRequestClose={this.handleClose}
				autoScrollBodyContent={true}
			>
				<Table fixedHeader={false} style={{ tableLayout: 'auto' }}>
					{this.props.data ? (
						<TableBody
							displayRowCheckbox={false}
							showRowHover={true}
						>
							<TableRow>
								<TableRowColumn>Transation Hash</TableRowColumn>
								<TableRowColumn>
									{this.props.data.blockInfo.transactionHash}
								</TableRowColumn>
							</TableRow>
							<TableRow>
								<TableRowColumn>Block Hash</TableRowColumn>
								<TableRowColumn>
									{this.props.data.blockInfo.blockHash}
								</TableRowColumn>
							</TableRow>
							<TableRow>
								<TableRowColumn>Block Number</TableRowColumn>
								<TableRowColumn>
									{this.props.data.blockInfo.blockNumber}
								</TableRowColumn>
							</TableRow>
							<TableRow>
								<TableRowColumn>Status</TableRowColumn>
								<TableRowColumn>
									{this.props.data.blockInfo.status}
								</TableRowColumn>
							</TableRow>
							{this.props.data.oldAmount ? (
								<TableRow>
									<TableRowColumn>
										異動前{this.props.data.pvId}
									</TableRowColumn>
									<TableRowColumn>
										{this.props.data.oldAmount}
									</TableRowColumn>
								</TableRow>
							) : null}
							{this.props.data.newAmount ? (
								<TableRow>
									<TableRowColumn>
										異動後{this.props.data.pvId}
									</TableRowColumn>
									<TableRowColumn>
										{this.props.data.newAmount}
									</TableRowColumn>
								</TableRow>
							) : null}
							{this.props.data.oldSttw ? (
								<TableRow>
									<TableRowColumn>異動前餘額</TableRowColumn>
									<TableRowColumn>
										{this.props.data.oldSttw}
									</TableRowColumn>
								</TableRow>
							) : null}
							{this.props.data.newSttw ? (
								<TableRow>
									<TableRowColumn>異動後餘額</TableRowColumn>
									<TableRowColumn>
										{this.props.data.newSttw}
									</TableRowColumn>
								</TableRow>
							) : null}
						</TableBody>
					) : null}
				</Table>
			</Dialog>
		);
	}
}

export default BlockDialog;
