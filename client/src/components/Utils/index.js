const pvStatusCodes = status => {
	switch (status) {
		case 0:
			return '發電中';
		case 1:
			return '建置中';
		default:
			return '未知';
	}
};

const cashoutStatusCodes = status => {
	switch (status) {
		case 0:
			return '處理中';
		case 1:
			return '已完成';
		default:
			return '未知';
	}
};

const strip = (num, precision = 12) => {
	return +parseFloat(num.toPrecision(precision));
};

module.exports = {
	pvStatusCodes,
	cashoutStatusCodes,
	strip
};
