import styled from 'styled-components';

const Page = styled.div`
	flex: 1;
	margin-top: 50px;
`;

const Paper = styled.div`
	flex: 1;
	padding: 0px 28px;
	margin-bottom: 1em;
	border-radius: 8px;
	border: 1px solid rgba(34, 36, 38, 0.15);
	box-shadow: 0 2px 4px 0 rgba(34, 36, 38, 0.12),
		0 2px 10px 0 rgba(34, 36, 38, 0.15);
	background-color: #26262b;
	@media screen and (max-width: 1024px) {
		padding: 0px 10px;
	}
`;

const HalfPaper = styled(Paper)`
	border-radius: 0 8px 8px 0;
`;

const Column = styled.div`
	display: flex;
`;

const SideBar = styled.div`
	width: 250px;
	margin-bottom: 1em;
	@media screen and (max-width: 1024px) {
		width: 200px;
	}
`;

const SideBarPaper = styled.div`
	background: #16161a;
	height: 100%;
`;

const Content = styled.div`
	flex: 1;
`;

const ColumnGap = styled.div`
	width: 1em;
`;

const TitleContainer = styled.div`
	height: 60px;
	display: flex;
	justify-content: space-between;
`;

const Title = styled.div`
	display: flex;
	justify-content: flex-start;
	align-items: center;
`;

const TitleIcon = styled.img`
	width: 28px;
	height: 28px;
	margin-right: 5px;
`;

const TitleLabel = styled.span`
	font-size: 17px;
	color: #f7b100;
`;

const TitleRight = styled.div`
	display: flex;
	justify-content: flex-end;
	align-items: center;
`;

const Hr = styled.hr`
	height: 1px;
	margin: 0px;
	border: none;
	border-top: 1px solid #37373e;
`;

const SideBarHr = styled(Hr)`
	border-color: #232429;
`;

const LittleTitle = styled.div`
	color: #696a6d;
	font-size: 16px;
`;

export {
	Page,
	Paper,
	HalfPaper,
	Column,
	ColumnGap,
	SideBar,
	SideBarPaper,
	Content,
	TitleContainer,
	Title,
	TitleIcon,
	TitleLabel,
	TitleRight,
	Hr,
	SideBarHr,
	LittleTitle
};
