import React from 'react';
import styled from 'styled-components';
import { grey400 } from 'material-ui/styles/colors';
import { TitleContainer, Title, TitleIcon, TitleLabel, Hr } from '../Layout';
import IconSvg from '../../svg/main_icon7.svg';

const BaseInfo = ({ data }) => {
	return (
		<div style={{ paddingBottom: '28px' }}>
			<TitleContainer>
				<Title>
					<TitleIcon src={IconSvg} />
					<TitleLabel>基本資料</TitleLabel>
				</Title>
			</TitleContainer>
			<Hr />
			<div style={{ margin: '1em' }}>
				<Table>
					<Tbody>
						<TR>
							<TD1>帳戶名稱</TD1>
							<TD2>{data.name}</TD2>
						</TR>
						<TR>
							<TD1>聯絡電話</TD1>
							<TD2>{data.telephone}</TD2>
						</TR>
						<TR>
							<TD1>聯絡地址</TD1>
							<TD2>{data.address}</TD2>
						</TR>
						<TR>
							<TD1>出金銀行帳戶</TD1>
							<TD2>{data.cashoutAccount}</TD2>
						</TR>
						<TR>
							<TD1>已存入總金額</TD1>
							<TD2>{data.totalCashin}元</TD2>
						</TR>
						<TR>
							<TD1>已提領總金額</TD1>
							<TD2>{data.totalCashout}元</TD2>
						</TR>
					</Tbody>
				</Table>
			</div>
		</div>
	);
};

const Table = styled.table`
	width: 100%;
	display: table;
	border-collapse: collapse;
	border-spacing: 0;
`;

const Tbody = styled.tbody`
	display: table-row-group;
	vertical-align: middle;
`;

const TR = styled.tr`
	display: table-row;
	border-bottom: 1px solid ${grey400};
	color: ${grey400};
`;

const TD1 = styled.td`
	display: table-cell;
	padding: 15px 5px;
	text-align: left;
`;

const TD2 = styled.td`
	display: table-cell;
	padding: 15px 5px;
	text-align: right;
`;

export default BaseInfo;
