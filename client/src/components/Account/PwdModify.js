import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import { black, amber600 } from 'material-ui/styles/colors';
import axios from 'axios';
import { TitleContainer, Title, TitleIcon, TitleLabel, Hr } from '../Layout';
import IconSvg from '../../svg/main_icon7.svg';

class PwdModify extends Component {
  constructor(props) {
    super(props);

    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleOk = this.handleOk.bind(this);
    this.handleCloseMsgDialog = this.handleCloseMsgDialog.bind(this);

    this.state = {
      open: false,
      message: '',
      isMsgDialogOpen: false,
      msgDialogData: {
        title: '',
        message: ''
      }
    };
  }

  handleOpen() {
    this.setState({ open: true });
  }

  handleClose() {
    this.setState({ open: false });
  }

  handleOk() {
    if (!this.refs.oldPwd.input.value) {
      this.setState({ message: '請輸入舊密碼' });
      return;
    }
    //TODO: 規範密碼的長度、格式
    if (!this.refs.newPwd1.input.value) {
      this.setState({ message: '請輸入新密碼' });
      return;
    }

    if (!this.refs.newPwd2.input.value) {
      this.setState({ message: '請再次輸入新密碼' });
      return;
    }

    if (this.refs.newPwd1.input.value !== this.refs.newPwd2.input.value) {
      this.setState({ message: '新密碼不一致, 請重新確認' });
      return;
    }

    if (this.props.googleAuthEnable && !this.refs.authToken.input.value) {
      this.setState({ message: '請輸入動態密碼' });
      return;
    }

    this.setState({ message: '', open: false });

    axios
      .post(
        `/api/v1/account/modifyPwd`,
        {
          managerId: this.props.auth.managerId,
          oldPwd: this.refs.oldPwd.input.value,
          newPwd: this.refs.newPwd1.input.value,
          authToken: this.refs.authToken ? this.refs.authToken.input.value : null
        },
        {
          headers: {
            Authorization: this.props.auth.token
          }
        }
      )
      .then(res => {
        if (res.data.status === true) {
          this.openMsgDialog('重設密碼完成');
        } else {
          // TODO: handle error_code
          this.openMsgDialog(res.data.msg);
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  handleCloseMsgDialog() {
    this.setState({ isMsgDialogOpen: false });
  }

  openMsgDialog(msg) {
    this.setState({
      isMsgDialogOpen: true,
      msgDialogData: {
        title: '提示訊息',
        message: msg
      }
    });
  }

  render() {
    const actions = [
      <FlatButton label="確認" primary={true} keyboardFocused={true} onClick={this.handleOk} />
    ];
    const msgActions = [
      <FlatButton label="確認" primary={true} keyboardFocused={true} onClick={this.handleCloseMsgDialog} />
    ];
    return (
      <div style={{ paddingBottom: '28px' }}>
        <TitleContainer>
          <Title>
            <TitleIcon src={IconSvg} />
            <TitleLabel>重設密碼</TitleLabel>
          </Title>
        </TitleContainer>
        <Hr />
        <div style={{ margin: '1em' }}>
          <label>該密碼將用於用戶登錄，請記住密碼。</label>
        </div>
        <RaisedButton
          label="重設"
          fullWidth={true}
          onClick={this.handleOpen}
          backgroundColor={amber600}
          labelColor={black}
        />
        <Dialog
          title="重設密碼"
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
        >
          輸入舊密碼:
          <TextField name="oldPwd" type="password" ref="oldPwd" fullWidth />
          <br />
          輸入新密碼:
          <TextField name="newPwd1" type="password" ref="newPwd1" fullWidth />
          <br />
          再次輸入新密碼:
          <TextField name="newPwd2" type="password" ref="newPwd2" fullWidth />
          {this.props.googleAuthEnable ? (
            <div>
              輸入Google Authenticator動態密碼
              <TextField name="authToken" type="text" ref="authToken" fullWidth />
            </div>
          ) : null}
          <div style={{ color: 'red' }}>{this.state.message}</div>
        </Dialog>
        <Dialog
          title={this.state.msgDialogData.title}
          actions={msgActions}
          modal={false}
          open={this.state.isMsgDialogOpen}
          onRequestClose={this.handleCloseMsgDialog}
          autoScrollBodyContent={true}
        >
          <div>{this.state.msgDialogData.message}</div>
        </Dialog>
      </div>
    );
  }
}

export default PwdModify;
