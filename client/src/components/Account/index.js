import React, { Component } from 'react';
import axios from 'axios';

import BaseInfo from './BaseInfo';
import PwdModify from './PwdModify';
import GoogleAuth from './GoogleAuth';
import Cashout from './Cashout';
//import ApiKey from './ApiKey';
import Footer from '../Footer';

import { Page, Column, ColumnGap, Paper, Content } from '../Layout';

class Account extends Component {
  constructor(props) {
    super(props);

    this.handleToggleGoogleAuth = this.handleToggleGoogleAuth.bind(this);

    this.state = {
      baseInfo: {
        name: '',
        telephone: '',
        address: '',
        cashoutAccount: '',
        totalCashin: 0,
        totalCashout: 0
      },
      googleAuth: null
    };
  }

  componentDidMount() {
    const { token, userId, managerId } = this.props.auth;

    if (!token) {
      this.props.route.history.push('/Home');
    } else {
      this.loadAccount(token, userId);
      this.loadTotalCashin(token, userId);
      this.loadTotalCashout(token, userId);
      this.loadGoogleAuthStatus(token, managerId);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.auth.token) {
      nextProps.route.history.push('/Home');
    }
  }

  loadAccount(token, user) {
    return axios
      .get(`/api/v1/account/` + user, {
        headers: {
          Authorization: token
        }
      })
      .then(res => {
        if (res.data.status === true) {
          const { name, telephone, address, cashoutAccount } = res.data.result.account;
          let baseInfo = { ...this.state.baseInfo };
          baseInfo.name = name;
          baseInfo.telephone = telephone;
          baseInfo.address = address;
          baseInfo.cashoutAccount = cashoutAccount;
          this.setState({ baseInfo });
        } else {
          // TODO: handle error_code, msg
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  loadTotalCashin(token, user) {
    return axios
      .get(`/api/v1/account/totalCashin/` + user, {
        headers: {
          Authorization: token
        }
      })
      .then(res => {
        if (res.data.status === true) {
          let baseInfo = { ...this.state.baseInfo };
          baseInfo.totalCashin = res.data.result.totalCashin;
          this.setState({ baseInfo });
        } else {
          // TODO: handle error_code, msg
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  loadTotalCashout(token, user) {
    return axios
      .get(`/api/v1/account/totalCashout/` + user, {
        headers: {
          Authorization: token
        }
      })
      .then(res => {
        if (res.data.status === true) {
          let baseInfo = { ...this.state.baseInfo };
          baseInfo.totalCashout = res.data.result.totalCashout;
          this.setState({ baseInfo });
        } else {
          // TODO: handle error_code, msg
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  loadGoogleAuthStatus(token, managerId) {
    return axios
      .get(`/api/v1/user/google2fa/status/` + managerId, {
        headers: {
          Authorization: token
        }
      })
      .then(res => {
        if (res.data.status === true) {
          this.setState({ googleAuth: res.data.result });
        } else {
          // TODO: handle error_code, msg
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  handleToggleGoogleAuth() {
    this.loadGoogleAuthStatus(this.props.auth.token, this.props.auth.managerId);
  }

  render() {
    const { baseInfo, googleAuth } = this.state;
    return (
      <Page>
        <div className="page-wrap container">
          <Column>
            <Paper>
              <BaseInfo data={baseInfo} />
            </Paper>
            <ColumnGap />
            <Content>
              {googleAuth ? (
                <Paper>
                  <PwdModify auth={this.props.auth} googleAuthEnable={googleAuth.enable} />
                </Paper>
              ) : null}
              {googleAuth ? (
                <Paper>
                  <GoogleAuth
                    auth={this.props.auth}
                    googleAuthEnable={googleAuth.enable}
                    toggle={this.handleToggleGoogleAuth}
                  />
                </Paper>
              ) : null}
              {googleAuth ? (
                <Paper>
                  <Cashout auth={this.props.auth} googleAuthEnable={googleAuth.enable} />
                </Paper>
              ) : null}
              {/*googleAuth ? (
                <Paper>
                  <ApiKey
                    auth={this.props.auth}
                    googleAuthEnable={googleAuth.enable}
                  />
                </Paper>
              ) : null*/}
            </Content>
          </Column>
        </div>
        <Footer />
      </Page>
    );
  }
}

export default Account;
