import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import { black, amber600 } from 'material-ui/styles/colors';
import axios from 'axios';
import { sprintf } from 'sprintf-js';
import { TitleContainer, Title, TitleIcon, TitleLabel, Hr } from '../Layout';
import IconSvg from '../../svg/main_icon7.svg';

class Cashout extends Component {
  constructor(props) {
    super(props);

    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleOk = this.handleOk.bind(this);
    this.handleCloseMsgDialog = this.handleCloseMsgDialog.bind(this);

    this.state = {
      open: false,
      message: '',
      isMsgDialogOpen: false,
      msgDialogData: {
        title: '',
        message: ''
      }
    };
  }

  handleOpen() {
    this.setState({ open: true });
  }

  handleClose() {
    this.setState({ open: false });
  }

  handleOk() {
    if (!(parseInt(this.refs.amount.input.value, 10) > 0)) {
      this.setState({ message: '請輸入大於0的金額' });
      return;
    }

    if (!this.refs.pwd.input.value) {
      this.setState({ message: '請輸入密碼' });
      return;
    }

    if (this.props.googleAuthEnable && !this.refs.authToken.input.value) {
      this.setState({ message: '請輸入動態密碼' });
      return;
    }

    this.setState({ message: '', open: false });

    axios
      .post(
        `/api/v1/account/cashout`,
        {
          managerId: this.props.auth.managerId,
          userId: this.props.auth.userId,
          amount: this.refs.amount.input.value,
          pwd: this.refs.pwd.input.value,
          authToken: this.refs.authToken ? this.refs.authToken.input.value : null
        },
        {
          headers: {
            Authorization: this.props.auth.token
          }
        }
      )
      .then(res => {
        if (res.data.status === true) {
          this.openMsgDialog(
            sprintf(`完成申請出金%s元，出金程序需3~7個工作日，可至明細查詢相關進度。`, res.data.result.amount)
          );
        } else {
          // TODO: handle error_code, msg
          this.openMsgDialog(res.data.msg);
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  handleCloseMsgDialog() {
    this.setState({ isMsgDialogOpen: false });
  }

  openMsgDialog(msg) {
    this.setState({
      isMsgDialogOpen: true,
      msgDialogData: {
        title: '提示訊息',
        message: msg
      }
    });
  }

  render() {
    const actions = [
      <FlatButton label="確認" primary={true} keyboardFocused={true} onClick={this.handleOk} />
    ];
    const msgActions = [
      <FlatButton label="確認" primary={true} keyboardFocused={true} onClick={this.handleCloseMsgDialog} />
    ];
    return (
      <div style={{ paddingBottom: '28px' }}>
        <TitleContainer>
          <Title>
            <TitleIcon src={IconSvg} />
            <TitleLabel>提領現金</TitleLabel>
          </Title>
        </TitleContainer>
        <Hr />
        <div style={{ margin: '1em' }}>
          <label>提領程序需要3-7個工作天，完成後將匯入您指定的銀行帳戶。</label>
        </div>
        <RaisedButton
          label="申請"
          fullWidth={true}
          onClick={this.handleOpen}
          backgroundColor={amber600}
          labelColor={black}
        />
        <Dialog
          title="出金申請"
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
        >
          輸入金額:
          <TextField name="amount" type="number" ref="amount" fullWidth />
          <br />
          輸入密碼:
          <TextField name="pwd" type="password" ref="pwd" fullWidth />
          {this.props.googleAuthEnable ? (
            <div>
              輸入Google Authenticator動態密碼
              <TextField name="authToken" type="text" ref="authToken" fullWidth />
            </div>
          ) : null}
          <div style={{ color: 'red' }}>{this.state.message}</div>
        </Dialog>
        <Dialog
          title={this.state.msgDialogData.title}
          actions={msgActions}
          modal={false}
          open={this.state.isMsgDialogOpen}
          onRequestClose={this.handleCloseMsgDialog}
          autoScrollBodyContent={true}
        >
          <div>{this.state.msgDialogData.message}</div>
        </Dialog>
      </div>
    );
  }
}

export default Cashout;
