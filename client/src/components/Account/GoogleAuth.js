import React, { Component } from 'react';
import axios from 'axios';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import { black, amber600 } from 'material-ui/styles/colors';
import { sprintf } from 'sprintf-js';
import { TitleContainer, Title, TitleIcon, TitleLabel, Hr } from '../Layout';
import IconSvg from '../../svg/main_icon7.svg';

class GoogleAuth extends Component {
  constructor(props) {
    super(props);

    this.openEnableDialog = this.openEnableDialog.bind(this);
    this.openDisableDialog = this.openDisableDialog.bind(this);
    this.handleGoogleAuthEnable = this.handleGoogleAuthEnable.bind(this);
    this.handleGoogleAuthDisable = this.handleGoogleAuthDisable.bind(this);
    this.handleClose = this.handleClose.bind(this);

    this.state = {
      enableOpen: false,
      disableOpen: false,
      message: '',
      googleAuthEnable: this.props.googleAuthEnable,
      googleAuthData: null
    };
  }

  componentDidMount() {
    if (!this.props.googleAuthEnable) {
      this.loadGoogle2fa();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.googleAuthEnable !== this.props.googleAuthEnable) {
      this.setState({ googleAuthEnable: nextProps.googleAuthEnable });
      if (!nextProps.googleAuthEnable) {
        this.loadGoogle2fa();
      }
    }
  }

  loadGoogle2fa() {
    return axios
      .get(`/api/v1/user/google2fa/` + this.props.auth.managerId, {
        headers: {
          Authorization: this.props.auth.token
        }
      })
      .then(res => {
        if (res.data.status === true) {
          this.setState({ googleAuthData: res.data.result });
        } else {
          // TODO: handle error_code, msg
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  openEnableDialog() {
    this.setState({ enableOpen: true, message: '' });
  }

  openDisableDialog() {
    this.setState({ disableOpen: true, message: '' });
  }

  handleGoogleAuthEnable() {
    axios
      .post(
        `/api/v1/user/google2fa/toggle/` + this.props.auth.managerId,
        {
          token: this.refs.authTokenEnable.input.value,
          enable: true
        },
        {
          headers: {
            Authorization: this.props.auth.token
          }
        }
      )
      .then(res => {
        if (res.data.status === true) {
          if (res.data.result.tokenValidates === true) {
            this.setState({ message: '驗證成功' });
            this.props.toggle();
            setTimeout(() => this.setState({ enableOpen: false }), 1000);
          } else {
            this.setState({
              message: sprintf('你輸入的動態密碼 (%s) 驗證失敗', this.refs.authTokenEnable.input.value)
            });
          }
        } else {
          // TODO: handle error_code, msg
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  handleGoogleAuthDisable() {
    axios
      .post(
        `/api/v1/user/google2fa/toggle/` + this.props.auth.managerId,
        {
          token: this.refs.authTokenDisable.input.value,
          enable: false
        },
        {
          headers: {
            Authorization: this.props.auth.token
          }
        }
      )
      .then(res => {
        if (res.data.status === true) {
          if (res.data.result.tokenValidates === true) {
            this.setState({ message: '驗證成功' });
            this.props.toggle();
            setTimeout(() => this.setState({ disableOpen: false }), 1000);
          } else {
            this.setState({
              message: sprintf('你輸入的動態密碼 (%s) 驗證失敗', this.refs.authTokenDisable.input.value)
            });
          }
        } else {
          // TODO: handle error_code, msg
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  handleClose() {
    this.setState({ enableOpen: false, disableOpen: false });
  }

  render() {
    const actions = [
      <FlatButton label="取消" primary={true} keyboardFocused={false} onClick={this.handleClose} />,
      <FlatButton
        label="確認"
        primary={true}
        keyboardFocused={true}
        onClick={this.props.googleAuthEnable ? this.handleGoogleAuthDisable : this.handleGoogleAuthEnable}
      />
    ];
    return (
      <div style={{ paddingBottom: '28px' }}>
        <TitleContainer>
          <Title>
            <TitleIcon src={IconSvg} />
            <TitleLabel>Google Authenticator兩步驗證</TitleLabel>
          </Title>
        </TitleContainer>
        <Hr />
        <div style={{ margin: '1em' }}>
          <label>透過Google Authenticator由行動裝置進行兩步驟驗證，將會增加您賬戶的安全性。</label>
        </div>

        <RaisedButton
          label={this.props.googleAuthEnable ? '關閉' : '開啟'}
          fullWidth={true}
          onClick={this.props.googleAuthEnable ? this.openDisableDialog : this.openEnableDialog}
          backgroundColor={amber600}
          labelColor={black}
        />
        {this.props.googleAuthEnable ? (
          <Dialog
            title="關閉Google Authenticator兩步驗證"
            actions={actions}
            modal={false}
            open={this.state.disableOpen}
            onRequestClose={this.handleClose}
            autoScrollBodyContent={true}
          >
            <div>
              輸入Google Authenticator動態密碼，關閉此功能
              <TextField name="authTokenDisable" type="text" ref="authTokenDisable" fullWidth />
            </div>
            <div style={{ color: 'red' }}>{this.state.message}</div>
          </Dialog>
        ) : (
          <Dialog
            title="開啟Google Authenticator兩步驗證"
            actions={actions}
            modal={false}
            open={this.state.enableOpen}
            onRequestClose={this.handleClose}
            autoScrollBodyContent={true}
          >
            <div>第一步: 在您的手機上安裝 Google Authenticator App.</div>
            <br />
            <div>第二步: 用 Google Authenticator App 掃描二維碼，或手動添加密文</div>
            <br />
            {this.state.googleAuthData ? (
              <img src={this.state.googleAuthData.data_url} alt="QR Code" />
            ) : null}
            {this.state.googleAuthData ? (
              <div>密文: {this.state.googleAuthData.google2FASecretBase32}</div>
            ) : null}
            <br />
            <div>
              第三步: 輸入收到的動態密碼，開通兩步驗證
              <TextField name="authTokenEnable" type="text" ref="authTokenEnable" fullWidth />
            </div>
            <div style={{ color: 'red' }}>{this.state.message}</div>
          </Dialog>
        )}
      </div>
    );
  }
}

export default GoogleAuth;
