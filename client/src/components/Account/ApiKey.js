import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import { black, amber600 } from 'material-ui/styles/colors';
import axios from 'axios';
import { TitleContainer, Title, TitleIcon, TitleLabel, Hr } from '../Layout';
import IconSvg from '../../svg/main_icon7.svg';

class ApiKey extends Component {
  constructor(props) {
    super(props);

    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleOk = this.handleOk.bind(this);
    this.handleCloseMsgDialog = this.handleCloseMsgDialog.bind(this);

    this.state = {
      open: false,
      message: '',
      isMsgDialogOpen: false,
      msgDialogData: {
        title: '',
        message: ''
      },
      key: '',
      secret: ''
    };
  }

  handleOpen() {
    axios
      .get(`/api/v1/apikeys/` + this.props.auth.userId, {
        headers: {
          Authorization: this.props.auth.token
        }
      })
      .then(res => {
        if (res.data.status === true) {
          if (res.data.result.apikeys && res.data.result.apikeys.length > 0) {
            this.setState({
              key:
                res.data.result.apikeys[res.data.result.apikeys.length - 1].key,
              secret:
                res.data.result.apikeys[res.data.result.apikeys.length - 1]
                  .secret
            });
          }
          this.setState({ open: true });
        } else {
          // TODO: handle error_code, msg
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  handleClose() {
    this.setState({ open: false });
  }

  handleOk() {
    //this.setState({ message: '', open: false });

    axios
      .post(
        `/api/v1/apikeys`,
        {
          authToken: 333294,
          options: {
            readInfo: true,
            trade: false,
            withdraw: false
          }
        },
        {
          headers: {
            Authorization: this.props.auth.token
          }
        }
      )
      .then(res => {
        if (res.data.status === true) {
          this.setState({
            key: res.data.result.apiKey,
            secret: res.data.result.apiSecret
          });
        } else {
          this.openMsgDialog(res.data.msg);
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  handleCloseMsgDialog() {
    this.setState({ isMsgDialogOpen: false });
  }

  openMsgDialog(msg) {
    this.setState({
      isMsgDialogOpen: true,
      msgDialogData: {
        title: '提示訊息',
        message: msg
      }
    });
  }

  render() {
    const actions = [
      <FlatButton
        label="產生金鑰"
        primary={true}
        keyboardFocused={true}
        onClick={this.handleOk}
      />
    ];
    const msgActions = [
      <FlatButton
        label="確認"
        primary={true}
        keyboardFocused={true}
        onClick={this.handleCloseMsgDialog}
      />
    ];
    return (
      <div style={{ paddingBottom: '28px' }}>
        <TitleContainer>
          <Title>
            <TitleIcon src={IconSvg} />
            <TitleLabel>API金鑰</TitleLabel>
          </Title>
        </TitleContainer>
        <Hr />
        <div style={{ margin: '1em' }}>
          <label>該金鑰組合將用於存取API功能。</label>
        </div>
        <RaisedButton
          label="查詢API金鑰"
          fullWidth={true}
          onClick={this.handleOpen}
          backgroundColor={amber600}
          labelColor={black}
        />
        <Dialog
          title="API金鑰"
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
        >
          API Key:
          <TextField
            name="apiKey"
            type="text"
            value={this.state.key}
            fullWidth
          />
          <br />
          API Secret:
          <TextField
            name="apiSecret"
            type="text"
            value={this.state.secret}
            fullWidth
          />
          <div style={{ color: 'red' }}>{this.state.message}</div>
        </Dialog>
        <Dialog
          title={this.state.msgDialogData.title}
          actions={msgActions}
          modal={false}
          open={this.state.isMsgDialogOpen}
          onRequestClose={this.handleCloseMsgDialog}
          autoScrollBodyContent={true}
        >
          <div>{this.state.msgDialogData.message}</div>
        </Dialog>
      </div>
    );
  }
}

export default ApiKey;
