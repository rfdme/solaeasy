import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import Analytics from 'react-router-ga';

import Main from './Main';

class App extends Component {
  constructor(props) {
    super(props);

    this.handleSignIn = this.handleSignIn.bind(this);
    this.handleSignOut = this.handleSignOut.bind(this);
    this.autoSignIn = this.autoSignIn.bind(this);
    this.handleChangeUser = this.handleChangeUser.bind(this);

    this.state = {
      auth: {
        token: null,
        managerId: null,
        userId: null,
        username: '',
        userList: [],
        signIn: this.handleSignIn,
        signOut: this.handleSignOut,
        changeUser: this.handleChangeUser
      }
    };
  }

  componentDidMount() {
    this.autoSignIn();
  }

  autoSignIn() {
    //try local sign in
    const token = window.sessionStorage.getItem('token');
    console.log(`session storage, token = ${token}`);
    if (token) {
      console.log('local sign in');
      let auth = { ...this.state.auth };
      auth.token = token;
      auth.managerId = parseInt(window.sessionStorage.getItem('managerId'), 10);
      auth.userId = parseInt(window.sessionStorage.getItem('userId'), 10);
      auth.username = window.sessionStorage.getItem('userName');
      auth.userList = JSON.parse(window.sessionStorage.getItem('userList'));
      this.setState({ auth });
    }
  }

  handleSignIn(data) {
    console.log('sign in manager: %s', data.manager.id);
    let token = 'Bearer ' + data.token;
    window.sessionStorage.setItem('token', token);
    window.sessionStorage.setItem('managerId', data.manager.id);
    window.sessionStorage.setItem('userId', data.users[0].id);
    window.sessionStorage.setItem('userName', data.users[0].name);
    window.sessionStorage.setItem('userList', JSON.stringify(data.users));

    let auth = { ...this.state.auth };
    auth.token = token;
    auth.managerId = data.manager.id;
    auth.userId = data.users[0].id;
    auth.username = data.users[0].name;
    auth.userList = data.users;
    this.setState({ auth });
  }

  handleSignOut() {
    window.sessionStorage.removeItem('token');
    window.sessionStorage.removeItem('managerId');
    window.sessionStorage.removeItem('userId');
    window.sessionStorage.removeItem('userName');
    window.sessionStorage.removeItem('userList');
    this.doSignOut();
  }

  doSignOut() {
    let auth = { ...this.state.auth };
    auth.token = null;
    auth.managerId = null;
    auth.userId = null;
    auth.username = '';
    auth.userList = [];
    this.setState({ auth });
  }

  handleChangeUser(id, name) {
    let auth = { ...this.state.auth };
    auth.userId = id;
    auth.username = name;
    this.setState({ auth });
  }

  render() {
    return (
      <BrowserRouter>
        <Analytics id="UA-118694706-1" debug>
          <Main auth={this.state.auth} />
        </Analytics>
      </BrowserRouter>
    );
  }
}

export default App;
