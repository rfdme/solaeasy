import React from 'react';

import FlatButton from 'material-ui/FlatButton';
import { amber600 } from 'material-ui/styles/colors';
import styled from 'styled-components';

const TablePaging = ({ page, showPageCount, totalPageCount, onPage }) => {
	let pages = [];

	pages.push(
		<FlatButton
			key={0}
			label={'上一頁'}
			onClick={() => {
				onPage(page - 1);
			}}
			disabled={page <= 0}
		/>
	);

	const showBegin = page - Math.floor(showPageCount / 2);
	const showEnd = page + Math.floor(showPageCount / 2);

	const paddingRight = showBegin < 0 ? Math.abs(showBegin) : 0;
	const paddingLeft =
		showEnd - (totalPageCount - 1) > 0 ? showEnd - (totalPageCount - 1) : 0;
	for (let i = 0; i < totalPageCount; i++) {
		if (i >= showBegin - paddingLeft && i <= showEnd + paddingRight)
			pages.push(
				<FlatButton
					key={i + 1}
					label={i + 1}
					onClick={() => {
						onPage(i);
					}}
					style={i === page ? { color: amber600 } : null}
				/>
			);
	}

	pages.push(
		<FlatButton
			key={totalPageCount + 1}
			label={'下一頁'}
			onClick={() => {
				onPage(page + 1);
			}}
			disabled={page >= totalPageCount - 1}
		/>
	);

	if (totalPageCount > 1) {
		return <Paging>{pages}</Paging>;
	} else {
		return null;
	}
};

const Paging = styled.div`
	width: 100%;
	display: inline-block;
	text-align: center;
`;

export default TablePaging;
