import React, { Component } from 'react';
import { sprintf } from 'sprintf-js';

import url from '../../config';

import BigNumber from 'bignumber.js';
const BN = BigNumber.clone({ DECIMAL_PLACES: 6, ROUNDING_MODE: 1 });

const io = require('socket.io-client');

const STOCK = 'SNTW1';
class Robot extends Component {
  constructor(props) {
    super(props);

    let token = props.auth.token ? props.auth.token.replace('Bearer ', '') : '';
    this.state = {
      socket: io(url.websocketURL, {
        autoConnect: false,
        query: {
          token: token
        }
      }),
      userId: props.auth.userId,
      token: props.auth.token
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      userId: nextProps.auth.userId,
      token: nextProps.auth.token
    });
  }

  componentDidMount() {
    this.state.socket.connect();

    this.state.socket.on('connect', () => {
      console.log('connect');
    });

    this.state.socket.on('disconnect', () => console.log('disconnect'));

    this.start();
  }

  componentWillUnmount() {
    this.state.socket.disconnect();
  }

  start() {
    const trade = [
      {
        price: BN(9.6, 10),
        amount: BN(/*Math.random() + */ 1, 10)
      },
      {
        price: BN(9.7, 10),
        amount: BN(/*Math.random() + */ 2, 10)
      },
      {
        price: BN(9.8, 10),
        amount: BN(/*Math.random() + */ 3, 10)
      },
      {
        price: BN(9.9, 10),
        amount: BN(/*Math.random() + */ 4, 10)
      },
      {
        price: BN(10, 10),
        amount: BN(/*Math.random() + */ 5, 10)
      },
      {
        price: BN(10.1, 10),
        amount: BN(/*Math.random() + */ 6, 10)
      },
      {
        price: BN(10.2, 10),
        amount: BN(/*Math.random() + */ 7, 10)
      },
      {
        price: BN(10.3, 10),
        amount: BN(/*Math.random() + */ 8, 10)
      },
      {
        price: BN(10.4, 10),
        amount: BN(/*Math.random() + */ 9, 10)
      }
    ];
    let interval = parseInt(this.props.route.match.params.interval, 10);

    setInterval(() => {
      const index = Math.floor(Math.random() * 9);
      const buy = trade[index];
      switch (Math.round(Math.random())) {
        case 0:
          this.orderBuy(STOCK, buy.price, buy.amount);
          break;
        case 1:
          this.orderSell(STOCK, buy.price, BN(11).minus(buy.amount));
          break;
        default:
      }
    }, interval);
  }

  initMySttw() {
    this.state.socket.emit('initMySttw', this.state.userId, data => {
      this.setState({
        remainedSttw: data ? data.sttw : 0
      });
    });
  }

  initMySntw(id) {
    this.state.socket.emit('initMySntw', this.state.userId, id, data => {
      this.setState({
        remainedSntw: data ? data.sntw : 0
      });
    });
  }

  orderBuy(pvId, price, amount) {
    if (!this.state.userId) {
      console.log('請先登入');
      return;
    }

    this.state.socket.emit(
      'orderBuy',
      {
        pvId: pvId,
        userId: this.state.userId,
        price: price,
        amount: amount
      },
      result => {
        if (result === '委託成功') {
          console.log(
            sprintf(
              '委託成功:<br/>以價格 %s 元，買進 %s 個%s',
              price,
              amount,
              pvId
            )
          );
          //this.initMyOrders();
          //this.initMySttw();
        } else {
          console.log(result);
        }
      }
    );
  }

  orderSell(pvId, price, amount) {
    if (!this.state.userId) {
      console.log('請先登入');
      return;
    }

    this.state.socket.emit(
      'orderSell',
      {
        pvId: pvId,
        userId: this.state.userId,
        price: price,
        amount: amount
      },
      result => {
        if (result === '委託成功') {
          console.log(
            sprintf(
              '委託成功:<br/>以價格 %s 元，賣出 %s 個%s',
              price,
              amount,
              pvId
            )
          );
          //this.initMyOrders();
          //this.initMySntw(pvId);
        } else {
          console.log(result);
        }
      }
    );
  }

  render() {
    return <div>Robot: {this.props.route.match.params.interval}</div>;
  }
}

export default Robot;
