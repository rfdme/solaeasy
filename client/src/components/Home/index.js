import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import styled, { injectGlobal } from 'styled-components';
import moment from 'moment';
import { sprintf } from 'sprintf-js';
import axios from 'axios';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import { white } from 'material-ui/styles/colors';

import Header from '../Header';
import Dashboard from '../Dashboard';
import PvInfo from '../PvInfo/';
import Exchange from '../Exchange';
import MockExchange from '../Exchange/MockExchange';
import EventBook from '../EventBook';
import Portfolio from '../Portfolio';
import Account from '../Account';
import DateSelector from './DateSelector';
import Detail from '../Detail';
import Login from '../Login';
import Donation from '../Donation';
import ResetPwd from '../ResetPwd';

import BigNumber from 'bignumber.js';
const BN = BigNumber.clone({ DECIMAL_PLACES: 16, ROUNDING_MODE: 1 });

class Home extends Component {
  constructor(props) {
    super(props);

    this.handleToggleMock = this.handleToggleMock.bind(this);
    this.handleMockBuy = this.handleMockBuy.bind(this);
    this.handleMockSell = this.handleMockSell.bind(this);
    this.handleGetMockData = this.handleGetMockData.bind(this);
    this.handleDateSelect = this.handleDateSelect.bind(this);
    this.handleGoogleAuth = this.handleGoogleAuth.bind(this);

    this.getMockTime = this.getMockTime.bind(this);

    this.state = {
      mock: {
        enable: false,
        toggle: this.handleToggleMock,
        getData: this.handleGetMockData,
        buy: this.handleMockBuy,
        sell: this.handleMockSell,
        date: '2017-12-05'
      },
      //不直接傳給MockExchange, 讓MockExchange主動請求, 避免整頁重新render
      mockData: {
        sttw: BN(1000000),
        assets: {},
        trades: [],
        eventBySimulate: []
      },
      allUnitProfit: null,
      googleAuthEnable: false,
      googleAuthMessage: ''
    };
  }

  componentDidMount() {
    if (this.props.auth.token) {
      this.loadGoogle2FAStatus(this.props.auth.token, this.props.auth.userId);
    }
  }

  //避免整頁重新render
  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps !== this.props) {
      return true;
    }

    if (nextState.mock.enable !== this.state.mock.enable) {
      return true;
    }

    if (nextState.mock.date !== this.state.mock.date) {
      return true;
    }

    if (nextState.googleAuthEnable !== this.state.googleAuthEnable) {
      return true;
    }

    if (nextState.googleAuthMessage !== this.state.googleAuthMessage) {
      return true;
    }

    return false;
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth !== this.props.auth && nextProps.auth.token) {
      this.loadGoogle2FAStatus(nextProps.auth.token, nextProps.auth.managerId);
    } else {
      this.setState({ googleAuthEnable: false });
    }

    if (!nextProps.auth.token) {
      let mock = { ...this.state.mock };
      mock.enable = false;
      this.setState({ mock });
    }
  }

  loadGoogle2FAStatus(token, managerId) {
    axios
      .get(`/api/v1/user/google2fa/status/` + managerId, {
        headers: {
          Authorization: token
        }
      })
      .then(res => {
        if (res.data.status === true) {
          this.setState({
            googleAuthEnable: res.data.result.enable ? true : false
          });
        } else {
          // TODO: handle error_code, msg
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  handleToggleMock() {
    let mock = { ...this.state.mock };
    mock.enable = !mock.enable;
    this.setState({ mock });

    if (mock.enable) {
      axios
        .get(`/api/v1/pv/allPvUnitProfit`)
        .then(res => {
          if (res.data.status === true) {
            console.log(res.data.result);
            let allUnitProfit = { ...this.state.allUnitProfit };
            allUnitProfit = res.data.result;
            this.setState({ allUnitProfit });
          } else {
            // TODO: handle error_code, msg
          }
        })
        .catch(error => {
          console.error(error);
        });
    }
  }

  handleGetMockData() {
    const sttw = this.state.mockData.sttw;
    const assets = JSON.parse(JSON.stringify(this.state.mockData.assets));
    const trades = this.state.mockData.trades.slice();
    const eventBySimulate = this.state.mockData.eventBySimulate.slice();

    return { sttw, assets, trades, eventBySimulate };
  }

  handleMockBuy(pvId, price, amount) {
    const tradetime = this.getMockTime();

    let mockData = { ...this.state.mockData };
    mockData.sttw = mockData.sttw.minus(BN(price).times(amount));
    if (mockData.assets[pvId]) {
      mockData.assets[pvId] = mockData.assets[pvId].plus(amount);
    } else {
      mockData.assets[pvId] = BN(amount);
    }

    mockData.trades.push({
      pvId: pvId,
      side: '買',
      datetime: tradetime,
      price: price,
      amount: amount,
      fee: 0
    });

    this.setState({ mockData });
  }

  handleMockSell(pvId, price, amount) {
    const tradetime = this.getMockTime();

    let mockData = { ...this.state.mockData };
    mockData.sttw = mockData.sttw.plus(BN(price).times(amount));
    mockData.assets[pvId] = mockData.assets[pvId].minus(amount);

    mockData.trades.push({
      pvId: pvId,
      side: '賣',
      datetime: tradetime,
      price: price,
      amount: amount,
      fee: 0
    });

    this.setState({ mockData });
  }

  getMockTime() {
    return sprintf('%s %s', this.state.mock.date, moment().format('HH:mm:ss'));
  }

  handleDateSelect(newDate) {
    let mock = { ...this.state.mock };
    let mockData = { ...this.state.mockData };
    console.log('handleDateSelect', newDate, mock.date);
    //重新計算mockData
    if (newDate < mock.date) {
      //向前
      for (let i = mockData.trades.length - 1; i >= 0; i--) {
        let item = mockData.trades[i];
        if (item.datetime >= newDate) {
          if (item.side === '買') {
            mockData.sttw = mockData.sttw.plus(item.price * item.amount);
            mockData.assets[item.pvId] = mockData.assets[item.pvId].minus(item.amount);
          } else if (item.side === '賣') {
            mockData.sttw = mockData.sttw.minus(item.price * item.amount);
            mockData.assets[item.pvId] = mockData.assets[item.pvId].plus(item.amount);
          }
          mockData.trades.splice(i, 1);
        }
      }

      for (let i = mockData.eventBySimulate.length - 1; i >= 0; i--) {
        let item = mockData.eventBySimulate[i];
        if (item.date >= newDate) {
          mockData.eventBySimulate.splice(i, 1);
        }
      }

      mock.date = newDate;
      this.setState({ mock, mockData });
    } else if (newDate > mock.date) {
      //向後
      let assetValue = Object.keys(mockData.assets).reduce(
        (sum, key) => sum.plus(mockData.assets[key]),
        BN(0)
      );
      console.log('assetValue', assetValue);
      if (assetValue === 0) {
        mock.date = newDate;
        this.setState({ mock });
        return;
      }

      const allUnitProfit = this.state.allUnitProfit;
      let moveDate = moment(mock.date);
      while (moveDate.format('YYYY-MM-DD') < newDate) {
        for (var pvId in mockData.assets) {
          if (mockData.assets[pvId] > 0) {
            let date = moveDate.format('YYYY-MM-DD');

            if (pvId in allUnitProfit) {
              let unitProfit = allUnitProfit[pvId].profitData.unitCost;
              if (date in allUnitProfit[pvId].profitData) {
                unitProfit = allUnitProfit[pvId].profitData[date];
              }

              let profit = mockData.assets[pvId].times(unitProfit.toString());

              mockData.eventBySimulate.push({
                pvId,
                date,
                profit
              });
            } else {
              console.log(sprintf("%s not in allUnitProfit, can't calculate the profit", pvId));
            }
          }
        }

        moveDate.add(1, 'days');
      }
      mock.date = newDate;
      this.setState({ mock, mockData });
    }
  }

  handleGoogleAuth() {
    axios
      .post(
        `/api/v1/user/google2fa/verify/` + this.props.auth.managerId,
        {
          token: this.refs.authToken.value
        },
        {
          headers: {
            Authorization: this.props.auth.token
          }
        }
      )
      .then(res => {
        if (res.data.status === true) {
          if (res.data.result.tokenValidates === true) {
            this.setState({ googleAuthMessage: '驗證成功' });
            setTimeout(() => this.setState({ googleAuthEnable: false }), 1000);
          } else {
            this.setState({
              googleAuthMessage: sprintf('你輸入的動態密碼 (%s) 驗證失敗', this.refs.authToken.value)
            });
          }
        } else {
          // TODO: handle error_code, msg
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  render() {
    /*
    if (window.innerWidth < 1024) {
      alert('目前SOLA‧bloc不支援螢幕寬度低於1024');
      return (
        <div style={{ fontSize: '48px', textAlign: 'center' }}>
          <div>手機版</div>
          <div>Coming Soon!</div>
        </div>
      );
    }
    */

    const { auth, route } = this.props;

    const actions = [
      <FlatButton label="確認" primary={true} keyboardFocused={true} onClick={this.handleGoogleAuth} />
    ];
    return (
      <SPA>
        <Header route={route} auth={auth} mock={this.state.mock} />
        {this.state.mock.enable ? (
          <MockBlock>
            <MockTitle>模擬交易功能已開啟</MockTitle>
            <div>　模擬交易的日期:</div>
            <DateSelector initDate={this.state.mock.date} onSelect={this.handleDateSelect} />
          </MockBlock>
        ) : null}
        <Body>
          <Route exact path={`${route.match.path}/`} component={() => <Dashboard auth={auth} />} />
          <Route path={`${route.match.path}/PvInfo`} component={() => <PvInfo auth={auth} />} />
          <Route
            path={`${route.match.path}/Exchange`}
            component={() => {
              return this.state.mock.enable ? (
                <MockExchange auth={auth} mock={this.state.mock} />
              ) : (
                <Exchange auth={auth} />
              );
            }}
          />
          <Route
            path={`${route.match.path}/EventBook`}
            component={() => <EventBook auth={auth} mock={this.state.mock} />}
          />
          <Route
            path={`${route.match.path}/Detail`}
            component={() => <Detail route={route} auth={auth} mock={this.state.mock} />}
          />
          <Route
            path={`${route.match.path}/Portfolio`}
            component={() => <Portfolio route={route} auth={auth} mock={this.state.mock} />}
          />
          <Route
            path={`${route.match.path}/Donation`}
            component={() => <Donation route={route} auth={auth} />}
          />
          <Route
            path={`${route.match.path}/Account`}
            component={() => <Account route={route} auth={auth} />}
          />
          <Route path={`${route.match.path}/Login`} component={() => <Login route={route} auth={auth} />} />
          <Route
            path={`${route.match.path}/ResetPwd`}
            component={() => <ResetPwd route={route} auth={auth} />}
          />
          <Dialog
            title="Google Authenticator驗證"
            actions={actions}
            modal={false}
            open={this.state.googleAuthEnable}
            onRequestClose={this.props.auth.signOut}
            autoScrollBodyContent={true}
          >
            <div>
              輸入Google Authenticator動態密碼
              <input type="text" ref="authToken" />
            </div>
            <div style={{ color: 'red' }}>{this.state.googleAuthMessage}</div>
          </Dialog>
        </Body>
      </SPA>
    );
  }
}

injectGlobal`
  @import url(https://fonts.googleapis.com/earlyaccess/notosanstc.css);
  @import url(https://fonts.googleapis.com/css?family=Montserrat:300);

  body {
    margin: 0;
        font-family: Arial, "Noto Sans", "Noto Sans TC", "Microsoft Yahei" !important;
        font-weight: 300;
  }

  label {
    font-size: 1em;
    color: #9e9e9e;
  }

  .title {
    color: #f7b100;
      font-size: 20px;
  }

  .title1 {
    color: #f7b100;
    margin-bottom: 1em;
    display:block;
      font-size: 20px;
  }

  .navSize * {
    font-size: 1.35em;
  }

  a:link, a:active, a:hover, a:visited {
    text-decoration: none;
  }

  .container {
    margin: 0 auto;
      max-width: 1364px;
  }

  @media screen and (max-width: 1280px){
    .container {max-width: 1278px;}
  }

  @media screen and (max-width: 1024px){
    .container {max-width: 1022px;}
  }

  //Sticky Footer
  .page-wrap {
    min-height: 100%;
    /* equal to footer height */
    margin-top: -1px;
    margin-bottom: -71px;
  }
  .page-wrap:after {
    content: "";
    display: block;
  }
  .site-footer, .page-wrap:after {
    height: 70px;
  }

  //scrollbar style
  ::-webkit-scrollbar-track
  {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    border-radius: 10px;
    background-color: #232327;
  }

  ::-webkit-scrollbar
  {
    width: 5px;
    height: 5px;
  }

  ::-webkit-scrollbar-thumb
  {
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
    background-color: #444649;
  }
`;

const SPA = styled.div`
  max-height: 100vh;
  height: 100vh;
  display: flex;
  flex-direction: column;
  background: #151515;
`;

const Body = styled.div`
  flex: 1;
  display: flex;
  overflow-y: auto;
`;

const MockBlock = styled.div`
  display: flex;
  align-items: center;
  background: #151515;
  color: ${white};
`;

const MockTitle = styled.span`
  text-align: center;
  font-size: 2em;
`;

export default Home;
