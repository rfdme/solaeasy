import React, { Component } from 'react';
import moment from 'moment';
import DatePicker from 'material-ui/DatePicker';
import FlatButton from 'material-ui/FlatButton';
import styled from 'styled-components';

class DateSelector extends Component {
	constructor(props) {
		super(props);

		this.handlePrevDay = this.handlePrevDay.bind(this);
		this.handleNextDay = this.handleNextDay.bind(this);
		this.handleSelectDate = this.handleSelectDate.bind(this);
	}

	componentDidMount() {}

	handlePrevDay() {
		let d = moment(this.props.initDate, 'YYYY-MM-DD');
		d.subtract(1, 'days');
		this.props.onSelect(d.format('YYYY-MM-DD'));
	}

	handleNextDay() {
		let d = moment(this.props.initDate, 'YYYY-MM-DD');
		d.add(1, 'days');
		this.props.onSelect(d.format('YYYY-MM-DD'));
	}

	handleSelectDate(event, date) {
		this.props.onSelect(moment(date).format('YYYY-MM-DD'));
	}

	render() {
		return (
			<MainBlock>
				<Block>
					<FlatButton label="<前一天" onClick={this.handlePrevDay} />
				</Block>
				<Block>
					<DatePicker
						hintText="Portrait Dialog"
						autoOk={true}
						value={new Date(this.props.initDate)}
						onChange={this.handleSelectDate}
						formatDate={date => moment(date).format('YYYY-MM-DD')}
						textFieldStyle={{ width: '5.5em' }}
					/>
				</Block>
				<Block>
					<FlatButton label="後一天>" onClick={this.handleNextDay} />
				</Block>
			</MainBlock>
		);
	}
}

const MainBlock = styled.div`
	flex: 1;
	display: inline-block;
`;

const Block = styled.div`
	display: inline-block;
	margin-left: 1em;
`;

export default DateSelector;
