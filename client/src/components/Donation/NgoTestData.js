import img1 from '../../ngo/中華民國發展遲緩兒童早期療育協會.jpg';
import img2 from '../../ngo/台灣原聲教育協會.jpg';
import img3 from '../../ngo/中華民國老人福利關懷協會.jpg';
import img4 from '../../ngo/台灣導盲犬協會.jpg';
import img5 from '../../ngo/台灣芒草心慈善協會.jpg';

import img6 from '../../ngo/台灣勞工陣線.jpg';
import img7 from '../../ngo/台灣更生保護會.jpg';
import img8 from '../../ngo/厲馨社會福利事業基金會.jpg';
import img9 from '../../ngo/台灣同志諮詢熱線協會.jpg';
import img10 from '../../ngo/台灣性別平等教育協會.jpg';

import img11 from '../../ngo/台灣少年權益與福利促進聯盟.jpg';
import img12 from '../../ngo/蠻野心足生態協會.jpg';
import img13 from '../../ngo/崔媽媽基金會.jpg';
import img14 from '../../ngo/台灣反迫遷連線.jpg';
import img15 from '../../ngo/台灣人權促進會.jpg';

import img16 from '../../ngo/綠色公民行動聯盟.jpg';
import img17 from '../../ngo/台灣再生能源推動聯盟.jpg';
import img18 from '../../ngo/環境法律人協會.jpg';
import img19 from '../../ngo/地球公民基金會.jpg';
import img20 from '../../ngo/中華鯨豚協會.jpg';

import logo1 from '../../ngo/中華民國發展遲緩兒童早期療育協會LOGO.jpg';
import logo2 from '../../ngo/台灣原聲教育協會LOGO.jpg';
import logo3 from '../../ngo/中華民國老人福利關懷協會LOGO.jpg';
import logo4 from '../../ngo/台灣導盲犬協會LOGO.jpg';
import logo5 from '../../ngo/台灣芒草心慈善協會LOGO.jpg';

import logo6 from '../../ngo/台灣勞工陣線LOGO.jpg';
import logo7 from '../../ngo/台灣更生保護會LOGO.jpg';
import logo8 from '../../ngo/厲馨社會福利事業基金會LOGO.jpg';
import logo9 from '../../ngo/台灣同志諮詢熱線協會LOGO.jpg';
import logo10 from '../../ngo/台灣性別平等教育協會LOGO.jpg';

import logo11 from '../../ngo/台灣少年權益與福利促進聯盟LOGO.jpg';
import logo12 from '../../ngo/蠻野心足生態協會LOGO.jpg';
import logo13 from '../../ngo/崔媽媽基金會LOGO.jpg';
import logo14 from '../../ngo/台灣反迫遷連線LOGO.jpg';
import logo15 from '../../ngo/台灣人權促進會LOGO.jpg';

import logo16 from '../../ngo/綠色公民行動聯盟LOGO.jpg';
import logo17 from '../../ngo/台灣再生能源推動聯盟LOGO.jpg';
import logo18 from '../../ngo/環境法律人協會LOGO.jpg';
import logo19 from '../../ngo/地球公民基金會LOGO.jpg';
import logo20 from '../../ngo/中華鯨豚協會LOGO.jpg';

const Ngos = [
	{
		userId: 3,
		label: '中華民國發展遲緩兒童早期療育協會',
		sublabel: '',
		web: `http://www.caeip.org.tw`,
		img: img1,
		logo: logo1,
		solar: 0,
		power: 0,
		income: 0
	},
	{
		userId: 4,
		label: '台灣原聲教育協會',
		sublabel: '',
		web: `http://voxnativa.org`,
		img: img2,
		logo: logo2,
		solar: 0,
		power: 0,
		income: 0
	},
	{
		userId: 5,
		label: '中華民國老人福利關懷協會',
		sublabel: '',
		web: `http://ewca.org.tw`,
		img: img3,
		logo: logo3,
		solar: 0,
		power: 0,
		income: 0
	},
	{
		userId: 6,
		label: '台灣導盲犬協會',
		sublabel: '',
		web: `http://www.guidedog.org.tw`,
		img: img4,
		logo: logo4,
		solar: 0,
		power: 0,
		income: 0
	},
	{
		userId: 7,
		label: '台灣芒草心慈善協會',
		sublabel: '',
		web: `https://www.homelesstaiwan.org`,
		img: img5,
		logo: logo5,
		solar: 0,
		power: 0,
		income: 0
	},
	{
		userId: 8,
		label: '台灣勞工陣線',
		sublabel: '',
		web: `http://labor.ngo.tw`,
		img: img6,
		logo: logo6,
		solar: 0,
		power: 0,
		income: 0
	},
	{
		userId: 9,
		label: '台灣更生保護會',
		sublabel: '',
		web: `http://www.after-care.org.tw`,
		img: img7,
		logo: logo7,
		solar: 0,
		power: 0,
		income: 0
	},
	{
		userId: 10,
		label: '厲馨社會福利事業基金會',
		sublabel: '',
		web: `https://www.goh.org.tw`,
		img: img8,
		logo: logo8,
		solar: 0,
		power: 0,
		income: 0
	},
	{
		userId: 11,
		label: '台灣同志諮詢熱線協會',
		sublabel: '',
		web: `https://hotline.org.tw`,
		img: img9,
		logo: logo9,
		solar: 0,
		power: 0,
		income: 0
	},
	{
		userId: 12,
		label: '台灣性別平等教育協會',
		sublabel: '',
		web: `https://www.tgeea.org.tw`,
		img: img10,
		logo: logo10,
		solar: 0,
		power: 0,
		income: 0
	},
	{
		userId: 13,
		label: '台灣少年權益與福利促進聯盟',
		sublabel: '',
		web: `http://www.youthrights.org.tw/about/alliance`,
		img: img11,
		logo: logo11,
		solar: 0,
		power: 0,
		income: 0
	},
	{
		userId: 14,
		label: '蠻野心足生態協會',
		sublabel: '',
		web: `http://zh.wildatheart.org.tw`,
		img: img12,
		logo: logo12,
		solar: 0,
		power: 0,
		income: 0
	},
	{
		userId: 15,
		label: '崔媽媽基金會',
		sublabel: '',
		web: `https://www.tmm.org.tw`,
		img: img13,
		logo: logo13,
		solar: 0,
		power: 0,
		income: 0
	},
	{
		userId: 16,
		label: '台灣反迫遷連線',
		sublabel: '',
		web: `https://www.taafe.org.tw`,
		img: img14,
		logo: logo14,
		solar: 0,
		power: 0,
		income: 0
	},
	{
		userId: 17,
		label: '台灣人權促進會',
		sublabel: '',
		web: `https://www.tahr.org.tw`,
		img: img15,
		logo: logo15,
		solar: 0,
		power: 0,
		income: 0
	},
	{
		userId: 18,
		label: '綠色公民行動聯盟',
		sublabel: '',
		web: `http://www.gcaa.org.tw`,
		img: img16,
		logo: logo16,
		solar: 0,
		power: 0,
		income: 0
	},
	{
		userId: 19,
		label: '台灣再生能源推動聯盟',
		sublabel: '',
		web: `http://taiwanrena.blogspot.com`,
		img: img17,
		logo: logo17,
		solar: 0,
		power: 0,
		income: 0
	},
	{
		userId: 20,
		label: '環境法律人協會',
		sublabel: '',
		web: `http://www.eja.org.tw`,
		img: img18,
		logo: logo18,
		solar: 0,
		power: 0,
		income: 0
	},
	{
		userId: 21,
		label: '地球公民基金會',
		sublabel: '',
		web: `https://www.cet-taiwan.org`,
		img: img19,
		logo: logo19,
		solar: 0,
		power: 0,
		income: 0
	},
	{
		userId: 22,
		label: '中華鯨豚協會',
		sublabel: '',
		web: `http://www.whale.org.tw`,
		img: img20,
		logo: logo20,
		solar: 0,
		power: 0,
		income: 0
	}
];

export default Ngos;
