import React from 'react';
import styled from 'styled-components';

import { Column } from '../Layout';
import IconSvg from '../../svg/icon_green.svg';

import BigNumber from 'bignumber.js';
const BN = BigNumber.clone({ DECIMAL_PLACES: 16, ROUNDING_MODE: 1 });

const NgoItem = ({
	img,
	logo,
	label,
	sublabel,
	web,
	showDonation,
	solar,
	power,
	income,
	openDonation
}) => (
	<Div height={showDonation ? '515px;' : ' 445px'}>
		<Img src={img} />
		<Icon src={logo} />
		<Content>
			<TitleContainer>
				<Title>
					<TitleIcon src={IconSvg} />
					<TitleLabel>
						{label}
						<TitleSubLabel>{sublabel}</TitleSubLabel>
					</TitleLabel>
				</Title>
				<TitleRight>
					<More
						onClick={() => {
							var win = window.open(web, '_blank');
							win.focus();
						}}
					>
						more
					</More>
				</TitleRight>
			</TitleContainer>
			<Hr />
			<Column>
				<Item>
					<Label>{'已募集  /  KWp'}</Label>
					<Value>{BN(solar).toFormat(1)}</Value>
				</Item>
				<Item>
					<Label>{'累積發電量  /  度'}</Label>
					<Value>{BN(power).toFormat(1)}</Value>
				</Item>
				<Item>
					<Label>{'總收入  /  元'}</Label>
					<Value>{BN(income).toFormat(1)}</Value>
				</Item>
			</Column>
			{showDonation ? <Hr /> : null}
			{showDonation ? (
				<DonationContainer>
					<Donation onClick={openDonation}>捐贈</Donation>
				</DonationContainer>
			) : null}
		</Content>
	</Div>
);

const Div = styled.div`
	width: 439px;
	height: ${props => props.height};
	background-color: #26262b;
	border-radius: 8px;
	margin-bottom: 20px;
	position: relative;
	@media screen and (max-width: 1280px) {
		width: 411px;
	}
	@media screen and (max-width: 1024px) {
		width: 439px;
	}
`;

const Img = styled.img`
	width: 100%;
	height: 300px;
	border-radius: 8px 8px 0 0;
`;

const Icon = styled.img`
	position: absolute;
	top: 220px;
	left: 359px;
	width: 60px;
	height: 60px;
	border: 3px solid rgba(255, 255, 255, 0.2);
	border-radius: 50%;
	@media screen and (max-width: 1280px) {
		left: 331px;
	}
	@media screen and (max-width: 1024px) {
		left: 359px;
	}
`;

const Content = styled.div`
	flex: 1;
	padding: 0 15px;
`;

const TitleContainer = styled.div`
	height: 60px;
	display: flex;
	justify-content: space-between;
`;

const Title = styled.div`
	display: flex;
	justify-content: flex-start;
	align-items: center;
`;

const TitleIcon = styled.img`
	width: 28px;
	height: 28px;
	margin-right: 5px;
`;

const TitleLabel = styled.span`
	font-size: 17px;
	color: #aaabad;
	@media screen and (max-width: 1280px) {
		font-size: 15px;
	}
	@media screen and (max-width: 1024px) {
		font-size: 17px;
	}
`;

const TitleSubLabel = styled.div`
	font-size: 12px;
	color: #707070;
`;

const TitleRight = styled.div`
	display: flex;
	justify-content: flex-end;
	align-items: center;
`;

const Hr = styled.hr`
	height: 1px;
	margin: 0px;
	border: none;
	border-top: 1px solid #37373e;
`;

const More = styled.div`
	color: grey;
	border: 1px solid grey;
	border-radius: 30px;
	padding: 1px 1em 1px 1em;
	margin-right: 0.5em;
	font-size: 14px;
	cursor: pointer;
`;

const Item = styled.div`
	flex: 1;
	font-family: Montserrat;
`;

const Label = styled.div`
	font-size: 12px;
	color: #707070;
	margin: 12px 0;
`;

const Value = styled.div`
	font-size: 24px;
	color: #f7b100;
	margin-bottom: 12px;
`;

const DonationContainer = styled.div`
	width: 100%;
	height: 70px;
	display: flex;
	align-items: center;
	justify-content: center;
`;

const Donation = styled.div`
	width: 150px;
	height: 33px;
	color: #f7b100;
	border: 1px solid #f7b100;
	border-radius: 50px;
	font-size: 18px;
	cursor: pointer;
	display: flex;
	align-items: center;
	justify-content: center;
`;

export default NgoItem;
