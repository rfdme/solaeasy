import React, { Component } from 'react';
import axios from 'axios';
import styled from 'styled-components';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import { white, amber600 } from 'material-ui/styles/colors';

import Footer from '../Footer';
import { Page } from '../Layout';
import NgoItem from './NgoItem';
import Ngos from './NgoTestData';
import IconSvg from '../../svg/icon_green.svg';

import BigNumber from 'bignumber.js';
const BN = BigNumber.clone({ DECIMAL_PLACES: 16, ROUNDING_MODE: 1 });

class Donation extends Component {
	constructor(props) {
		super(props);

		this.handleOpen = this.handleOpen.bind(this);
		this.handleClose = this.handleClose.bind(this);
		this.handleOk = this.handleOk.bind(this);
		this.handleSelectChange = this.handleSelectChange.bind(this);
		this.handleCloseMsgDialog = this.handleCloseMsgDialog.bind(this);

		this.state = {
			open: false,
			ngoIndex: null,
			label: '',
			sublabel: '',
			message: '',
			selectedIndex: null,
			selectedPvId: null,
			disabled: false,
			pvList: [],
			ngoAsset: {},
			isMsgDialogOpen: false,
			msgDialogData: {
				title: '',
				message: ''
			}
		};
	}

	componentDidMount() {
		this.init();
	}

	init() {
		if (this.props.auth.userId) {
			this.loadUserAsset();
		}

		this.loadNgoAsset();
	}

	loadUserAsset() {
		axios
			.get(`/api/v1/donation/userAsset/` + this.props.auth.userId, {
				headers: { Authorization: this.props.auth.token }
			})
			.then(res => {
                if(res.data.status === true) {
                    this.setState({ pvList: res.data.result.userAsset });
                } else {
                    // TODO: handle error_code, msg
                }
			})
			.catch(error => {
				console.error(error);
			});
	}

	loadNgoAsset() {
		axios
			.get(`/api/v1/donation/ngoAsset`)
			.then(res => {
                if(res.data.status === true) {
                    let ngoAsset = {};
                    for (let i = 0; i < res.data.result.ngoAsset.length; i++) {
                        const d = res.data.result.ngoAsset[i];
                        let income = BN(d.income);
                        if (income.lt(0)) {
                            income = BN(0);
                        }
                        //TODO: 暫時先隨便預估
                        let power = income.div(d.sellPrice).times(3.3);
                        //TODO: 暫時粗估
                        let solar = d.amount / 1000;

                        ngoAsset[d.id.toString()] = {
                            solar: solar,
                            power: power,
                            income: income
                        };
                    }
                    this.setState({ ngoAsset: ngoAsset });
                } else {
                    // TODO: handle error_code, msg
                }
			})
			.catch(error => {
				console.error(error);
			});
	}

	handleOpen(i) {
		if (this.state.pvList.length === 0) {
			this.openMsgDialog('無資產可捐贈');
			return;
		}

		this.setState({
			open: true,
			ngoIndex: i,
			label: Ngos[i].label,
			sublabel: Ngos[i].sublabel,
			selectedIndex: null,
			selectedPvId: null,
			message: '',
			disabled: false
		});
	}

	handleClose() {
		this.setState({ open: false });
	}

	handleOk() {
		if (this.state.selectedPvId === null) {
			this.setState({ message: '請選擇電廠代號' });
			return;
		}

		const floatAmount = parseFloat(this.refs.amount.input.value, 10);
		const amount = parseInt(this.refs.amount.input.value, 10);
		if (amount !== floatAmount) {
			this.setState({ message: '請輸入整數數量' });
			return;
		}
		if (!(amount > 0)) {
			this.setState({ message: '請輸入大於0的數量' });
			return;
		}

		const { remained } = this.state.pvList[this.state.selectedIndex];
		if (remained < amount) {
			this.setState({ message: this.state.selectedPvId + '餘額不足' });
			return;
		}

		this.setState({ message: '捐贈處理中', disabled: true });

		axios
			.post(
				`/api/v1/donation/donate`,
				{
					giver: parseInt(this.props.auth.userId, 10),
					receiver: Ngos[this.state.ngoIndex].userId,
					pvId: this.state.selectedPvId,
					amount: amount
				},
				{
					headers: { Authorization: this.props.auth.token }
				}
			)
			.then(res => {
                if(res.data.status === true) {
                    setTimeout(() => {
                        this.handleClose();
                        if (res.data.result.response === 'ok') {
                            this.openMsgDialog('已捐贈成功');
                        } else {
                            this.openMsgDialog(res.data.msg);
                        }
                        this.init();
                    }, 3000);
                } else {
                    // TODO: handle error_code, msg
                }
			})
			.catch(error => {
				console.error(error);
			});
	}

	handleSelectChange(event, index, value) {
		this.setState({ selectedIndex: index, selectedPvId: value });
	}

	handleCloseMsgDialog() {
		this.setState({ isMsgDialogOpen: false });
	}

	openMsgDialog(msg) {
		this.setState({
			isMsgDialogOpen: true,
			msgDialogData: {
				title: '提示訊息',
				message: msg
			}
		});
	}

	render() {
		let items = [];
		for (let i = 0; i < Math.ceil(Ngos.length / 3) * 3; i++) {
			if (i < Ngos.length) {
				const ngo = Ngos[i];
				const asset = this.state.ngoAsset[ngo.userId.toString()];
				items.push(
					<NgoItem
						key={i}
						img={ngo.img}
						logo={ngo.logo}
						label={ngo.label}
						sublabel={ngo.sublabel}
						web={ngo.web}
						solar={asset ? asset.solar : ngo.solar}
						power={asset ? asset.power : ngo.power}
						income={asset ? asset.income : ngo.income}
						showDonation={this.props.auth.token ? true : false}
						openDonation={() => this.handleOpen(i)}
					/>
				);
			} else {
				items.push(<Empty key={i} />);
			}
		}

		let pvs = [];
		for (let i = 0; i < this.state.pvList.length; i++) {
			const pv = this.state.pvList[i];
			pvs.push(
				<MenuItem
					key={i}
					value={pv.id}
					primaryText={`${pv.id} ${pv.name}`}
				/>
			);
		}
		const actions = [
			<div style={{ padding: '0 39px 15px 39px' }}>
				<RaisedButton
					label="確認捐贈"
					fullWidth={true}
					onClick={this.handleOk}
					backgroundColor={amber600}
					labelColor={white}
					labelStyle={{ fontSize: '18px' }}
					disabled={this.state.disabled}
				/>
			</div>
		];
		const msgActions = [
			<FlatButton
				label="確認"
				primary={true}
				keyboardFocused={true}
				onClick={this.handleCloseMsgDialog}
			/>
		];
		return (
			<Page>
				<div className="page-wrap container">
					<div
						style={{
							display: 'flex',
							flexWrap: 'wrap',
							alignItems: 'center',
							justifyContent: 'space-around'
						}}
					>
						{items}
					</div>
				</div>
				<Footer />
				<Dialog
					actions={actions}
					modal={false}
					open={this.state.open}
					onRequestClose={this.handleClose}
					autoScrollBodyContent={true}
					contentStyle={{
						width: '400px'
					}}
				>
					<TitleContainer>
						<Title>
							<TitleIcon src={IconSvg} />
							<TitleLabel>
								{this.state.label}
								<TitleSubLabel>
									{this.state.sublabel}
								</TitleSubLabel>
							</TitleLabel>
						</Title>
					</TitleContainer>
					<Input>
						<InputLabel>電廠代號</InputLabel>
						<DropDownMenu
							onChange={this.handleSelectChange}
							style={{ width: '100%', fontSize: '20px' }}
							autoWidth={false}
							value={this.state.selectedPvId}
						>
							{pvs}
						</DropDownMenu>
					</Input>
					<Input>
						<InputLabel>捐贈數量</InputLabel>
						<div style={{ margin: '0 24px' }}>
							<TextField
								name="amount"
								type="number"
								ref="amount"
								inputStyle={{
									color: '#f7b100',
									fontSize: '24px'
								}}
								fullWidth
							/>
						</div>
					</Input>
					<div
						style={{
							color: 'red',
							marginTop: '20px',
							textAlign: 'center'
						}}
					>
						{this.state.message}
					</div>
				</Dialog>
				<Dialog
					title={this.state.msgDialogData.title}
					actions={msgActions}
					modal={false}
					open={this.state.isMsgDialogOpen}
					onRequestClose={this.handleCloseMsgDialog}
					autoScrollBodyContent={true}
				>
					<div>{this.state.msgDialogData.message}</div>
				</Dialog>
			</Page>
		);
	}
}

const TitleContainer = styled.div`
	height: 60px;
	display: flex;
	justify-content: space-between;
`;

const Title = styled.div`
	display: flex;
	justify-content: flex-start;
	align-items: center;
`;

const TitleIcon = styled.img`
	width: 36px;
	height: 36px;
	margin-left: -8px;
`;

const TitleLabel = styled.span`
	font-size: 22px;
	color: #aaabad;
`;

const TitleSubLabel = styled.div`
	font-size: 15px;
	color: #707070;
`;

const Input = styled.div`
	margin-top: 40px;
`;

const InputLabel = styled.div`
	font-size: 15px;
`;

const Empty = styled.div`
	width: 439px;
	@media screen and (max-width: 1280px) {
		width: 411px;
	}
	@media screen and (max-width: 1024px) {
		width: 439px;
	}
`;

export default Donation;
