import React from 'react';
import { ProgressBar, PercentageCircle, SelectedPoint } from '../Widget';

const Test = props => {
	return (
		<div>
			<div>Test</div>
			<ProgressBar value={0.1} />
			<PercentageCircle value={0.3} />
			<SelectedPoint x={500} y={500} color={'red'} />
		</div>
	);
};

export default Test;
