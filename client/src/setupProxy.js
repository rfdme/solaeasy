const proxy = require('http-proxy-middleware');
const keys = require('./config');

module.exports = function(app) {
  app.use(proxy('/api/**', { target: keys.proxyTarget }));
};