package main

import (
	"fmt"
	"html/template"
	"net/http"
)

func main() {
	start()
}

func start() {
	fmt.Println("starting")
	webServer()
}

func webServer() {
	http.HandleFunc("/SolaGame", handleUI)
	http.Handle("/", http.FileServer(http.Dir("./")))

	err := http.ListenAndServe(":9101", nil)

	if err != nil {
		panic("ListenAndServe: " + err.Error())
	}
}

func handleUI(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("./SolaGame.html")
	t.Execute(w, nil)
}
