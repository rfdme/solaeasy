FROM node:8.12-alpine

COPY server /app/asset/server

# --no-cache: download package index on-the-fly, no need to cleanup afterwards
# --virtual: bundle packages, remove whole bundle at once, when done
RUN apk --no-cache --virtual build-dependencies add \
    python \
    make \
    g++ \
    && cd /app/asset/server/websocket && npm install \
    && apk del build-dependencies

# busybox-extras contains telnet
RUN apk add busybox-extras