#!/bin/bash

project=asset

pip install ansible==2.6.3
#
mkdir /root/.ssh
echo -n "$GIT_SSH_KEY" > /root/.ssh/id_rsa
chmod 600 /root/.ssh/id_rsa
#
touch /root/.ssh/known_hosts
chmod 600 /root/.ssh/known_hosts
ssh-keyscan -p "$GIT_HOST_PORT" -H "$GIT_HOST_NAME" > /etc/ssh/ssh_known_hosts 2> /dev/null
#
set -e
git clone "ssh://git@$GIT_HOST_NAME:$GIT_HOST_PORT/cm/$project.git"
cd $project
echo "$ANSIBLE_VAULT_SE" > ansible.vault.se
# ansible-vault decrypt --vault-password-file=ansible.vault.se inventories/staging/vault_files/staging.solase