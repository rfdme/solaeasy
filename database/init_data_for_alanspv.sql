TRUNCATE TABLE accounts;
INSERT INTO accounts ("userId", name, telephone, address, "cashoutAccount") VALUES
(1,'索拉艾倫股份有限公司','待確認','台北市中山區南京東路二段132號8樓','待確認'),
(2,'勵得國際股份有限公司','待確認','台北市中山區南京東路二段132號8樓','待確認'),
(3,'亞太國際物流股份有限公司','待確認','待確認','待確認'),
(4,'至怡投資股份有限公司','待確認','待確認','待確認'),
(5,'實豐投資股份有限公司','待確認','待確認','待確認'),
(6,'誼達投資有限公司','待確認','待確認','待確認'),
(7,'索能科技股份有限公司','待確認','待確認','待確認');

TRUNCATE TABLE apikeys RESTART IDENTITY;
INSERT INTO apikeys (key, secret, "readInfo", trade, withdraw, "userId") VALUES
('649f41cc2166f951dc18853f32fa5d57', '25107a27c336e70924d5e912ba3775548179fd4a0d1dfb86d30b5af270f49cb8', true, false, false, 1);

TRUNCATE TABLE "billsPreparing" RESTART IDENTITY;
INSERT INTO "billsPreparing" ("pvId", "rangeBegin", "rangeEnd", "totalPower", "incomeWithTax", "meterRentCost", "updateDt") VALUES
('P0001','2018-02-14','2018-05-30',47236,261669,873,'2018-02-17 03:05:06'),
('P0002','2018-02-14','2018-05-30',33495,169205,873,'2018-02-17 03:05:06'),
('P0001','2018-05-30','2018-06-11',4117,22806,100,'2018-06-02 03:05:06'),
('P0002','2018-05-30','2018-06-11',3556,17963,99,'2018-06-02 03:05:06'),
('P0001','2018-06-11','2018-07-11',12829,71067,250,'2018-07-14 03:05:06'),
('P0002','2018-06-11','2018-07-11',10671,53906,249,'2018-07-14 03:05:06'),
('P0001','2018-07-11','2018-08-07',12510,69300,225,'2018-08-10 03:05:06'),
('P0002','2018-07-11','2018-08-07',10431,52694,224,'2018-08-10 03:05:06'),
('P0001','2018-08-07','2018-08-10',1039,5756,25,'2018-08-13 03:05:06'),
('P0002','2018-08-07','2018-08-10',799,4036,24,'2018-08-13 03:05:06'),
('N0002','2018-04-26','2018-08-23',17824,89657,608,'2018-08-26 03:05:06'),
('P0001','2018-08-10','2018-09-11',11830,65534,250,'2018-09-14 03:05:06'),
('P0002','2018-08-10','2018-09-11',9192,46435,249,'2018-09-14 03:05:06'),
('P0001','2018-09-11','2018-10-11',11950,66198,250,'2018-10-14 03:05:06'),
('P0002','2018-09-11','2018-10-11',8672,43808,249,'2018-10-14 03:05:06'),
('N0002','2018-08-23','2018-10-16',7154,35986,280,'2018-10-19 03:05:06'),
('P0001','2018-10-11','2018-11-09',11031,61107,250,'2018-11-12 03:05:06'),
('P0002','2018-10-11','2018-11-09',6474,32704,249,'2018-11-12 03:05:06'),
('P0001','2018-11-09','2018-12-11',10671,59113,250,'2018-12-13 03:05:06'),
('P0002','2018-11-09','2018-12-11',5955,30082,249,'2018-12-13 03:05:06'),
('N0002','2018-10-16','2018-12-14',6114,30755,312,'2018-12-15 03:05:06'),
('P0001','2018-12-11','2019-01-10',9112,50477,250,'2019-01-13 03:05:06'),
('P0002','2018-12-11','2019-01-10',5036,25440,249,'2019-01-13 03:05:06');


TRUNCATE TABLE config RESTART IDENTITY;
INSERT INTO config (field, value) VALUES
('bcTransaction', 'false'),
('schedule', 'true');

TRUNCATE TABLE "eventByPV" RESTART IDENTITY;
INSERT INTO "eventByPV" ("pvId", date, name, content, "updateDt") VALUES
('P0001', '2017-11-30', '租約公證',  'TODO content', '2018-01-13 03:05:06'),
('P0001', '2017-07-25', '取得併聯審查意見書', 'TODO content', '2018-01-13 03:05:06'),
('P0001', '2017-11-03', '取得同意備案函', 'TODO content', '2018-01-13 03:05:06'),
('P0001', '2018-02-14', '掛表併聯', 'TODO content', '2018-01-13 03:05:06'),
('P0001', '2018-05-17', '能源局設備登記', 'TODO content', '2018-01-13 03:05:06'),
('P0001', '2018-05-31', '建置完成', 'TODO content', '2018-01-13 03:05:06'),
('P0002', '2017-11-30', '租約公證',  'TODO content', '2018-01-13 03:05:06'),
('P0002', '2017-09-28', '取得併聯審查意見書', 'TODO content', '2018-01-13 03:05:06'),
('P0002', '2017-11-16', '取得同意備案函', 'TODO content', '2018-01-13 03:05:06'),
('P0002', '2018-02-14', '掛表併聯', 'TODO content', '2018-01-13 03:05:06'),
('P0002', '2018-05-23', '能源局設備登記', 'TODO content', '2018-01-13 03:05:06'),
('P0002', '2018-06-07', '建置完成', 'TODO content', '2018-01-13 03:05:06'),
('H0001', '2018-01-23', '租約公證',  'TODO content', '2018-01-13 03:05:06'),
('H0001', '2018-03-30', '取得併聯審查意見書', 'TODO content', '2018-01-13 03:05:06'),
('H0001', '2018-05-14', '取得同意備案函', 'TODO content', '2018-01-13 03:05:06'),
('N0001', '2018-09-25', '取得併聯審查意見書', 'TODO content', '2018-01-13 03:05:06');

TRUNCATE TABLE "pvInsurance";
INSERT INTO "pvInsurance" ("pvId", "beginDate", "endDate", cost) VALUES
('P0001', '2018-08-09', '2019-08-09', 17071),
('P0002', '2018-08-09', '2019-08-09', 13636),
('N0002', '2018-08-09', '2019-08-09', 4618),
('H0001', '2018-12-18', '2019-12-18', 20826),
('D0001', '2019-01-03', '2020-01-03', 7916);

TRUNCATE TABLE pvs;
INSERT INTO pvs (
    id,
    name,
    "projectCode",
    location,
    "coordLat",
    "coordLng",
    capacity,
    decay,
    "annualAmount",
    stage,
    status,
    "totalCapital",
    "ownedCapital",
    "initialPrice",
    "loanAmount",
    "loanPeriod",
    "loanRate",
    "loanBeginDate",
    "validityDateBegin",
    "validityDateEnd",
    "sellPrice",
    "operationRateByCapital",
    "operationRateByIncome",
    "serviceRateByIncome",
    "rentRateByIncome",
    "rentYearCost",
    "rentWithTax", 
    "owner") VALUES
('P0001', '雲林褒忠雞舍1', 'T6308', '雲林縣褒忠鄉馬鳴村鎮安路203-3號', 23.702838, 120.274352, 99.9, 0.007, 1350, 7, 0, 6398595, 1699549, 10, NULL, NULL, NULL, NULL, '2018-02-14', '2038-02-14', 5.2758, 0, 0.11, 0.035, 0.05, 0, false, '東迪能源股份有限公司'),
('P0002', '雲林褒忠雞舍2', 'T6309', '雲林縣褒忠鄉馬鳴村鎮安路203-3號', 23.702838, 120.274352, 79.8, 0.007, 1350, 7, 0, 4608450, 1231913, 10, NULL, NULL, NULL, NULL, '2018-02-14', '2038-02-14', 4.8111, 0, 0.10, 0.035, 0.05, 0, false, '東迪能源股份有限公司'),
('H0001', '桃園民享', 'T6338', '桃園市新屋區新榮路583號', 24.952536, 121.151069, 156.465, 0.007, 1131, 7, 0, 8251706, 2219392, 10, NULL, NULL, NULL, NULL, '2018-08-21', '2038-08-21', 5.4324, 0, 0.11, 0.035, 0, 71400, true, '東迪能源股份有限公司'),
('N0001', '彰化竹塘', 'T7321', '彰化縣竹塘鄉河陽路建173號', 23.842770, 120.404457, 280.24, 0.007, 1314, 3, 1, 14966539, 4021875, 10, NULL, NULL, NULL, NULL, NULL, NULL, 4.6254, 0, 0.1, 0.035, 0.08, 0, false, '東迪能源股份有限公司'),
('B0001', '台中啟貿', 'T6173', '台中市大雅區三和里7鄰仁和北路和62、66號', 24.218429, 120.660259, 265.98, 0.007, 1237, 3, 1, 12455564, 3379871, 10, NULL, NULL, NULL, NULL, NULL, NULL, 4.6254, 0, 0.1, 0.035, 0, 200000, false, '東迪能源股份有限公司'),
('N0002', '彰化蕭明清', 'T6176', '彰化縣溪州鄉瓦厝村埤圳北路200號', 23.854250, 120.495692, 33, 0.007, 1314, 7, 0, 1874669, 501667, 10, NULL, NULL, NULL, NULL, '2018-04-26', '2033-04-26', 4.7906, 0, 0.11, 0.035, 0.06, 0, false, '東迪能源股份有限公司'),
('D0001', '台南豐禾', 'T7030', '台南市學甲區中洲里中洲1-1號', 23.238457, 120.161461, 53.68, 0.007, 1300, 5, 0, 2987292, 800503, 10, NULL, NULL, NULL, NULL, NULL, NULL, 4.9698, 0, 0.1, 0.035, 0, 28350, false, '東迪能源股份有限公司'),
('D0002', '台南耘揚', 'T7031', '台南市山上區新庄里新庄80號', 23.094101, 120.360025, 248, 0.007, 1300, 3, 1, 12759600, 3437900, 10, NULL, NULL, NULL, NULL, NULL, NULL, 4.6254, 0, 0.1, 0.035, 0.08, 0, false, '東迪能源股份有限公司'),
('K0001', '苗栗耀順', 'T6351', '苗栗縣後龍鎮龍坑里15鄰十班坑153-8號', 24.595132, 120.779264, 58.56, 0.007, 1150, 3, 1, 3546874, 945279, 10, NULL, NULL, NULL, NULL, NULL, NULL, 5.7153, 0, 0.1, 0.035, 0, 94500, false, '東迪能源股份有限公司'),
('E0001', '高雄中庄國小', 'T7386', '待確認', 22.634509, 120.397564,89.28,0.007,1314,1,1,4637516,1104171,10,NULL,NULL,NULL,NULL,NULL,NULL,4.6254,NULL,NULL,NULL,NULL,NULL,NULL,'索能科技股份有限公司'),
('E0002', '高雄建山國小', 'T7389', '待確認', 23.105464, 120.682581,99.82,0.007,1270,1,1,5185000,1234524,10,NULL,NULL,NULL,NULL,NULL,NULL,4.9698,NULL,NULL,NULL,NULL,NULL,NULL,'索能科技股份有限公司'),
('E0003', '高雄六龜國小', 'T7390', '待確認', 22.996605, 120.634430,217.62,0.007,1314,1,1,11303944,2691415,10,NULL,NULL,NULL,NULL,NULL,NULL,4.6254,NULL,NULL,NULL,NULL,NULL,NULL,'索能科技股份有限公司'),
('E0004', '高雄八卦國小', 'T7395', '待確認', 22.689174, 120.333318,297.6,0.007,1314,1,1,15458386,3680568,10,NULL,NULL,NULL,NULL,NULL,NULL,4.6254,NULL,NULL,NULL,NULL,NULL,NULL,'索能科技股份有限公司'),
('E0005', '高雄永清國小', 'T7385', '待確認', 22.675184, 120.286842,206.46,0.007,1314,1,1,10724255,2553394,10,NULL,NULL,NULL,NULL,NULL,NULL,4.6254,NULL,NULL,NULL,NULL,NULL,NULL,'索能科技股份有限公司'),
('E0006', '高雄梓官國小', 'T7397', '待確認', 22.760303, 120.264535,341,0.007,1314,1,1,17712734,4217318,10,NULL,NULL,NULL,NULL,NULL,NULL,4.6254,NULL,NULL,NULL,NULL,NULL,NULL,'索能科技股份有限公司'),
('E0007', '高雄中正國小', 'T7418', '待確認', 22.628005, 120.374683,350,0.007,1314,1,1,18180225,4328625,10,NULL,NULL,NULL,NULL,NULL,NULL,4.6254,NULL,NULL,NULL,NULL,NULL,NULL,'索能科技股份有限公司'),
('E0008', '高雄鹽埕國小', 'T7416', '待確認', 22.623750, 120.282946,499,0.007,1314,1,1,25919807,6171383,10,NULL,NULL,NULL,NULL,NULL,NULL,4.6254,NULL,NULL,NULL,NULL,NULL,NULL,'索能科技股份有限公司'),
('E0009', '高雄新莊高中1', 'T7396','待確認', 22.681407, 120.317583,499,0.007,1314,1,1,25919807,6171383,10,NULL,NULL,NULL,NULL,NULL,NULL,4.6254,NULL,NULL,NULL,NULL,NULL,NULL,'索能科技股份有限公司'),
('E0010', '高雄六龜高中', 'T7391', '待確認', 22.993459, 120.635446,181.66,0.007,1314,1,1,9436056,2246680,10,NULL,NULL,NULL,NULL,NULL,NULL,4.6254,NULL,NULL,NULL,NULL,NULL,NULL,'索能科技股份有限公司');


TRUNCATE TABLE "userAssets";
INSERT INTO "userAssets" ("userId", "pvId", amount, "lockAmount") VALUES
(1, 'P0001', 169954.9, 0.0),
(1, 'P0002', 123191.3, 0.0),
(1, 'H0001', 221939.2, 0.0),
(1, 'N0001', 402187.5, 0.0),
(1, 'B0001', 159984.1, 0.0),
(2, 'B0001', 178003.0, 0.0),
(2, 'N0002',  50166.7, 0.0),
(2, 'D0001',  80050.3, 0.0),
(2, 'D0002', 343790.0, 0.0),
(1, 'K0001',  54172.8, 0.0),
(2, 'K0001',  40355.1, 0.0),
(6, 'E0001',   9742.7, 0.0),
(5, 'E0001',  16237.8, 0.0),
(4, 'E0001',  16237.8, 0.0),
(3, 'E0001',  68198.8, 0.0),
(6, 'E0002',  10892.9, 0.0),
(5, 'E0002',  18154.8, 0.0),
(4, 'E0002',  18154.8, 0.0),
(3, 'E0002',  76249.9, 0.0),
(6, 'E0003',  23747.8, 0.0),
(5, 'E0003',  39579.6, 0.0),
(4, 'E0003',  39579.6, 0.0),
(3, 'E0003', 166234.5, 0.0),
(6, 'E0004',  32475.6, 0.0),
(5, 'E0004',  54126.0, 0.0),
(4, 'E0004',  54126.0, 0.0),
(3, 'E0004', 227329.2, 0.0),
(6, 'E0005',  22529.9, 0.0),
(5, 'E0005',  37549.9, 0.0),
(4, 'E0005',  37549.9, 0.0),
(3, 'E0005', 157709.7, 0.0),
(6, 'E0006',  37211.6, 0.0),
(5, 'E0006',  62019.4, 0.0),
(4, 'E0006',  62019.4, 0.0),
(3, 'E0006', 260481.4, 0.0),
(6, 'E0007',  38193.8, 0.0),
(5, 'E0007',  63656.3, 0.0),
(4, 'E0007',  63656.3, 0.0),
(3, 'E0007', 267356.1, 0.0),
(6, 'E0008',  54453.4, 0.0),
(5, 'E0008',  90755.6, 0.0),
(4, 'E0008',  90755.6, 0.0),
(3, 'E0008', 381173.7, 0.0),
(6, 'E0009',  54453.4, 0.0),
(5, 'E0009',  90755.6, 0.0),
(4, 'E0009',  90755.6, 0.0),
(3, 'E0009', 381173.7, 0.0),
(6, 'E0010',  16298.9, 0.0),
(5, 'E0010',  27165.0, 0.0),
(4, 'E0010',  27165.0, 0.0),
(3, 'E0010', 114093.0, 0.0),
(7, 'E0010',  39946.1, 0.0);

TRUNCATE TABLE users RESTART IDENTITY;
INSERT INTO users (name, email, pwd, sttw, "lockSttw") VALUES
('索拉艾倫', 'alan@solabloc.com', '$2b$10$b9wjGtNcd5yEzY2.Q5LEUu0wn31LNF46mEAl4RKDpDo/8l.kXmTYW', 0, 0),
('勵得國際', 'chriswang917@gmail.com', '$2b$10$b9wjGtNcd5yEzY2.Q5LEUu0wn31LNF46mEAl4RKDpDo/8l.kXmTYW', 0, 0),
('亞太國際物流', 'apadm@mail.apli.com.tw', '$2b$10$b9wjGtNcd5yEzY2.Q5LEUu0wn31LNF46mEAl4RKDpDo/8l.kXmTYW', 0, 0),
('至怡投資', 'crowninvestment67@gmail.com', '$2b$10$b9wjGtNcd5yEzY2.Q5LEUu0wn31LNF46mEAl4RKDpDo/8l.kXmTYW', 0, 0),
('實豐投資', 'shifeng5078@gmail.com', '$2b$10$b9wjGtNcd5yEzY2.Q5LEUu0wn31LNF46mEAl4RKDpDo/8l.kXmTYW', 0, 0),
('誼達投資', 'eda.invest77@gmail.com', '$2b$10$b9wjGtNcd5yEzY2.Q5LEUu0wn31LNF46mEAl4RKDpDo/8l.kXmTYW', 0, 0),
('索能科技', 'alantruekimo@gmail.com', '$2b$10$b9wjGtNcd5yEzY2.Q5LEUu0wn31LNF46mEAl4RKDpDo/8l.kXmTYW', 0, 0);