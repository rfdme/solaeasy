DROP TABLE IF EXISTS public.accounts;
CREATE TABLE public.accounts
(
    "userId" integer NOT NULL,
    name character varying COLLATE pg_catalog."default",
    telephone character varying COLLATE pg_catalog."default",
    address character varying COLLATE pg_catalog."default",
    "cashoutAccount" character varying COLLATE pg_catalog."default",
    CONSTRAINT accounts_pkey PRIMARY KEY ("userId")
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public.apikeys;
CREATE TABLE public.apikeys
(
    id serial NOT NULL,
    key character(32) COLLATE pg_catalog."default" NOT NULL,
    secret character(64) COLLATE pg_catalog."default" NOT NULL,
    "readInfo" boolean NOT NULL,
    trade boolean NOT NULL,
    withdraw boolean NOT NULL,
    "userId" integer NOT NULL,
    CONSTRAINT apikeys_pkey PRIMARY KEY (id, key, secret)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public.appointment;
CREATE TABLE public.appointment
(
    "userId" integer NOT NULL,
    "managerId" integer NOT NULL,
    CONSTRAINT appointment_pkey PRIMARY KEY ("userId", "managerId")
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public."bcTransation";
CREATE TABLE public."bcTransation"
(
    id bigserial NOT NULL,
    "txType" character varying COLLATE pg_catalog."default" NOT NULL,
    "txId" bigint NOT NULL,
    data json NOT NULL,
    status boolean,
    CONSTRAINT "bcTransation_pkey" PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public."bcTransationHistory";
CREATE TABLE public."bcTransationHistory"
(
    id bigint NOT NULL,
    "txType" character varying COLLATE pg_catalog."default" NOT NULL,
    "txId" bigint NOT NULL,
    data json NOT NULL,
    info json NOT NULL,
    CONSTRAINT "bcTransationHistory_pkey" PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public."billsPreparing";
CREATE TABLE public."billsPreparing"
(
    id serial NOT NULL,
    "pvId" character varying COLLATE pg_catalog."default" NOT NULL,
    "file" character varying COLLATE pg_catalog."default",
    "rangeBegin" date NOT NULL,
    "rangeEnd" date NOT NULL,
    "totalPower" integer NOT NULL,
    "incomeWithTax" integer NOT NULL,
    "meterRentCost" integer NOT NULL,
    "profit" integer,
    "rentCostFixed" integer,
    "rentCostFloat" integer,
    "operateCostFixed" integer,
    "operateCostFloat" integer,
    "serviceCostFloat" integer,
    "insuranceCost" integer,
    "loanCost" integer,
    "loanFeeCost" integer,
    "businessTax" integer,
    "updateDt" timestamp with time zone NOT NULL,
    "status" smallint,
    CONSTRAINT "billsPreparing_pkey" PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public.bills;
CREATE TABLE public.bills
(
    id integer NOT NULL,
    cleared boolean,
    "pvId" character varying COLLATE pg_catalog."default" NOT NULL,
    "rangeBegin" date NOT NULL,
    "rangeEnd" date NOT NULL,
    "totalPower" integer NOT NULL,
    "incomeWithTax" integer NOT NULL,
    "meterRentCost" integer NOT NULL,
    "profit" integer NOT NULL,
    "rentCostFixed" integer NOT NULL,
    "rentCostFloat" integer NOT NULL,
    "operateCostFixed" integer NOT NULL,
    "operateCostFloat" integer NOT NULL,
    "serviceCostFloat" integer NOT NULL,
    "insuranceCost" integer NOT NULL,
    "loanCost" integer NOT NULL,
    "loanFeeCost" integer NOT NULL,
    "businessTax" integer NOT NULL,
    "file" character varying COLLATE pg_catalog."default",
    "inTrust" boolean,
    "updateDt" timestamp with time zone NOT NULL,
    CONSTRAINT bills_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public."cashinRecords";
CREATE TABLE public."cashinRecords"
(
    id serial NOT NULL,
    "userId" integer NOT NULL,
    date date NOT NULL,
    amount integer NOT NULL,
    "oldSttw" numeric(34,16) NOT NULL,
    "newSttw" numeric(34,16) NOT NULL,
    "updateDt" timestamp with time zone NOT NULL,
    "blockInfo" json,
    "blockData" json,
    CONSTRAINT "cashinRecords_pkey" PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public."cashoutRecords";
CREATE TABLE public."cashoutRecords"
(    
    id serial NOT NULL,
    "userId" integer NOT NULL,
    date date NOT NULL,
    amount integer NOT NULL,
    status smallint NOT NULL,
    "oldSttw" numeric(34,16),
    "newSttw" numeric(34,16),
    "updateDt" timestamp with time zone NOT NULL,
    "blockInfo" json,
    "blockData" json,
    comment character varying COLLATE pg_catalog."default",
    CONSTRAINT "cashoutRecords_pkey" PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public.config;
CREATE TABLE public.config
(
    id serial NOT NULL,
    field character varying COLLATE pg_catalog."default" NOT NULL,
    value character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT config_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public."dailyClearance";
CREATE TABLE public."dailyClearance"
(
    id serial NOT NULL,
    "userId" integer NOT NULL,
    date date NOT NULL,
    item character varying COLLATE pg_catalog."default" NOT NULL,
    amount numeric(34,16) NOT NULL,
    "presentValue" numeric(34,16) NOT NULL,
    "updateDt" timestamp with time zone NOT NULL,
    CONSTRAINT "dailyClearance_pkey" PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public."donations";
CREATE TABLE public.donations
(
    id serial NOT NULL,
    receiver int NOT NULL,
    giver int NOT NULL,
    datetime timestamp with time zone NOT NULL,
    amount numeric(34,16) NOT NULL,
    "oldAmountReceiver" numeric(34,16) NOT NULL,
    "newAmountReceiver" numeric(34,16) NOT NULL,
    "oldAmountGiver" numeric(34,16) NOT NULL,
    "newAmountGiver" numeric(34,16) NOT NULL,
    "blockInfo" json,
    "blockData" json,
    CONSTRAINT donations_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public."eventByBill";
CREATE TABLE public."eventByBill"
(
    id serial NOT NULL,
    "billId" integer NOT NULL,
    "userId" integer NOT NULL,
    date date NOT NULL,
    profit numeric(34,16) NOT NULL,
    "rentCost" numeric(34,16) NOT NULL,
    "meterRentCost" numeric(34,16) NOT NULL,
    "insuranceCost" numeric(34,16) NOT NULL,
    "operateCost" numeric(34,16) NOT NULL,
    "serviceCost" numeric(34,16) NOT NULL,
    "loanCost" numeric(34,16) NOT NULL,
    "loanFeeCost" numeric(34,16) NOT NULL,
    "businessTax" numeric(34,16) NOT NULL,
    "oldSttw" numeric(34,16) NOT NULL,
    "newSttw" numeric(34,16) NOT NULL,
    "updateDt" timestamp with time zone NOT NULL,
    "blockInfo" json,
    "blockData" json,
    CONSTRAINT "eventByBill_pkey" PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public."eventByPV";
CREATE TABLE public."eventByPV"
(
    id serial NOT NULL,
    "pvId" character varying COLLATE pg_catalog."default" NOT NULL,
    date date NOT NULL,
    name character varying COLLATE pg_catalog."default" NOT NULL,
    content character varying COLLATE pg_catalog."default" NOT NULL,
    "updateDt" timestamp with time zone NOT NULL,
    CONSTRAINT "eventByPV_pkey" PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public."eventByRepay";
CREATE TABLE public."eventByRepay"
(
    id serial NOT NULL,
    "pvId" character varying COLLATE pg_catalog."default" NOT NULL,
    "userId" integer NOT NULL,
    date date NOT NULL,
    "oldAmount" numeric(34,16) NOT NULL,
    "newAmount" numeric(34,16) NOT NULL,
    "oldSttw" numeric(34,16) NOT NULL,
    "newSttw" numeric(34,16) NOT NULL,
    repay numeric(34,16) NOT NULL,
    "updateDt" timestamp with time zone NOT NULL,
    "blockInfo" json,
    "blockData" json,
    CONSTRAINT "eventByRepay_pkey" PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public."eventBySimulate";
CREATE TABLE public."eventBySimulate"
(
    id serial NOT NULL,
    "pvId" character varying COLLATE pg_catalog."default" NOT NULL,
    "userId" integer NOT NULL,
    date date NOT NULL,
    profit numeric(34,16) NOT NULL,
    "rentCost" numeric(34,16) NOT NULL,
    "meterRentCost" numeric(34,16) NOT NULL,
    "insuranceCost" numeric(34,16) NOT NULL,
    "operateCost" numeric(34,16) NOT NULL,
    "serviceCost" numeric(34,16) NOT NULL,
    "loanCost" numeric(34,16) NOT NULL,
    "loanFeeCost" numeric(34,16) NOT NULL,
    "businessTax" numeric(34,16) NOT NULL,
    "updateDt" timestamp with time zone NOT NULL,
    CONSTRAINT "eventBySimulate_pkey" PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public.managers;
CREATE TABLE public.managers
(
    id serial NOT NULL,
    email character varying COLLATE pg_catalog."default",
    pwd character varying COLLATE pg_catalog."default",
    "google2FASecretBase32" character varying COLLATE pg_catalog."default",
    "google2FAStatus" boolean,
    "mailVerified" boolean,
    CONSTRAINT managers_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public."marketHistory";
CREATE TABLE public."marketHistory"
(
    "pvId" character varying COLLATE pg_catalog."default" NOT NULL,
    date date NOT NULL,
    open numeric(34,16) NOT NULL,
    high numeric(34,16) NOT NULL,
    low numeric(34,16) NOT NULL,
    close numeric(34,16) NOT NULL,
    volume numeric(34,16) NOT NULL,
    CONSTRAINT "marketHistory_pkey" PRIMARY KEY ("pvId", date)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public."marketInfo";
CREATE TABLE public."marketInfo"
(
    "pvId" character varying COLLATE pg_catalog."default" NOT NULL,
    "updateDt" timestamp with time zone NOT NULL,
    open numeric(34,16) NOT NULL,
    high numeric(34,16) NOT NULL,
    low numeric(34,16) NOT NULL,
    price numeric(34,16) NOT NULL,
    volume numeric(34,16) NOT NULL,
    CONSTRAINT "marketInfo_pkey" PRIMARY KEY ("pvId")
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public."orderBuys";
CREATE TABLE public."orderBuys"
(
    id bigserial NOT NULL,
    "pvId" character varying COLLATE pg_catalog."default" NOT NULL,
    datetime timestamp with time zone NOT NULL,
    "userId" integer NOT NULL,
    price numeric(34,16) NOT NULL,
    filled numeric(34,16) NOT NULL DEFAULT 0,
    amount numeric(34,16) NOT NULL,
    status smallint NOT NULL DEFAULT 0,
    CONSTRAINT "orderBuys_pkey" PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public."orderSells";
CREATE TABLE public."orderSells"
(
    id bigserial NOT NULL,
    "pvId" character varying COLLATE pg_catalog."default" NOT NULL,
    datetime timestamp with time zone NOT NULL,
    "userId" integer NOT NULL,
    price numeric(34,16) NOT NULL,
    filled numeric(34,16) NOT NULL DEFAULT 0,
    amount numeric(34,16) NOT NULL,
    status smallint NOT NULL DEFAULT 0,
    CONSTRAINT "orderSells_pkey" PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public."pvFile";
CREATE TABLE public."pvFile"
(
    "pvId" character varying COLLATE pg_catalog."default" NOT NULL,
    items json NOT NULL, /*{"租約": {"files": ['test1.pdf', 'test2.png'] "getTime": null, "updateTime": null, "checkTime": null, "syncTime": null}}*/
    CONSTRAINT "pvFile_pkey" PRIMARY KEY ("pvId")
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public."pvLoan";
CREATE TABLE public."pvLoan"
(
    "pvId" character varying COLLATE pg_catalog."default" NOT NULL,
    "beginDate" date NOT NULL,
    "period" integer NOT NULL,
    "amount" integer NOT NULL,
    "rate" real NOT NULL,
    "files" json NOT NULL,
    CONSTRAINT "pvLoan_pkey" PRIMARY KEY ("pvId", "beginDate")
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public."pvLoanFee";
CREATE TABLE public."pvLoanFee"
(
    "pvId" character varying COLLATE pg_catalog."default" NOT NULL,
    "beginDate" date NOT NULL,
    "duration" smallint NOT NULL,
    "cost" integer NOT NULL,
    "files" json NOT NULL,
    CONSTRAINT "pvLoanFee_pkey" PRIMARY KEY ("pvId", "beginDate")
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public."pvImg";
CREATE TABLE public."pvImg"
(
    id serial NOT NULL,
    "pvId" character varying COLLATE pg_catalog."default" NOT NULL,
    img character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "pvImg_pkey" PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public."pvImage";
CREATE TABLE public."pvImage"
(
    "pvId" character varying COLLATE pg_catalog."default" NOT NULL,
    "files" json NOT NULL,
    CONSTRAINT "pvImage_pkey" PRIMARY KEY ("pvId")
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public."pvInsurance";
CREATE TABLE public."pvInsurance"
(
    "pvId" character varying COLLATE pg_catalog."default" NOT NULL,
    "beginDate" date NOT NULL,
    "duration" smallint NOT NULL,
    "cost" integer NOT NULL,
    "files" json NOT NULL,
    CONSTRAINT "pvInsurance_pkey" PRIMARY KEY ("pvId", "beginDate")
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public."pvStage";
CREATE TABLE public."pvStage"
(
    "pvId" character varying COLLATE pg_catalog."default" NOT NULL,
    stage smallint NOT NULL,
    content character varying COLLATE pg_catalog."default",
    file character varying COLLATE pg_catalog."default",
    dataUrl character varying COLLATE pg_catalog."default",
    CONSTRAINT "pvStage_pkey" PRIMARY KEY ("pvId", stage)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public."pvSummary";
CREATE TABLE public."pvSummary"
(
    "values" numeric NOT NULL,
    co2 numeric NOT NULL,
    income numeric NOT NULL,
    "lastDate" date NOT NULL,
    "updateAt" timestamp with time zone NOT NULL
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public.pvs;
CREATE TABLE public.pvs
(
    id character varying COLLATE pg_catalog."default" NOT NULL,
    "solaName" character varying COLLATE pg_catalog."default" NOT NULL,
    "name" character varying COLLATE pg_catalog."default",
    "projectCode" character varying COLLATE pg_catalog."default",
    "location" character varying COLLATE pg_catalog."default",
    "coordLat" double precision,
    "coordLng" double precision,
    "capacity" real,
    "decay" real,
    "annualAmount" real,
    "stage" smallint,
    "status" smallint,
    "constructionCost" integer,
    "totalCapital" integer,
    "ownedCapital" integer,
    "initialPrice" double precision,
    "validityDateBegin" date,
    "validityDuration" smallint,
    "sellPrice" real,
    "operationRateByCapital" real,
    "operationRateByIncome" real,
    "serviceRateByIncome" real,
    "rentRateByIncome" real,
    "rentYearCost" real,
    "rentWithTax" boolean,
    "owner" character varying COLLATE pg_catalog."default",
    "blockInfo" json,
    "blockData" json,
    CONSTRAINT pvs_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public.trades;
CREATE TABLE public.trades
(
    id bigserial NOT NULL,
    "orderBuyId" bigint NOT NULL,
    "orderSellId" bigint NOT NULL,
    datetime timestamp with time zone NOT NULL,
    price numeric(34,16) NOT NULL,
    amount numeric(34,16) NOT NULL,
    fee numeric(34,16) NOT NULL,
    "oldAmountBuyer" numeric(34,16) NOT NULL,
    "newAmountBuyer" numeric(34,16) NOT NULL,
    "oldSttwBuyer" numeric(34,16) NOT NULL,
    "newSttwBuyer" numeric(34,16) NOT NULL,
    "oldAmountSeller" numeric(34,16) NOT NULL,
    "newAmountSeller" numeric(34,16) NOT NULL,
    "oldSttwSeller" numeric(34,16) NOT NULL,
    "newSttwSeller" numeric(34,16) NOT NULL,
    "blockInfo" json,
    "blockData" json,
    CONSTRAINT trades_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public."userAssets";
CREATE TABLE public."userAssets"
(
    "userId" integer NOT NULL,
    "pvId" character varying COLLATE pg_catalog."default" NOT NULL,
    amount numeric(34,16) NOT NULL,
    "lockAmount" numeric(34,16) NOT NULL DEFAULT 0,
    CONSTRAINT "userAssets_pkey" PRIMARY KEY ("userId", "pvId")
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public.users;
CREATE TABLE public.users
(
    id serial NOT NULL,
    name character varying COLLATE pg_catalog."default",
    sttw numeric(34,16) NOT NULL DEFAULT 0,
    "lockSttw" numeric(34,16) NOT NULL DEFAULT 0,
    CONSTRAINT users_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;