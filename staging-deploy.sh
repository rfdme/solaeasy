#!/bin/bash
########################################################
# $1: target machine to deploy 
# $2: image tag name
# $3: docker registry name
########################################################

[ "$#" -lt 3 ] && echo "3 parameters are required" && exit 256

source prepare-b4-deploy.sh
ansible-vault decrypt --vault-password-file=ansible.vault.se inventories/staging/vault_files/staging.solase

TA_GROUP="$1"
IMAGE_TAG="$2"
DOCKER_REGISTRY="$3"

TMP_ENV={\"__docker_registry\":\"$DOCKER_REGISTRY\",\"__docker_registry_user\":\"$DOCKER_USERNAME\",\"__docker_registry_pwd\":\"$DOCKER_PASSWORD\",\"__image_tag\":\"$IMAGE_TAG\"}

ansible-galaxy install nickhammond.logrotate
ansible-playbook chmod_key.yml --vault-id ansible.vault.se
ansible-playbook prerequisite.yml -i inventories/staging/tokyo -l "$TA_GROUP" --vault-id ansible.vault.se
ansible-playbook fetch-env.yml -i inventories/staging/tokyo -l "$TA_GROUP" --vault-id ansible.vault.se
# ansible-playbook provision-maintenance.yml -i inventories/staging/tokyo -l "$TA_GROUP" -e "$TMP_ENV" --vault-id ansible.vault.se
# ansible-playbook provision-nginx-mt.yml -i inventories/staging/tokyo --vault-id ansible.vault.se
# sleep 3
# ansible-playbook provision-schema.yml -i inventories/staging/tokyo -l "$TA_GROUP" -e "$TMP_ENV" --vault-id ansible.vault.se
# ansible-playbook provision-websocket.yml -i inventories/staging/tokyo -l "$TA_GROUP" -e "$TMP_ENV" --vault-id ansible.vault.se
ansible-playbook provision-regular.yml -i inventories/staging/tokyo -l "$TA_GROUP" -e "$TMP_ENV" --vault-id ansible.vault.se

# echo "Sleep until npm build finished"
# sleep 180
# ansible-playbook provision-letsencrypt.yml -i inventories/staging/tokyo --vault-id ansible.vault.se
# ansible-playbook provision-nginx.yml -i inventories/staging/tokyo --vault-id ansible.vault.se