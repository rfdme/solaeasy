var sprintf = require('sprintf-js').sprintf;
var moment = require('moment');
const schedule = require('node-schedule');
const BN = require('bignumber.js').clone({ DECIMAL_PLACES: 16 });

var dataLoader = require('./exchangeDataLoader.js');

const db = require('./db');

const path = require('path');
const log4js = require('log4js');

// 以filename設定logger分類
const logger = log4js.getLogger(path.basename(__filename));
logger.level = 'trace';

var semNewOrders = require('semaphore')(1);
var newOrders = [];
var semOpenOrder = require('semaphore')(1);
var openOrderBuys = {};
var openOrderSells = {};

var started = false;
var io;
const tradeInterval = 300;
const logInterval = 10000;
let matching = false;

exports.start = async socketIO => {
  if (started) {
    return;
  }

  started = true;
  io = socketIO;

  logger.info('trader start');
  await loadOpenOrder();

  scheduleResetOpenOrder();

  setInterval(() => {
    if (matching) return;

    semNewOrders.take(() => {
      if (newOrders.length > 0) {
        let order = null;
        order = newOrders.shift();
        semNewOrders.leave();

        semOpenOrder.take(() => {
          matching = true;
          console.time('tradeWithOrder');

          if (order.type === 'buy') {
            tradeWithOrderSell(order.data);
          } else if (order.type === 'sell') {
            tradeWithOrderBuy(order.data);
          }

          console.timeEnd('tradeWithOrder');
          matching = false;

          semOpenOrder.leave();
        });
      } else {
        semNewOrders.leave();
      }
    });
  }, tradeInterval);

  setInterval(() => {
    if (newOrders.length > 0) {
      logger.info('newOrders:', newOrders.length, 'matching:', matching);
    }
  }, logInterval);
};

exports.handleOrderBuy = (data, resultFn) => {
  data.price = BN(data.price);
  data.amount = BN(data.amount);

  if (data.price.lt(0)) {
    resultFn('無效參數: 價格不得為負');
    return;
  }

  if (data.amount.lt(0)) {
    resultFn('無效參數: 數量不得為負');
    return;
  }

  (async () => {
    try {
      if (moment().hours() >= 21) {
        resultFn('目前已收盤, 交易時間為00:00~21:00');
        return;
      }
      await db.query('BEGIN');

      const checkRemainedResult = await db.query(
        `SELECT (sttw - "lockSttw") as remained FROM users WHERE id=$1`,
        [data.userId]
      );

      if (checkRemainedResult.rows[0] === undefined) {
        resultFn('無STTW資產');
        await db.query('ROLLBACK');
        return;
      }

      const { remained } = checkRemainedResult.rows[0];
      if (remained.lt(data.price.times(data.amount).times(1.015))) {
        resultFn('STTW餘額不足');
        await db.query('ROLLBACK');
        return;
      }

      const lockResult = await db.query(
        `UPDATE users SET "lockSttw"="lockSttw"+$1 WHERE id=$2`,
        [
          data.price
            .times(data.amount)
            .times(1.015)
            .toString(),
          data.userId
        ]
      );

      if (lockResult.rowCount != 1) {
        resultFn("can't lock STTW");
        await db.query('ROLLBACK');
        return;
      }

      const inserOrderResult = await db.query(
        `INSERT INTO "orderBuys"("pvId", datetime, "userId", price, amount) VALUES ($1,$2,$3,$4,$5) RETURNING *`,
        [
          data.pvId,
          moment(),
          data.userId,
          data.price.toString(),
          data.amount.toString()
        ]
      );

      await db.query('COMMIT');

      resultFn('委託成功');
      const lastID = inserOrderResult.rows[0].id;
      return { lastID, data };
    } catch (e) {
      resultFn('委託失敗');
      await db.query('ROLLBACK');
      throw e;
    }
  })()
    .then(result => {
      if (result) {
        const { lastID, data } = result;
        tryTradeOrderBuy(lastID, data);
      }
    })
    .catch(e => logger.trace(e.stack));
};

//須優化成promise or await寫法
exports.handleOrderSell = (data, resultFn) => {
  data.price = BN(data.price);
  data.amount = BN(data.amount);

  if (data.price.lt(0)) {
    resultFn('無效參數: 價格不得為負');
    return;
  }

  if (data.amount.lt(0)) {
    resultFn('無效參數: 數量不得為負');
    return;
  }

  (async () => {
    try {
      if (moment().hours() >= 21) {
        resultFn('目前已收盤, 交易時間為00:00~21:00');
        return;
      }
      await db.query('BEGIN');

      const checkRemainedResult = await db.query(
        `SELECT (amount - "lockAmount") as remained FROM "userAssets" WHERE "userId"=$1 AND "pvId"=$2`,
        [data.userId, data.pvId]
      );

      if (checkRemainedResult.rows[0] === undefined) {
        resultFn('無此SNTW資產');
        await db.query('ROLLBACK');
        return;
      }

      const { remained } = checkRemainedResult.rows[0];
      if (remained.lt(data.amount)) {
        resultFn('SNTW餘額不足');
        await db.query('ROLLBACK');
        return;
      }

      const lockResult = await db.query(
        `UPDATE "userAssets" SET "lockAmount"="lockAmount"+$1 WHERE "userId"=$2 AND "pvId"=$3`,
        [data.amount.toString(), data.userId, data.pvId]
      );

      if (lockResult.rowCount != 1) {
        resultFn("can't lock STTW");
        await db.query('ROLLBACK');
        return;
      }

      const inserOrderResult = await db.query(
        `INSERT INTO "orderSells"("pvId", datetime, "userId", price, amount) VALUES ($1,$2,$3,$4,$5) RETURNING *`,
        [
          data.pvId,
          moment(),
          data.userId,
          data.price.toString(),
          data.amount.toString()
        ]
      );

      await db.query('COMMIT');

      const lastID = inserOrderResult.rows[0].id;
      resultFn('委託成功');
      return { lastID, data };
    } catch (e) {
      resultFn('委託失敗');
      await db.query('ROLLBACK');
      throw e;
    }
  })()
    .then(result => {
      if (result) {
        const { lastID, data } = result;
        tryTradeOrderSell(lastID, data);
      }
    })
    .catch(e => logger.trace(e.stack));
};

exports.handleCancelOrderBuy = (orderId, resultFn) => {
  (async () => {
    try {
      await db.query('BEGIN');

      const checkRemainedResult = await db.query(
        `SELECT "pvId", "userId", price, (amount - filled) as remained, status FROM "orderBuys" WHERE id=$1`,
        [orderId]
      );

      if (checkRemainedResult.rows[0] === undefined) {
        resultFn('查無此訂單');
        await db.query('ROLLBACK');
        return;
      }

      const {
        pvId,
        userId,
        price,
        status,
        remained
      } = checkRemainedResult.rows[0];

      if (status !== 0) {
        resultFn('此訂單已被取消');
        await db.query('ROLLBACK');
        return;
      }

      if (remained.eq(0)) {
        resultFn('此訂單已全部成交');
        await db.query('ROLLBACK');
        return;
      }

      const lockResult = await db.query(
        `UPDATE users SET "lockSttw"="lockSttw"-$1 WHERE id=$2`,
        [
          price
            .times(remained)
            .times(1.015)
            .toString(),
          userId
        ]
      );

      if (lockResult.rowCount != 1) {
        resultFn("can't unlock STTW");
        await db.query('ROLLBACK');
        return;
      }

      const updateOrderResult = await db.query(
        `UPDATE "orderBuys" SET status=1 WHERE id=$1`,
        [orderId]
      );

      await db.query('COMMIT');

      resultFn('取消委託成功');
      return pvId;
    } catch (e) {
      resultFn('取消失敗');
      await db.query('ROLLBACK');
      throw e;
    }
  })()
    .then(pvId => {
      if (pvId) {
        cancelOrderBuy(orderId);
        dataLoader.getOrderBookBuy(pvId, data => {
          const result = { pvId, data };
          io.sockets.emit('syncOrderBookBuy', result);
        });
      }
    })
    .catch(e => logger.trace(e.stack));
};

exports.handleCancelOrderSell = (orderId, resultFn) => {
  (async () => {
    try {
      await db.query('BEGIN');

      const checkRemainedResult = await db.query(
        `SELECT "pvId", "userId", price, (amount - filled) as remained, status FROM "orderSells" WHERE id=$1`,
        [orderId]
      );

      if (checkRemainedResult.rows[0] === undefined) {
        resultFn('查無此訂單');
        await db.query('ROLLBACK');
        return;
      }

      const {
        pvId,
        userId,
        price,
        status,
        remained
      } = checkRemainedResult.rows[0];

      if (status !== 0) {
        resultFn('此訂單已被取消');
        await db.query('ROLLBACK');
        return;
      }

      if (remained.eq(0)) {
        resultFn('此訂單已全部成交');
        await db.query('ROLLBACK');
        return;
      }

      const lockResult = await db.query(
        `UPDATE "userAssets" SET "lockAmount"="lockAmount"-$1 WHERE "userId"=$2 AND "pvId"=$3`,
        [remained.toString(), userId, pvId]
      );

      if (lockResult.rowCount != 1) {
        resultFn("can't unlock SNTW");
        await db.query('ROLLBACK');
        return;
      }

      const updateOrderResult = await db.query(
        `UPDATE "orderSells" SET status=1 WHERE id=$1`,
        [orderId]
      );

      await db.query('COMMIT');

      resultFn('取消委託成功');
      return pvId;
    } catch (e) {
      resultFn('取消失敗');
      await db.query('ROLLBACK');
      throw e;
    }
  })()
    .then(pvId => {
      if (pvId) {
        cancelOrderSell(orderId);
        dataLoader.getOrderBookSell(pvId, data => {
          const result = { pvId, data };
          io.sockets.emit('syncOrderBookSell', result);
        });
      }
    })
    .catch(e => logger.trace(e.stack));
};

const expireAllOrder = async () => {
  try {
    await db.query('BEGIN');

    const backOrderSellToAsset = await db.query(`
        UPDATE "userAssets" A SET "lockAmount" = "lockAmount" - B.remained
        FROM (
          SELECT "userId", "pvId", SUM(amount - filled) AS remained FROM "orderSells"
          WHERE status = 0 AND amount > filled
          GROUP BY "userId", "pvId"
        ) AS B
        WHERE A."userId" = B."userId"`);

    const backOrderBuyToUser = await db.query(`
        UPDATE users A SET "lockSttw" = "lockSttw" - B.remained * 1.015
        FROM (
          SELECT "userId", "pvId", SUM(price * (amount - filled)) AS remained FROM "orderBuys"
          WHERE status = 0 AND amount > filled
          GROUP BY "userId", "pvId"
        ) AS B
        WHERE A.id = B."userId"`);

    const changeOrderSellStatus = await db.query(
      `UPDATE "orderSells" SET status = 2 WHERE amount > filled AND status = 0`
    );

    const changeOrderBuyStatus = await db.query(
      `UPDATE "orderBuys" SET status = 2 WHERE amount > filled AND  status = 0`
    );

    await db.query('COMMIT');
    return;
  } catch (e) {
    await db.query('ROLLBACK');
    logger.trace(e.stack);
  }
};

const loadOpenOrder = async () => {
  await db
    .query({
      text: `SELECT id, "pvId", "userId", price, filled, amount FROM "orderBuys" WHERE filled < amount AND status = 0 AND datetime AT TIME ZONE 'Asia/Taipei' >= $1`,
      values: [moment().format('YYYY-MM-DD')]
    })
    .then(res => {
      for (let i = 0; i < res.rows.length; ++i) {
        const row = res.rows[i];

        if (openOrderBuys[row.pvId] === undefined) {
          openOrderBuys[row.pvId] = [];
        }

        openOrderBuys[row.pvId].push(row);
      }

      sortOpenOrderBuy();
      logger.info('openOrderBuys', openOrderBuys);
    })
    .catch(err => logger.trace(err.stack));

  await db
    .query({
      text: `SELECT id, "pvId", "userId", price, filled, amount FROM "orderSells" WHERE filled < amount AND status = 0 AND datetime AT TIME ZONE 'Asia/Taipei' >= $1`,
      values: [moment().format('YYYY-MM-DD')]
    })
    .then(res => {
      for (let i = 0; i < res.rows.length; ++i) {
        const row = res.rows[i];

        if (openOrderSells[row.pvId] === undefined) {
          openOrderSells[row.pvId] = [];
        }

        openOrderSells[row.pvId].push(row);
      }

      sortOpenOrderSell();
      logger.info('openOrderSells', openOrderSells);
    })
    .catch(err => logger.trace(err.stack));
};

const scheduleResetOpenOrder = () => {
  logger.info('schedule [scheduleResetOpenOrder]');
  //每天23:55, 清除掛單
  var j1 = schedule.scheduleJob('55 23 * * *', async () => {
    await expireAllOrder();
  });
  //每天零點整, 重新載入掛單
  var j2 = schedule.scheduleJob('0 0 * * *', async () => {
    await loadOpenOrder();
    io.sockets.emit('reset');
  });
};

const sortOpenOrderBuy = () => {
  for (let pvId in openOrderBuys) {
    openOrderBuys[pvId].sort((a, b) => {
      if (a.price.gt(b.price)) return -1;
      if (a.price.lt(b.price)) return 1;
      if (a.id < b.id) return -1;
      if (a.id > b.id) return 1;
      return 0;
    });
  }
};

const sortOpenOrderSell = () => {
  for (let pvId in openOrderSells) {
    openOrderSells[pvId].sort((a, b) => {
      if (a.price.lt(b.price)) return -1;
      if (a.price.gt(b.price)) return 1;
      if (a.id < b.id) return -1;
      if (a.id > b.id) return 1;
      return 0;
    });
  }
};

const cancelOrderBuy = id => {
  let found = false;
  semNewOrders.take(() => {
    for (let i = 0; i < newOrders.length; i++) {
      const order = newOrders[i];
      if (order.type === 'buy' && order.data.id === id) {
        newOrders.splice(i, 1);
        found = true;
        break;
      }
    }
    semNewOrders.leave();
  });

  if (found) return;

  semOpenOrder.take(() => {
    for (let pvId in openOrderBuys) {
      for (let i = 0; i < openOrderBuys[pvId].length; i++) {
        if (openOrderBuys[pvId][i].id === id) {
          openOrderBuys[pvId].splice(i, 1);
          semOpenOrder.leave();
          return;
        }
      }
    }
    semOpenOrder.leave();
  });
};

const cancelOrderSell = id => {
  let found = false;
  semNewOrders.take(() => {
    for (let i = 0; i < newOrders.length; i++) {
      const order = newOrders[i];
      if (order.type === 'sell' && order.id === id) {
        newOrders.splice(i, 1);
        found = true;
        break;
      }
    }
    semNewOrders.leave();
  });

  if (found) return;

  semOpenOrder.take(() => {
    for (let pvId in openOrderSells) {
      for (let i = 0; i < openOrderSells[pvId].length; i++) {
        if (openOrderSells[pvId][i].id === id) {
          openOrderSells[pvId].splice(i, 1);
          semOpenOrder.leave();
          return;
        }
      }
    }
    semOpenOrder.leave();
  });
};

const tryTradeOrderBuy = (id, data) => {
  let order = { type: 'buy' };
  data.id = id;
  data.filled = BN(0);
  order.data = data;
  semNewOrders.take(() => {
    newOrders.push(order);
    semNewOrders.leave();
  });
  semOpenOrder.take(() => {
    if (openOrderSells[data.pvId] === undefined) {
      openOrderSells[data.pvId] = [];
    }
    semOpenOrder.leave();
  });
};

const tryTradeOrderSell = (id, data) => {
  let order = { type: 'sell' };
  data.id = id;
  data.filled = BN(0);
  order.data = data;
  semNewOrders.take(() => {
    newOrders.push(order);
    semNewOrders.leave();
  });
  semOpenOrder.take(() => {
    if (openOrderBuys[data.pvId] === undefined) {
      openOrderBuys[data.pvId] = [];
    }
    semOpenOrder.leave();
  });
};

const removeFullFilledOpenOrder = openOrders => {
  //針對排序過的
  let removed = false;
  while (openOrders.length > 0) {
    if (openOrders[0].filled.eq(openOrders[0].amount)) {
      openOrders.splice(0, 1);
      removed = true;
    } else {
      break;
    }
  }

  return removed;
};

async function tradeWithOrderSell(buy) {
  let tradedOrder = {
    buys: [],
    sells: []
  };
  for (
    let i = 0;
    openOrderSells[buy.pvId] && i < openOrderSells[buy.pvId].length;
    i++
  ) {
    let sell = openOrderSells[buy.pvId][i];
    let remainedBuy = buy.amount.minus(buy.filled);
    if (remainedBuy.gt(0) && sell.price.lte(buy.price)) {
      let remainedSell = sell.amount.minus(sell.filled);
      let amount = remainedBuy.lt(remainedSell) ? remainedBuy : remainedSell;
      if (amount.gt(0)) {
        let ok = await doTrade(buy, sell, amount, sell.price);
        if (ok) {
          buy.filled = buy.filled.plus(amount);
          sell.filled = sell.filled.plus(amount);
          tradedOrder.buys.push(buy.id);
          tradedOrder.sells.push(sell.id);
          //console.log(buy);
          //console.log(sell);
        }
      }
    } else {
      break;
    }
  }

  let removed = removeFullFilledOpenOrder(openOrderSells[buy.pvId]);
  if (removed || tradedOrder.sells.length > 0) {
    dataLoader.getOrderBookSell(buy.pvId, data => {
      let result = {
        pvId: buy.pvId,
        data: data
      };
      io.sockets.emit('syncOrderBookSell', result);
    });
  }

  let remainedBuy = buy.amount.minus(buy.filled);
  if (remainedBuy.gt(0)) {
    if (openOrderBuys[buy.pvId] === undefined) {
      openOrderBuys[buy.pvId] = [];
    }
    openOrderBuys[buy.pvId].push(buy);

    dataLoader.getOrderBookBuy(buy.pvId, data => {
      let result = {
        pvId: buy.pvId,
        data: data
      };
      io.sockets.emit('syncOrderBookBuy', result);
    });
  }

  if (buy.filled.gt(0)) {
    dataLoader.getMarketSummary(data => {
      io.sockets.emit('updateMarketSummary', data);
    });
    dataLoader.getTradeHistory(buy.pvId, data => {
      io.sockets.emit('updateTradeHistory', data);
    });
    dataLoader.getTrendData(buy.pvId, data => {
      let result = {
        pvId: buy.pvId,
        data: data
      };
      io.sockets.emit('updateTrend', result);
    });

    io.sockets.emit('syncTrades', tradedOrder);
  }

  sortOpenOrderBuy();
}

async function tradeWithOrderBuy(sell) {
  let tradedOrder = {
    buys: [],
    sells: []
  };
  for (
    let i = 0;
    openOrderBuys[sell.pvId] && i < openOrderBuys[sell.pvId].length;
    i++
  ) {
    let buy = openOrderBuys[sell.pvId][i];
    let remainedSell = sell.amount.minus(sell.filled);
    if (remainedSell.gt(0) && buy.price.gte(sell.price)) {
      let remainedBuy = buy.amount.minus(buy.filled);
      let amount = remainedSell.lt(remainedBuy) ? remainedSell : remainedBuy;
      if (amount.gt(0)) {
        let ok = await doTrade(buy, sell, amount, buy.price);
        if (ok) {
          buy.filled = buy.filled.plus(amount);
          sell.filled = sell.filled.plus(amount);
          tradedOrder.buys.push(buy.id);
          tradedOrder.sells.push(sell.id);
          //console.log(buy);
          //console.log(sell);
        }
      }
    } else {
      break;
    }
  }

  let removed = removeFullFilledOpenOrder(openOrderBuys[sell.pvId]);
  if (removed || tradedOrder.buys.length > 0) {
    dataLoader.getOrderBookBuy(sell.pvId, data => {
      let result = {
        pvId: sell.pvId,
        data: data
      };
      io.sockets.emit('syncOrderBookBuy', result);
    });
  }

  let remainedSell = sell.amount.minus(sell.filled);
  if (remainedSell.gt(0)) {
    if (openOrderSells[sell.pvId] === undefined) {
      openOrderSells[sell.pvId] = [];
    }
    openOrderSells[sell.pvId].push(sell);

    dataLoader.getOrderBookSell(sell.pvId, data => {
      let result = {
        pvId: sell.pvId,
        data: data
      };
      io.sockets.emit('syncOrderBookSell', result);
    });
  }

  if (sell.filled.gt(0)) {
    dataLoader.getMarketSummary(data => {
      io.sockets.emit('updateMarketSummary', data);
    });
    dataLoader.getTradeHistory(sell.pvId, data => {
      io.sockets.emit('updateTradeHistory', data);
    });
    dataLoader.getTrendData(sell.pvId, data => {
      let result = {
        pvId: sell.pvId,
        data: data
      };
      io.sockets.emit('updateTrend', result);
    });

    io.sockets.emit('syncTrades', tradedOrder);
  }

  sortOpenOrderSell();
}

const doTrade = (buy, sell, amount, price) => {
  return new Promise((resolve, reject) => {
    (async () => {
      try {
        await db.query('BEGIN');

        const fillOrderBuyResult = await db.query(
          `UPDATE "orderBuys" SET filled=filled+$1 WHERE id=$2`,
          [amount.toString(), buy.id]
        );

        if (fillOrderBuyResult.rowCount !== 1) {
          await db.query('ROLLBACK');
          return '查無此訂單';
        }

        const fillOrderSellResult = await db.query(
          `UPDATE "orderSells" SET filled=filled+$1 WHERE id=$2`,
          [amount.toString(), sell.id]
        );

        if (fillOrderSellResult.rowCount !== 1) {
          await db.query('ROLLBACK');
          return '查無此訂單';
        }

        const oldSttwBuyerResult = await db.query(
          `SELECT sttw FROM users WHERE id=$1`,
          [buy.userId]
        );
        const oldSttwBuyer = oldSttwBuyerResult.rows[0].sttw;

        const oldSntwSellerResult = await db.query(
          `SELECT amount FROM "userAssets" WHERE "userId"=$1 AND "pvId"=$2`,
          [sell.userId, sell.pvId]
        );
        const oldSntwSeller = oldSntwSellerResult.rows[0].amount;

        const unlockAndPaySttwResult = await db.query(
          `UPDATE users SET "lockSttw"="lockSttw"-$1, sttw=sttw-$2 WHERE id=$3 RETURNING sttw`,
          [
            amount
              .times(buy.price)
              .times(1.015)
              .toString(),
            amount
              .times(price)
              .times(1.015)
              .toString(),
            buy.userId
          ]
        );

        if (unlockAndPaySttwResult.rowCount !== 1) {
          await db.query('ROLLBACK');
          return '交易失敗: 買家無法扣除STTW';
        }

        const newSttwBuyer = unlockAndPaySttwResult.rows[0].sttw;

        const unlockAndPaySntwResult = await db.query(
          `UPDATE "userAssets" SET "lockAmount"="lockAmount"-$1, amount=amount-$1 WHERE "userId"=$2 AND "pvId"=$3 RETURNING amount`,
          [amount.toString(), sell.userId, sell.pvId]
        );

        if (unlockAndPaySntwResult.rowCount !== 1) {
          await db.query('ROLLBACK');
          return '交易失敗: 賣家無法扣除SNTW';
        }

        const newSntwSeller = unlockAndPaySntwResult.rows[0].amount;

        const oldSttwSellerResult = await db.query(
          `SELECT sttw FROM users WHERE id=$1`,
          [sell.userId]
        );
        const oldSttwSeller = oldSttwSellerResult.rows[0].sttw;

        const oldSntwBuyerResult = await db.query(
          `SELECT amount FROM "userAssets" WHERE "userId"=$1 AND "pvId"=$2`,
          [buy.userId, buy.pvId]
        );
        const oldSntwBuyer =
          oldSntwBuyerResult.rowCount > 0
            ? oldSntwBuyerResult.rows[0].amount
            : BN(0);

        const getSntwResult = await db.query(
          `INSERT INTO "userAssets" as A ("userId", "pvId", amount) VALUES ($1,$2,$3)
           ON CONFLICT ("userId", "pvId") DO UPDATE SET amount=A.amount+$3 RETURNING amount`,
          [buy.userId, buy.pvId, amount.toString()]
        );

        if (getSntwResult.rowCount !== 1) {
          await db.query('ROLLBACK');
          return '交易失敗: 買家無法得到SNTW';
        }

        const newSntwBuyer = getSntwResult.rows[0].amount;

        const getSttwResult = await db.query(
          `UPDATE users SET sttw=sttw+$1 WHERE id=$2 RETURNING sttw`,
          [
            amount
              .times(price)
              .times(0.985)
              .toString(),
            sell.userId
          ]
        );

        if (getSttwResult.rowCount !== 1) {
          await db.query('ROLLBACK');
          return '交易失敗: 賣家無法得到STTW';
        }

        const newSttwSeller = getSttwResult.rows[0].sttw;

        const createTradeRecordResult = await db.query(
          `INSERT INTO trades(
            "orderBuyId", 
            "orderSellId", 
            datetime, 
            price, 
            amount, 
            fee, 
            "oldAmountBuyer", 
            "newAmountBuyer", 
            "oldSttwBuyer", 
            "newSttwBuyer", 
            "oldAmountSeller", 
            "newAmountSeller", 
            "oldSttwSeller", 
            "newSttwSeller")
          VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14) RETURNING id`,
          [
            buy.id,
            sell.id,
            moment(),
            price.toString(),
            amount.toString(),
            amount
              .times(price)
              .times(0.03)
              .toString(),
            oldSntwBuyer.toString(),
            newSntwBuyer.toString(),
            oldSttwBuyer.toString(),
            newSttwBuyer.toString(),
            oldSntwSeller.toString(),
            newSntwSeller.toString(),
            oldSttwSeller.toString(),
            newSttwSeller.toString()
          ]
        );

        if (createTradeRecordResult.rowCount !== 1) {
          await db.query('ROLLBACK');
          return '交易失敗: 無法建立交易紀錄';
        }

        const updateMarketResult = await db.query(
          `INSERT INTO "marketInfo" as A ("pvId", "updateDt", open, high, low, price, volume) VALUES ($1,$2,$3,$3,$3,$3,$4)
           ON CONFLICT ("pvId") 
           DO UPDATE SET price=$3, high=GREATEST(A.high, $3), low=LEAST(A.low, $3), "updateDt"=$2, volume=A.volume+$4`,
          [buy.pvId, moment(), price.toString(), amount.toString()]
        );

        if (updateMarketResult.rowCount !== 1) {
          await db.query('ROLLBACK');
          return '交易失敗: 無法更新市場';
        }

        //記錄區塊交易
        const fee = price.times(amount).times(0.015); //TODO
        const txType = 'tradeSync';
        const txId = createTradeRecordResult.rows[0].id;
        const data = {
          orderBuyId: buy.id,
          buyerId: buy.userId,
          orderSellId: sell.id,
          sellerId: sell.userId,
          pvId: buy.pvId,
          price: price
            .times(Math.pow(10, 16))
            .integerValue()
            .toString(10),
          amount: amount
            .times(Math.pow(10, 16))
            .integerValue()
            .toString(10),
          buyerFee: fee
            .times(Math.pow(10, 16))
            .integerValue()
            .toString(10),
          sellerFee: fee
            .times(Math.pow(10, 16))
            .integerValue()
            .toString(10),
          date: moment().format('YYYY-MM-DD HH:mm:ss')
        };

        const insertBcTxResult = await db.query(
          'INSERT INTO "bcTransation" ("txType", "txId", data) VALUES ($1,$2,$3)',
          [txType, txId, data]
        );

        if (insertBcTxResult.rowCount !== 1) {
          await db.query('ROLLBACK');
          return '交易失敗: 新增區塊交易';
        }

        await db.query('COMMIT');

        return true;
      } catch (e) {
        await db.query('ROLLBACK');
        throw e;
      }
    })()
      .then(result => {
        if (result === true) {
          resolve(true);
        } else {
          logger.info(result);
          resolve(false);
        }
      })
      .catch(e => {
        logger.trace(e.stack);
        resolve(false);
      });
  });
};
