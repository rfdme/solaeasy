var sprintf = require('sprintf-js').sprintf;
var moment = require('moment');

const db2 = require('./db');

const path = require('path');
const log4js = require('log4js');

// 以filename設定logger分類
const logger = log4js.getLogger(path.basename(__filename));
logger.level = 'debug';

const queryForResult = async (resultFn, query) => {
  try {
    const { rows } = await db2.query(query);
    resultFn(rows);
  } catch (err) {
    logger.info(query.text);
    logger.error(err.stack);
  }
};

const getForResult = async (resultFn, query) => {
  try {
    const { rows } = await db2.query(query);
    resultFn(rows[0]);
  } catch (err) {
    logger.info(query.text);
    logger.error(err.stack);
  }
};

exports.getPvList = resultFn => {
  const query = {
    text: `SELECT id, name FROM pvs ORDER BY id`
  };
  queryForResult(resultFn, query);
};

exports.getMarketSummary = resultFn => {
  const query = {
    text: `SELECT "pvId", price, open, high, low, volume FROM "marketInfo" ORDER BY "pvId"`
  };
  queryForResult(resultFn, query);
};

exports.getTradeHistory = (pvId, resultFn) => {
  const query = {
    text: `SELECT A.datetime, A.price, A.amount, B.datetime as "orderTimeBuy", C.datetime as "orderTimeSell" FROM trades A
           LEFT JOIN "orderBuys" B ON B.id = A."orderBuyId"
           LEFT JOIN "orderSells" C ON C.id = A."orderSellId"
           WHERE B."pvId"=$1 AND C."pvId"=$1 AND A.datetime AT TIME ZONE 'Asia/Taipei' >= $2
           ORDER BY A.datetime DESC
           limit 10`,
    values: [pvId, moment().format('YYYY-MM-DD')]
  };
  queryForResult(resultFn, query);
};

exports.getOrderBookBuy = (pvId, resultFn) => {
  const query = {
    text: `SELECT "pvId", price, SUM(amount) - SUM(filled) as amount FROM "orderBuys"
           WHERE "pvId"=$1 AND amount != filled AND status=0 AND datetime AT TIME ZONE 'Asia/Taipei' >= $2
           GROUP BY "pvId", price`,
    values: [pvId, moment().format('YYYY-MM-DD')]
  };
  queryForResult(resultFn, query);
};

exports.getOrderBookSell = (pvId, resultFn) => {
  const query = {
    text: `SELECT "pvId", price, SUM(amount) - SUM(filled) as amount FROM "orderSells"
           WHERE "pvId"=$1 AND amount != filled AND status=0 AND datetime AT TIME ZONE 'Asia/Taipei' >= $2
           GROUP BY "pvId", price`,
    values: [pvId, moment().format('YYYY-MM-DD')]
  };
  queryForResult(resultFn, query);
};

exports.getUserTrades = (userId, resultFn) => {
  const query = {
    text: `SELECT '買' as side, B."pvId", A.datetime, A.price, A.amount, A.fee FROM trades A
           LEFT JOIN "orderBuys" B ON B.id = A."orderBuyId"
           WHERE B."userId"=$1 AND A.datetime AT TIME ZONE 'Asia/Taipei' >= $2
           UNION ALL
           SELECT '賣' as side, B."pvId", A.datetime, A.price, A.amount, A.fee FROM trades A
           LEFT JOIN "orderSells" B ON B.id = A."orderSellId"
           WHERE B."userId"=$1 AND A.datetime AT TIME ZONE 'Asia/Taipei' >= $2`,
    values: [userId, moment().format('YYYY-MM-DD')]
  };
  queryForResult(resultFn, query);
};

exports.getUserOrders = (userId, resultFn) => {
  const query = {
    text: `SELECT '買' as side, id, "pvId", datetime, price, filled, amount, status FROM "orderBuys"
           WHERE "userId"=$1 AND amount != filled AND datetime AT TIME ZONE 'Asia/Taipei' >= $2
           UNION ALL
           SELECT '賣' as side, id, "pvId", datetime, price, filled, amount, status FROM "orderSells"
           WHERE "userId"=$1 AND amount != filled AND datetime AT TIME ZONE 'Asia/Taipei' >= $2`,
    values: [userId, moment().format('YYYY-MM-DD')]
  };
  queryForResult(resultFn, query);
};

exports.getUserSttw = (userId, resultFn) => {
  const query = {
    text: `SELECT (sttw - "lockSttw") as sttw FROM users WHERE id=$1`,
    values: [userId]
  };
  getForResult(resultFn, query);
};

exports.getUserSntw = (userId, pvId, resultFn) => {
  const query = {
    text: `SELECT (amount - "lockAmount") as sntw FROM "userAssets" WHERE "userId"=$1 AND "pvId"=$2`,
    values: [userId, pvId]
  };
  getForResult(resultFn, query);
};

exports.getLastDayPrice = (pvId, resultFn) => {
  const query = {
    text: `
      SELECT COALESCE(B.close, A."initialPrice") as close FROM pvs A
      LEFT JOIN "marketHistory" B ON B."pvId"=A.id
      WHERE A.id=$1
      ORDER BY date DESC limit 1;;
    `,
    values: [pvId]
  };
  getForResult(resultFn, query);
};

exports.getTrendData = (pvId, resultFn) => {
  const query = {
    text: `
      SELECT LPAD(hour::text, 2, '0') || ':' || LPAD(minute::text, 2, '0') as time, D.price FROM (
        SELECT max(A.id) as id, B."pvId", date_part('day', A.datetime AT TIME ZONE 'Asia/Taipei') as day, date_part('hour', A.datetime AT TIME ZONE 'Asia/Taipei') as hour, floor(date_part('minute', A.datetime AT TIME ZONE 'Asia/Taipei')/10)*10 as minute from trades A
        LEFT JOIN "orderBuys" B ON B.id = A."orderBuyId"
        WHERE B."pvId" = $1 AND A.datetime AT TIME ZONE 'Asia/Taipei' >= $2
        GROUP BY B."pvId", date_part('day', A.datetime AT TIME ZONE 'Asia/Taipei'), date_part('hour', A.datetime AT TIME ZONE 'Asia/Taipei'), floor(date_part('minute', A.datetime AT TIME ZONE 'Asia/Taipei')/10)) C
      LEFT JOIN trades D
      ON D.id = C.id
      ORDER BY time`,
    values: [pvId, moment().format('YYYY-MM-DD')]
  };
  queryForResult(resultFn, query);
};
