module.exports = {
  jwtSecret: process.env.JWT_SECRET,
  postgresql: process.env.POSTGRESQL
}