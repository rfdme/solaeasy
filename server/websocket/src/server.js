var express = require('express');
const jwt = require('jsonwebtoken');
var app = express();
const keys = require('./config/keys');
const path = require('path');
const log4js = require('log4js');

// 以filename設定logger分類
const logger = log4js.getLogger(path.basename(__filename));
logger.level = 'debug';

app.use(express.static('static'));

if (process.env.NODE_ENV === 'production') {
  // setup https certificate and key
  const fs = require('fs');
  const options = {
    cert: fs.readFileSync('./sslcert/fullchain.pem'),
    key: fs.readFileSync('./sslcert/privkey.pem')
  };
  var server = require('https').createServer(options, app);
  var io = require('socket.io')(server);
  var port = 443;
} else {
  var server = require('http').createServer(app);
  var io = require('socket.io')(server);
  var port = process.env.PORT || 8082;
}

server.listen(port, () => {
  logger.info('Server listening at port %d', port);
});

var trader = require('./trader.js');
trader.start(io);

var dataLoader = require('./exchangeDataLoader.js');

io.use((socket, next) => {
  if (socket.handshake.query.token === '') {
    socket.userInfo = null;
    next();
  } else if (socket.handshake.query && socket.handshake.query.token) {
    jwt.verify(socket.handshake.query.token, keys.jwtSecret, function(
      err,
      decoded
    ) {
      if (err) {
        logger.error(`error: ${err}`);
        return next(new Error('Authentication error'));
      }
      socket.userInfo = decoded;
      next();
    });
  } else {
    logger.error('Authentication error');
    next(new Error('Authentication error'));
  }
});

io.on('connection', function(socket) {
  logger.info('connection');
  socket.on('disconnect', () => {
    logger.info('disconnect');
  });

  socket.on('initPvList', fn => {
    dataLoader.getPvList(fn);
  });

  socket.on('initMarketSummary', fn => {
    dataLoader.getMarketSummary(fn);
  });

  socket.on('initTradeHistory', (pvId, fn) => {
    dataLoader.getTradeHistory(pvId, fn);
  });

  socket.on('initOrderBookBuy', (pvId, fn) => {
    dataLoader.getOrderBookBuy(pvId, fn);
  });

  socket.on('initOrderBookSell', (pvId, fn) => {
    dataLoader.getOrderBookSell(pvId, fn);
  });

  socket.on('initMyTrades', (userId, fn) => {
    if (socket.userInfo && socket.userInfo.id !== userId) {
      logger.error('403 Forbidden, initMyTrades');
      fn('403 Forbidden.');
    } else {
      dataLoader.getUserTrades(userId, fn);
    }
  });

  socket.on('initMyOrders', (userId, fn) => {
    if (socket.userInfo && socket.userInfo.id !== userId) {
      logger.error('403 Forbidden, initMyOrders');
      fn('403 Forbidden.');
    } else {
      dataLoader.getUserOrders(userId, fn);
    }
  });

  socket.on('initMySttw', (userId, fn) => {
    if (socket.userInfo && socket.userInfo.id !== userId) {
      logger.error('403 Forbidden, initMySttw');
      fn('403 Forbidden.');
    } else {
      dataLoader.getUserSttw(userId, fn);
    }
  });

  socket.on('initMySntw', (userId, pvId, fn) => {
    if (socket.userInfo && socket.userInfo.id !== userId) {
      logger.error('403 Forbidden, initMySntw');
      fn('403 Forbidden.');
    } else {
      dataLoader.getUserSntw(userId, pvId, fn);
    }
  });

  socket.on('getLastDayPrice', (pvId, fn) => {
    dataLoader.getLastDayPrice(pvId, fn);
  });

  socket.on('getTrendData', (pvId, fn) => {
    dataLoader.getTrendData(pvId, fn);
  });

  socket.on('orderBuy', (data, fn) => {
    if (socket.userInfo && socket.userInfo.id !== data.userId) {
      logger.error('403 Forbidden, orderBuy');
      fn('403 Forbidden.');
    } else {
      trader.handleOrderBuy(data, fn);
    }
  });

  socket.on('orderSell', (data, fn) => {
    if (socket.userInfo && socket.userInfo.id !== data.userId) {
      logger.error('403 Forbidden, orderSell');
      fn('403 Forbidden.');
    } else {
      trader.handleOrderSell(data, fn);
    }
  });

  // 這邊也要檢查userId，所以data裡面要帶 userId, orderId
  socket.on('cancelOrderBuy', (data, fn) => {
    if (socket.userInfo && socket.userInfo.id !== data.userId) {
      logger.error('403 Forbidden, cancelOrderBuy');
      fn('403 Forbidden.');
    } else {
      trader.handleCancelOrderBuy(data.orderId, fn);
    }
  });

  // 這邊也要檢查userId，所以data裡面要帶 userId, orderId
  socket.on('cancelOrderSell', (data, fn) => {
    if (socket.userInfo && socket.userInfo.id !== data.userId) {
      logger.error('403 Forbidden, cancelOrderSell');
      fn('403 Forbidden.');
    } else {
      trader.handleCancelOrderSell(data.orderId, fn);
    }
  });
});
