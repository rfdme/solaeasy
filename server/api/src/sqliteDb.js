const _ = require('lodash');
var sprintf = require('sprintf-js').sprintf;
var moment = require('moment');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const axios = require('axios');
const keys = require('./config/keys');
const db = require('./db');
const path = require('path');
const log4js = require('log4js');

// 以filename設定logger分類
const logger = log4js.getLogger(path.basename(__filename));
logger.level = 'debug';
const url = require('./config/urls');

const findPv = id => {
  return new Promise(async (resolve, reject) => {
    try {
      const query = {
        text: 'SELECT * FROM pvs WHERE id = $1',
        values: [id]
      };
      const { rows } = await db.query(query);
      resolve(rows);
    } catch (err) {
      logger.error(err.stack);
      reject(err);
    }
  });
};

const findAllPv = id => {
  return new Promise(async (resolve, reject) => {
    try {
      const query = {
        text: 'SELECT * FROM pvs'
      };
      const { rows } = await db.query(query);
      resolve(rows);
    } catch (err) {
      logger.error(err.stack);
      reject(err);
    }
  });
};

const findManagerById = id => {
  return new Promise(async (resolve, reject) => {
    try {
      const query = {
        text: 'SELECT id, email, pwd, "google2FASecretBase32", "google2FAStatus" FROM managers WHERE id=$1',
        values: [id]
      };
      const { rows } = await db.query(query);
      resolve(rows);
    } catch (err) {
      logger.error(err.stack);
      reject(err);
    }
  });
};

const findManagerByEmail = email => {
  return new Promise(async (resolve, reject) => {
    try {
      const query = {
        text:
          'SELECT id, email, pwd, "google2FASecretBase32", "google2FAStatus" FROM managers WHERE email=$1 AND pwd IS NOT NULL',
        values: [email]
      };
      const { rows } = await db.query(query);
      resolve(rows);
    } catch (err) {
      logger.error(err.stack);
      reject(err);
    }
  });
};

const findUsersByManager = id => {
  return new Promise(async (resolve, reject) => {
    try {
      const query = {
        text: `SELECT B.id, B.name FROM appointment A
          LEFT JOIN users B ON B.id=A."userId"
          WHERE A."managerId"=$1 ORDER BY B.id`,
        values: [id]
      };
      const { rows } = await db.query(query);
      resolve(rows);
    } catch (err) {
      logger.error(err.stack);
      reject(err);
    }
  });
};

const verifyPassword = (email, pwd) => {
  return new Promise(async (resolve, reject) => {
    try {
      const query = {
        text: 'SELECT email, pwd FROM managers WHERE email = $1',
        values: [email]
      };
      const { rows } = await db.query(query);
      if (_.isEmpty(rows)) {
        logger.info('no this manager');
        resolve(false);
      } else {
        const match = await bcrypt.compare(pwd, rows[0].pwd);
        if (match) {
          resolve(true);
        } else {
          logger.info('password not match');
          resolve(false);
        }
      }
    } catch (err) {
      logger.error(err.stack);
      reject(err);
    }
  });
};

const modifyPassword = (email, newPwd) => {
  return new Promise(async (resolve, reject) => {
    try {
      const saltRounds =
        typeof keys.saltRounds === 'number' ? keys.saltRounds : parseInt(keys.saltRounds, 10);
      let digest = await bcrypt.hash(newPwd, saltRounds);

      const query = {
        text: 'UPDATE managers SET pwd=$1 WHERE email=$2',
        values: [digest, email]
      };
      const updateResult = await db.query(query);
      if (updateResult.rowCount === 1) {
        resolve(true);
      } else {
        resolve(false);
      }
    } catch (err) {
      logger.error(err.stack);
      reject(err);
    }
  });
};

const resetPassword = email => {
  return new Promise(async (resolve, reject) => {
    try {
      const randomPassword = crypto.randomBytes(4).toString('hex');
      const saltRounds =
        typeof keys.saltRounds === 'number' ? keys.saltRounds : parseInt(keys.saltRounds, 10);
      let digest = await bcrypt.hash(randomPassword, saltRounds);

      const query = {
        text: 'UPDATE managers SET pwd=$1, "mailVerified"=false WHERE email=$2',
        values: [digest, email]
      };
      const updateResult = await db.query(query);
      if (updateResult.rowCount === 1) {
        resolve(randomPassword);
      } else {
        resolve(false);
      }
    } catch (err) {
      logger.error(err.stack);
      reject(err);
    }
  });
};

const generateEventBySimulate = (token, totalAmount, date, data) => {
  return new Promise(async (resolve, reject) => {
    try {
      const deleteSql = {
        text: 'DELETE FROM "eventBySimulate" WHERE date=$1 AND "pvId"=$2',
        values: [date, token]
      };
      await db.query(deleteSql);

      const dayProfit = data.dayProfit;
      const dayRentCost = data.rentDayCostFloat + data.rentDayCostFixed;
      const dayMeterRentCost = data.meterRentDayCost;
      const dayOperateCost = data.operationDayCostFloat + data.operationDayCostFixed;
      const dayServiceCost = data.serviceDayCostFloat;
      const dayLoanCost = data.loanDayCost;
      const dayInsuranceCost = data.insuranceDayCost;
      const dayLoanFeeCost = data.loanFeeDayCost;
      const dayBusinessTax = data.dayBusinessTax;

      const insertSql = {
        text: `INSERT INTO "eventBySimulate" ("userId", "pvId", date, profit, "rentCost", "meterRentCost", "insuranceCost", "operateCost", "serviceCost", "loanCost", "loanFeeCost", "businessTax", "updateDt")
                SELECT "userId", "pvId", $1, amount*$3/$2, amount*$4/$2, amount*$5/$2, amount*$6/$2, amount*$7/$2, amount*$8/$2, amount*$9/$2, amount*$10/$2, amount*$11/$2, $12 FROM "userAssets"
                WHERE "pvId" = $13 AND amount > 0`,
        values: [
          date,
          totalAmount,
          dayProfit,
          dayRentCost,
          dayMeterRentCost,
          dayInsuranceCost,
          dayOperateCost,
          dayServiceCost,
          dayLoanCost,
          dayLoanFeeCost,
          dayBusinessTax, //TODO借款還完後, 應屬於profit
          moment(),
          token
        ]
      };
      await db.query(insertSql);
      resolve(true);
    } catch (err) {
      logger.error(err.stack);
      resolve(false);
    }
  });
};

const addGoogle2faSecretById = (id, secret) => {
  return new Promise(async (resolve, reject) => {
    try {
      const query = {
        text: 'UPDATE managers SET "google2FASecretBase32" = $1 WHERE id = $2 RETURNING *',
        values: [secret, id]
      };
      const { rows } = await db.query(query);
      resolve(rows);
    } catch (err) {
      logger.error(err.stack);
      reject(err);
    }
  });
};

const getInsuranceList = pvId => {
  return new Promise(async (resolve, reject) => {
    try {
      const result = await db.query(
        `SELECT "beginDate", "duration", "cost" FROM "pvInsurance"
           WHERE "pvId"=$1`,
        [pvId]
      );

      resolve(result.rows);
    } catch (err) {
      logger.error(err.stack);
      reject(err);
    }
  });
};

const getLoanFeeList = pvId => {
  return new Promise(async (resolve, reject) => {
    try {
      const result = await db.query(
        `SELECT "beginDate", "duration", "cost" FROM "pvLoanFee"
           WHERE "pvId"=$1`,
        [pvId]
      );

      resolve(result.rows);
    } catch (err) {
      logger.error(err.stack);
      reject(err);
    }
  });
};

const getLoanList = pvId => {
  return new Promise(async (resolve, reject) => {
    try {
      const result = await db.query(
        `SELECT "beginDate", period, amount, rate FROM "pvLoan"
           WHERE "pvId"=$1`,
        [pvId]
      );

      resolve(result.rows);
    } catch (err) {
      logger.error(err.stack);
      reject(err);
    }
  });
};

module.exports = {
  findPv,
  findAllPv,
  findManagerById,
  findManagerByEmail,
  findUsersByManager,
  verifyPassword,
  modifyPassword,
  resetPassword,
  generateEventBySimulate,
  addGoogle2faSecretById,
  getInsuranceList,
  getLoanFeeList,
  getLoanList
};
