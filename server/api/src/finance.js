const moment = require('moment');

//本金平均攤還法
const equalPrincipalPayment = ({ loanAmount, loanPeriod, loanRate, loanBeginDate, date }) => {
  const terms = loanPeriod * 12;
  const loanEnd = moment(loanBeginDate).add(loanPeriod, 'years');

  if (moment(date, 'YYYY-MM-DD').isBetween(loanBeginDate, loanEnd, null, '[)')) {
    let termIndex = moment(date, 'YYYY-MM-DD').diff(loanBeginDate, 'months');
    let termStart = moment(loanBeginDate, 'YYYY-MM-DD').add(termIndex, 'months');
    let termEnd = moment(loanBeginDate, 'YYYY-MM-DD').add(termIndex + 1, 'months');

    //當遇到繳費日是29~31, 而當期又是小月時, 必須將將此日計入下一期
    if (date === termEnd.format('YYYY-MM-DD')) {
      termIndex = termIndex + 1;
      termStart = moment(loanBeginDate, 'YYYY-MM-DD').add(termIndex, 'months');
      termEnd = moment(loanBeginDate, 'YYYY-MM-DD').add(termIndex + 1, 'months');
    }

    let principalPerTerm = Math.ceil(loanAmount / terms);
    const remainPrincipal = loanAmount - principalPerTerm * termIndex;
    const interest = Math.round((remainPrincipal * loanRate) / 12);
    const daysOfTerm = termEnd.diff(termStart, 'days');

    //最後一期的本金
    if (termIndex === terms - 1) {
      principalPerTerm = remainPrincipal;
    }

    const dayLoanCost = (principalPerTerm + interest) / daysOfTerm;
    return dayLoanCost;
  } else {
    return 0;
  }
};

const simulatedDailyProfit = ({
  date,
  power, //發電量
  sellPrice, //躉售價格
  totalCapital, //系統成本
  operationRateByCapital, //營運費率(固定系統成本)
  operationRateByIncome, //營運費率(電費抽成)
  serviceRateByIncome, //SOLA服務費率(電費抽成)
  rentRateByIncome, //租金費率(電費抽成)
  rentYearCost, //年固定租金
  rentWithTax, //租金計算基礎(含稅未稅)
  insuranceList, //保險清單
  loanFeeList, //貸款手續單清單
  loanList //貸款清單
}) => {
  //根據發電量, 帳單公式, 模擬出電費
  const dayIncome = sellPrice * power * 0.999; //0.001線路損失率
  const dayIncomeWithTax = dayIncome * 1.05;
  const meterRentDayCost = 8; //月租金150~300

  const daysOfYear = moment().isLeapYear() ? 366 : 365;

  //屋頂租金(固定)
  const rentDayCostFixed = rentYearCost / daysOfYear;

  //屋頂租金(抽成), 自然人計算基礎(不扣電錶租費), 法人計算基礎(扣電錶租費)
  let rentDayCostFloat = 0;
  if (rentWithTax) {
    rentDayCostFloat = Math.round(rentRateByIncome * (dayIncomeWithTax - meterRentDayCost));
  } else {
    const incomeForRent = dayIncomeWithTax / 1.05;
    rentDayCostFloat = Math.round(rentRateByIncome * incomeForRent);
  }
  if (rentDayCostFloat < 0) {
    rentDayCostFloat = 0;
  }

  //營運費用(固定)
  const operationDayCostFixed = (operationRateByCapital * totalCapital) / daysOfYear;
  //營運費用(抽成)
  let operationDayCostFloat = operationRateByIncome * (dayIncomeWithTax - meterRentDayCost);
  if (operationDayCostFloat < 0) {
    operationDayCostFloat = 0;
  }

  //索拉費用(抽成)
  let serviceDayCostFloat = serviceRateByIncome * (dayIncomeWithTax - meterRentDayCost);
  if (serviceDayCostFloat < 0) {
    serviceDayCostFloat = 0;
  }

  const endDate = moment(date)
    .add(1, 'days')
    .format('YYYY-MM-DD');

  //保險費用
  const insuranceDayCost = getRangeCost(insuranceList, date, endDate);

  //貸款手續費用
  const loanFeeDayCost = getRangeCost(loanFeeList, date, endDate);

  //貸款費用
  const loanDayCost = getRangeLoanCost(loanList, date, endDate);

  let dayProfit =
    dayIncomeWithTax -
    operationDayCostFixed -
    operationDayCostFloat -
    serviceDayCostFloat -
    rentDayCostFixed -
    rentDayCostFloat -
    meterRentDayCost -
    insuranceDayCost -
    loanDayCost -
    loanFeeDayCost;

  //預扣5%營業稅
  const taxRate = 0.05;
  let dayBusinessTax = 0;
  /*TODO: 待確認
  dayBusinessTax = dayProfit * taxRate;
  dayProfit = dayProfit - dayBusinessTax;
  */

  const result = {
    dayIncomeWithTax,
    dayProfit,
    rentDayCostFixed,
    rentDayCostFloat,
    operationDayCostFixed,
    operationDayCostFloat,
    serviceDayCostFloat,
    meterRentDayCost,
    insuranceDayCost,
    loanDayCost,
    loanFeeDayCost,
    dayBusinessTax
  };

  return result;
};

const getRangeCost = (list, begin, end) => {
  let counter = [];
  for (let i = 0; i < list.length; i++) {
    counter.push(0);
  }

  let day = moment(begin, 'YYYY-MM-DD');
  while (day.isBefore(end)) {
    for (let i = 0; i < list.length; i++) {
      const endDate = moment(list[i].beginDate)
        .add(list[i].duration, 'years')
        .format('YYYY-MM-DD');
      if (day.isBetween(list[i].beginDate, endDate, null, '[)')) {
        counter[i] += 1;
      }
    }
    day.add(1, 'days');
  }

  let totalCost = 0;
  for (let i = 0; i < list.length; i++) {
    const { beginDate, duration, cost } = list[i];
    const endDate = moment(beginDate)
      .add(duration, 'years')
      .format('YYYY-MM-DD');
    const days = moment(endDate, 'YYYY-MM-DD').diff(beginDate, 'days');
    totalCost += (cost * counter[i]) / days;
  }

  return totalCost;
};

const getRangeLoanCost = (list, begin, end) => {
  let loanCost = 0;
  for (let i = 0; i < list.length; i++) {
    const { amount, period, rate, beginDate } = list[i];
    let day = moment(begin);
    while (day.isBefore(end)) {
      loanCost += equalPrincipalPayment({
        loanAmount: amount,
        loanPeriod: period,
        loanRate: rate,
        loanBeginDate: beginDate,
        date: day.format('YYYY-MM-DD')
      });
      day.add(1, 'days');
    }
  }
  return loanCost;
};

module.exports = {
  equalPrincipalPayment,
  simulatedDailyProfit,
  getRangeCost,
  getRangeLoanCost
};
