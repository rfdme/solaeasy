const { Pool, types } = require('pg');
const moment = require('moment');
const BN = require('bignumber.js').clone({ DECIMAL_PLACES: 16 });
const keys = require('../config/keys');

const DATE_OID = 1082;
const TIMESTAMPTZ_OID = 1184;
const TIMESTAMP_OID = 1114;
const NUMERIC_OID = 1700;

const dateParseFn = val => {
	return val === null ? null : moment(val).format('YYYY-MM-DD');
};

const datetimeParseFn = val => {
	return val === null ? null : moment(val).format('YYYY-MM-DD HH:mm:ss');
};

const numericParseFn = val => {
	return BN(val);
};

types.setTypeParser(DATE_OID, dateParseFn);
types.setTypeParser(TIMESTAMPTZ_OID, datetimeParseFn);
types.setTypeParser(TIMESTAMP_OID, datetimeParseFn);
types.setTypeParser(NUMERIC_OID, numericParseFn);

//const connectionString = 'postgresql://postgres:sola1234@localhost:5432/sola';

const connectionString = keys.postgresql;

const pool = new Pool({
	connectionString: connectionString
});

module.exports = {
	query: (text, params, callback) => {
		return pool.query(text, params, callback);
	}
};
