const axios = require('axios');
const moment = require('moment');
const db = require('../db');

const path = require('path');
const log4js = require('log4js');

// 以filename設定logger分類
const logger = log4js.getLogger(path.basename(__filename));
logger.level = 'debug';

exports.start = async () => {
  while (true) {
    await sendTransaction();
  }
};

const sendTransaction = async () => {
  try {
    const checkConfig = await db.query(
      'SELECT value FROM config WHERE field=$1',
      ['bcTransaction']
    );

    if (checkConfig.rowCount === 0 || checkConfig.rows[0].value === 'false') {
      return;
    }

    await db.query('BEGIN');

    //先進先出, 避免因順序不同造成區塊失敗
    const getTransationResult = await db.query(
      'SELECT id, "txType", "txId", data FROM "bcTransation" WHERE status is NULL ORDER BY id limit 1'
    );

    if (getTransationResult.rowCount > 0) {
      const id = getTransationResult.rows[0].id;
      const txType = getTransationResult.rows[0].txType;
      const txId = getTransationResult.rows[0].txId;
      const data = getTransationResult.rows[0].data;

      await axios
        .post(`http://54.95.137.247:3000/bc/private/exchange/` + txType, data)
        .then(async response => {
          if (response.data.status === 1) {
            await db.query(
              'INSERT INTO "bcTransationHistory" (id, "txType", "txId", info, data) VALUES($1,$2,$3,$4,$5)',
              [id, txType, txId, response.data, data]
            );
            await db.query('DELETE FROM "bcTransation" WHERE id=$1', [id]);
            await updateBlockInfo(txType, txId, response.data, data);
          } else {
            await db.query(
              'UPDATE "bcTransation" SET status=FALSE WHERE id=$1',
              [id]
            );
            await db.query(
              `UPDATE config SET value='false' WHERE field='bcTransaction'`
            );
          }
        })
        .catch(async err => {
          logger.error(err);
          await db.query('UPDATE "bcTransation" SET status=FALSE WHERE id=$1', [
            id
          ]);
          await db.query(
            `UPDATE config SET value='false' WHERE field='bcTransaction'`
          );
        });
    }
    await db.query('COMMIT');
  } catch (e) {
    await db.query('ROLLBACK');
  }
};

const updateBlockInfo = async (txType, txId, info, data) => {
  switch (txType) {
    case 'tradeSync':
      await db.query(
        'UPDATE trades SET "blockInfo"=$1, "blockData"=$2 WHERE id=$3',
        [info, data, txId]
      );
      break;
    case 'profitSync':
      await db.query(
        'UPDATE "eventByBill" SET "blockInfo"=$1, "blockData"=$2 WHERE id=$3',
        [info, data, txId]
      );
      break;
    case 'repaySync':
      await db.query(
        'UPDATE "eventByRepay" SET "blockInfo"=$1, "blockData"=$2 WHERE id=$3',
        [info, data, txId]
      );
      break;
    case 'depositSync':
      await db.query(
        'UPDATE "cashinRecords" SET "blockInfo"=$1, "blockData"=$2 WHERE id=$3',
        [info, data, txId]
      );
      break;
    case 'withdrawSync':
      await db.query(
        'UPDATE "cashoutRecords" SET "blockInfo"=$1, "blockData"=$2 WHERE id=$3',
        [info, data, txId]
      );
      break;
    default:
      break;
  }
};
