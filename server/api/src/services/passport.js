const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const keys = require('../config/keys');
const { findManagerById, findManagerByEmail, verifyPassword } = require('../sqliteDb');
const _ = require('lodash');

passport.serializeUser((manager, done) => {
  console.log(`manager = ${JSON.stringify(manager)}`);
  done(null, manager.id);
});

passport.deserializeUser(async (id, done) => {
  console.log(`id = ${id}`);
  const manager = await findManagerById(id);
  done(null, manager[0]);
});

passport.use(
  new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'pwd',
      cookie: true
    },
    async (email, pwd, done) => {
      console.log(`email = ${email}, pwd = ${pwd}`);
      const existingManager = await findManagerByEmail(email);

      if (!_.isEmpty(existingManager)) {
        const pass = await verifyPassword(email, pwd);
        if (!pass) {
          return done(null, false, { error: '帳號或密碼不正確' });
        } else {
          return done(null, existingManager[0]);
        }
      }

      done(null, false, { error: '尚未註冊' });
    }
  )
);
