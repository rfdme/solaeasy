const schedule = require('node-schedule');
const axios = require('axios');
const moment = require('moment');
const db = require('../db');
const path = require('path');
const log4js = require('log4js');

// 以filename設定logger分類
const logger = log4js.getLogger(path.basename(__filename));
logger.level = 'debug';

const url = require('../config/urls');

exports.start = () => {
  logger.info('start schedule');
  simulateDaily();
  clearanceAssetDaily();
  clearanceBillDaily();
  dailyMarketRecord();
  repay();
};

const checkSuspend = async () => {
  const result = await db.query('SELECT value FROM config WHERE field=$1', ['schedule']);

  if (result.rowCount === 0 || result.rows[0].value === 'false') {
    return true;
  }

  return false;
};

//每5分鐘產生模擬收益
const simulateDaily = () => {
  var j = schedule.scheduleJob('*/5 * * * *', () => {
    logger.info('run schedule [simulateDaily]');
    logger.info(`${url.schedulerURL}/api/v1/simulateDaily?date=${moment().format('YYYY-MM-DD')}`);
    axios
      .get(`${url.schedulerURL}/api/v1/simulateDaily?date=${moment().format('YYYY-MM-DD')}`)
      .then(response => {
        logger.info(response.data);
      })
      .catch(err => {
        logger.error(err);
      });
  });
};

//每天22點進行結算資產
const clearanceAssetDaily = () => {
  var j = schedule.scheduleJob('0 22 * * *', async () => {
    const suspend = await checkSuspend();
    if (suspend) {
      logger.info('suspend schedule [clearanceAssetDaily]');
      return;
    }

    logger.info('run schedule [clearanceAssetDaily]');
    axios
      .get(`${url.schedulerURL}/api/v1/clearance/asset?date=${moment().format('YYYY-MM-DD')}`)
      .then(response => {
        logger.info(response.data);
      })
      .catch(err => {
        logger.error(err);
      });
  });
};

//每天22:30進行結算帳單
const clearanceBillDaily = () => {
  var j = schedule.scheduleJob('30 22 * * *', async () => {
    const suspend = await checkSuspend();
    if (suspend) {
      logger.info('suspend schedule [clearanceBillDaily]');
      return;
    }

    logger.info('run schedule [clearanceBillDaily]');
    axios
      .get(`${url.schedulerURL}/api/v1/bill/dispatch?date=${moment().format('YYYY-MM-DD')}`)
      .then(response => {
        logger.info(response.data);
      })
      .catch(err => {
        logger.error(err);
      });
  });
};

//每天23點進行還本
const repay = () => {
  var j = schedule.scheduleJob('0 23 * * *', async () => {
    const suspend = await checkSuspend();
    if (suspend) {
      logger.info('suspend schedule [repay]');
      return;
    }

    logger.info('run schedule [repay]');
    axios
      .get(`${url.schedulerURL}/api/v1/repay?date=${moment().format('YYYY-MM-DD')}`)
      .then(response => {
        logger.info(response.data);
      })
      .catch(err => {
        logger.error(err);
      });
  });
};

//每天23:30點進行記錄並重置market
const dailyMarketRecord = () => {
  var j = schedule.scheduleJob('30 23 * * *', async () => {
    logger.info('run schedule [dailyMarketRecord]');
    try {
      await db.query('BEGIN');
      const recordMarket = {
        text: `
          INSERT INTO "marketHistory" ("pvId", date, open, high, low, close, volume)
          SELECT A.id, $1,
            COALESCE(B.open, A."initialPrice") as open,
            COALESCE(B.high, A."initialPrice") as high,
            COALESCE(B.low, A."initialPrice") as low,
            COALESCE(B.price, A."initialPrice") as close,
            COALESCE(B.volume, 0) as volume FROM pvs A
          LEFT JOIN "marketInfo" B ON B."pvId" = A.id`,
        values: [moment().format('YYYY-MM-DD')]
      };
      await db.query(recordMarket);

      const resetMarket = {
        text: `UPDATE "marketInfo" SET open=price, high=price, low=price, volume=0`
      };
      await db.query(resetMarket);

      await db.query('COMMIT');
    } catch (err) {
      await db.query('ROLLBACK');
      logger.error(err.stack);
      logger.info('dailyMarketRecord fail', moment().format('YYYY-MM-DD'));
    }
  });
};
