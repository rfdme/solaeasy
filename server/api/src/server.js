let express = require('express');
const bodyParser = require('body-parser');
let cors = require('cors');
const morgan = require('morgan');
const cookieSession = require('cookie-session');
const passport = require('passport');
const keys = require('./config/keys');
require('./services/passport');
const axios = require('axios');
const expressJWT = require('express-jwt');
const https = require('https');
const path = require('path');
// const log4js = require('log4js');
const log4js = require('./modules/logger');

// get logger 'exit', this logger will record request/response info. at the end of api service
const logger = log4js.getLogger('exit');
const uuid = require('uuid');

const response = require('./modules/rsps');
// 以filename設定logger分類
// const logger = log4js.getLogger(path.basename(__filename));
// logger.level = 'debug';

// load api router
const authRoutes = require('./routes/authRoutes');
const pv = require('./routes/pv');
const event = require('./routes/event');
const assets = require('./routes/assets');
const profit = require('./routes/profit');
const simulateDaily = require('./routes/simulateDaily');
const repay = require('./routes/repay');
const clearance = require('./routes/clearance');
const user = require('./routes/user');
const account = require('./routes/account');
const detail = require('./routes/detail');
const dashboard = require('./routes/dashboard');
const donation = require('./routes/donation');
const test = require('./routes/test');
const apikeys = require('./routes/apikeys');
const spv = require('./routes/spv');
const bill = require('./routes/bill');

if (process.env.NODE_ENV === 'production') {
  //execute scheduler
  const scheduler = require('./services/scheduler.js');
  scheduler.start();

  //execute bcTransation
  const bcTransation = require('./services/bcTransation.js');
  bcTransation.start();
}
let app = express();
app.use(express.static('static'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(cors({ origin: '*' }));
// temporary suspend morgan, using logger
// app.use(morgan('dev'));

app.use(
  expressJWT({
    secret: keys.jwtSecret
  }).unless({
    path: [
      '/api/v1/auth/local',
      '/api/v1/auth/resetPwd',
      /\/api\/v1\/pv\//,
      '/api/v1/event/pv',
      '/api/v1/profit',
      '/api/v1/simulateDaily',
      '/api/v1/simulateDaily/test',
      '/api/v1/test',
      '/api/v1/test/billsPreparing',
      '/api/v1/repay',
      '/api/v1/repay/test',
      '/api/v1/clearance/asset',
      '/api/v1/clearance/test',
      /\/api\/v1\/bill\//,
      '/api/v1/account/cashin',
      '/api/v1/donation/ngoAsset',
      /\/api\/v1\/spv\//,
      /\/api\/v1\/dashboard\//,
      /\/api\/v1\/files\//,
      '/',
      /\/Home/,
      /\/Robot\//,
      '/favicon.ico',
      '/favicon.png',
      /\/static\//
    ]
  })
);

app.use((err, req, res, next) => {
  if (err.name === 'UnauthorizedError') {
    logger.error(err);
    let rsps = new response();
    rsps.msg = 'authorized error';
    rsps.result = { error: err };
    res.status(401).send(rsps.getResult());
  }
});

app.use(
  cookieSession({
    maxAge: 30 * 24 * 60 * 60 * 1000,
    keys: [keys.cookieKey]
  })
);
app.use(passport.initialize());
app.use(passport.session());

// for logger, assign every incoming api request an unique id
app.use(function(req, res, next) {
  req.reqId = uuid.v1();
  next();
});
/*
    for logger, at the end of every api request, log req date time, level, uuid, token, ip, method, http versin, response status,
    content-length, referrer, user-agent and response time (ms)
    ex.
    [2018-10-15T17:40:36.491] [INFO] [5e7131e0-d05e-11e8-a416-a9bea5fe0c98] [token:Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNTM3OTUwMzQzfQ.18memWm41BonuxpIfAFuJxAqNI2fOhrs0_1lG-9U5uI] ::1 - "GET /api/v1/company HTTP/1.1" 200 229  PostmanRuntime/7.3.0 12 ms
  */
app.use(
  log4js.connectLogger(logger, {
    level: log4js.levels.INFO,
    format: (req, res, format) => {
      return format(
        `[${req.reqId}] [userId:${
          req.user === undefined || req.user === null ? 'no need jwt' : req.user.id
        }] [token:${
          req.headers.authorization
        }] :remote-addr - ":method :url HTTP/:http-version" :status :content-length :referrer :user-agent :response-time ms`
      );
    }
  })
);

app.use('/api/v1/auth', authRoutes);
app.use('/api/v1/pv', pv);
app.use('/api/v1/event', event);
app.use('/api/v1/assets', assets);
app.use('/api/v1/profit', profit);
app.use('/api/v1/simulateDaily', simulateDaily);
app.use('/api/v1/repay', repay);
app.use('/api/v1/clearance', clearance);
app.use('/api/v1/user', user);
app.use('/api/v1/account', account);
app.use('/api/v1/detail', detail);
app.use('/api/v1/dashboard', dashboard);
app.use('/api/v1/donation', donation);
app.use('/api/v1/test', test);
app.use('/api/v1/files', express.static(__dirname + '/files'));
app.use('/api/v1/apikeys', apikeys);
app.use('/api/v1/spv', spv);
app.use('/api/v1/bill', bill);

const port = process.env.PORT || 8081;
let server = app.listen(port, function() {
  let host = server.address().address;

  logger.info('API at http://%s:%s', host, port);
});

if (process.env.NODE_ENV === 'production') {
  // Express will serve up production assets like our main.js file, or main.css file
  app.use(express.static('../../client/build'));

  // Express will serve up the index.html file if it doesn't recognize the route.
  const path = require('path');
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../../../client', 'build', 'index.html'));
  });
}

function rollback(transaction) {
  transaction.rollback(function(err) {
    if (err) {
      logger.error(err);
    }
  });
}

module.exports = server;
