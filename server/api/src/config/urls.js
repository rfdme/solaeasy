module.exports = {
  apiURL: process.env.API_URL || 'http://localhost',
  schedulerURL: `${process.env.API_URL || 'http://localhost'}:${process.env.PORT || 8081}`
};
