module.exports = {
  cookieKey: process.env.COOKIE_KEY,
  jwtSecret: process.env.JWT_SECRET,
  postgresql: process.env.POSTGRESQL,
  saltRound: process.env.SALT_ROUND,
  loggerfile: `${process.env.LOG_FILE || 'spv.log'}`,
  loggerpath: `${process.env.LOG_PATH || './logs'}`,
  loggerlevel: `${process.env.LOG_LEVEL || 'INFO'}`,
  loggertype: `${process.env.LOG_TYPE || 'file'}`,

  //urls
  assetURL: process.env.ASSET_URL,
  itsURL: process.env.ITS_URL
};
