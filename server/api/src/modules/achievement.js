const moment = require('moment');
const axios = require('axios');
const keys = require('../config/keys');

//row contain property: id, projectCode, capacity, decay, annualAmount, validityDateBegin
exports.getRate = async rows => {
  const now = moment();

  const data = await Promise.all(
    rows.map(async pv => {
      const begin = moment(pv.validityDateBegin);
      if (!begin.isValid()) {
        return {
          id: pv.id,
          annualAmount: pv.annualAmount,
          totalValues: 0,
          achievementRate: 0
        };
      }

      if (now.isSame(begin, 'year')) {
        // 第一年，需要照比例減少分母
        if (now.isBefore(begin)) {
          return {
            id: pv.id,
            annualAmount: pv.annualAmount,
            totalValues: 0,
            achievementRate: 0
          };
        } else {
          const values = await axios.get(
            `${keys.itsURL}/v1/values/${pv.projectCode}?start=${begin.format('YYYY-MM-DD')}&end=${now
              .add(1, 'days')
              .format('YYYY-MM-DD')}`
          );
          const reducer = (accumulator, currentValue) => {
            let sum = parseInt(accumulator.value, 10) + currentValue.value;
            return { value: sum };
          };
          const total =
            values.data.result == undefined || values.data.result.pvdata == undefined
              ? { value: 0 }
              : values.data.result.pvdata.reduce(reducer, { value: 0 });

          return {
            id: pv.id,
            annualAmount: pv.annualAmount,
            totalValues: total.value,
            achievementRate: (
              total.value /
              ((pv.capacity *
                pv.annualAmount *
                moment()
                  .endOf('year')
                  .add(1, 'days')
                  .diff(begin, 'days')) /
                (moment().isLeapYear() ? 366 : 365))
            ).toPrecision(5)
          };
        }
      } else {
        const values = await axios.get(
          `${keys.itsURL}/v1/values/${pv.projectCode}?start=${now.format('YYYY')}-01-01&end=${now
            .add(1, 'days')
            .format('YYYY-MM-DD')}`
        );
        const reducer = (accumulator, currentValue) => {
          let sum = parseInt(accumulator.value, 10) + currentValue.value;
          return { value: sum };
        };

        //隨每年進行效率衰減
        const diffYear = now.diff(begin, 'years');
        const annualAmount = pv.annualAmount * Math.pow(1 - pv.decay, diffYear);

        const total =
          values.data.result == undefined || values.data.result.pvdata == undefined
            ? { value: 0 }
            : values.data.result.pvdata.reduce(reducer, { value: 0 });
        return {
          id: pv.id,
          annualAmount: annualAmount,
          totalValues: total.value,
          achievementRate: (total.value / (pv.capacity * annualAmount)).toPrecision(5)
        };
      }
    })
  );

  return data;
};
