var log4js = require('log4js');
var fs = require('fs');

const keys = require('../config/keys');
var basePath = keys.loggerpath;

var confirmPath = function (pathStr) {
  if (!fs.existsSync(pathStr)) {
    fs.mkdirSync(pathStr);
    console.log('createPath: ' + pathStr);
  }
}

if (basePath) {
  confirmPath(basePath)
}
/*
[pattern]
%d - date time
%p - level
%X{reqId} - request uuid
%m - log data
%n - newline

[appenders]
default: use by routes/ooxx.js, log detail info. about the request input or other necessary data
exit: use by server.js, log for every api request at the end of service
*/
log4js.configure({
  appenders: {
    default: {
      type: keys.loggertype,
      filename: basePath + '/' + keys.loggerfile,
      layout: {
        type: 'pattern',
        pattern: '[%d] [%p] [%X{reqId}] - %m%n'
      }
    },
    exit: {
        type: keys.loggertype,
        filename: basePath + '/' + keys.loggerfile,
        layout: {
          type: 'pattern',
          pattern: '[%d] [%p] %m%n'
        }
      }
  },
  categories: {
    default: {
      appenders: ['default'],
      level: keys.loggerlevel
    },
    exit: {
        appenders: ['exit'],
        level: keys.loggerlevel
      }
  }
})

module.exports = log4js