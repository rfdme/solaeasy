const canAccessUser = (user, userId) => {
  for (let i = 0; i < user.users.length; i++) {
    if (userId === user.users[i].id) {
      return true;
    }
  }
  return false;
};

module.exports = {
  canAccessUser
};
