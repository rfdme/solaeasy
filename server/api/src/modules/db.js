const { Pool, types } = require('pg');
const moment = require('moment');
const BN = require('bignumber.js').clone({ DECIMAL_PLACES: 16 });
const keys = require('../config/keys');
const speakeasy = require('speakeasy');
const axios = require('axios');

const achievement = require('../modules/achievement.js');

class Db {
  constructor() {
    const DATE_OID = 1082;
    const TIMESTAMPTZ_OID = 1184;
    const TIMESTAMP_OID = 1114;
    const NUMERIC_OID = 1700;

    const dateParseFn = val => {
      return val === null ? null : moment(val).format('YYYY-MM-DD');
    };

    const datetimeParseFn = val => {
      return val === null ? null : moment(val).format('YYYY-MM-DD HH:mm:ss');
    };

    const numericParseFn = val => {
      return BN(val);
    };

    types.setTypeParser(DATE_OID, dateParseFn);
    types.setTypeParser(TIMESTAMPTZ_OID, datetimeParseFn);
    types.setTypeParser(TIMESTAMP_OID, datetimeParseFn);
    types.setTypeParser(NUMERIC_OID, numericParseFn);

    this.pool = new Pool({
      connectionString: keys.postgresql
    });
  }

  query(text, params, callback) {
    return this.pool.query(text, params, callback);
  }

  async check2FAenable(managerId) {
    const { rows } = await this.query(`SELECT "google2FAStatus" FROM managers WHERE id=$1`, [managerId]);
    return rows[0].google2FAStatus;
  }

  async verify2FAToken(managerId, token) {
    const { rows } = await this.query(`SELECT "google2FASecretBase32" FROM managers WHERE id=$1`, [
      managerId
    ]);
    const tokenValidates = speakeasy.totp.verify({
      secret: rows[0].google2FASecretBase32,
      encodeing: 'base32',
      token: token
    });
    // console.log(`tokenValidates = ${tokenValidates}`)
    return tokenValidates;
  }

  async insertApiKey(data) {
    try {
      await this.query('BEGIN');
      await this.query(
        `INSERT INTO "apikeys" (key, secret, "readInfo", trade, withdraw, "userId") VALUES($1,$2,$3,$4,$5,$6) RETURNING id`,
        [data.key, data.secret, data.readInfo, data.trade, data.withdraw, data.userId]
      );
      await this.query('COMMIT');
    } catch (err) {
      console.log(err);
      await this.query('ROLLBACK');
    }
  }

  async queryApiKeys(userId) {
    const { rows } = await this.query(`SELECT * FROM apikeys WHERE "userId"=$1`, [userId]);
    if (rows.length > 0) {
      return rows;
    } else {
      return null;
    }
  }

  async queryAssetsByUserId(userId) {
    const { rows } = await this.query(
      `SELECT id, "holdAmount", name, status, capacity, "totalAmount", "initialPrice", price, "validityDateBegin", "validityDuration", MAX(date) as "lastRepayDate" FROM (SELECT 
        A."pvId" as id, 
        A.amount as "holdAmount", 
        B.name, 
        B.status, 
        B.capacity, 
        B."ownedCapital"/10.0 as "totalAmount",
        COALESCE(C.price, B."initialPrice") as price,
        B."validityDateBegin",
        B."validityDuration",
        D.date
      FROM "userAssets" A
      LEFT JOIN pvs B ON B.id = A."pvId"
      LEFT JOIN "marketInfo" C ON C."pvId" = A."pvId"
      LEFT JOIN "eventByRepay" D ON D."pvId" = A."pvId"
      WHERE A."userId"=$1) AS T
      GROUP BY id, "holdAmount", name, status, capacity, "totalAmount", price, "validityDateBegin"
      ORDER BY id`,
      [userId]
    );
    if (rows.length > 0) {
      return rows;
    } else {
      return null;
    }
  }

  async querySttwByUserId(userId) {
    const { rows } = await this.query(`SELECT sttw FROM users WHERE id=$1`, [userId]);
    if (rows.length > 0) {
      return rows[0].sttw;
    } else {
      return null;
    }
  }

  async querySecretByApiKey(apiKey) {
    const { rows } = await this.query(`SELECT secret, "userId" FROM apikeys WHERE key=$1`, [apiKey]);
    if (rows.length > 0) {
      return {
        secret: rows[0].secret,
        userId: rows[0].userId
      };
    } else {
      return null;
    }
  }

  async queryValueHistoryByUserId(userId) {
    const { rows } = await this.query(
      `SELECT date, SUM("presentValue") AS value FROM "dailyClearance"
      WHERE "userId"=$1
      GROUP BY date
      ORDER BY date`,
      [userId]
    );
    if (rows.length > 0) {
      return rows;
    } else {
      return null;
    }
  }

  async queryUnrealizedProfitByUserId(userId, pvs) {
    const pvList = pvs.split(',');
    const { rows } = await this.query(
      `SELECT date, "pvId", SUM(profit) AS value FROM "eventBySimulate"
      WHERE "userId"=$1 AND "pvId"=ANY($2::text[])
      GROUP BY date, "pvId"
      ORDER BY date, "pvId"`,
      [userId, pvList]
    );
    if (rows.length > 0) {
      return rows;
    } else {
      return null;
    }
  }

  async queryRealizedProfitByUserId(userId, pvs) {
    const pvList = pvs.split(',');
    const { rows } = await this.query(
      `SELECT date, "pvId", SUM(A.profit) AS value FROM "eventByBill" A
       LEFT JOIN bills B ON B.id=A."billId"
       WHERE "userId"=$1 AND "pvId"=ANY($2::text[])
       GROUP BY date, "pvId"
       ORDER BY date, "pvId"`,
      [userId, pvList]
    );
    if (rows.length > 0) {
      return rows;
    } else {
      return null;
    }
  }

  async queryEventSummary(userId, begin, end) {
    const { rows } = await this.query(
      `SELECT * FROM (
          SELECT 'pv' as event, date FROM "eventByPV" WHERE date >=$1 AND date <=$2
          UNION ALL
          SELECT 'simulate' as event, date FROM "eventBySimulate" WHERE date >=$1 AND date <=$2 AND "userId"=$3
          UNION ALL
          SELECT 'bill' as event, date FROM "eventByBill" WHERE date >=$1 AND date <=$2 AND "userId"=$3
          UNION ALL
          SELECT 'repay' as event, date FROM "eventByRepay" WHERE date >=$1 AND date <=$2 AND "userId"=$3
        ) T GROUP BY event, date`,
      [begin, end, userId]
    );
    if (rows.length > 0) {
      return rows;
    } else {
      return null;
    }
  }

  async queryEventPv(begin, end) {
    const { rows } = await this.query(
      `SELECT A."pvId", B.name AS "pvName", A.date, A.name, A.content FROM "eventByPV" A
       LEFT JOIN pvs B ON B.id=A."pvId"
       WHERE A.date >= $1 AND A.date <= $2`,
      [begin, end]
    );
    if (rows.length > 0) {
      return rows;
    } else {
      return null;
    }
  }

  async queryEventSimulate(userId, begin, end) {
    const { rows } = await this.query(
      `SELECT "pvId", date, profit FROM "eventBySimulate" WHERE "userId"=$1 AND date >= $2 AND date <= $3`,
      [userId, begin, end]
    );
    if (rows.length > 0) {
      return rows;
    } else {
      return null;
    }
  }

  async queryEventBill(userId, begin, end) {
    const { rows } = await this.query(
      `SELECT B."pvId", A.date, A.profit as profit FROM "eventByBill" A
      LEFT JOIN bills B ON B.id = A."billId"
       WHERE A."userId"=$1 AND A.date >= $2 AND A.date <= $3`,
      [userId, begin, end]
    );
    if (rows.length > 0) {
      return rows;
    } else {
      return null;
    }
  }

  async queryEventRepay(userId, begin, end) {
    const { rows } = await this.query(
      `SELECT "pvId", date, repay AS profit FROM "eventByRepay" WHERE "userId"=$1 AND date >= $2 AND date <= $3`,
      [userId, begin, end]
    );
    if (rows.length > 0) {
      return rows;
    } else {
      return null;
    }
  }

  async queryUserPv(userId) {
    const { rows } = await this.query(
      `SELECT id, name, status, COALESCE(B.price, A."initialPrice") as price, C.amount, D."totalAmount" FROM pvs A
        LEFT JOIN "marketInfo" B ON B."pvId" = A.id
        LEFT JOIN "userAssets" C ON C."pvId" = A.id
        LEFT JOIN (
            SELECT "pvId", SUM(amount) as "totalAmount" FROM "userAssets"
            GROUP BY "pvId") D ON D."pvId" = A.id
        WHERE C."userId" = $1
        ORDER BY id`,
      [userId]
    );
    if (rows.length > 0) {
      return rows;
    } else {
      return null;
    }
  }

  async queryUserPvAchievementRate(userId) {
    const { rows } = await this.query(
      `SELECT A.id, A."projectCode", A.capacity, A.decay, A."annualAmount", A."validityDateBegin" FROM pvs A
        LEFT JOIN "userAssets" B ON B."pvId" = A.id
        WHERE B."userId" = $1`,
      [userId]
    );

    return await achievement.getRate(rows);
  }

  async queryPvData(userId) {
    const { rows } = await this.query(
      `SELECT "pvId", repay FROM "eventByRepay" WHERE "userId"=$1 AND date=$2 ORDER BY "pvId"`,
      [userId, date]
    );
    if (rows.length > 0) {
      return rows;
    } else {
      return null;
    }
  }

  async queryPvData(pvId) {
    const query = {
      text: `SELECT 
          A.id, 
          A.name, 
          A."projectCode", 
          A.location,
          A.capacity,
          A."sellPrice",
          A.status, 
          A.stage, 
          COALESCE(B.price, A."initialPrice") as price
        FROM pvs A 
        LEFT JOIN "marketInfo" B ON B."pvId" = A.id
        WHERE id=$1`,
      values: [pvId]
    };
    const { rows } = await this.query(query);
    if (rows.length > 0) {
      return rows;
    } else {
      return null;
    }
  }

  async queryPvValues(pvId) {
    const query = {
      text: `SELECT "projectCode" FROM pvs WHERE id=$1`,
      values: [pvId]
    };
    const { rows } = await this.query(query);

    if (rows.length === 0) {
      return null;
    }

    const response = await axios.get('https://sola-api.herokuapp.com/api/v1/values/' + rows[0].projectCode);
    let powerData = {};
    response.data.forEach((item, index) => {
      const date = item.dt.slice(0, 10);
      let v = powerData[date];
      if (v) {
        v += item.value;
        powerData[date] = v;
      } else {
        powerData[date] = item.value;
      }
    });

    let labels = [];
    let values = [];
    for (let k in powerData) {
      if (powerData.hasOwnProperty(k)) {
        labels.push(k);
      }
    }

    labels.sort();
    labels.forEach((item, index) => {
      values[index] = powerData[item].toFixed(3);
    });

    return { labels, values };
  }

  async queryPvImg(pvId) {
    const query = {
      text: 'SELECT img FROM "pvImg" WHERE "pvId"=$1',
      values: [pvId]
    };
    const { rows } = await this.query(query);
    if (rows.length > 0) {
      return rows[0].img;
    } else {
      return null;
    }
  }

  async queryPvCoord(pvId) {
    const query = {
      text: 'SELECT "coordLat", "coordLng" FROM pvs WHERE id=$1',
      values: [pvId]
    };
    let data = {};
    const { rows } = await this.query(query);
    if (rows.length !== 0) {
      return { lat: rows[0].coordLat, lng: rows[0].coordLng };
    } else {
      return null;
    }
  }

  async queryPvStage(pvId) {
    const query = {
      text: 'SELECT stage, content, file FROM "pvStage" WHERE "pvId"=$1',
      values: [pvId]
    };
    const { rows } = await this.query(query);
    if (rows.length > 0) {
      return rows;
    } else {
      return null;
    }
  }

  async queryUserBill(pvId) {
    const query = {
      text:
        'SELECT "pvId", "rangeBegin", "rangeEnd", profit, "updateDt" FROM bills WHERE "pvId"=$1 AND cleared=TRUE',
      values: [pvId]
    };
    const { rows } = await this.query(query);
    if (rows.length > 0) {
      return rows;
    } else {
      return null;
    }
  }
}

module.exports = Db;
