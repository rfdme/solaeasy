class Rsps {
  constructor() {
    this.status = false;
    this.msg = "";
    this.errorCode = "";
    this.result = {};
  }
  getResult() {
    return {
      status: this.status,
      msg: this.msg,
      error_code: this.errorCode,
      result: this.result,
    };
  }
}
module.exports = Rsps;