const axios = require('axios');
const base64 = require('js-base64').Base64;
const keys = require('../config/keys');
const log4js = require('../modules/logger');
const logger = log4js.getLogger();

const sendResetPwdMail = async (email, name, pwd) => {
  const link = `${keys.assetURL}/Home/Login`;

  //發送驗證信
  const mailContent = `
  尊敬的 <strong>${name}</strong>：
  <br />
  您好！歡迎使用SOLA Bloc資產管理服務！
  <div>&nbsp;</div>
  <div>您的管理員已為您重設密碼。</div>
  <div>&nbsp;</div>
  <div><strong>您的新密碼:${pwd}</strong></div>
  <div>&nbsp;</div>
  <div>請透過下列連結登入：</div>
  <div><a href="${link}">${link}</a></div>
  <div>&nbsp;</div>
  <div>此致&nbsp;<br /><strong>SOLA團隊敬上</strong></div>
  <div>--</div>
  <div>SOLA索拉能源股份有限公司</div>
  <div>臺北市松山區民生東路4段75巷5之1號1樓</div>
  <img src="https://i.imgur.com/sMkUWhS.png" title="source: imgur.com" />
  `;
  // content must be encode with base64
  const encodedContent = base64.encode(mailContent);
  // sender must be 'no-reply@solabloc.com' in this version
  const sender = 'no-reply@solabloc.com';
  const mail = {
    from: sender,
    to: email,
    subject: 'SOLA Asset密碼重設通知信', // Subject line
    content: encodedContent
  };
  try {
    let response = await axios.post(keys.itsURL + '/v1/send_email', mail);
    logger.debug('response: ' + JSON.stringify(response.data));
    if (response.data.status == false) {
      logger.error('send email failed');
      throw new Error('send email failed');
    }
  } catch (e) {
    logger.error('send email failed');
    throw e;
  }
};

module.exports = {
  sendResetPwdMail
};
