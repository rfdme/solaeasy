const importTest = (name, path) => {
  describe(name, () => {
    require(path);
  });
};

describe('test', () => {
  //importTest('spv test', './spv.test.js');

  // init table schema, test user
  require('./init_db.js');

  importTest('finance test', './finance.test.js');
  importTest('account test', './account.test.js');
  importTest('user test', './user.test.js');
  importTest('apikeys test', './apikeys.test.js');
  importTest('assets test', './assets.test.js');
  importTest('authRoutes test', './authRoutes.test.js');
  importTest('dashboard test', './dashboard.test.js');
  importTest('detail test', './detail.test.js');
  importTest('donation test', './donation.test.js');
  importTest('event test', './event.test.js');

  after(() => {
    // console.log('=====TEST FINISHED=====');
  });
});
