
const chai = require('chai')
const should = chai.should()
const chaiHttp = require('chai-http')
const execSync = require('child_process').execSync
const jwt = require('jsonwebtoken');

chai.use(chaiHttp)
const api = require('../server.js')
const assert = chai.assert;

// setting
const key = 'eaa54363c00ec7b5137643ad734df327';
const secret = 'd95504495d82b5f15918560fc9a145fbbe630511637e456dc2dcf49b3c2d86d4';

describe('routes: /api/v1/spv/assets', () => {
  // Post a value
  it('測試JWT', async () => {
    try {
      const resource = '/api/v1/spv/assets?apiKey=' + key;
      const token = jwt.sign({
        resource,
        key
      }, secret);
      // console.log(`token temp: ${token}`);
      let res = await chai.request(api).get('/api/v1/spv/assets')
                          .query({apiKey: key})
                          .set('Authorization', `Bearer ${token}`);
      assert.notEqual(res.text, 'UnauthorizedError', 'JWT認證失敗');

    } catch(error) {
      console.error(error.stack)
      assert.fail();
    }
  })
})

describe('routes: /api/v1/spv/assets/valueHistory', () => {
  // Post a value
  it('測試JWT', async () => {
    try {
      const resource = '/api/v1/spv/assets/valueHistory?apiKey=' + key;
      const token = jwt.sign({
        resource,
        key
      }, secret);
      // console.log(`token temp: ${token}`);
      let res = await chai.request(api).get('/api/v1/spv/assets/valueHistory')
                          .query({apiKey: key})
                          .set('Authorization', `Bearer ${token}`);
      // console.log(JSON.stringify(res));
      assert.notEqual(res.text, 'UnauthorizedError', 'JWT認證失敗');

    } catch(error) {
      console.error(error.stack)
      assert.fail();
    }
  })
})

describe('routes: /api/v1/spv/assets/unrealizedProfit', () => {
  // Post a value
  it('測試JWT', async () => {
    try {
      const resource = '/api/v1/spv/assets/unrealizedProfit?apiKey=' + key;
      const token = jwt.sign({
        resource,
        key
      }, secret);
      // console.log(`token temp: ${token}`);
      let res = await chai.request(api).get('/api/v1/spv/assets/unrealizedProfit')
                          .query({apiKey: key})
                          .set('Authorization', `Bearer ${token}`);
      // console.log(JSON.stringify(res));
      assert.notEqual(res.text, 'UnauthorizedError', 'JWT認證失敗');

    } catch(error) {
      console.error(error.stack)
      assert.fail();
    }
  })
})

describe('routes: /api/v1/spv/assets/realizedProfit', () => {
  // Post a value
  it('測試JWT', async () => {
    try {
      const resource = '/api/v1/spv/assets/realizedProfit?apiKey=' + key;
      const token = jwt.sign({
        resource,
        key
      }, secret);
      // console.log(`token temp: ${token}`);
      let res = await chai.request(api).get('/api/v1/spv/assets/realizedProfit')
                          .query({apiKey: key})
                          .set('Authorization', `Bearer ${token}`);
      // console.log(JSON.stringify(res));
      assert.notEqual(res.text, 'UnauthorizedError', 'JWT認證失敗');

    } catch(error) {
      console.error(error.stack)
      assert.fail();
    }
  })
})

describe('routes: /api/v1/spv/event/pv', () => {
  // Post a value
  it('測試JWT', async () => {
    try {
      const resource = '/api/v1/spv/event/pv?begin=2018-07-01&end=2018-07-31&apiKey=' + key;
      const token = jwt.sign({
        resource,
        key
      }, secret);
      // console.log(`token temp: ${token}`);
      let res = await chai.request(api).get('/api/v1/spv/event/pv')
                          .query({
                            begin: '2018-07-01',
                            end: '2018-07-31',
                            apiKey: key
                          })
                          .set('Authorization', `Bearer ${token}`);
      // console.log(JSON.stringify(res.text));
      assert.notEqual(res.text, 'UnauthorizedError', 'JWT認證失敗');

    } catch(error) {
      console.error(error.stack)
      assert.fail();
    }
  })
})

describe('routes: /api/v1/spv/event/simulate', () => {
  // Post a value
  it('測試JWT', async () => {
    try {
      const resource = '/api/v1/spv/event/simulate?begin=2018-07-01&end=2018-07-31&apiKey=' + key;
      const token = jwt.sign({
        resource,
        key
      }, secret);
      // console.log(`token temp: ${token}`);
      let res = await chai.request(api).get('/api/v1/spv/event/simulate')
                          .query({
                            begin: '2018-07-01',
                            end: '2018-07-31',
                            apiKey: key
                          })
                          .set('Authorization', `Bearer ${token}`);
      // console.log(JSON.stringify(res.text));
      assert.notEqual(res.text, 'UnauthorizedError', 'JWT認證失敗');

    } catch(error) {
      console.error(error.stack)
      assert.fail();
    }
  })
})

describe('routes: /api/v1/spv/event/simulateDetail', () => {
  // Post a value
  it('測試JWT', async () => {
    try {
      const resource = '/api/v1/spv/event/simulateDetail?date=2018-07-01&apiKey=' + key;
      const token = jwt.sign({
        resource,
        key
      }, secret);
      // console.log(`token temp: ${token}`);
      let res = await chai.request(api).get('/api/v1/spv/event/simulateDetail')
                          .query({
                            date: '2018-07-01',
                            apiKey: key
                          })
                          .set('Authorization', `Bearer ${token}`);
      // console.log(JSON.stringify(res.text));
      assert.notEqual(res.text, 'UnauthorizedError', 'JWT認證失敗');

    } catch(error) {
      console.error(error.stack)
      assert.fail();
    }
  })
})

describe('routes: /api/v1/spv/event/bill', () => {
  // Post a value
  it('測試JWT', async () => {
    try {
      const resource = '/api/v1/spv/event/bill?begin=2018-07-01&end=2018-07-31&apiKey=' + key;
      const token = jwt.sign({
        resource,
        key
      }, secret);
      // console.log(`token temp: ${token}`);
      let res = await chai.request(api).get('/api/v1/spv/event/bill')
                          .query({
                            begin: '2018-07-01',
                            end: '2018-07-31',
                            apiKey: key
                          })
                          .set('Authorization', `Bearer ${token}`);
      // console.log(JSON.stringify(res.text));
      assert.notEqual(res.text, 'UnauthorizedError', 'JWT認證失敗');

    } catch(error) {
      console.error(error.stack)
      assert.fail();
    }
  })
})

describe('routes: /api/v1/spv/event/billDetail', () => {
  // Post a value
  it('測試JWT', async () => {
    try {
      const resource = '/api/v1/spv/event/billDetail?date=2018-07-01&apiKey=' + key;
      const token = jwt.sign({
        resource,
        key
      }, secret);
      // console.log(`token temp: ${token}`);
      let res = await chai.request(api).get('/api/v1/spv/event/billDetail')
                          .query({
                            date: '2018-07-01',
                            apiKey: key
                          })
                          .set('Authorization', `Bearer ${token}`);
      // console.log(JSON.stringify(res.text));
      assert.notEqual(res.text, 'UnauthorizedError', 'JWT認證失敗');

    } catch(error) {
      console.error(error.stack)
      assert.fail();
    }
  })
})

describe('routes: /api/v1/spv/event/repay', () => {
  // Post a value
  it('測試JWT', async () => {
    try {
      const resource = '/api/v1/spv/event/repay?begin=2018-07-01&end=2018-07-31&apiKey=' + key;
      const token = jwt.sign({
        resource,
        key
      }, secret);
      // console.log(`token temp: ${token}`);
      let res = await chai.request(api).get('/api/v1/spv/event/repay')
                          .query({
                            begin: '2018-07-01',
                            end: '2018-07-31',
                            apiKey: key
                          })
                          .set('Authorization', `Bearer ${token}`);
      // console.log(JSON.stringify(res.text));
      assert.notEqual(res.text, 'UnauthorizedError', 'JWT認證失敗');

    } catch(error) {
      console.error(error.stack)
      assert.fail();
    }
  })
})

describe('routes: /api/v1/spv/event/repayDetail', () => {
  // Post a value
  it('測試JWT', async () => {
    try {
      const resource = '/api/v1/spv/event/repayDetail?date=2018-07-01&apiKey=' + key;
      const token = jwt.sign({
        resource,
        key
      }, secret);
      // console.log(`token temp: ${token}`);
      let res = await chai.request(api).get('/api/v1/spv/event/repayDetail')
                          .query({
                            date: '2018-07-01',
                            apiKey: key
                          })
                          .set('Authorization', `Bearer ${token}`);
      // console.log(JSON.stringify(res.text));
      assert.notEqual(res.text, 'UnauthorizedError', 'JWT認證失敗');

    } catch(error) {
      console.error(error.stack)
      assert.fail();
    }
  })
})
