const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
chai.should();
const db = require('../db');
const api = require('../server.js');
const assert = require('assert');
let token;

// get token for testing
describe('get token', () => {
  it('should login successfully and get JWT', async () => {
    try {
      let res = await chai
        .request(api)
        .post('/api/v1/auth/local')
        .set('content-type', 'application/json')
        .send({
          email: 'test@mail.com',
          pwd: '1234'
        });
      res.status.should.equal(200);
      res.body.result.manager.email.should.equal('test@mail.com');
      token = res.body.result.token;
    } catch (error) {
      assert.fail(error.stack);
    }
  });
});

// api/v1/donation/donate
describe('routes: /api/v1/donation/donate', () => {
  describe('access without token', () => {
    it('should get authorization error', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/donation/userAsset/1');

        res.status.should.equal(401);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('authorized error');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get other user's assets", () => {
    it('should get forbidden', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/donation/userAsset/2')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(403);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's assets", () => {
    before(async () => {
      // insert some test data
      let insertUserAssets = `
        INSERT INTO "userAssets"("userId", "pvId", amount, "lockAmount")
        VALUES($1,$2,$3,$4);
      `;
      let userAssetsData_01 = [1, 'testPvId', 123456, 456];

      let insertPvs = `
        INSERT INTO pvs(id, name, "solaName", "projectCode", location, "coordLat", "coordLng", capacity, decay, "annualAmount",
        stage, status, "totalCapital", "ownedCapital", "initialPrice",
        "validityDateBegin", "validityDuration", 
        "sellPrice", "operationRateByCapital", "operationRateByIncome", "serviceRateByIncome", 
        "rentRateByIncome", "rentYearCost", "rentWithTax", "owner")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25);`;
      let pvsData_01 = [
        'testPvId',
        'testPvsName',
        'testPvsName',
        'testPrjcode',
        'testLocation',
        12.123456,
        123.123456,
        4567,
        1,
        2,
        2,
        3,
        100,
        10,
        10,
        '2018-11-03',
        20,
        99,
        98,
        97,
        0.02,
        0.1,
        7,
        true,
        'owner'
      ];
      try {
        result = await db.query(insertUserAssets, userAssetsData_01);
        assert.equal(result.rowCount, 1, 'insert userAssets data failed');

        result = await db.query(insertPvs, pvsData_01);
        assert.equal(result.rowCount, 1, 'insert pvs data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get user assets', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/donation/userAsset/1')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.result.userAsset[0].id.should.equal('testPvId');
        res.body.result.userAsset[0].name.should.equal('testPvsName');
        res.body.result.userAsset[0].remained.should.equal('123000');
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    // reset test data
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE "userAssets" RESTART IDENTITY;
          TRUNCATE pvs RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(false, true, 'reset test data failed while testing /api/v1/donation/donate');
      }
    });
  });
});

// api/v1/donation/ngoAsset
describe('routes: /api/v1/donation/ngoAsset', () => {
  describe("get ngo's assets", () => {
    before(async () => {
      // insert some test data
      let insertUserAssets = `
        INSERT INTO "userAssets"("userId", "pvId", amount, "lockAmount")
        VALUES($1,$2,$3,$4);
      `;
      let userAssetsData_01 = [1, 'testPvId', 123456, 456];

      let insertPvs = `
        INSERT INTO pvs(id, name, "solaName", "projectCode", location, "coordLat", "coordLng", capacity, decay, "annualAmount",
        stage, status, "totalCapital", "ownedCapital", "initialPrice",
        "validityDateBegin", "validityDuration", 
        "sellPrice", "operationRateByCapital", "operationRateByIncome", "serviceRateByIncome", 
        "rentRateByIncome", "rentYearCost", "rentWithTax", "owner")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25);`;
      let pvsData_01 = [
        'testPvId',
        'testPvsName',
        'testPvsName',
        'testPrjcode',
        'testLocation',
        12.123456,
        123.123456,
        4567,
        1,
        2,
        2,
        3,
        100,
        10,
        10,
        '2018-11-03',
        20,
        99,
        98,
        97,
        0.02,
        0.1,
        7,
        true,
        'owner'
      ];

      let insertEventBySimulate = `
        INSERT INTO "eventBySimulate"(id, "pvId", "userId", date, profit, "rentCost", "meterRentCost", "insuranceCost",
          "operateCost", "serviceCost", "loanCost", "loanFeeCost", "businessTax", "updateDt")
          VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14);
      `;
      let eventBySimulateData_01 = [
        1,
        'testPvId',
        1,
        '2018-11-02',
        100,
        2,
        1,
        5,
        8,
        9,
        10,
        9,
        8,
        '2018-12-12 12:12:12'
      ];
      try {
        result = await db.query(insertUserAssets, userAssetsData_01);
        assert.equal(result.rowCount, 1, 'insert userAssets data failed');

        result = await db.query(insertPvs, pvsData_01);
        assert.equal(result.rowCount, 1, 'insert pvs data failed');

        result = await db.query(insertEventBySimulate, eventBySimulateData_01);
        assert.equal(result.rowCount, 1, 'insert eventBySimulate data_01 failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get ngo assets', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/donation/ngoAsset');

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.result.ngoAsset[0].id.should.equal(1);
        res.body.result.ngoAsset[0].sellPrice.should.equal(99);
        res.body.result.ngoAsset[0].amount.should.equal('123456');
        res.body.result.ngoAsset[0].income.should.equal('100');
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    // reset test data
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE "userAssets" RESTART IDENTITY;
          TRUNCATE pvs RESTART IDENTITY;
          TRUNCATE "eventBySimulate" RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(false, true, 'reset test data failed while testing /api/v1/donation/ngoAsset');
      }
    });
  });
});

// api/v1/donation/donate
describe('routes: /api/v1/donation/donate', () => {
  describe('access without token', () => {
    it('should get authorization error', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/donation/donate')
          .set('content-type', 'application/json')
          .send({
            giver: '1',
            receiver: '2',
            pvId: 'testPvId',
            amount: 1000
          });
        res.status.should.equal(401);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('authorized error');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("donate other user's assets", () => {
    it('should get forbidden', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/donation/donate')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({
            giver: '2',
            receiver: '3',
            pvId: 'testPvId',
            amount: 1000
          });

        res.status.should.equal(403);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("donate user's assets - not enough assets", () => {
    it('should get failed', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/donation/donate')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({
            giver: '1',
            receiver: '2',
            pvId: 'testPvId',
            amount: 1000
          });

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('捐贈失敗: 贈與者的SNTW餘額不足');
        res.body.result.response.should.equal('fail');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("donate user's assets", () => {
    before(async () => {
      // insert some test data
      let insertUserAssets = `
        INSERT INTO "userAssets"("userId", "pvId", amount, "lockAmount")
        VALUES($1,$2,$3,$4);
      `;
      let userAssetsData_01 = [1, 'testPvId', 123456, 456];

      let insertPvs = `
        INSERT INTO pvs(id, name, "solaName", "projectCode", location, "coordLat", "coordLng", capacity, decay, "annualAmount",
        stage, status, "totalCapital", "ownedCapital", "initialPrice",
        "validityDateBegin", "validityDuration", 
        "sellPrice", "operationRateByCapital", "operationRateByIncome", "serviceRateByIncome", 
        "rentRateByIncome", "rentYearCost", "rentWithTax", "owner")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25);`;
      let pvsData_01 = [
        'testPvId',
        'testPvsName',
        'testPvsName',
        'testPrjcode',
        'testLocation',
        12.123456,
        123.123456,
        4567,
        1,
        2,
        2,
        3,
        100,
        10,
        10,
        '2018-11-03',
        20,
        99,
        98,
        97,
        0.02,
        0.1,
        7,
        true,
        'owner'
      ];
      try {
        result = await db.query(insertUserAssets, userAssetsData_01);
        assert.equal(result.rowCount, 1, 'insert userAssets data failed');

        result = await db.query(insertPvs, pvsData_01);
        assert.equal(result.rowCount, 1, 'insert pvs data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get user origin assets', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/donation/userAsset/1')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.result.userAsset[0].id.should.equal('testPvId');
        res.body.result.userAsset[0].name.should.equal('testPvsName');
        res.body.result.userAsset[0].remained.should.equal('123000');
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should donate user assets', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/donation/donate')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({
            giver: '1',
            receiver: '2',
            pvId: 'testPvId',
            amount: 1000
          });

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.result.response.should.equal('ok');
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get user rest assets', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/donation/userAsset/1')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.result.userAsset[0].id.should.equal('testPvId');
        res.body.result.userAsset[0].name.should.equal('testPvsName');
        res.body.result.userAsset[0].remained.should.equal('122000');
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    // reset test data
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE "userAssets" RESTART IDENTITY;
          TRUNCATE pvs RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(false, true, 'reset test data failed while testing /api/v1/donation/donate');
      }
    });
  });
});
