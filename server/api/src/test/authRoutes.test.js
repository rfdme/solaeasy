const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
chai.should();
const bcrypt = require('bcrypt');
const keys = require('../config/keys');
const db = require('../db');
const api = require('../server.js');
const assert = require('assert');
let token;

// api/v1/auth/local
describe('routes: /api/v1/auth/local', () => {
  describe('access with undefined email', () => {
    it('should get invalid parameter', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/auth/local')
          .set('content-type', 'application/json')
          .send({
            pwd: '1234'
          });

        res.status.should.equal(200);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('invalid parameters');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('access with undefined pwd', () => {
    it('should get invalid parameter', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/auth/local')
          .set('content-type', 'application/json')
          .send({
            email: 'test@mail.com'
          });

        res.status.should.equal(200);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('invalid parameters');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('access with incorrect email', () => {
    it('should get incorrect response', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/auth/local')
          .set('content-type', 'application/json')
          .send({
            email: 'tester@mail.com',
            pwd: '1234'
          });

        res.status.should.equal(200);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('帳號或密碼不正確');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('access with incorrect pwd', () => {
    it('should get incorrect response', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/auth/local')
          .set('content-type', 'application/json')
          .send({
            email: 'test@mail.com',
            pwd: '4321'
          });

        res.status.should.equal(200);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('帳號或密碼不正確');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('access with unregister user', () => {
    it('should get unregister response', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/auth/local')
          .set('content-type', 'application/json')
          .send({
            email: 'tester@mail.com',
            pwd: '1234'
          });

        res.status.should.equal(200);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('帳號或密碼不正確');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('get token', () => {
    it('should login successfully and get JWT', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/auth/local')
          .set('content-type', 'application/json')
          .send({
            email: 'test@mail.com',
            pwd: '1234'
          });
        res.status.should.equal(200);
        res.body.result.manager.email.should.equal('test@mail.com');
        token = res.body.result.token;
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
});
