const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
chai.should();
const bcrypt = require('bcrypt');
const keys = require('../config/keys');
const db = require('../db');
const api = require('../server.js');
const assert = require('assert');
let token;

// get token for testing
describe('get token', () => {
  it('should login successfully and get JWT', async () => {
    try {
      let res = await chai
        .request(api)
        .post('/api/v1/auth/local')
        .set('content-type', 'application/json')
        .send({
          email: 'test@mail.com',
          pwd: '1234'
        });
      res.status.should.equal(200);
      res.body.result.manager.email.should.equal('test@mail.com');
      token = res.body.result.token;
    } catch (error) {
      assert.fail(error.stack);
    }
  });
});

// api/v1/account/:id
describe('routes: /api/v1/account/:id', () => {
  describe('access without token', () => {
    it('should get authorization error', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/account/1');

        res.status.should.equal(401);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('authorized error');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get other user's data", () => {
    it('should get forbidden', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/account/2')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(403);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's data", () => {
    before(async () => {
      // insert some test data
      let insertAccount = `INSERT INTO accounts("userId", name, telephone, address, "cashoutAccount") VALUES($1,$2,$3,$4,$5);`;
      let accountData_01 = [1, 'testName', '0212345678', 'testAddress', 'testAccount'];

      try {
        result = await db.query(insertAccount, accountData_01);
        assert.equal(result.rowCount, 1, 'insert account data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get user data', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/account/1')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.account.name.should.equal('testName');
        res.body.result.account.telephone.should.equal('0212345678');
        res.body.result.account.address.should.equal('testAddress');
        res.body.result.account.cashoutAccount.should.equal('testAccount');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    // delete insert test data
    after(async () => {
      try {
        //reset deposit table
        const resetAccount = `TRUNCATE accounts RESTART IDENTITY;`;
        result = await db.query(resetAccount);
      } catch (error) {
        assert.fail(false, true, 'reset test data failed while testing /api/v1/account/:id');
      }
    });
  });
});

// api/v1/account/totalCashin/:id
describe('routes: /api/v1/account/totalCashin/:id', () => {
  describe('access without token', () => {
    it('should get authorization error', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/account/totalCashin/1');

        res.status.should.equal(401);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('authorized error');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get other user's total cashin", () => {
    it('should get forbidden', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/account/totalCashin/2')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(403);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's total cashin", () => {
    it('should get user total cashin', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/account/totalCashin/1')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.totalCashin.should.equal('0');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
});

// api/v1/account/totalCashout/:id
describe('routes: /api/v1/account/totalCashout/:id', () => {
  describe('access without token', () => {
    it('should get authorization error', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/account/totalCashout/1');

        res.status.should.equal(401);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('authorized error');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get other user's total cashout", () => {
    it('should get forbidden', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/account/totalCashout/2')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(403);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's total cashout", () => {
    it('should get user total cashout', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/account/totalCashout/1')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.totalCashout.should.equal('0');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
});

// api/v1/account/modifyPwd
describe('routes: /api/v1/account/modifyPwd', () => {
  describe('access without token', () => {
    it('should get authorization error', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/account/modifyPwd')
          .set('content-type', 'application/json')
          .send({ managerId: '2', oldPwd: '1234', newPwd: '1234' });

        res.status.should.equal(401);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('authorized error');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('access with undefined req.body', () => {
    it('should get invalid parameters error', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/account/modifyPwd')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({ managerId: '1', oldPwd: '1234' });

        res.status.should.equal(200);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('invalid parameters');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("modify other user's pwd", () => {
    it('should get forbidden', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/account/modifyPwd')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({ managerId: '2', oldPwd: '1234', newPwd: '1234' });

        res.status.should.equal(403);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("modify user's pwd with wrong pwd", () => {
    it('should get error', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/account/modifyPwd')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({ managerId: '1', oldPwd: '2345', newPwd: '1234' });

        res.status.should.equal(200);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('舊密碼驗證失敗');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });

  describe("modify user's pwd", () => {
    it('should modify user pwd', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/account/modifyPwd')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({ managerId: '1', oldPwd: '1234', newPwd: '2345' });

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');

        res = await chai
          .request(api)
          .post('/api/v1/auth/local')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({ email: 'test@mail.com', pwd: '2345' });

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.manager.id.should.equal(1);
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    // recover test data
    after(async () => {
      try {
        //reset account pwd
        let updatePwd = `UPDATE managers SET pwd=$1 WHERE id=$2`;
        const saltRounds =
          typeof keys.saltRounds === 'number' ? keys.saltRounds : parseInt(keys.saltRounds, 10);
        let digest = await bcrypt.hash('1234', saltRounds);
        let pwdData_01 = [digest, 1];

        await db.query(updatePwd, pwdData_01);
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
});

// api/v1/account/cashout
describe('routes: /api/v1/account/cashout', () => {
  describe('access without token', () => {
    it('should get authorization error', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/account/cashout')
          .set('content-type', 'application/json')
          .send({ managerId: '1', userId: '1', amount: '1', pwd: '1234' });

        res.status.should.equal(401);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('authorized error');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('access with undefined req.body', () => {
    it('should get invalid parameters error', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/account/cashout')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({ managerId: '1', userId: '1', amount: '1' });

        res.status.should.equal(200);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('invalid parameters');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('cashout with invalid amount: 0', () => {
    it('should get invalid cashout amount', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/account/cashout')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({ managerId: '1', userId: '1', amount: '0', pwd: '1234' });

        res.status.should.equal(200);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('invalid cashout amount');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('cashout with invalid amount: < 0', () => {
    it('should get invalid cashout amount', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/account/cashout')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({ managerId: '1', userId: '1', amount: '-99999', pwd: '1234' });

        res.status.should.equal(200);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('invalid cashout amount');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('cashout from other user', () => {
    it('should get forbidden', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/account/cashout')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({ managerId: '2', userId: '1', amount: '1', pwd: '1234' });

        res.status.should.equal(403);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('cashout with wrong pwd', () => {
    it('should get error', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/account/cashout')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({ managerId: '1', userId: '1', amount: '1', pwd: '2345' });

        res.status.should.equal(200);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('驗證密碼失敗');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('cashout with not enough balance', () => {
    it('should not have enough balance', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/account/cashout')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({ managerId: '1', userId: '1', amount: '9999999', pwd: '1234' });

        res.status.should.equal(200);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('餘額不足');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('cashout with valid request', () => {
    before(async () => {
      // update test data
      try {
        let updateAsset = `UPDATE users SET sttw=$1, "lockSttw"=$2 WHERE id=$3;`;
        let assetData_01 = [1000, 999, '1'];

        await db.query(updateAsset, assetData_01);
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should cashout successfully', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/account/cashout')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({ managerId: '1', userId: '1', amount: '1', pwd: '1234' });

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.result.amount.should.equal(1);
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    // reset test data
    after(async () => {
      try {
        let updateAsset = `UPDATE users SET sttw=$1, "lockSttw"=$2 WHERE id=$3;`;
        let assetData_01 = [0, 0, '1'];

        await db.query(updateAsset, assetData_01);

        await db.query(`TRUNCATE "cashoutRecords" RESTART IDENTITY;`);
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
});

// api/v1/account/cashin
describe('routes: /api/v1/account/cashin', () => {
  describe('access with undefined req.body', () => {
    it('should get invalid parameters error', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/account/cashin')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({ userId: '1' });

        res.status.should.equal(200);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('invalid parameters');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('cashin with invalid amount: 0', () => {
    it('should get invalid cashin amount', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/account/cashin')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({ userId: '1', amount: '0' });

        res.status.should.equal(200);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('invalid cashin amount');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('cashin with invalid amount: < 0', () => {
    it('should get invalid cashin amount', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/account/cashin')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({ userId: '1', amount: '-99999' });

        res.status.should.equal(200);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('invalid cashin amount');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('cashin with valid request', () => {
    it('should cashin successfully', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/account/cashin')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({ userId: '1', amount: '17' });

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.result.amount.should.equal(17);
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    // reset test data
    after(async () => {
      try {
        let updateAsset = `UPDATE users SET sttw=$1 WHERE id=$2;`;
        let assetData_01 = [0, '1'];
        await db.query(updateAsset, assetData_01);
        await db.query(`TRUNCATE "cashinRecords" RESTART IDENTITY;`);
        await db.query(`TRUNCATE "bcTransation" RESTART IDENTITY;`);
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
});
