const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
chai.should();
const db = require('../db');
const api = require('../server.js');
const assert = require('assert');
let token;

// get token for testing
describe('get token', () => {
  it('should login successfully and get JWT', async () => {
    try {
      let res = await chai
        .request(api)
        .post('/api/v1/auth/local')
        .set('content-type', 'application/json')
        .send({
          email: 'test@mail.com',
          pwd: '1234'
        });
      res.status.should.equal(200);
      res.body.result.manager.email.should.equal('test@mail.com');
      token = res.body.result.token;
    } catch (error) {
      assert.fail(error.stack);
    }
  });
});

// api/v1/assets/:user
describe('routes: /api/v1/assets/:user', () => {
  describe('access without token', () => {
    it('should get authorization error', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/assets/1');

        res.status.should.equal(401);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('authorized error');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get other user's assets", () => {
    it('should get forbidden', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/assets/2')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(403);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's assets", () => {
    before(async () => {
      // insert some test data
      let insertUserAssets = `INSERT INTO "userAssets"("userId", "pvId", amount, "lockAmount") VALUES($1,$2,$3,$4);`;
      let userAssetsData_01 = [1, 'testPvId', 123456, 456];

      let insertPvs = `
        INSERT INTO pvs(id, name, "solaName", "projectCode", location, "coordLat", "coordLng", capacity, decay, "annualAmount",
        stage, status, "totalCapital", "ownedCapital", "initialPrice",
        "validityDateBegin", "validityDuration", 
        "sellPrice", "operationRateByCapital", "operationRateByIncome", "serviceRateByIncome", 
        "rentRateByIncome", "rentYearCost", "rentWithTax", "owner")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25);`;
      let pvsData_01 = [
        'testPvId',
        'testPvsName',
        'testPvsName',
        'testPrjcode',
        'testLocation',
        12.123456,
        123.123456,
        4567,
        1,
        2,
        2,
        3,
        100,
        10,
        10,
        '2018-11-03',
        20,
        99,
        98,
        97,
        0.02,
        0.1,
        7,
        true,
        'owner'
      ];

      let insertMarketInfo = `INSERT INTO "marketInfo"("pvId", "updateDt", open, high, low, price, volume) VALUES($1,$2,$3,$4,$5,$6,$7);`;
      let marketInfoData_01 = ['testPvId', '2018-11-02 11:11:11', 100, 200, 10, 95, 99999];

      let insertEventByRepay = `INSERT INTO "eventByRepay"("pvId", "userId", date, "oldAmount", "newAmount", "oldSttw",
                                  "newSttw", repay, "updateDt") VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9);`;
      let eventByRepayData_01 = ['testPvId', 1, '2018-11-02', 12, 13, 50, 100, 9090, '2018-12-01 12:12:12'];

      try {
        result = await db.query(insertUserAssets, userAssetsData_01);
        assert.equal(result.rowCount, 1, 'insert userAssets data failed');

        result = await db.query(insertPvs, pvsData_01);
        assert.equal(result.rowCount, 1, 'insert pvs data failed');

        result = await db.query(insertMarketInfo, marketInfoData_01);
        assert.equal(result.rowCount, 1, 'insert marketInfo data failed');

        result = await db.query(insertEventByRepay, eventByRepayData_01);
        assert.equal(result.rowCount, 1, 'insert eventByRepay data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get user assets', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/assets/1')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.result.assets.pvList[0].id.should.equal('testPvId');
        res.body.result.assets.pvList[0].holdAmount.should.equal('123456');
        res.body.result.assets.pvList[0].name.should.equal('testPvsName');
        res.body.result.assets.pvList[0].status.should.equal(3);
        res.body.result.assets.pvList[0].capacity.should.equal(4567);
        res.body.result.assets.pvList[0].totalAmount.should.equal('1');
        res.body.result.assets.pvList[0].price.should.equal(95);
        res.body.result.assets.pvList[0].validityDateBegin.should.equal('2018-11-03');
        res.body.result.assets.pvList[0].validityDuration.should.equal(20);
        res.body.result.assets.pvList[0].lastRepayDate.should.equal('2018-11-02');
        res.body.result.assets.sttw.should.equal('0');
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    // reset test data
    after(async () => {
      try {
        //reset user account
        const resetTestData = `TRUNCATE "userAssets" RESTART IDENTITY;
          TRUNCATE pvs RESTART IDENTITY;
          TRUNCATE "marketInfo" RESTART IDENTITY;
          TRUNCATE "eventByRepay" RESTART IDENTITY;`;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(false, true, 'reset test data failed while testing /api/v1/assets/:id');
      }
    });
  });
});

// api/v1/assets/valueHistory/:user
describe('routes: /api/v1/assets/valueHistory/:user', () => {
  describe('access without token', () => {
    it('should get authorization error', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/assets/valueHistory/1');

        res.status.should.equal(401);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('authorized error');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get other user's valueHistory assets", () => {
    it('should get forbidden', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/assets/valueHistory/2')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(403);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's valueHistory assets", () => {
    before(async () => {
      // insert some test data
      let insertDailyClearance_01 = `INSERT INTO "dailyClearance"(id, "userId", date, item, amount, "presentValue", "updateDt")
                              VALUES($1,$2,$3,$4,$5,$6,$7);`;
      let dailyClearanceData_01 = [1, 1, '2018-11-02', 'testItem01', 100, 95, '2018-12-12 12:12:12'];

      let insertDailyClearance_02 = `INSERT INTO "dailyClearance"(id, "userId", date, item, amount, "presentValue", "updateDt")
                              VALUES($1,$2,$3,$4,$5,$6,$7);`;
      let dailyClearanceData_02 = [2, 1, '2018-11-03', 'testItem02', 200, 195, '2019-02-02 21:21:21'];

      try {
        result = await db.query(insertDailyClearance_01, dailyClearanceData_01);
        assert.equal(result.rowCount, 1, 'insert dailyClearance data_01 failed');

        result = await db.query(insertDailyClearance_02, dailyClearanceData_02);
        assert.equal(result.rowCount, 1, 'insert dailyClearance data_02 failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get user valueHistory assets', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/assets/valueHistory/1')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.result.valueHistory[0].date.should.equal('2018-11-02');
        res.body.result.valueHistory[0].value.should.equal('95');
        res.body.result.valueHistory[1].date.should.equal('2018-11-03');
        res.body.result.valueHistory[1].value.should.equal('195');
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    // reset test data
    after(async () => {
      try {
        //reset user account
        const resetTestData = 'TRUNCATE "dailyClearance" RESTART IDENTITY;';
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(false, true, 'reset test data failed while testing /api/v1/assets/valueHistory/:id');
      }
    });
  });
});

// api/v1/assets/unrealizedProfit/:user
describe('routes: /api/v1/assets/unrealizedProfit/:user', () => {
  describe('access without token', () => {
    it('should get authorization error', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/assets/unrealizedProfit/1');

        res.status.should.equal(401);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('authorized error');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get other user's unrealizedProfit assets", () => {
    it('should get forbidden', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/assets/unrealizedProfit/2')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(403);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's unrealizedProfit assets", () => {
    before(async () => {
      // insert some test data
      let insertEventBySimulate = `
        INSERT INTO "eventBySimulate"(id, "pvId", "userId", date, profit, "rentCost", "meterRentCost", "insuranceCost",
          "operateCost", "serviceCost", "loanCost", "loanFeeCost", "businessTax", "updateDt")
          VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14);
      `;
      let eventBySimulateData_01 = [
        1,
        'testPvId',
        1,
        '2018-11-02',
        100,
        2,
        1,
        5,
        8,
        9,
        10,
        9,
        8,
        '2018-12-12 12:12:12'
      ];

      let eventBySimulateData_02 = [
        2,
        'testPvId02',
        1,
        '2018-11-03',
        101,
        3,
        2,
        6,
        9,
        10,
        11,
        10,
        9,
        '2018-12-21 21:21:21'
      ];

      try {
        result = await db.query(insertEventBySimulate, eventBySimulateData_01);
        assert.equal(result.rowCount, 1, 'insert eventBySimulate data_01 failed');

        result = await db.query(insertEventBySimulate, eventBySimulateData_02);
        assert.equal(result.rowCount, 1, 'insert eventBySimulate data_02 failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get user unrealizedProfit assets', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/assets/unrealizedProfit/1')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.result.unrealizedProfit[0].date.should.equal('2018-11-02');
        res.body.result.unrealizedProfit[0].value.should.equal('100');
        res.body.result.unrealizedProfit[1].date.should.equal('2018-11-03');
        res.body.result.unrealizedProfit[1].value.should.equal('101');
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    // reset test data
    after(async () => {
      try {
        //reset user account
        const resetTestData = 'TRUNCATE "eventBySimulate" RESTART IDENTITY;';
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(false, true, 'reset test data failed while testing /api/v1/assets/unrealizedProfit/:id');
      }
    });
  });
});

// api/v1/assets/realizedProfit/:user
describe('routes: /api/v1/assets/realizedProfit/:user', () => {
  describe('access without token', () => {
    it('should get authorization error', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/assets/realizedProfit/1');

        res.status.should.equal(401);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('authorized error');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get other user's realizedProfit assets", () => {
    it('should get forbidden', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/assets/realizedProfit/2')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(403);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's realizedProfit assets", () => {
    before(async () => {
      // insert some test data
      let insertEventByBill = `
        INSERT INTO "eventByBill"(id, "billId", "userId", date, profit, "rentCost", "meterRentCost",
          "insuranceCost", "operateCost", "serviceCost", "loanCost", "loanFeeCost",
          "businessTax", "oldSttw", "newSttw", "updateDt")
          VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16);
      `;
      let eventByBillData_01 = [
        1,
        1,
        1,
        '2018-11-02',
        101,
        1,
        2,
        3,
        5,
        6,
        5,
        4,
        3,
        2,
        1,
        '2018-12-31 12:12:12'
      ];
      let eventByBillData_02 = [
        2,
        2,
        1,
        '2018-11-03',
        202,
        2,
        4,
        6,
        10,
        12,
        10,
        8,
        6,
        4,
        2,
        '2019-01-31 21:21:21'
      ];

      try {
        result = await db.query(insertEventByBill, eventByBillData_01);
        assert.equal(result.rowCount, 1, 'insert eventByBill data_01 failed');

        result = await db.query(insertEventByBill, eventByBillData_02);
        assert.equal(result.rowCount, 1, 'insert eventByBill data_02 failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get user realizedProfit assets', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/assets/realizedProfit/1')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(200);
        res.body.status.should.equal(true);

        res.body.result.realizedProfit[0].date.should.equal('2018-11-02');
        res.body.result.realizedProfit[0].value.should.equal('101');
        res.body.result.realizedProfit[1].date.should.equal('2018-11-03');
        res.body.result.realizedProfit[1].value.should.equal('202');
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    // reset test data
    after(async () => {
      try {
        //reset user account
        const resetTestData = 'TRUNCATE "eventByBill" RESTART IDENTITY;';
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(false, true, 'reset test data failed while testing /api/v1/assets/realizedProfit/:id');
      }
    });
  });
});

// api/v1/assets/etherscanLink
describe('routes: /api/v1/assets/etherscanLink', () => {
  describe('access without token', () => {
    it('should get authorization error', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/assets/etherscanLink')
          .set('content-type', 'application/json')
          .send({
            user: 1,
            token: 'solacoin'
          });

        res.status.should.equal(401);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('authorized error');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get other user's etherscanLink assets", () => {
    it('should get forbidden', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/assets/etherscanLink')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({
            user: 2,
            token: 'solacoin'
          });

        res.status.should.equal(403);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's etherscanLink assets", () => {
    it('should get user etherscanLink assets', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/assets/etherscanLink')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({
            user: 1,
            token: 'solacoin'
          });
        // TODO: this test is not really complete yet,
        // it calls the private api
        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
});
