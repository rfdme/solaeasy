const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
chai.should();
const db = require('../db');
const api = require('../server.js');
const assert = require('assert');

// api/v1/dashboard/allPv
describe('routes: /api/v1/dashboard/allPv', () => {
  describe('get empty all pv', () => {
    it('should get empty', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/dashboard/allPv');

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.allPv.should.be.empty;
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('get all pv', () => {
    before(async () => {
      // insert test data
      let insertPvs = `
        INSERT INTO pvs(id, name, "solaName", "projectCode", location, "coordLat", "coordLng", capacity, decay, "annualAmount",
        stage, status, "totalCapital", "ownedCapital", "initialPrice", 
        "validityDateBegin", "validityDuration", 
        "sellPrice", "operationRateByCapital", "operationRateByIncome", "serviceRateByIncome", 
        "rentRateByIncome", "rentYearCost", "rentWithTax", "owner")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25);`;
      let pvsData_01 = [
        'testPvId',
        'testPvsName',
        'testPvsName',
        'testPrjcode',
        'testLocation',
        12.123456,
        123.123456,
        4567,
        1,
        2,
        2,
        3,
        100,
        10,
        10,
        '2018-11-03',
        20,
        99,
        98,
        97,
        0.02,
        0.1,
        7,
        true,
        'owner'
      ];

      let insertMarketInfo = `INSERT INTO "marketInfo"("pvId", "updateDt", open, high, low, price, volume) VALUES($1,$2,$3,$4,$5,$6,$7);`;
      let marketInfoData_01 = ['testPvId', '2018-11-02 11:11:11', 100, 200, 10, 95, 99999];

      try {
        result = await db.query(insertPvs, pvsData_01);
        assert.equal(result.rowCount, 1, 'insert pvs data failed');

        result = await db.query(insertMarketInfo, marketInfoData_01);
        assert.equal(result.rowCount, 1, 'insert marketInfo data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get all pv', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/dashboard/allPv');

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.allPv[0].id.should.equal('testPvId');
        res.body.result.allPv[0].name.should.equal('testPvsName');
        res.body.result.allPv[0].totalAmount.should.equal('1');
        res.body.result.allPv[0].open.should.equal(100);
        res.body.result.allPv[0].price.should.equal(95);
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE "marketInfo" RESTART IDENTITY;
          TRUNCATE pvs RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(`reset test data failed while testing /api/v1/dashboard/allPv, ${error.stack}`);
      }
    });
  });
});

// api/v1/dashboard/userPv/:id
describe('routes: /api/v1/dashboard/userPv/:id', () => {
  describe('get empty userPv', () => {
    it('should get empty', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/dashboard/userPv/1');

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.userPv.should.be.empty;
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('get userPv', () => {
    before(async () => {
      // insert test data
      let insertPvs = `
        INSERT INTO pvs(id, name, "solaName", "projectCode", location, "coordLat", "coordLng", capacity, decay, "annualAmount",
        stage, status, "totalCapital", "ownedCapital", "initialPrice", 
        "validityDateBegin", "validityDuration", 
        "sellPrice", "operationRateByCapital", "operationRateByIncome", "serviceRateByIncome", 
        "rentRateByIncome", "rentYearCost", "rentWithTax", "owner")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25);`;
      let pvsData_01 = [
        'testPvId',
        'testPvsName',
        'testPvsName',
        'testPrjcode',
        'testLocation',
        12.123456,
        123.123456,
        4567,
        1,
        2,
        2,
        3,
        100,
        10,
        10,
        '2018-11-03',
        20,
        99,
        98,
        97,
        0.02,
        0.1,
        7,
        true,
        'owner'
      ];

      let insertMarketInfo = `INSERT INTO "marketInfo"("pvId", "updateDt", open, high, low, price, volume) VALUES($1,$2,$3,$4,$5,$6,$7);`;
      let marketInfoData_01 = ['testPvId', '2018-11-02 11:11:11', 100, 200, 10, 95, 99999];

      let insertUserAssets = `INSERT INTO "userAssets"("userId", "pvId", amount, "lockAmount") VALUES($1,$2,$3,$4);`;
      let userAssetsData_01 = [1, 'testPvId', 123456, 456];

      try {
        result = await db.query(insertPvs, pvsData_01);
        assert.equal(result.rowCount, 1, 'insert pvs data failed');

        result = await db.query(insertMarketInfo, marketInfoData_01);
        assert.equal(result.rowCount, 1, 'insert marketInfo data failed');

        result = await db.query(insertUserAssets, userAssetsData_01);
        assert.equal(result.rowCount, 1, 'insert userAssets data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get userPv', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/dashboard/userPv/1');

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.userPv[0].id.should.equal('testPvId');
        res.body.result.userPv[0].name.should.equal('testPvsName');
        res.body.result.userPv[0].open.should.equal(100);
        res.body.result.userPv[0].price.should.equal(95);
        res.body.result.userPv[0].amount.should.equal('123456');
        res.body.result.userPv[0].totalAmount.should.equal('123456');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE "marketInfo" RESTART IDENTITY;
          TRUNCATE pvs RESTART IDENTITY;
          TRUNCATE "userAssets" RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(`reset test data failed while testing /api/v1/dashboard/userPv/:id, ${error.stack}`);
      }
    });
  });
});

// api/v1/dashboard/timeline
describe('routes: /api/v1/dashboard/timeline', () => {
  describe('get empty timeline', () => {
    it('should get empty', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/dashboard/timeline')
          .set('content-type', 'application/json')
          .send({
            dateBegin: '2018-11-01',
            dateEnd: '2018-12-31',
            userId: 1
          });

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.timeline.should.be.empty;
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('get timeline', () => {
    before(async () => {
      // insert test data
      let insertEventByPv = `
        INSERT INTO "eventByPV" ("pvId", date, name, content, "updateDt")
        VALUES($1,$2,$3,$4,$5);
      `;
      let eventByPv_Data_01 = ['SNTW1', '2017-12-01', '簽訂工程合約', 'TODO content', '2018-11-15 03:04:05'];

      let insertCashinRecords = `
        INSERT INTO "cashinRecords"("userId", date, amount, "oldSttw",
        "newSttw", "updateDt")
        VALUES($1,$2,$3,$4,$5,$6);
      `;
      let cashinRecords_Data_01 = [1, '2018-11-15', 100, 50, 80, '2018-11-30 12:12:12'];

      let insertCashoutRecords = `
        INSERT INTO "cashoutRecords"("userId", date, amount, status, "oldSttw",
         "newSttw", "updateDt")
        VALUES($1,$2,$3,$4,$5,$6,$7);
      `;
      let cashoutRecords_Data_01 = [1, '2018-11-16', 50, 2, 25, 40, '2018-12-01 13:13:13'];

      let insertEventByBill = `
        INSERT INTO "eventByBill"(id, "billId", "userId", date, profit,
          "rentCost", "meterRentCost", "insuranceCost",
          "operateCost", "serviceCost", "loanCost", "loanFeeCost",
          "businessTax", "oldSttw", "newSttw", "updateDt")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16);
      `;
      let eventByBill_Data_01 = [
        1,
        1,
        1,
        '2018-11-02',
        101,
        1,
        2,
        3,
        5,
        6,
        5,
        4,
        3,
        2,
        1,
        '2018-12-31 12:12:12'
      ];

      let insertEventByRepay = `
        INSERT INTO "eventByRepay"("pvId", "userId", date, "oldAmount",
          "newAmount", "oldSttw", "newSttw", repay, "updateDt")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9);
      `;
      let eventByRepay_Data_01 = ['testPvId', 1, '2018-11-02', 12, 13, 50, 100, 9090, '2018-12-01 12:12:12'];

      let insertTrades = `
        INSERT INTO trades("orderBuyId", "orderSellId", datetime, price,
          amount, fee, "oldAmountBuyer", "newAmountBuyer", "oldSttwBuyer",
          "newSttwBuyer", "oldAmountSeller", "newAmountSeller", "oldSttwSeller",
          "newSttwSeller")
        VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14);
      `;

      let trades_Data_01 = [1, 2, '2018-12-02 14:21:21', 50, 5, 0.1, 20, 25, 100, 50, 30, 25, 50, 100];

      let insertOrderBuys = `
        INSERT INTO "orderBuys"("pvId", datetime, "userId", price, amount)
        VALUES ($1,$2,$3,$4,$5);
      `;

      let orderBuys_Data_01 = ['SNTW1', '2018-11-03 03:03:03', 1, 50, 5];

      let insertOrderSells = `
        INSERT INTO "orderSells"("pvId", datetime, "userId", price, amount)
        VALUES ($1,$2,$3,$4,$5);
      `;

      let orderSells_Data_01 = ['SNTW1', '2018-11-03 03:03:03', 2, 50, 5];
      try {
        result = await db.query(insertEventByPv, eventByPv_Data_01);
        assert.equal(result.rowCount, 1, 'insert eventByPv data failed');

        result = await db.query(insertCashinRecords, cashinRecords_Data_01);
        assert.equal(result.rowCount, 1, 'insert cashinRecords data failed');

        result = await db.query(insertCashoutRecords, cashoutRecords_Data_01);
        assert.equal(result.rowCount, 1, 'insert cashoutRecords data failed');

        result = await db.query(insertEventByBill, eventByBill_Data_01);
        assert.equal(result.rowCount, 1, 'insert eventByBill data failed');

        result = await db.query(insertEventByRepay, eventByRepay_Data_01);
        assert.equal(result.rowCount, 1, 'insert eventByRepay data failed');

        result = await db.query(insertTrades, trades_Data_01);
        assert.equal(result.rowCount, 1, 'insert trades data failed');

        result = await db.query(insertOrderBuys, orderBuys_Data_01);
        assert.equal(result.rowCount, 1, 'insert orderBuys data failed');

        result = await db.query(insertOrderSells, orderSells_Data_01);
        assert.equal(result.rowCount, 1, 'insert orderSells data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get timeline', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/dashboard/timeline')
          .send({
            dateBegin: '2018-11-01',
            dateEnd: '2018-12-31',
            userId: 1
          });

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.timeline[0].event.should.equal('bill');
        res.body.result.timeline[0].date.should.equal('2018-11-02');
        res.body.result.timeline[1].event.should.equal('buy');
        res.body.result.timeline[1].date.should.equal('2018-12-02');
        res.body.result.timeline[2].event.should.equal('cashin');
        res.body.result.timeline[2].date.should.equal('2018-11-15');
        res.body.result.timeline[3].event.should.equal('cashout');
        res.body.result.timeline[3].date.should.equal('2018-11-16');
        res.body.result.timeline[4].event.should.equal('repay');
        res.body.result.timeline[4].date.should.equal('2018-11-02');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE "eventByPV" RESTART IDENTITY;
          TRUNCATE "cashinRecords" RESTART IDENTITY;
          TRUNCATE "cashoutRecords" RESTART IDENTITY;
          TRUNCATE "eventByBill" RESTART IDENTITY;
          TRUNCATE "eventByRepay" RESTART IDENTITY;
          TRUNCATE trades RESTART IDENTITY;
          TRUNCATE "orderBuys" RESTART IDENTITY;
          TRUNCATE "orderSells" RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(`reset test data failed while testing /api/v1/dashboard/timeline, ${error.stack}`);
      }
    });
  });
});

// api/v1/dashboard/dateEvent
describe('routes: /api/v1/dashboard/dateEvent', () => {
  describe('get empty dateEvent', () => {
    it('should get empty', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/dashboard/dateEvent')
          .set('content-type', 'application/json')
          .send({
            date: '2018-11-02',
            userId: 1,
            checkPv: true,
            checkBuy: true,
            checkSell: true,
            checkBill: true,
            checkRepay: true,
            checkCashout: true,
            checkCashin: true
          });

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.dateEvent.should.be.empty;
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('get dateEvent', () => {
    before(async () => {
      // insert test data
      let insertEventByPv = `
        INSERT INTO "eventByPV" ("pvId", date, name, content, "updateDt")
        VALUES($1,$2,$3,$4,$5);
      `;
      let eventByPv_Data_01 = ['SNTW1', '2017-12-01', '簽訂工程合約', 'TODO content', '2018-11-15 03:04:05'];

      let insertCashinRecords = `
        INSERT INTO "cashinRecords"("userId", date, amount, "oldSttw",
        "newSttw", "updateDt")
        VALUES($1,$2,$3,$4,$5,$6);
      `;
      let cashinRecords_Data_01 = [1, '2017-12-01', 100, 50, 80, '2018-11-30 12:12:12'];

      let insertCashoutRecords = `
        INSERT INTO "cashoutRecords"("userId", date, amount, status, "oldSttw",
         "newSttw", "updateDt")
        VALUES($1,$2,$3,$4,$5,$6,$7);
      `;
      let cashoutRecords_Data_01 = [1, '2017-12-01', 50, 2, 25, 40, '2018-12-01 13:13:13'];

      let insertEventByBill = `
        INSERT INTO "eventByBill"(id, "billId", "userId", date, profit,
          "rentCost", "meterRentCost", "insuranceCost",
          "operateCost", "serviceCost", "loanCost", "loanFeeCost",
          "businessTax", "oldSttw", "newSttw", "updateDt")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16);
      `;
      let eventByBill_Data_01 = [
        1,
        1,
        1,
        '2017-12-01',
        101,
        1,
        2,
        3,
        5,
        6,
        5,
        4,
        3,
        2,
        1,
        '2018-12-31 12:12:12'
      ];

      let insertEventByRepay = `
        INSERT INTO "eventByRepay"("pvId", "userId", date, "oldAmount",
          "newAmount", "oldSttw", "newSttw", repay, "updateDt")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9);
      `;
      let eventByRepay_Data_01 = ['testPvId', 1, '2017-12-01', 12, 13, 50, 100, 9090, '2018-12-01 12:12:12'];

      let insertTrades = `
        INSERT INTO trades("orderBuyId", "orderSellId", datetime, price,
          amount, fee, "oldAmountBuyer", "newAmountBuyer", "oldSttwBuyer",
          "newSttwBuyer", "oldAmountSeller", "newAmountSeller", "oldSttwSeller",
          "newSttwSeller")
        VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14);
      `;

      let trades_Data_01 = [1, 2, '2017-12-01 21:21:21', 50, 5, 0.1, 20, 25, 100, 50, 30, 25, 50, 100];

      let insertOrderBuys = `
        INSERT INTO "orderBuys"("pvId", datetime, "userId", price, amount)
        VALUES ($1,$2,$3,$4,$5);
      `;

      let orderBuys_Data_01 = ['SNTW1', '2018-11-03 03:03:03', 1, 50, 5];

      let insertOrderSells = `
        INSERT INTO "orderSells"("pvId", datetime, "userId", price, amount)
        VALUES ($1,$2,$3,$4,$5);
      `;

      let orderSells_Data_01 = ['SNTW1', '2018-11-03 03:03:03', 2, 50, 5];
      try {
        result = await db.query(insertEventByPv, eventByPv_Data_01);
        assert.equal(result.rowCount, 1, 'insert eventByPv data failed');

        result = await db.query(insertCashinRecords, cashinRecords_Data_01);
        assert.equal(result.rowCount, 1, 'insert cashinRecords data failed');

        result = await db.query(insertCashoutRecords, cashoutRecords_Data_01);
        assert.equal(result.rowCount, 1, 'insert cashoutRecords data failed');

        result = await db.query(insertEventByBill, eventByBill_Data_01);
        assert.equal(result.rowCount, 1, 'insert eventByBill data failed');

        result = await db.query(insertEventByRepay, eventByRepay_Data_01);
        assert.equal(result.rowCount, 1, 'insert eventByRepay data failed');

        result = await db.query(insertTrades, trades_Data_01);
        assert.equal(result.rowCount, 1, 'insert trades data failed');

        result = await db.query(insertOrderBuys, orderBuys_Data_01);
        assert.equal(result.rowCount, 1, 'insert orderBuys data failed');

        result = await db.query(insertOrderSells, orderSells_Data_01);
        assert.equal(result.rowCount, 1, 'insert orderSells data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get dateEvent', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/dashboard/dateEvent')
          .send({
            date: '2017-12-01',
            userId: 1,
            checkPv: true,
            checkBuy: true,
            checkSell: true,
            checkBill: true,
            checkRepay: true,
            checkCashout: true,
            checkCashin: true
          });

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.dateEvent[0].event.should.equal('bill');
        res.body.result.dateEvent[0].date.should.equal('2017-12-01');
        res.body.result.dateEvent[1].event.should.equal('buy');
        res.body.result.dateEvent[1].date.should.equal('2017-12-01');
        res.body.result.dateEvent[2].event.should.equal('cashin');
        res.body.result.dateEvent[2].date.should.equal('2017-12-01');
        res.body.result.dateEvent[3].event.should.equal('cashout');
        res.body.result.dateEvent[3].date.should.equal('2017-12-01');
        res.body.result.dateEvent[4].event.should.equal('pv');
        res.body.result.dateEvent[4].date.should.equal('2017-12-01');
        res.body.result.dateEvent[5].event.should.equal('repay');
        res.body.result.dateEvent[5].date.should.equal('2017-12-01');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE "eventByPV" RESTART IDENTITY;
          TRUNCATE "cashinRecords" RESTART IDENTITY;
          TRUNCATE "cashoutRecords" RESTART IDENTITY;
          TRUNCATE "eventByBill" RESTART IDENTITY;
          TRUNCATE "eventByRepay" RESTART IDENTITY;
          TRUNCATE trades RESTART IDENTITY;
          TRUNCATE "orderBuys" RESTART IDENTITY;
          TRUNCATE "orderSells" RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(`reset test data failed while testing /api/v1/dashboard/dateEvent, ${error.stack}`);
      }
    });
  });
});

// api/v1/dashboard/eventData/pv
describe('routes: /api/v1/dashboard/eventData/pv', () => {
  describe('get empty eventData pv', () => {
    it('should get empty', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/dashboard/eventData/pv')
          .set('content-type', 'application/json')
          .send({
            date: '2018-11-02'
          });

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.eventData.should.be.empty;
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('get eventData pv', () => {
    before(async () => {
      // insert test data
      let insertEventByPv = `
        INSERT INTO "eventByPV" ("pvId", date, name, content, "updateDt")
        VALUES($1,$2,$3,$4,$5);
      `;
      let eventByPv_Data_01 = ['SNTW1', '2017-12-01', '簽訂工程合約', 'TODO content', '2018-11-15 03:04:05'];

      let insertPvs = `
        INSERT INTO pvs(id, name, "solaName", "projectCode", location, "coordLat", "coordLng", capacity, decay, "annualAmount",
        stage, status, "totalCapital", "ownedCapital", "initialPrice", 
        "validityDateBegin", "validityDuration", 
        "sellPrice", "operationRateByCapital", "operationRateByIncome", "serviceRateByIncome", 
        "rentRateByIncome", "rentYearCost", "rentWithTax", "owner")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25);`;
      let pvsData_01 = [
        'SNTW1',
        'testPvsName',
        'testPvsName',
        'testPrjcode',
        'testLocation',
        12.123456,
        123.123456,
        4567,
        1,
        2,
        2,
        3,
        100,
        10,
        10,
        '2018-11-03',
        20,
        99,
        98,
        97,
        0.02,
        0.1,
        7,
        true,
        'owner'
      ];

      try {
        result = await db.query(insertEventByPv, eventByPv_Data_01);
        assert.equal(result.rowCount, 1, 'insert eventByPv data failed');

        result = await db.query(insertPvs, pvsData_01);
        assert.equal(result.rowCount, 1, 'insert pvs data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get eventData pv', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/dashboard/eventData/pv')
          .send({
            date: '2017-12-01'
          });
        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.eventData[0].pvId.should.equal('SNTW1');
        res.body.result.eventData[0].pvName.should.equal('testPvsName');
        res.body.result.eventData[0].name.should.equal('簽訂工程合約');
        res.body.result.eventData[0].content.should.equal('TODO content');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE "eventByPV" RESTART IDENTITY;
          TRUNCATE pvs RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(`reset test data failed while testing /api/v1/dashboard/eventData/pv, ${error.stack}`);
      }
    });
  });
});

// api/v1/dashboard/eventData/cashin
describe('routes: /api/v1/dashboard/eventData/cashin', () => {
  describe('get empty eventData cashin', () => {
    it('should get empty', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/dashboard/eventData/cashin')
          .set('content-type', 'application/json')
          .send({
            date: '2018-11-02',
            userId: 1
          });

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.eventData.should.be.empty;
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('get eventData cashin', () => {
    before(async () => {
      // insert test data
      let insertCashinRecords = `
        INSERT INTO "cashinRecords"("userId", date, amount, "oldSttw",
        "newSttw", "updateDt")
        VALUES($1,$2,$3,$4,$5,$6);
      `;
      let cashinRecords_Data_01 = [1, '2017-12-01', 100, 50, 80, '2018-11-30 12:12:12'];

      try {
        result = await db.query(insertCashinRecords, cashinRecords_Data_01);
        assert.equal(result.rowCount, 1, 'insert cashinRecords data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get eventData cashin', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/dashboard/eventData/cashin')
          .send({
            date: '2017-12-01',
            userId: 1
          });
        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.eventData[0].amount.should.equal(100);
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE "cashinRecords" RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(
          `reset test data failed while testing /api/v1/dashboard/eventData/cashin, ${error.stack}`
        );
      }
    });
  });
});

// api/v1/dashboard/eventData/cashout
describe('routes: /api/v1/dashboard/eventData/cashout', () => {
  describe('get empty eventData cashout', () => {
    it('should get empty', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/dashboard/eventData/cashout')
          .set('content-type', 'application/json')
          .send({
            date: '2018-11-02',
            userId: 1
          });

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.eventData.should.be.empty;
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('get eventData cashout', () => {
    before(async () => {
      // insert test data
      let insertCashoutRecords = `
        INSERT INTO "cashoutRecords"("userId", date, amount, status, "oldSttw",
         "newSttw", "updateDt")
        VALUES($1,$2,$3,$4,$5,$6,$7);
      `;
      let cashoutRecords_Data_01 = [1, '2017-12-01', 50, 2, 25, 40, '2018-12-01 13:13:13'];

      try {
        result = await db.query(insertCashoutRecords, cashoutRecords_Data_01);
        assert.equal(result.rowCount, 1, 'insert cashoutRecords data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get eventData cashout', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/dashboard/eventData/cashout')
          .send({
            date: '2017-12-01',
            userId: 1
          });
        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.eventData[0].amount.should.equal(50);
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE "cashoutRecords" RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(
          `reset test data failed while testing /api/v1/dashboard/eventData/cashout, ${error.stack}`
        );
      }
    });
  });
});

// api/v1/dashboard/eventData/bill
describe('routes: /api/v1/dashboard/eventData/bill', () => {
  describe('get empty eventData bill', () => {
    it('should get empty', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/dashboard/eventData/bill')
          .set('content-type', 'application/json')
          .send({
            date: '2018-11-02',
            userId: 1
          });

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.eventData.should.be.empty;
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('get eventData bill', () => {
    before(async () => {
      // insert test data
      let insertEventByBill_01 = `
        INSERT INTO "eventByBill"(id, "billId", "userId", date, profit, "rentCost", "meterRentCost",
          "insuranceCost", "operateCost", "serviceCost", "loanCost", "loanFeeCost",
          "businessTax", "oldSttw", "newSttw", "updateDt")
          VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16);
      `;
      let eventByBillData_01 = [
        1,
        1,
        1,
        '2017-12-01',
        101,
        1,
        2,
        3,
        5,
        6,
        5,
        4,
        3,
        2,
        1,
        '2018-12-31 12:12:12'
      ];

      try {
        result = await db.query(insertEventByBill_01, eventByBillData_01);
        assert.equal(result.rowCount, 1, 'insert billRecords data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get eventData bill', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/dashboard/eventData/bill')
          .send({
            date: '2017-12-01',
            userId: 1
          });
        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.eventData[0].profit.should.equal('101');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE "eventByBill" RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(`reset test data failed while testing /api/v1/dashboard/eventData/bill, ${error.stack}`);
      }
    });
  });
});

// api/v1/dashboard/eventData/repay
describe('routes: /api/v1/dashboard/eventData/repay', () => {
  describe('get empty eventData repay', () => {
    it('should get empty', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/dashboard/eventData/repay')
          .set('content-type', 'application/json')
          .send({
            date: '2018-11-02',
            userId: 1
          });

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.eventData.should.be.empty;
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('get eventData repay', () => {
    before(async () => {
      // insert test data
      let insertEventByRepay = `
        INSERT INTO "eventByRepay"("pvId", "userId", date, "oldAmount",
          "newAmount", "oldSttw", "newSttw", repay, "updateDt")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9);
      `;
      let eventByRepayData_01 = ['testPvId', 1, '2018-11-02', 12, 13, 50, 100, 9090, '2018-12-01 12:12:12'];

      try {
        result = await db.query(insertEventByRepay, eventByRepayData_01);
        assert.equal(result.rowCount, 1, 'insert repayRecords data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get eventData repay', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/dashboard/eventData/repay')
          .send({
            date: '2018-11-02',
            userId: 1
          });
        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.eventData[0].repay.should.equal('9090');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE "eventByRepay" RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(`reset test data failed while testing /api/v1/dashboard/eventData/repay, ${error.stack}`);
      }
    });
  });
});

// api/v1/dashboard/eventData/buy
describe('routes: /api/v1/dashboard/eventData/buy', () => {
  describe('get empty eventData buy', () => {
    it('should get empty', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/dashboard/eventData/buy')
          .set('content-type', 'application/json')
          .send({
            date: '2018-11-02',
            userId: 1
          });

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.eventData.should.be.empty;
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('get eventData buy', () => {
    before(async () => {
      // insert test data
      let insertPvs = `
        INSERT INTO pvs(id, name, "solaName", "projectCode", location, "coordLat", "coordLng", capacity, decay, "annualAmount",
        stage, status, "totalCapital", "ownedCapital", "initialPrice",
        "validityDateBegin", "validityDuration", 
        "sellPrice", "operationRateByCapital", "operationRateByIncome", "serviceRateByIncome", 
        "rentRateByIncome", "rentYearCost", "rentWithTax", "owner")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25);`;
      let pvsData_01 = [
        'SNTW1',
        'testPvsName',
        'testPvsName',
        'testPrjcode',
        'testLocation',
        12.123456,
        123.123456,
        4567,
        1,
        2,
        2,
        3,
        100,
        10,
        10,
        '2018-11-03',
        20,
        99,
        98,
        97,
        0.02,
        0.1,
        7,
        true,
        'owner'
      ];

      let insertOrderBuys = `
        INSERT INTO "orderBuys"("pvId", datetime, "userId", price, amount)
        VALUES ($1,$2,$3,$4,$5);
      `;

      let orderBuys_Data_01 = ['SNTW1', '2018-11-03 03:03:03', 1, 50, 5];

      let insertTrades = `
        INSERT INTO trades("orderBuyId", "orderSellId", datetime, price,
          amount, fee, "oldAmountBuyer", "newAmountBuyer", "oldSttwBuyer",
          "newSttwBuyer", "oldAmountSeller", "newAmountSeller", "oldSttwSeller",
          "newSttwSeller")
        VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14);
      `;

      let trades_Data_01 = [1, 2, '2018-12-02 14:21:21', 50, 5, 0.1, 20, 25, 100, 50, 30, 25, 50, 100];

      try {
        result = await db.query(insertPvs, pvsData_01);
        assert.equal(result.rowCount, 1, 'insert pvs data failed');

        result = await db.query(insertOrderBuys, orderBuys_Data_01);
        assert.equal(result.rowCount, 1, 'insert orderBuys data failed');

        result = await db.query(insertTrades, trades_Data_01);
        assert.equal(result.rowCount, 1, 'insert trades data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get eventData buy', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/dashboard/eventData/buy')
          .send({
            date: '2018-12-02',
            userId: 1
          });
        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.eventData[0].pvId.should.equal('SNTW1');
        res.body.result.eventData[0].name.should.equal('testPvsName');
        res.body.result.eventData[0].datetime.should.equal('2018-12-02 14:21:21');
        res.body.result.eventData[0].price.should.equal('50');
        res.body.result.eventData[0].amount.should.equal('5');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE pvs RESTART IDENTITY;
          TRUNCATE "orderBuys" RESTART IDENTITY;
          TRUNCATE trades RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(`reset test data failed while testing /api/v1/dashboard/eventData/buy, ${error.stack}`);
      }
    });
  });
});

// api/v1/dashboard/eventData/sell
describe('routes: /api/v1/dashboard/eventData/sell', () => {
  describe('get empty eventData sell', () => {
    it('should get empty', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/dashboard/eventData/sell')
          .set('content-type', 'application/json')
          .send({
            date: '2018-11-02',
            userId: 1
          });

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.eventData.should.be.empty;
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('get eventData sell', () => {
    before(async () => {
      // insert test data
      let insertTrades = `
        INSERT INTO trades("orderBuyId", "orderSellId", datetime, price,
          amount, fee, "oldAmountBuyer", "newAmountBuyer", "oldSttwBuyer",
          "newSttwBuyer", "oldAmountSeller", "newAmountSeller", "oldSttwSeller",
          "newSttwSeller")
        VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14);
      `;

      let trades_Data_01 = [2, 1, '2018-12-02 14:21:21', 50, 5, 0.1, 20, 25, 100, 50, 30, 25, 50, 100];

      let insertPvs = `
        INSERT INTO pvs(id, name, "solaName", "projectCode", location, "coordLat", "coordLng", capacity, decay, "annualAmount",
        stage, status, "totalCapital", "ownedCapital", "initialPrice", 
        "validityDateBegin", "validityDuration", 
        "sellPrice", "operationRateByCapital", "operationRateByIncome", "serviceRateByIncome", 
        "rentRateByIncome", "rentYearCost", "rentWithTax", "owner")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25);`;
      let pvsData_01 = [
        'SNTW1',
        'testPvsName',
        'testPvsName',
        'testPrjcode',
        'testLocation',
        12.123456,
        123.123456,
        4567,
        1,
        2,
        2,
        3,
        100,
        10,
        10,
        '2018-11-03',
        20,
        99,
        98,
        97,
        0.02,
        0.1,
        7,
        true,
        'owner'
      ];

      let insertOrderSells = `
        INSERT INTO "orderSells"("pvId", datetime, "userId", price, amount)
        VALUES ($1,$2,$3,$4,$5);
      `;

      let orderSells_Data_01 = ['SNTW1', '2018-11-03 03:03:03', 1, 50, 5];

      try {
        result = await db.query(insertTrades, trades_Data_01);
        assert.equal(result.rowCount, 1, 'insert trades data failed');

        result = await db.query(insertPvs, pvsData_01);
        assert.equal(result.rowCount, 1, 'insert pvs data failed');

        result = await db.query(insertOrderSells, orderSells_Data_01);
        assert.equal(result.rowCount, 1, 'insert orderSells data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get eventData sell', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/dashboard/eventData/sell')
          .send({
            date: '2018-12-02',
            userId: 1
          });
        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.eventData[0].pvId.should.equal('SNTW1');
        res.body.result.eventData[0].name.should.equal('testPvsName');
        res.body.result.eventData[0].datetime.should.equal('2018-12-02 14:21:21');
        res.body.result.eventData[0].price.should.equal('50');
        res.body.result.eventData[0].amount.should.equal('5');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE trades RESTART IDENTITY;
          TRUNCATE pvs RESTART IDENTITY;
          TRUNCATE "orderSells" RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(`reset test data failed while testing /api/v1/dashboard/eventData/sell, ${error.stack}`);
      }
    });
  });
});

// api/v1/dashboard/lastTrades/:userId
describe('routes: /api/v1/dashboard/lastTrades/:userId', () => {
  describe("get empty user's last trades", () => {
    it('should get empty', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/dashboard/lastTrades/1');

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.lastTrades.should.be.empty;
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's last trades", () => {
    before(async () => {
      // insert test data
      let insertTrades = `
        INSERT INTO trades("orderBuyId", "orderSellId", datetime, price,
          amount, fee, "oldAmountBuyer", "newAmountBuyer", "oldSttwBuyer",
          "newSttwBuyer", "oldAmountSeller", "newAmountSeller", "oldSttwSeller",
          "newSttwSeller")
        VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14);
      `;

      let trades_Data_01 = [1, 1, '2018-12-02 14:21:21', 50, 5, 0.1, 20, 25, 100, 50, 30, 25, 50, 100];

      let insertOrderBuys = `
        INSERT INTO "orderBuys"("pvId", datetime, "userId", price, amount)
        VALUES ($1,$2,$3,$4,$5);
      `;

      let orderBuys_Data_01 = ['SNTW1', '2018-11-03 03:03:03', 1, 50, 5];

      let insertOrderSells = `
        INSERT INTO "orderSells"("pvId", datetime, "userId", price, amount)
        VALUES ($1,$2,$3,$4,$5);
      `;

      let orderSells_Data_01 = ['SNTW2', '2018-11-03 03:03:03', 1, 50, 5];

      let insertPvs = `
        INSERT INTO pvs(id, name, "solaName", "projectCode", location, "coordLat", "coordLng", capacity, decay, "annualAmount",
        stage, status, "totalCapital", "ownedCapital", "initialPrice",
        "validityDateBegin", "validityDuration", 
        "sellPrice", "operationRateByCapital", "operationRateByIncome", "serviceRateByIncome", 
        "rentRateByIncome", "rentYearCost", "rentWithTax", "owner")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25);`;
      let pvsData_01 = [
        'SNTW1',
        'testPvsName',
        'testPvsName',
        'testPrjcode',
        'testLocation',
        12.123456,
        123.123456,
        4567,
        1,
        2,
        2,
        3,
        100,
        10,
        10,
        '2018-11-03',
        20,
        99,
        98,
        97,
        0.02,
        0.1,
        7,
        true,
        'owner'
      ];
      let pvsData_02 = [
        'SNTW2',
        'testPvsName2',
        'testPvsName2',
        'testPrjcode',
        'testLocation',
        12.123456,
        123.123456,
        4567,
        1,
        2,
        2,
        3,
        100,
        10,
        10,
        '2018-11-03',
        20,
        99,
        98,
        97,
        0.02,
        0.1,
        7,
        true,
        'owner'
      ];
      try {
        result = await db.query(insertTrades, trades_Data_01);
        assert.equal(result.rowCount, 1, 'insert trades data failed');

        result = await db.query(insertOrderBuys, orderBuys_Data_01);
        assert.equal(result.rowCount, 1, 'insert orderBuys data failed');

        result = await db.query(insertOrderSells, orderSells_Data_01);
        assert.equal(result.rowCount, 1, 'insert orderSells data failed');

        result = await db.query(insertPvs, pvsData_01);
        assert.equal(result.rowCount, 1, 'insert pvs data failed');
        result = await db.query(insertPvs, pvsData_02);
        assert.equal(result.rowCount, 1, 'insert pvs data2 failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it("should get user's last trade", async () => {
      try {
        let res = await chai.request(api).get('/api/v1/dashboard/lastTrades/1');
        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.lastTrades[0].side.should.equal('買');
        res.body.result.lastTrades[0].pvId.should.equal('SNTW1');
        res.body.result.lastTrades[0].datetime.should.equal('2018-12-02 14:21:21');
        res.body.result.lastTrades[0].price.should.equal('50');
        res.body.result.lastTrades[0].amount.should.equal('5');
        res.body.result.lastTrades[0].name.should.equal('testPvsName');
        res.body.result.lastTrades[1].side.should.equal('賣');
        res.body.result.lastTrades[1].pvId.should.equal('SNTW2');
        res.body.result.lastTrades[1].datetime.should.equal('2018-12-02 14:21:21');
        res.body.result.lastTrades[1].price.should.equal('50');
        res.body.result.lastTrades[1].amount.should.equal('5');
        res.body.result.lastTrades[1].name.should.equal('testPvsName2');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE trades RESTART IDENTITY;
          TRUNCATE "orderBuys" RESTART IDENTITY;
          TRUNCATE "orderSells" RESTART IDENTITY;
          TRUNCATE pvs RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(`reset test data failed while testing /api/v1/dashboard/eventData/sell, ${error.stack}`);
      }
    });
  });
});

// api/v1/dashboard/lastMarketTrades
describe('routes: /api/v1/dashboard/lastMarketTrades', () => {
  describe('get empty lastMarketTrades', () => {
    it('should get empty', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/dashboard/lastMarketTrades');

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.lastTrades.should.be.empty;
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('get lastMarketTrades', () => {
    before(async () => {
      // insert test data
      let insertTrades = `
        INSERT INTO trades("orderBuyId", "orderSellId", datetime, price,
          amount, fee, "oldAmountBuyer", "newAmountBuyer", "oldSttwBuyer",
          "newSttwBuyer", "oldAmountSeller", "newAmountSeller", "oldSttwSeller",
          "newSttwSeller")
        VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14);
      `;

      let trades_Data_01 = [1, 2, '2018-12-02 14:21:21', 50, 5, 0.1, 20, 25, 100, 50, 30, 25, 50, 100];

      let insertOrderBuys = `
        INSERT INTO "orderBuys"("pvId", datetime, "userId", price, amount)
        VALUES ($1,$2,$3,$4,$5);
      `;

      let orderBuys_Data_01 = ['SNTW1', '2018-11-03 03:03:03', 1, 50, 5];

      let insertPvs = `
        INSERT INTO pvs(id, name, "solaName", "projectCode", location, "coordLat", "coordLng", capacity, decay, "annualAmount",
        stage, status, "totalCapital", "ownedCapital", "initialPrice",
        "validityDateBegin", "validityDuration", 
        "sellPrice", "operationRateByCapital", "operationRateByIncome", "serviceRateByIncome", 
        "rentRateByIncome", "rentYearCost", "rentWithTax", "owner")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25);`;
      let pvsData_01 = [
        'SNTW1',
        'testPvsName',
        'testPvsName',
        'testPrjcode',
        'testLocation',
        12.123456,
        123.123456,
        4567,
        1,
        2,
        2,
        3,
        100,
        10,
        10,
        '2018-11-03',
        20,
        99,
        98,
        97,
        0.02,
        0.1,
        7,
        true,
        'owner'
      ];
      try {
        result = await db.query(insertTrades, trades_Data_01);
        assert.equal(result.rowCount, 1, 'insert trades data failed');

        result = await db.query(insertOrderBuys, orderBuys_Data_01);
        assert.equal(result.rowCount, 1, 'insert orderBuys data failed');

        result = await db.query(insertPvs, pvsData_01);
        assert.equal(result.rowCount, 1, 'insert pvs data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get lastMarketTrades', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/dashboard/lastMarketTrades');
        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.lastTrades[0].pvId.should.equal('SNTW1');
        res.body.result.lastTrades[0].name.should.equal('testPvsName');
        res.body.result.lastTrades[0].price.should.equal('50');
        res.body.result.lastTrades[0].amount.should.equal('5');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE trades RESTART IDENTITY;
          TRUNCATE "orderBuys" RESTART IDENTITY;
          TRUNCATE pvs RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(
          `reset test data failed while testing /api/v1/dashboard/lastMarketTrades, ${error.stack}`
        );
      }
    });
  });
});

// api/v1/dashboard/favoritePv/:userId
describe('routes: /api/v1/dashboard/favoritePv/:userId', () => {
  describe('get empty user favoritePv', () => {
    it('should get empty', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/dashboard/favoritePv/1');

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.favoritePv.should.be.empty;
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('get user favoritePv', () => {
    before(async () => {
      // insert test data
      let insertUserAssets = `
        INSERT INTO "userAssets"("userId", "pvId", amount, "lockAmount")
        VALUES($1,$2,$3,$4);
      `;
      let userAssetsData_01 = [1, 'SNTW1', 123456, 456];

      let insertPvs = `
        INSERT INTO pvs(id, name, "solaName", "projectCode", location, "coordLat", "coordLng", capacity, decay, "annualAmount",
        stage, status, "totalCapital", "ownedCapital", "initialPrice",
        "validityDateBegin", "validityDuration", 
        "sellPrice", "operationRateByCapital", "operationRateByIncome", "serviceRateByIncome", 
        "rentRateByIncome", "rentYearCost", "rentWithTax", "owner")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25);`;
      let pvsData_01 = [
        'SNTW1',
        'testPvsName',
        'testPvsName',
        'testPrjcode',
        'testLocation',
        12.123456,
        123.123456,
        4567,
        1,
        2,
        2,
        3,
        100,
        10,
        10,
        '2018-11-03',
        20,
        99,
        98,
        97,
        0.02,
        0.1,
        7,
        true,
        'owner'
      ];

      let insertMarketInfo = `
        INSERT INTO "marketInfo"("pvId", "updateDt", open, high,
          low, price, volume)
        VALUES($1,$2,$3,$4,$5,$6,$7);`;
      let marketInfoData_01 = ['SNTW1', '2018-11-02 11:11:11', 100, 200, 10, 95, 99999];

      try {
        result = await db.query(insertUserAssets, userAssetsData_01);
        assert.equal(result.rowCount, 1, 'insert userAssets data failed');

        result = await db.query(insertPvs, pvsData_01);
        assert.equal(result.rowCount, 1, 'insert pvs data failed');

        result = await db.query(insertMarketInfo, marketInfoData_01);
        assert.equal(result.rowCount, 1, 'insert marketInfo data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get user favoritePv', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/dashboard/favoritePv/1');
        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.favoritePv[0].pvId.should.equal('SNTW1');
        res.body.result.favoritePv[0].name.should.equal('testPvsName');
        res.body.result.favoritePv[0].price.should.equal(95);
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE "userAssets" RESTART IDENTITY;
          TRUNCATE pvs RESTART IDENTITY;
          TRUNCATE "marketInfo" RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(
          `reset test data failed while testing /api/v1/dashboard/favoritePv/:userId, ${error.stack}`
        );
      }
    });
  });
});

// api/v1/dashboard/hotPv
describe('routes: /api/v1/dashboard/hotPv', () => {
  describe('get empty hotPv', () => {
    it('should get empty', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/dashboard/hotPv');

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.hotPv.should.be.empty;
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('get hotPv', () => {
    before(async () => {
      // insert test data
      let insertPvs = `
        INSERT INTO pvs(id, name, "solaName", "projectCode", location, "coordLat", "coordLng", capacity, decay, "annualAmount",
        stage, status, "totalCapital", "ownedCapital", "initialPrice",
        "validityDateBegin", "validityDuration", 
        "sellPrice", "operationRateByCapital", "operationRateByIncome", "serviceRateByIncome", 
        "rentRateByIncome", "rentYearCost", "rentWithTax", "owner")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25);`;
      let pvsData_01 = [
        'SNTW1',
        'testPvsName',
        'testPvsName',
        'testPrjcode',
        'testLocation',
        12.123456,
        123.123456,
        4567,
        1,
        2,
        2,
        3,
        100,
        10,
        10,
        '2018-11-03',
        20,
        99,
        98,
        97,
        0.02,
        0.1,
        7,
        true,
        'owner'
      ];

      let insertMarketInfo = `
        INSERT INTO "marketInfo"("pvId", "updateDt", open, high,
          low, price, volume)
        VALUES($1,$2,$3,$4,$5,$6,$7);`;
      let marketInfoData_01 = ['SNTW1', '2018-11-02 11:11:11', 100, 200, 10, 95, 99999];

      try {
        result = await db.query(insertPvs, pvsData_01);
        assert.equal(result.rowCount, 1, 'insert pvs data failed');

        result = await db.query(insertMarketInfo, marketInfoData_01);
        assert.equal(result.rowCount, 1, 'insert marketInfo data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get hotPv', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/dashboard/hotPv');
        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.hotPv[0].pvId.should.equal('SNTW1');
        res.body.result.hotPv[0].name.should.equal('testPvsName');
        res.body.result.hotPv[0].price.should.equal(95);
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE pvs RESTART IDENTITY;
          TRUNCATE "marketInfo" RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(`reset test data failed while testing /api/v1/dashboard/hotPv, ${error.stack}`);
      }
    });
  });
});
