const chai = require('chai');
const should = chai.should();
const assert = chai.assert;
const moment = require('moment');

const { simulatedDailyProfit, equalPrincipalPayment, getRangeLoanCost, getRangeCost } = require('../finance');

describe('getRangeCost should be correct', () => {
  it('get cost in range', () => {
    const list = [{ beginDate: '2018-03-03', duration: 1, cost: 365 }];
    const cost = getRangeCost(list, '2018-03-03', '2018-03-04');
    cost.should.equal(1);
  });

  it('get cost out of range', () => {
    const list = [{ beginDate: '2018-03-03', duration: 1, cost: 365 }];
    const cost = getRangeCost(list, '2019-03-03', '2018-03-04');
    cost.should.equal(0);
  });

  it('get some cost in range', () => {
    const list = [{ beginDate: '2018-03-03', duration: 1, cost: 365 }];
    const cost = getRangeCost(list, '2019-02-15', '2019-03-30');
    cost.should.equal(16);
  });
});

describe('simulatedDailyProfit should be correct', () => {
  it('for natural landlord', () => {
    try {
      const diffDay = moment('2018-05-30').diff('2018-02-14', 'days');
      const result = simulatedDailyProfit({
        date: '2018-02-14',
        power: 47236 / 0.999 / diffDay, //該日發電量
        sellPrice: 5.2758, //躉售電價單(未稅)
        totalCapital: 6093900, //系統成本
        operationRateByCapital: 0, //營運費率(固定系統成本)
        operationRateByIncome: 0.1, //營運費率(電費抽成)
        serviceRateByIncome: 0.03, //SOLA服務費率(電費抽成)
        rentRateByIncome: 0.05, //租金費率(電費抽成)
        rentYearCost: 0, //年固定租金
        rentWithTax: false, //租金計算基礎(含稅未稅)
        insuranceList: [],
        loanFeeList: [],
        loanList: [{ amount: 6093900 * 0.75, rate: 0.0266, period: 15, beginDate: '2018-02-14' }]
      });

      Math.round(result.dayIncomeWithTax).should.equal(2492);
      Math.round(result.dayProfit).should.equal(773);
      Math.round(result.rentDayCostFixed).should.equal(0);
      Math.round(result.rentDayCostFloat).should.equal(119);
      Math.round(result.operationDayCostFixed).should.equal(0);
      Math.round(result.operationDayCostFloat).should.equal(248);
      Math.round(result.serviceDayCostFloat).should.equal(75);
      Math.round(result.meterRentDayCost).should.equal(8);
      Math.round(result.insuranceDayCost).should.equal(0);
      Math.round(result.loanDayCost).should.equal(1269);
      Math.round(result.loanFeeDayCost).should.equal(0);
      Math.round(result.dayBusinessTax).should.equal(0);
    } catch (error) {
      console.error(error.stack);
      assert.fail();
    }
  });

  it('for juristic landlord', () => {
    try {
      const diffDay = moment('2018-05-30').diff('2018-02-14', 'days');
      const result = simulatedDailyProfit({
        date: '2018-02-14',
        power: 47236 / 0.999 / diffDay, //該日發電量
        sellPrice: 5.2758, //躉售電價單(未稅)
        totalCapital: 6093900, //系統成本
        operationRateByCapital: 0, //營運費率(固定系統成本)
        operationRateByIncome: 0.1, //營運費率(電費抽成)
        serviceRateByIncome: 0.03, //SOLA服務費率(電費抽成)
        rentRateByIncome: 0.05, //租金費率(電費抽成)
        rentYearCost: 0, //年固定租金
        rentWithTax: true, //租金計算基礎(含稅未稅)
        insuranceList: [],
        loanFeeList: [],
        loanList: [{ amount: 6093900 * 0.75, rate: 0.0266, period: 15, beginDate: '2018-02-14' }]
      });

      Math.round(result.dayIncomeWithTax).should.equal(2492);
      Math.round(result.dayProfit).should.equal(768);
      Math.round(result.rentDayCostFixed).should.equal(0);
      Math.round(result.rentDayCostFloat).should.equal(124);
      Math.round(result.operationDayCostFixed).should.equal(0);
      Math.round(result.operationDayCostFloat).should.equal(248);
      Math.round(result.serviceDayCostFloat).should.equal(75);
      Math.round(result.meterRentDayCost).should.equal(8);
      Math.round(result.insuranceDayCost).should.equal(0);
      Math.round(result.loanDayCost).should.equal(1269);
      Math.round(result.loanFeeDayCost).should.equal(0);
      Math.round(result.dayBusinessTax).should.equal(0);
    } catch (error) {
      console.error(error.stack);
      assert.fail();
    }
  });

  it('for loanFee', () => {
    try {
      const diffDay = moment('2018-05-30').diff('2018-02-14', 'days');
      const result = simulatedDailyProfit({
        date: '2018-02-14',
        power: 47236 / 0.999 / diffDay, //該日發電量
        sellPrice: 5.2758, //躉售電價單(未稅)
        totalCapital: 6093900, //系統成本
        operationRateByCapital: 0, //營運費率(固定系統成本)
        operationRateByIncome: 0.1, //營運費率(電費抽成)
        serviceRateByIncome: 0.03, //SOLA服務費率(電費抽成)
        rentRateByIncome: 0.05, //租金費率(電費抽成)
        rentYearCost: 0, //年固定租金
        rentWithTax: true, //租金計算基礎(含稅未稅)
        insuranceList: [],
        loanFeeList: [{ beginDate: '2018-02-14', duration: 5, cost: 311025 }],
        loanList: [{ amount: 6093900 * 0.75, rate: 0.0266, period: 15, beginDate: '2018-02-14' }]
      });

      Math.round(result.dayIncomeWithTax).should.equal(2492);
      Math.round(result.dayProfit).should.equal(598);
      Math.round(result.rentDayCostFixed).should.equal(0);
      Math.round(result.rentDayCostFloat).should.equal(124);
      Math.round(result.operationDayCostFixed).should.equal(0);
      Math.round(result.operationDayCostFloat).should.equal(248);
      Math.round(result.serviceDayCostFloat).should.equal(75);
      Math.round(result.meterRentDayCost).should.equal(8);
      Math.round(result.insuranceDayCost).should.equal(0);
      Math.round(result.loanDayCost).should.equal(1269);
      Math.round(result.loanFeeDayCost).should.equal(170);
      Math.round(result.dayBusinessTax).should.equal(0);
    } catch (error) {
      console.error(error.stack);
      assert.fail();
    }
  });
});

describe('equal principal payment should be correct', () => {
  it('get day loan cost in loan period, case 1', () => {
    const loanAmount = 1000000;
    const loanPeriod = 15;
    const loanRate = 0.0266;
    const loanBeginDate = '2018-02-14';
    const date = '2018-03-13';
    const dayCost = equalPrincipalPayment({ loanAmount, loanPeriod, loanRate, loanBeginDate, date });
    Math.round(dayCost).should.equal(278);
  });

  it('get day loan cost in loan period, case 2', () => {
    const loanAmount = 1000000;
    const loanPeriod = 15;
    const loanRate = 0.0266;
    const loanBeginDate = '2018-02-14';
    const date = '2018-03-14';
    const dayCost = equalPrincipalPayment({ loanAmount, loanPeriod, loanRate, loanBeginDate, date });
    Math.round(dayCost).should.equal(250);
  });

  it('get day loan cost at month end', () => {
    const loanAmount = 1000000;
    const loanPeriod = 15;
    const loanRate = 0.0266;
    const loanBeginDate = '2017-12-31';
    const date = '2018-02-28';
    const dayCost = equalPrincipalPayment({ loanAmount, loanPeriod, loanRate, loanBeginDate, date });
    Math.round(dayCost).should.equal(250);
  });

  it('get day loan cost berfore loan period', () => {
    const loanAmount = 1000000;
    const loanPeriod = 15;
    const loanRate = 0.0266;
    const loanBeginDate = '2018-02-14';
    const date = '2018-02-13';
    const dayCost = equalPrincipalPayment({ loanAmount, loanPeriod, loanRate, loanBeginDate, date });
    Math.round(dayCost).should.equal(0);
  });

  it('get day loan cost after loan period', () => {
    const loanAmount = 1000000;
    const loanPeriod = 15;
    const loanRate = 0.0266;
    const loanBeginDate = '2018-02-14';
    const date = '2033-02-14';
    const dayCost = equalPrincipalPayment({ loanAmount, loanPeriod, loanRate, loanBeginDate, date });
    Math.round(dayCost).should.equal(0);
  });
});
