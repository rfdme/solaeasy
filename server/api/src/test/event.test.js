const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
chai.should();
const db = require('../db');
const api = require('../server.js');
const assert = require('assert');
let token;

// get token for testing
describe('get token', () => {
  it('should login successfully and get JWT', async () => {
    try {
      let res = await chai
        .request(api)
        .post('/api/v1/auth/local')
        .set('content-type', 'application/json')
        .send({
          email: 'test@mail.com',
          pwd: '1234'
        });
      res.status.should.equal(200);
      res.body.result.manager.email.should.equal('test@mail.com');
      token = res.body.result.token;
    } catch (error) {
      assert.fail(error.stack);
    }
  });
});

// api/v1/event/pv
describe('routes: /api/v1/event/pv', () => {
  describe('get empty all pv', () => {
    it('should get empty', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/event/pv')
          .query({ begin: '2018-10-10', end: '2019-10-10' });

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.events.should.be.empty;
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe('get all pv', () => {
    before(async () => {
      // insert test data
      let insertEventByPv = `
      INSERT INTO "eventByPV" ("pvId", date, name, content, "updateDt")
      VALUES($1,$2,$3,$4,$5);
      `;
      let eventByPv_Data_01 = ['SNTW1', '2018-12-01', '簽訂工程合約', 'TODO content', '2018-11-15 03:04:05'];
      let eventByPv_Data_02 = [
        'SNTW2',
        '2018-12-31',
        '簽訂工程合約2',
        'TODO content2',
        '2018-11-15 03:04:05'
      ];
      try {
        result = await db.query(insertEventByPv, eventByPv_Data_01);
        assert.equal(result.rowCount, 1, 'insert eventByPV data failed');

        result = await db.query(insertEventByPv, eventByPv_Data_02);
        assert.equal(result.rowCount, 1, 'insert eventByPV data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get all pv', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/event/pv')
          .query({ begin: '2018-10-10', end: '2019-10-10' });

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.events[0].pvId.should.equal('SNTW1');
        res.body.result.events[0].date.should.equal('2018-12-01');
        res.body.result.events[0].name.should.equal('簽訂工程合約');
        res.body.result.events[0].content.should.equal('TODO content');
        res.body.result.events[1].pvId.should.equal('SNTW2');
        res.body.result.events[1].date.should.equal('2018-12-31');
        res.body.result.events[1].name.should.equal('簽訂工程合約2');
        res.body.result.events[1].content.should.equal('TODO content2');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE "eventByPV" RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(`reset test data failed while testing /api/v1/event/pv, ${error.stack}`);
      }
    });
  });
});

// api/v1/event/simulate
describe('routes: /api/v1/simulate', () => {
  describe('access without token', () => {
    it('should get authorization error', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/event/simulate')
          .query({ begin: '2018-10-10', end: '2019-10-10', user: '1' });

        res.status.should.equal(401);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('authorized error');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get other user's simulate", () => {
    it('should get forbidden', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/event/simulate')
          .set('Authorization', `Bearer ${token}`)
          .query({ begin: '2018-10-10', end: '2019-10-10', user: '2' });

        res.status.should.equal(403);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's simulate", () => {
    before(async () => {
      // insert some test data
      let insertEventBySimulate = `
        INSERT INTO "eventBySimulate"(id, "pvId", "userId", date, profit,
        "rentCost", "meterRentCost", "insuranceCost",
        "operateCost", "serviceCost", "loanCost", "loanFeeCost",
        "businessTax", "updateDt")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14);
      `;
      let eventBySimulateData_01 = [
        1,
        'testPvId',
        1,
        '2018-11-02',
        100,
        2,
        1,
        5,
        8,
        9,
        10,
        9,
        8,
        '2018-12-12 12:12:12'
      ];
      let eventBySimulateData_02 = [
        2,
        'testPvId02',
        1,
        '2018-11-03',
        101,
        3,
        2,
        6,
        9,
        10,
        11,
        10,
        9,
        '2018-12-21 21:21:21'
      ];
      try {
        result = await db.query(insertEventBySimulate, eventBySimulateData_01);
        assert.equal(result.rowCount, 1, 'insert eventBySimulate data failed');

        result = await db.query(insertEventBySimulate, eventBySimulateData_02);
        assert.equal(result.rowCount, 1, 'insert eventBySimulate data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get user simulate', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/event/simulate')
          .set('Authorization', `Bearer ${token}`)
          .query({ begin: '2018-10-10', end: '2019-10-10', user: '1' });

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.result.events[0].date.should.equal('2018-11-02');
        res.body.result.events[0].profit.should.equal('100');
        res.body.result.events[1].date.should.equal('2018-11-03');
        res.body.result.events[1].profit.should.equal('101');
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    // reset test data
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE "eventBySimulate" RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(false, true, 'reset test data failed while testing /api/v1/event/simulate');
      }
    });
  });
});

// api/v1/event/simulateDetail
describe('routes: /api/v1/event/simulateDetail', () => {
  describe('access without token', () => {
    it('should get authorization error', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/event/simulateDetail')
          .query({ date: '2018-11-02', user: '1' });

        res.status.should.equal(401);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('authorized error');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get other user's simulateDetail", () => {
    it('should get forbidden', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/event/simulateDetail')
          .set('Authorization', `Bearer ${token}`)
          .query({ date: '2018-11-02', user: '2' });

        res.status.should.equal(403);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's simulateDetail", () => {
    before(async () => {
      // insert some test data
      let insertEventBySimulate = `
        INSERT INTO "eventBySimulate"(id, "pvId", "userId", date, profit,
        "rentCost", "meterRentCost", "insuranceCost",
        "operateCost", "serviceCost", "loanCost", "loanFeeCost",
        "businessTax", "updateDt")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14);
      `;
      let eventBySimulateData_01 = [
        1,
        'testPvId',
        1,
        '2018-11-02',
        100,
        2,
        1,
        5,
        8,
        9,
        10,
        9,
        8,
        '2018-12-12 12:12:12'
      ];

      let eventBySimulateData_02 = [
        2,
        'testPvId2',
        1,
        '2018-11-02',
        200,
        2,
        1,
        5,
        8,
        9,
        10,
        9,
        8,
        '2018-12-12 12:12:12'
      ];
      try {
        result = await db.query(insertEventBySimulate, eventBySimulateData_01);
        assert.equal(result.rowCount, 1, 'insert eventBySimulate data failed');
        result = await db.query(insertEventBySimulate, eventBySimulateData_02);
        assert.equal(result.rowCount, 1, 'insert eventBySimulate data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get user simulateDetail', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/event/simulateDetail')
          .set('Authorization', `Bearer ${token}`)
          .query({ date: '2018-11-02', user: '1' });

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.result.events[0].pvId.should.equal('testPvId');
        res.body.result.events[0].profit.should.equal('100');
        res.body.result.events[1].pvId.should.equal('testPvId2');
        res.body.result.events[1].profit.should.equal('200');
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    // reset test data
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE "eventBySimulate" RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(false, true, 'reset test data failed while testing /api/v1/event/simulateDetail');
      }
    });
  });
});

// api/v1/event/bill
describe('routes: /api/v1/event/bill', () => {
  describe('access without token', () => {
    it('should get authorization error', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/event/bill')
          .query({ begin: '2018-10-10', end: '2019-10-10', user: '1' });

        res.status.should.equal(401);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('authorized error');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get other user's bill", () => {
    it('should get forbidden', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/event/bill')
          .set('Authorization', `Bearer ${token}`)
          .query({ begin: '2018-10-10', end: '2019-10-10', user: '2' });

        res.status.should.equal(403);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's bill", () => {
    before(async () => {
      // insert some test data
      let insertEventByBill = `
        INSERT INTO "eventByBill"(id, "billId", "userId", date, profit, "rentCost", "meterRentCost",
          "insuranceCost", "operateCost", "serviceCost", "loanCost", "loanFeeCost",
          "businessTax", "oldSttw", "newSttw", "updateDt")
          VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16);
      `;
      let eventByBillData_01 = [
        1,
        1,
        1,
        '2018-11-02',
        101,
        1,
        2,
        3,
        5,
        6,
        5,
        4,
        3,
        2,
        1,
        '2018-12-31 12:12:12'
      ];
      let eventByBillData_02 = [
        2,
        2,
        1,
        '2018-11-03',
        202,
        2,
        4,
        6,
        10,
        12,
        10,
        8,
        6,
        4,
        2,
        '2019-01-31 21:21:21'
      ];
      try {
        result = await db.query(insertEventByBill, eventByBillData_01);
        assert.equal(result.rowCount, 1, 'insert eventByBill data failed');
        result = await db.query(insertEventByBill, eventByBillData_02);
        assert.equal(result.rowCount, 1, 'insert eventByBill data2 failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get user bill', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/event/bill')
          .set('Authorization', `Bearer ${token}`)
          .query({ begin: '2018-10-10', end: '2019-10-10', user: '1' });

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.result.events[0].date.should.equal('2018-11-02');
        res.body.result.events[0].profit.should.equal('101');
        res.body.result.events[1].date.should.equal('2018-11-03');
        res.body.result.events[1].profit.should.equal('202');
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    // reset test data
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE "eventByBill" RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(false, true, 'reset test data failed while testing /api/v1/event/bill');
      }
    });
  });
});

// api/v1/event/billDetail
describe('routes: /api/v1/event/billDetail', () => {
  describe('access without token', () => {
    it('should get authorization error', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/event/billDetail')
          .query({ date: '2018-11-02', user: '1' });

        res.status.should.equal(401);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('authorized error');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get other user's billDetail", () => {
    it('should get forbidden', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/event/billDetail')
          .set('Authorization', `Bearer ${token}`)
          .query({ date: '2018-11-02', user: '2' });

        res.status.should.equal(403);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's billDetail", () => {
    before(async () => {
      // insert some test data
      let insertEventByBill = `
      INSERT INTO "eventByBill"(id, "billId", "userId", date, profit, "rentCost", "meterRentCost",
        "insuranceCost", "operateCost", "serviceCost", "loanCost", "loanFeeCost",
        "businessTax", "oldSttw", "newSttw", "updateDt")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16);
      `;
      let eventByBillData_01 = [
        1,
        1,
        1,
        '2018-11-02',
        101,
        1,
        2,
        3,
        5,
        6,
        5,
        4,
        3,
        2,
        1,
        '2018-12-31 12:12:12'
      ];
      let insertBills = `
        INSERT INTO bills (id, "pvId", "rangeBegin", "rangeEnd", "totalPower", "incomeWithTax", "meterRentCost",
          "profit", "rentCostFloat", "rentCostFixed", "operateCostFloat", "operateCostFixed", 
          "serviceCostFloat", "insuranceCost", "loanCost", "loanFeeCost", "businessTax",
          "updateDt")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17, $18);
      `;
      let bills_Data_01 = [
        1,
        'P0001',
        '2018-02-14',
        '2018-05-30',
        47236,
        261669,
        873,
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        '2018-06-01 03:05:06'
      ];
      try {
        result = await db.query(insertEventByBill, eventByBillData_01);
        assert.equal(result.rowCount, 1, 'insert eventByBill data failed');

        result = await db.query(insertBills, bills_Data_01);
        assert.equal(result.rowCount, 1, 'insert bills data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get user billDetail', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/event/billDetail')
          .set('Authorization', `Bearer ${token}`)
          .query({ date: '2018-11-02', user: '1' });

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.result.events[0].pvId.should.equal('P0001');
        res.body.result.events[0].rangeBegin.should.equal('2018-02-14');
        res.body.result.events[0].rangeEnd.should.equal('2018-05-30');
        res.body.result.events[0].profit.should.equal('101');
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    // reset test data
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE "eventByBill" RESTART IDENTITY;
          TRUNCATE bills;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(false, true, 'reset test data failed while testing /api/v1/event/billDetail');
      }
    });
  });
});

// api/v1/event/repay
describe('routes: /api/v1/event/repay', () => {
  describe('access without token', () => {
    it('should get authorization error', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/event/repay')
          .query({ begin: '2018-10-10', end: '2019-10-10', user: '1' });

        res.status.should.equal(401);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('authorized error');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get other user's repay", () => {
    it('should get forbidden', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/event/repay')
          .set('Authorization', `Bearer ${token}`)
          .query({ begin: '2018-10-10', end: '2019-10-10', user: '2' });

        res.status.should.equal(403);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's repay", () => {
    before(async () => {
      // insert some test data
      let insertEventByRepay = `
        INSERT INTO "eventByRepay"("pvId", "userId", date, "oldAmount",
          "newAmount", "oldSttw", "newSttw", repay, "updateDt")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9);
      `;
      let eventByRepayData_01 = ['testPvId', 1, '2018-11-02', 12, 13, 50, 100, 9090, '2018-12-01 12:12:12'];
      let eventByRepayData_02 = ['testPvId', 1, '2018-11-02', 12, 13, 50, 100, 9090, '2018-12-01 12:12:12'];
      let eventByRepayData_03 = ['testPvId2', 1, '2018-11-03', 12, 13, 50, 100, 9090, '2018-12-01 12:12:12'];
      try {
        result = await db.query(insertEventByRepay, eventByRepayData_01);
        assert.equal(result.rowCount, 1, 'insert eventByRepay data failed');
        result = await db.query(insertEventByRepay, eventByRepayData_02);
        assert.equal(result.rowCount, 1, 'insert eventByRepay data failed');
        result = await db.query(insertEventByRepay, eventByRepayData_03);
        assert.equal(result.rowCount, 1, 'insert eventByRepay data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get user repay', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/event/repay')
          .set('Authorization', `Bearer ${token}`)
          .query({ begin: '2018-10-10', end: '2019-10-10', user: '1' });

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.result.events[0].date.should.equal('2018-11-02');
        res.body.result.events[0].repay.should.equal('18180');
        res.body.result.events[1].date.should.equal('2018-11-03');
        res.body.result.events[1].repay.should.equal('9090');
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    // reset test data
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE "eventByRepay" RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(false, true, 'reset test data failed while testing /api/v1/event/repay');
      }
    });
  });
});

// api/v1/event/repayDetail
describe('routes: /api/v1/event/repayDetail', () => {
  describe('access without token', () => {
    it('should get authorization error', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/event/repayDetail')
          .query({ date: '2018-11-02', user: '1' });

        res.status.should.equal(401);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('authorized error');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get other user's repayDetail", () => {
    it('should get forbidden', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/event/repayDetail')
          .set('Authorization', `Bearer ${token}`)
          .query({ date: '2018-11-02', user: '2' });

        res.status.should.equal(403);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's repayDetail", () => {
    before(async () => {
      // insert some test data
      let insertEventByRepay = `
        INSERT INTO "eventByRepay"("pvId", "userId", date, "oldAmount",
          "newAmount", "oldSttw", "newSttw", repay, "updateDt")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9);
      `;
      let eventByRepayData_01 = ['testPvId', 1, '2018-11-02', 12, 13, 50, 100, 50, '2018-12-01 12:12:12'];
      let eventByRepayData_02 = ['testPvId', 1, '2018-11-02', 12, 13, 50, 100, 100, '2018-12-01 12:12:12'];
      let eventByRepayData_03 = ['testPvId2', 1, '2018-11-02', 12, 13, 50, 100, 150, '2018-12-01 12:12:12'];
      let eventByRepayData_04 = ['testPvId2', 1, '2018-11-02', 12, 13, 50, 100, 200, '2018-12-01 12:12:12'];
      try {
        result = await db.query(insertEventByRepay, eventByRepayData_01);
        assert.equal(result.rowCount, 1, 'insert eventByRepay data failed');
        result = await db.query(insertEventByRepay, eventByRepayData_02);
        assert.equal(result.rowCount, 1, 'insert eventByRepay data failed');
        result = await db.query(insertEventByRepay, eventByRepayData_03);
        assert.equal(result.rowCount, 1, 'insert eventByRepay data failed');
        result = await db.query(insertEventByRepay, eventByRepayData_04);
        assert.equal(result.rowCount, 1, 'insert eventByRepay data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get user repayDetail', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/event/repayDetail')
          .set('Authorization', `Bearer ${token}`)
          .query({ date: '2018-11-02', user: '1' });

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.result.events[0].pvId.should.equal('testPvId');
        res.body.result.events[0].repay.should.equal('50');
        res.body.result.events[1].pvId.should.equal('testPvId');
        res.body.result.events[1].repay.should.equal('100');
        res.body.result.events[2].pvId.should.equal('testPvId2');
        res.body.result.events[2].repay.should.equal('150');
        res.body.result.events[3].pvId.should.equal('testPvId2');
        res.body.result.events[3].repay.should.equal('200');
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    // reset test data
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE "eventByRepay" RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(false, true, 'reset test data failed while testing /api/v1/event/repayDetail');
      }
    });
  });
});
