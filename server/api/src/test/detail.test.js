const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
chai.should();
const db = require('../db');
const api = require('../server.js');
const assert = require('assert');
let token;

// get token for testing
describe('get token', () => {
  it('should login successfully and get JWT', async () => {
    try {
      let res = await chai
        .request(api)
        .post('/api/v1/auth/local')
        .set('content-type', 'application/json')
        .send({
          email: 'test@mail.com',
          pwd: '1234'
        });
      res.status.should.equal(200);
      res.body.result.manager.email.should.equal('test@mail.com');
      token = res.body.result.token;
    } catch (error) {
      assert.fail(error.stack);
    }
  });
});

// api/v1/detail/order/:id
describe('routes: /api/v1/detail/order/:id', () => {
  describe('access without token', () => {
    it('should get authorization error', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/detail/order/1');

        res.status.should.equal(401);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('authorized error');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get other user's order", () => {
    it('should get forbidden', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/detail/order/2')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(403);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's order", () => {
    before(async () => {
      // insert some test data
      let insertOrderBuys = `
        INSERT INTO "orderBuys"("pvId", datetime, "userId", price, amount, status)
        VALUES ($1,$2,$3,$4,$5,$6);
      `;
      let orderBuys_Data_01 = ['SNTW1', '2018-11-03 03:03:03', 1, 50, 5, 6];

      let insertOrderSells = `
        INSERT INTO "orderSells"("pvId", datetime, "userId", price, amount, status)
        VALUES ($1,$2,$3,$4,$5,$6);
      `;
      let orderSells_Data_01 = ['SNTW1', '2018-11-03 03:03:03', 1, 50, 5, 7];
      try {
        result = await db.query(insertOrderBuys, orderBuys_Data_01);
        assert.equal(result.rowCount, 1, 'insert orderBuys data failed');

        result = await db.query(insertOrderSells, orderSells_Data_01);
        assert.equal(result.rowCount, 1, 'insert orderSells data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get user order', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/detail/order/1')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.orderList[0].side.should.equal('買');
        res.body.result.orderList[0].id.should.equal('1');
        res.body.result.orderList[0].pvId.should.equal('SNTW1');
        res.body.result.orderList[0].datetime.should.equal('2018-11-03 03:03:03');
        res.body.result.orderList[0].price.should.equal('50');
        res.body.result.orderList[0].filled.should.equal('0');
        res.body.result.orderList[0].amount.should.equal('5');
        res.body.result.orderList[0].status.should.equal(6);
        res.body.result.orderList[1].side.should.equal('賣');
        res.body.result.orderList[1].id.should.equal('1');
        res.body.result.orderList[1].pvId.should.equal('SNTW1');
        res.body.result.orderList[1].datetime.should.equal('2018-11-03 03:03:03');
        res.body.result.orderList[1].price.should.equal('50');
        res.body.result.orderList[1].filled.should.equal('0');
        res.body.result.orderList[1].amount.should.equal('5');
        res.body.result.orderList[1].status.should.equal(7);
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    // reset test data
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE "orderBuys" RESTART IDENTITY;
          TRUNCATE "orderSells" RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(false, true, 'reset test data failed while testing /api/v1/detail/order/:id');
      }
    });
  });
});

// api/v1/detail/trade/:id
describe('routes: /api/v1/detail/trade/:id', () => {
  describe('access without token', () => {
    it('should get authorization error', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/detail/trade/1');

        res.status.should.equal(401);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('authorized error');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get other user's trade", () => {
    it('should get forbidden', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/detail/trade/2')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(403);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's trade", () => {
    before(async () => {
      // insert some test data
      let insertTrades = `
        INSERT INTO trades("orderBuyId", "orderSellId", datetime, price,
          amount, fee, "oldAmountBuyer", "newAmountBuyer", "oldSttwBuyer",
          "newSttwBuyer", "oldAmountSeller", "newAmountSeller", "oldSttwSeller",
          "newSttwSeller", "blockInfo")
        VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15);
      `;

      let trades_Data_01 = [
        1,
        1,
        '2018-12-02 21:21:21',
        50,
        5,
        0.1,
        20,
        25,
        100,
        50,
        30,
        25,
        50,
        100,
        '{"blockInfo": "infoExp"}'
      ];

      let insertOrderBuys = `
        INSERT INTO "orderBuys"("pvId", datetime, "userId", price, amount,
          status)
        VALUES ($1,$2,$3,$4,$5,$6);
      `;
      let orderBuys_Data_01 = ['SNTW1', '2018-11-04 03:03:03', 1, 50, 5, 6];

      let insertOrderSells = `
        INSERT INTO "orderSells"("pvId", datetime, "userId", price, amount,
          status)
        VALUES ($1,$2,$3,$4,$5,$6);
      `;
      let orderSells_Data_01 = ['SNTW1', '2018-11-03 03:03:03', 1, 50, 5, 7];
      try {
        result = await db.query(insertTrades, trades_Data_01);
        assert.equal(result.rowCount, 1, 'insert orderBuys data failed');

        result = await db.query(insertOrderBuys, orderBuys_Data_01);
        assert.equal(result.rowCount, 1, 'insert orderBuys data failed');

        result = await db.query(insertOrderSells, orderSells_Data_01);
        assert.equal(result.rowCount, 1, 'insert orderSells data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get user order', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/detail/trade/1')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.tradeList[0].side.should.equal('買');
        res.body.result.tradeList[0].pvId.should.equal('SNTW1');
        res.body.result.tradeList[0].datetime.should.equal('2018-12-02 21:21:21');
        res.body.result.tradeList[0].price.should.equal('50');
        res.body.result.tradeList[0].amount.should.equal('5');
        res.body.result.tradeList[0].fee.should.equal('0.1');
        res.body.result.tradeList[0].oldAmount.should.equal('20');
        res.body.result.tradeList[0].newAmount.should.equal('25');
        res.body.result.tradeList[0].oldSttw.should.equal('100');
        res.body.result.tradeList[0].newSttw.should.equal('50');
        res.body.result.tradeList[0].blockInfo.blockInfo.should.equal('infoExp');
        res.body.result.tradeList[1].side.should.equal('賣');
        res.body.result.tradeList[1].pvId.should.equal('SNTW1');
        res.body.result.tradeList[1].datetime.should.equal('2018-12-02 21:21:21');
        res.body.result.tradeList[1].price.should.equal('50');
        res.body.result.tradeList[1].amount.should.equal('5');
        res.body.result.tradeList[1].fee.should.equal('0.1');
        res.body.result.tradeList[1].oldAmount.should.equal('30');
        res.body.result.tradeList[1].newAmount.should.equal('25');
        res.body.result.tradeList[1].oldSttw.should.equal('50');
        res.body.result.tradeList[1].newSttw.should.equal('100');
        res.body.result.tradeList[1].blockInfo.blockInfo.should.equal('infoExp');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    // reset test data
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE trades RESTART IDENTITY;
          TRUNCATE "orderBuys" RESTART IDENTITY;
          TRUNCATE "orderSells" RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(false, true, 'reset test data failed while testing /api/v1/detail/trade/:id');
      }
    });
  });
});

// api/v1/detail/cashin/:id
describe('routes: /api/v1/detail/cashin/:id', () => {
  describe('access without token', () => {
    it('should get authorization error', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/detail/cashin/1');

        res.status.should.equal(401);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('authorized error');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get other user's cashin", () => {
    it('should get forbidden', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/detail/cashin/2')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(403);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's cashin", () => {
    before(async () => {
      // insert some test data
      let insertCashinRecords = `
        INSERT INTO "cashinRecords"("userId", date, amount, "oldSttw",
        "newSttw", "updateDt", "blockInfo")
        VALUES($1,$2,$3,$4,$5,$6,$7);
      `;
      let cashinRecords_Data_01 = [
        1,
        '2018-11-15',
        100,
        50,
        80,
        '2018-11-30 12:12:12',
        '{"blockInfo": "infoExp"}'
      ];
      try {
        result = await db.query(insertCashinRecords, cashinRecords_Data_01);
        assert.equal(result.rowCount, 1, 'insert cashinRecords data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get user order', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/detail/cashin/1')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.cashinList[0].date.should.equal('2018-11-15');
        res.body.result.cashinList[0].amount.should.equal(100);
        res.body.result.cashinList[0].oldSttw.should.equal('50');
        res.body.result.cashinList[0].newSttw.should.equal('80');
        res.body.result.cashinList[0].blockInfo.blockInfo.should.equal('infoExp');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    // reset test data
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE "cashinRecords" RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(false, true, 'reset test data failed while testing /api/v1/detail/cashin/:id');
      }
    });
  });
});

// api/v1/detail/cashout/:id
describe('routes: /api/v1/detail/cashout/:id', () => {
  describe('access without token', () => {
    it('should get authorization error', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/detail/cashout/1');

        res.status.should.equal(401);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('authorized error');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get other user's cashout", () => {
    it('should get forbidden', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/detail/cashout/2')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(403);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's cashout", () => {
    before(async () => {
      // insert some test data
      let insertCashoutRecords = `
        INSERT INTO "cashoutRecords"("userId", date, amount, status, "oldSttw",
         "newSttw", "updateDt", "blockInfo")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8);
      `;
      let cashoutRecords_Data_01 = [
        1,
        '2018-11-16',
        50,
        2,
        25,
        40,
        '2018-12-01 13:13:13',
        '{"blockInfo": "infoExp"}'
      ];

      try {
        result = await db.query(insertCashoutRecords, cashoutRecords_Data_01);
        assert.equal(result.rowCount, 1, 'insert cashoutRecords data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get user order', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/detail/cashout/1')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.cashoutList[0].date.should.equal('2018-11-16');
        res.body.result.cashoutList[0].amount.should.equal(50);
        res.body.result.cashoutList[0].status.should.equal(2);
        res.body.result.cashoutList[0].oldSttw.should.equal('25');
        res.body.result.cashoutList[0].newSttw.should.equal('40');
        res.body.result.cashoutList[0].blockInfo.blockInfo.should.equal('infoExp');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    // reset test data
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE "cashoutRecords" RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(false, true, 'reset test data failed while testing /api/v1/detail/cashout/:id');
      }
    });
  });
});

// api/v1/detail/profit/:id
describe('routes: /api/v1/detail/profit/:id', () => {
  describe('access without token', () => {
    it('should get authorization error', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/detail/profit/1');

        res.status.should.equal(401);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('authorized error');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get other user's profit", () => {
    it('should get forbidden', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/detail/profit/2')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(403);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's profit", () => {
    before(async () => {
      // insert some test data
      let insertEventByBill = `
        INSERT INTO "eventByBill"(id, "billId", "userId", date, profit,
          "rentCost", "meterRentCost", "insuranceCost",
          "operateCost", "serviceCost", "loanCost", "loanFeeCost",
          "businessTax", "oldSttw", "newSttw", "updateDt", "blockInfo")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,
          $17);
      `;
      let eventByBill_Data_01 = [
        1,
        1,
        1,
        '2018-11-02',
        101,
        1,
        2,
        3,
        5,
        6,
        5,
        4,
        3,
        2,
        1,
        '2018-12-31 12:12:12',
        '{"blockInfo": "infoExp"}'
      ];

      let insertBills = `
        INSERT INTO bills (id, "pvId", "rangeBegin", "rangeEnd", "totalPower", "incomeWithTax", "meterRentCost",
          "profit", "rentCostFloat", "rentCostFixed", "operateCostFloat", "operateCostFixed", 
          "serviceCostFloat", "insuranceCost", "loanCost", "loanFeeCost", "businessTax",
          "updateDt")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17, $18);
      `;
      let bills_Data_01 = [
        1,
        'P0001',
        '2018-02-14',
        '2018-05-30',
        47236,
        261669,
        873,
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        '2018-06-01 03:05:06'
      ];
      try {
        result = await db.query(insertEventByBill, eventByBill_Data_01);
        assert.equal(result.rowCount, 1, 'insert eventByBill data failed');

        result = await db.query(insertBills, bills_Data_01);
        assert.equal(result.rowCount, 1, 'insert bills data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get user order', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/detail/profit/1')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.profitList[0].date.should.equal('2018-11-02');
        res.body.result.profitList[0].pvId.should.equal('P0001');
        res.body.result.profitList[0].rangeBegin.should.equal('2018-02-14');
        res.body.result.profitList[0].rangeEnd.should.equal('2018-05-30');
        res.body.result.profitList[0].profit.should.equal('101');
        res.body.result.profitList[0].oldSttw.should.equal('2');
        res.body.result.profitList[0].newSttw.should.equal('1');
        res.body.result.profitList[0].blockInfo.blockInfo.should.equal('infoExp');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    // reset test data
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE "eventByBill" RESTART IDENTITY;
          TRUNCATE bills;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(false, true, 'reset test data failed while testing /api/v1/detail/profit/:id');
      }
    });
  });
});

// api/v1/detail/repay/:id
describe('routes: /api/v1/detail/repay/:id', () => {
  describe('access without token', () => {
    it('should get authorization error', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/detail/repay/1');

        res.status.should.equal(401);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('authorized error');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get other user's repay", () => {
    it('should get forbidden', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/detail/repay/2')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(403);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's repay", () => {
    before(async () => {
      // insert some test data
      let insertEventByRepay = `
        INSERT INTO "eventByRepay"("pvId", "userId", date, "oldAmount",
          "newAmount", "oldSttw", "newSttw", repay, "updateDt", "blockInfo")
        VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10);
      `;
      let eventByRepay_Data_01 = [
        'testPvId',
        1,
        '2018-11-02',
        12,
        13,
        50,
        100,
        9090,
        '2018-12-01 12:12:12',
        '{"blockInfo": "infoExp"}'
      ];
      try {
        result = await db.query(insertEventByRepay, eventByRepay_Data_01);
        assert.equal(result.rowCount, 1, 'insert eventByRepay data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get user order', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/detail/repay/1')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.msg.should.equal('');
        res.body.result.repayList[0].date.should.equal('2018-11-02');
        res.body.result.repayList[0].pvId.should.equal('testPvId');
        res.body.result.repayList[0].oldAmount.should.equal('12');
        res.body.result.repayList[0].newAmount.should.equal('13');
        res.body.result.repayList[0].oldSttw.should.equal('50');
        res.body.result.repayList[0].newSttw.should.equal('100');
        res.body.result.repayList[0].repay.should.equal('9090');
        res.body.result.repayList[0].blockInfo.blockInfo.should.equal('infoExp');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    // reset test data
    after(async () => {
      try {
        //reset user account
        const resetTestData = `
          TRUNCATE "eventByRepay" RESTART IDENTITY;
        `;
        await db.query(resetTestData);
      } catch (error) {
        assert.fail(false, true, 'reset test data failed while testing /api/v1/detail/repay/:id');
      }
    });
  });
});
