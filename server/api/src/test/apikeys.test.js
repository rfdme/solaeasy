const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
chai.should();
const db = require('../db');
const api = require('../server.js');
const assert = require('assert');
let token;

// get token for testing
describe('get token', () => {
  it('should login successfully and get JWT', async () => {
    try {
      let res = await chai
        .request(api)
        .post('/api/v1/auth/local')
        .set('content-type', 'application/json')
        .send({
          email: 'test@mail.com',
          pwd: '1234'
        });
      res.status.should.equal(200);
      res.body.result.manager.email.should.equal('test@mail.com');
      token = res.body.result.token;
    } catch (error) {
      assert.fail(error.stack);
    }
  });
});

// api/v1/apikeys/:id
describe('routes: /api/v1/apikeys/:id', () => {
  describe('access without token', () => {
    it('should get authorization error', async () => {
      try {
        let res = await chai.request(api).get('/api/v1/apikeys/1');

        res.status.should.equal(401);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('authorized error');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get other user's apikeys", () => {
    it('should get forbidden', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/apikeys/2')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(403);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's apikeys", () => {
    before(async () => {
      // insert some test data
      let insertApikey = `INSERT INTO apikeys(id, key, secret, "readInfo", trade, withdraw, "userId") VALUES($1,$2,$3,$4,$5,$6,$7);`;
      let apikeyData_01 = [1, 'testKeytestKeytestKeytestKey1234', 'testSecret', true, true, true, 1];

      try {
        result = await db.query(insertApikey, apikeyData_01);
        assert.equal(result.rowCount, 1, 'insert apikey data failed');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    it('should get user apikeys', async () => {
      try {
        let res = await chai
          .request(api)
          .get('/api/v1/apikeys/1')
          .set('Authorization', `Bearer ${token}`);

        res.status.should.equal(200);
        res.body.status.should.equal(true);
        res.body.result.apikeys[0].id.should.equal(1);
        res.body.result.apikeys[0].key.should.equal('testKeytestKeytestKeytestKey1234');
        res.body.msg.should.equal('');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
    // reset test data
    after(async () => {
      try {
        //reset user account
        const resetApikey = `TRUNCATE apikeys RESTART IDENTITY;`;
        result = await db.query(resetApikey);
      } catch (error) {
        assert.fail(false, true, 'reset test data failed while testing /api/v1/apikeys/:id');
      }
    });
  });
});

// api/v1/apikeys
describe('routes: /api/v1/apikeys', () => {
  describe('access without token', () => {
    it('should get authorization error', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/apikeys')
          .set('content-type', 'application/json')
          .send({
            authToken: 123456,
            options: {
              readInfo: true,
              trade: false,
              withdraw: false
            }
          });

        res.status.should.equal(401);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('authorized error');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's apikeys with undefined authToken", () => {
    it('should get invalid parameters', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/apikeys')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({
            options: {
              readInfo: true,
              trade: false,
              withdraw: false
            }
          });

        res.status.should.equal(200);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('invalid parameters, missing authToken');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's apikeys with empty authToken", () => {
    it('should get invalid parameters', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/apikeys')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({
            authToken: '',
            options: {
              readInfo: true,
              trade: false,
              withdraw: false
            }
          });

        res.status.should.equal(200);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('invalid parameters, missing authToken');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's apikeys with undefined options", () => {
    it('should get invalid parameters', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/apikeys')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({
            authToken: 123456
          });

        res.status.should.equal(200);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('invalid parameters, missing options');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's apikeys with undefined options.readInfo", () => {
    it('should get invalid parameters', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/apikeys')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({
            authToken: 123456,
            options: {
              trade: false,
              withdraw: false
            }
          });

        res.status.should.equal(200);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('invalid parameters, missing options.readInfo');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's apikeys with undefined options.trade", () => {
    it('should get invalid parameters', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/apikeys')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({
            authToken: 123456,
            options: {
              readInfo: true,
              withdraw: false
            }
          });

        res.status.should.equal(200);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('invalid parameters, missing options.trade');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's apikeys with undefined options.withdraw", () => {
    it('should get invalid parameters', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/apikeys')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({
            authToken: 123456,
            options: {
              readInfo: true,
              trade: false
            }
          });

        res.status.should.equal(200);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('invalid parameters, missing options.withdraw');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  describe("get user's apikeys with google2fa inactive", () => {
    it('should get activate message', async () => {
      try {
        let res = await chai
          .request(api)
          .post('/api/v1/apikeys')
          .set('content-type', 'application/json')
          .set('Authorization', `Bearer ${token}`)
          .send({
            authToken: 123456,
            options: {
              readInfo: true,
              trade: false,
              withdraw: false
            }
          });

        res.status.should.equal(200);
        res.body.status.should.equal(false);
        res.body.msg.should.equal('請先啟用Google Authenticator兩步驗證功能.');
      } catch (error) {
        assert.fail(error.stack);
      }
    });
  });
  // cannot get google2fa token in auto testing,
  // please test this api manually
});
