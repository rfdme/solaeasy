const express = require('express');
const router = express.Router();
const moment = require('moment');
const sprintf = require('sprintf-js').sprintf;
const axios = require('axios');
const BN = require('bignumber.js').clone({ DECIMAL_PLACES: 16 });

const db = require('../db');

const url = require('../config/urls');

const path = require('path');
const log4js = require('log4js');

// 以filename設定logger分類
const logger = log4js.getLogger(path.basename(__filename));
logger.level = 'debug';

//測試:結算資產
router.get('/test/', async (req, res) => {
  let day = moment('2017-02-02', 'YYYY-MM-DD');
  try {
    while (day.isBefore(moment())) {
      let response = await axios.get(
        `${url.schedulerURL}/api/v1/clearance/asset?date=${day.format('YYYY-MM-DD')}`
      );
      day.add(1, 'days');
      logger.info(day.format('YYYY-MM-DD'));
    }
    res.send({ msg: 'ok' });
    logger.info('ok');
  } catch (e) {
    logger.error(e);
    res.status(500).send();
  }
});

router.get('/asset/', (req, res) => {
  if (req.query.date === undefined) {
    res.send({
      error: 'invalid params. e.g. api/v1/clearance/asset?date=2018-01-1'
    });
    logger.info('invalid params');
    return;
  }

  const date = req.query.date;

  (async () => {
    try {
      await db.query('BEGIN');

      //清除結算記錄
      await db.query('DELETE FROM "dailyClearance" WHERE date=$1', [date]);

      //產生asset結算紀錄
      await db.query(
        `INSERT INTO "dailyClearance" ("userId", date, item, amount, "presentValue", "updateDt")
         SELECT A."userId", $1, A."pvId", A.amount,  A.amount * COALESCE(C.price, B."initialPrice"), $2 FROM "userAssets" A
         LEFT JOIN pvs B ON B.id=A."pvId"
         LEFT JOIN "marketInfo" C ON C."pvId" = B.id
         WHERE A.amount > 0`,
        [date, moment()]
      );

      //產生sttw結算紀錄
      await db.query(
        `INSERT INTO "dailyClearance" ("userId", date, item, amount, "presentValue", "updateDt")
         SELECT id, $1, 'STTW', sttw, sttw, $2 FROM users`,
        [date, moment()]
      );

      //記錄區塊交易
      const txType = 'checkPointSync';
      const txId = 0;
      const data = {};

      const insertBcTxResult = await db.query(
        'INSERT INTO "bcTransation" ("txType", "txId", data) VALUES ($1,$2,$3)',
        [txType, txId, data]
      );

      await db.query('COMMIT');

      return { msg: '結算資產成功' };
    } catch (e) {
      await db.query('ROLLBACK');
      throw e;
    }
  })()
    .then(result => {
      res.send(result);
      logger.info(`result = ${JSON.stringify(result)}`);
    })
    .catch(e => {
      logger.error(e.stack);
      res.send({ msg: 'fail' });
      logger.info('fail');
    });
});

module.exports = router;
