const express = require('express');
const router = express.Router();
const db = require('../db');
const path = require('path');
const { canAccessUser } = require('../modules/accessController');
const response = require('../modules/rsps');
const log4js = require('../modules/logger');
const logger = log4js.getLogger();
router.use(function(req, res, next) {
  logger.addContext('reqId', req.reqId);
  next();
});

router.get('/pv', async (req, res) => {
  let rsps = new response();
  logger.info(`req.query = ${JSON.stringify(req.query)}`);
  try {
    const query = {
      text: `SELECT "pvId", date, name, content FROM "eventByPV" WHERE date >= $1 AND date <= $2`,
      values: [req.query.begin, req.query.end]
    };
    const { rows } = await db.query(query);
    rsps.result = { events: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/simulate', async (req, res) => {
  let rsps = new response();
  logger.info(`req.query = ${JSON.stringify(req.query)}`);
  try {
    if (!canAccessUser(req.user, parseInt(req.query.user, 10))) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access user id= ${
          req.query.user
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    const query = {
      text: `SELECT date, SUM(profit) as profit FROM "eventBySimulate" WHERE "userId"=$1 AND date >= $2 AND date <= $3
        GROUP BY date`,
      values: [req.query.user, req.query.begin, req.query.end]
    };
    const { rows } = await db.query(query);
    rsps.result = { events: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/simulateDetail', async (req, res) => {
  let rsps = new response();
  logger.info(`req.query = ${JSON.stringify(req.query)}`);
  try {
    if (!canAccessUser(req.user, parseInt(req.query.user, 10))) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access user id= ${
          req.query.user
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    const query = {
      text: `SELECT "pvId", profit FROM "eventBySimulate" WHERE "userId"=$1 AND date=$2 ORDER BY "pvId"`,
      values: [req.query.user, req.query.date]
    };
    const { rows } = await db.query(query);
    rsps.result = { events: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/bill', async (req, res) => {
  let rsps = new response();
  logger.info(`req.query = ${JSON.stringify(req.query)}`);
  try {
    if (!canAccessUser(req.user, parseInt(req.query.user, 10))) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access user id= ${
          req.query.user
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    const query = {
      text: `SELECT date, SUM(profit) as profit FROM "eventByBill" WHERE "userId"=$1 AND date >= $2 AND date <= $3 GROUP BY date`,
      values: [req.query.user, req.query.begin, req.query.end]
    };
    const { rows } = await db.query(query);
    rsps.result = { events: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/billDetail', async (req, res) => {
  let rsps = new response();
  logger.info(`req.query = ${JSON.stringify(req.query)}`);
  try {
    if (!canAccessUser(req.user, parseInt(req.query.user, 10))) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access user id= ${
          req.query.user
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    const query = {
      text: `SELECT B."pvId", B."rangeBegin", B."rangeEnd", A.profit FROM "eventByBill" A
       LEFT JOIN bills B ON B.id = A."billId"
       WHERE "userId"=$1 AND date=$2 ORDER BY "pvId"`,
      values: [req.query.user, req.query.date]
    };
    const { rows } = await db.query(query);
    rsps.result = { events: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/repay', async (req, res) => {
  let rsps = new response();
  logger.info(`req.query = ${JSON.stringify(req.query)}`);
  try {
    if (!canAccessUser(req.user, parseInt(req.query.user, 10))) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access user id= ${
          req.query.user
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    const query = {
      text: `SELECT date, SUM(repay) as repay FROM "eventByRepay" WHERE "userId"=$1 AND date >= $2 AND date <= $3 GROUP BY date`,
      values: [req.query.user, req.query.begin, req.query.end]
    };
    const { rows } = await db.query(query);
    rsps.result = { events: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/repayDetail', async (req, res) => {
  let rsps = new response();
  logger.info(`req.query = ${JSON.stringify(req.query)}`);
  try {
    if (!canAccessUser(req.user, parseInt(req.query.user, 10))) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access user id= ${
          req.query.user
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    const query = {
      text: `SELECT "pvId", repay FROM "eventByRepay" WHERE "userId"=$1 AND date=$2 ORDER BY "pvId"`,
      values: [req.query.user, req.query.date]
    };
    const { rows } = await db.query(query);
    rsps.result = { events: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

module.exports = router;
