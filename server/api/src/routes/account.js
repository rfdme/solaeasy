const express = require('express');
const router = express.Router();
const db = require('../db');
const moment = require('moment');
const speakeasy = require('speakeasy');
const BN = require('bignumber.js').clone({ DECIMAL_PLACES: 16 });
const _ = require('lodash');
const { findManagerById, verifyPassword, modifyPassword } = require('../sqliteDb');
const { canAccessUser } = require('../modules/accessController');
const response = require('../modules/rsps');
const log4js = require('../modules/logger');
const logger = log4js.getLogger();
router.use(function(req, res, next) {
  logger.addContext('reqId', req.reqId);
  next();
});

router.get('/:id', async (req, res) => {
  let rsps = new response();
  logger.info('req params user id: ' + req.params.id);
  try {
    if (!canAccessUser(req.user, parseInt(req.params.id, 10))) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access user id= ${
          req.params.id
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    const query = {
      text: `SELECT name, telephone, address, "cashoutAccount" FROM accounts WHERE "userId"=$1`,
      values: [req.params.id]
    };
    const { rows } = await db.query(query);
    rsps.result = { account: rows[0] };
    rsps.status = true;
    logger.debug(`data = ${JSON.stringify(rows[0])}`);
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/totalCashin/:id', async (req, res) => {
  let rsps = new response();
  logger.info('req params user id: ' + req.params.id);
  try {
    if (!canAccessUser(req.user, parseInt(req.params.id, 10))) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access user id= ${
          req.params.id
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    const query = {
      text: `SELECT COALESCE(SUM(amount), 0) as cashin FROM "cashinRecords" WHERE "userId"=$1`,
      values: [req.params.id]
    };
    const { rows } = await db.query(query);
    rsps.result = { totalCashin: rows[0].cashin };
    logger.debug(`data = ${JSON.stringify(rows[0])}`);
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/totalCashout/:id', async (req, res) => {
  let rsps = new response();
  logger.info('req params user id: ' + req.params.id);
  try {
    if (!canAccessUser(req.user, parseInt(req.params.id, 10))) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access user id= ${
          req.params.id
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    const query = {
      text: `SELECT COALESCE(SUM(amount), 0) as cashout FROM "cashoutRecords" WHERE "userId"=$1 AND status=1`,
      values: [req.params.id]
    };
    const { rows } = await db.query(query);
    rsps.result = { totalCashout: rows[0].cashout };
    logger.debug(`data = ${JSON.stringify(rows[0])}`);
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.post('/modifyPwd', async (req, res) => {
  let rsps = new response();
  logger.info('req manager id: ' + req.body.managerId);
  const { managerId, oldPwd, newPwd } = req.body;
  if (managerId === undefined || oldPwd === undefined || newPwd === undefined) {
    rsps.msg = 'invalid parameters';
    res.send(rsps.getResult());
    logger.info(`invalid parameters in managerId||oldPwd||newPwd`);
    return;
  }

  try {
    if (req.user.id !== parseInt(managerId, 10)) {
      logger.warn(
        `403 Forbidden. current manager id= ${
          req.user.id
        }, trying to access manager id= ${managerId} 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }

    const managers = await findManagerById(managerId);
    if (_.isEmpty(managers)) {
      rsps.msg = '找不到該名經理人';
      res.send(rsps.getResult());
      logger.info(`no this manager`);
      return;
    }

    const manager = managers[0];
    const pass = await verifyPassword(manager.email, oldPwd);
    if (!pass) {
      rsps.msg = '舊密碼驗證失敗';
      res.send(rsps.getResult());
      logger.info(`old password is not match`);
      return;
    }

    if (manager.google2FAStatus) {
      const tokenValidates = speakeasy.totp.verify({
        secret: manager.google2FASecretBase32,
        encodeing: 'base32',
        token: req.body.authToken
      });
      if (!tokenValidates) {
        rsps.msg = '動態密碼錯誤';
        res.send(rsps.getResult());
        logger.info(`google 2FA verification failed`);
        return;
      }
    }

    const reseted = await modifyPassword(manager.email, newPwd);
    if (!reseted) {
      rsps.msg = '修改密碼失敗';
      res.send(rsps.getResult());
      logger.info(`modify password failed`);
      return;
    }

    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.post('/cashout', async (req, res) => {
  let rsps = new response();
  const { managerId, userId, amount, pwd } = req.body;
  logger.info(`managerId=${managerId}, userId=${userId}, amount=${amount}`);
  if (managerId === undefined || userId === undefined || amount === undefined || pwd === undefined) {
    rsps.msg = 'invalid parameters';
    res.send(rsps.getResult());
    logger.info(`invalid parameters in managerId||userId||amount||pwd`);
    return;
  }

  const cashoutAmount = parseInt(amount);
  if (!(cashoutAmount > 0)) {
    rsps.msg = 'invalid cashout amount';
    res.send(rsps.getResult());
    logger.info(`invalid cashout amount: ${cashoutAmount}`);
    return;
  }

  try {
    if (req.user.id !== parseInt(managerId, 10) || !canAccessUser(req.user, parseInt(userId, 10))) {
      logger.warn(
        `403 Forbidden. current manager id= ${
          req.user.id
        }, trying to access user id= ${userId} 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }

    await db.query('BEGIN');

    const managers = await findManagerById(managerId);
    if (_.isEmpty(managers)) {
      await db.query('ROLLBACK');
      rsps.msg = '找不到該名經理人';
      res.send(rsps.getResult());
      logger.info(`no this user`);
      return;
    }
    const manager = managers[0];

    const pass = await verifyPassword(manager.email, pwd);
    if (!pass) {
      await db.query('ROLLBACK');
      logger.info(`password is not match`);
      rsps.msg = '驗證密碼失敗';
      res.send(rsps.getResult());
      return;
    }

    if (manager.google2FAStatus) {
      const tokenValidates = speakeasy.totp.verify({
        secret: manager.google2FASecretBase32,
        encodeing: 'base32',
        token: req.body.authToken
      });
      if (!tokenValidates) {
        await db.query('ROLLBACK');
        rsps.msg = '動態密碼錯誤';
        res.send(rsps.getResult());
        logger.info(`google 2FA verification failed`);
        return;
      }
    }

    const checkRemainSttw = {
      text: 'SELECT sttw - "lockSttw" as remained FROM users WHERE id=$1',
      values: [userId]
    };
    const checkRemainSttwResult = await db.query(checkRemainSttw);
    if (checkRemainSttwResult.rows[0].remained.lt(cashoutAmount)) {
      await db.query('ROLLBACK');
      rsps.msg = '餘額不足';
      res.send(rsps.getResult());
      logger.info(`not enough balance`);
      return;
    }

    const lockSttw = {
      text: 'UPDATE users SET "lockSttw"="lockSttw"+$1 WHERE id=$2',
      values: [cashoutAmount, userId]
    };
    await db.query(lockSttw);

    const newCashoutRecord = {
      text:
        'INSERT INTO "cashoutRecords" ("userId", date, amount, status, "updateDt") VALUES($1,$2,$3,$4,$5)',
      values: [userId, moment().format('YYYY-MM-DD'), cashoutAmount, 0, moment()]
    };
    await db.query(newCashoutRecord);

    await db.query('COMMIT');
    rsps.status = true;
    rsps.result = { amount: cashoutAmount };
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    await db.query('ROLLBACK');
    rsps.msg = 'something wrong';
    res.send(rsps.getResult());
  }
});

router.post('/cashin', async (req, res) => {
  let rsps = new response();
  const { userId, amount } = req.body;
  logger.info(`userId=${userId}, amount=${amount}`);
  if (userId === undefined || amount === undefined) {
    logger.info(`invalid parameters in userId||amount`);
    rsps.msg = 'invalid parameters';
    res.send(rsps.getResult());
    return;
  }

  const cashinAmount = parseInt(amount);
  if (!(cashinAmount > 0)) {
    logger.info(`cashin amount is <= 0, invalid`);
    rsps.msg = 'invalid cashin amount';
    res.send(rsps.getResult());
    return;
  }

  try {
    await db.query('BEGIN');

    const oldSttwResult = await db.query(`SELECT sttw FROM users WHERE id=$1`, [userId]);
    const oldSttw = oldSttwResult.rows[0].sttw;

    const addSttwResult = await db.query('UPDATE users SET "sttw"="sttw"+$1 WHERE id=$2 RETURNING sttw', [
      cashinAmount,
      userId
    ]);
    const newSttw = addSttwResult.rows[0].sttw;

    const cashinResult = await db.query(
      'INSERT INTO "cashinRecords" ("userId", date, amount, "oldSttw", "newSttw", "updateDt") VALUES($1,$2,$3,$4,$5,$6) RETURNING id',
      [userId, moment().format('YYYY-MM-DD'), cashinAmount, oldSttw.toString(), newSttw.toString(), moment()]
    );

    if (cashinResult.rowCount !== 1) {
      await db.query('ROLLBACK');
      logger.warn('cashin failed: cannot insert cashin record');
      rsps.msg = 'cashin failed';
    }

    //記錄區塊交易
    const txType = 'depositSync';
    const txId = cashinResult.rows[0].id;
    const data = {
      id: cashinResult.rows[0].id,
      userId: userId,
      date: moment().format('YYYY-MM-DD HH:mm:ss'),
      amount: BN(cashinAmount)
        .times(Math.pow(10, 16))
        .integerValue()
        .toString(10)
    };

    const insertBcTxResult = await db.query(
      'INSERT INTO "bcTransation" ("txType", "txId", data) VALUES ($1,$2,$3)',
      [txType, txId, data]
    );

    if (insertBcTxResult.rowCount != 1) {
      await db.query('ROLLBACK');
      logger.warn('cashin failed: cannot insert tx record');
      rsps.msg = 'cashin failed';
    }

    await db.query('COMMIT');
    rsps.result = { amount: cashinAmount };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    await db.query('ROLLBACK');
    rsps.msg = 'something wrong';
    res.send(rsps.getResult());
  }
});

module.exports = router;
