var express = require('express');
var router = express.Router();
const axios = require('axios');
const moment = require('moment');
const { findAllPv, generateEventBySimulate } = require('../sqliteDb');
const path = require('path');
const log4js = require('log4js');

// 以filename設定logger分類
const logger = log4js.getLogger(path.basename(__filename));
logger.level = 'debug';

const url = require('../config/urls');

//測試:產生模擬收益
router.get('/test/', async (req, res) => {
  let day = moment('2017-02-02', 'YYYY-MM-DD');
  try {
    while (day.isBefore(moment())) {
      let response = await axios.get(
        `${url.schedulerURL}/api/v1/simulateDaily?date=${day.format('YYYY-MM-DD')}`
      );
      day.add(1, 'days');
      logger.info(day.format('YYYY-MM-DD'));
    }
    res.send({ msg: 'ok' });
  } catch (e) {
    logger.error(e);
  }
});

router.get('/', async function(req, res, next) {
  if (req.query.date === undefined)
    return res.send({
      error: 'invalid params. e.g. api/v1/simulateDaily?date=2018-01-1'
    });

  let date = req.query.date;

  let rows = await findAllPv();

  let result = {};
  for (let i = 0; i < rows.length; i++) {
    let response = await axios.get(`${url.schedulerURL}/api/v1/profit?token=${rows[i].id}&date=${date}`);
    result[rows[i].id] = response.data;

    if (response.data.hasData === false) {
      continue;
    }
    //取得所有電廠的當日發電收益
    //取得所有人的電廠資產比例
    //依比例產生eventSim
    let pv = rows[i];
    await generateEventBySimulate(pv.id, pv.ownedCapital / 10.0, date, response.data.result);
  }

  return res.send(result);
});

module.exports = router;
