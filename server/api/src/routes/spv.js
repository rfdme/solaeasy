const express = require('express');
const router = express.Router();
const db = require('../modules/db');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const path = require('path');
const log4js = require('log4js');

// 以filename設定logger分類
const logger = log4js.getLogger(path.basename(__filename));
logger.level = 'debug';

const dbHandler = new db();

const authorize = async (req, res, next) => {
  const authorization = _.get(req.headers, 'authorization');
  if (authorization === undefined) {
    res.status(401).send('UnauthorizedError');
    return;
  }

  if (authorization.indexOf('Bearer ') !== 0) {
    res.status(401).send('UnauthorizedError');
    return;
  }
  const token = authorization.replace('Bearer ', '');

  if (req.query.apiKey === undefined) {
    res.status(401).send('UnauthorizedError');
    return;
  }

  const { secret, userId } = await dbHandler.querySecretByApiKey(
    req.query.apiKey
  );

  // const tokenTemp = jwt.sign({
  //   resource: '/api/v1/spv/assets?apiKey='+req.query.apiKey,
  //   key: 'eaa54363c00ec7b5137643ad734df327'
  // }, secret);
  // console.log(`token temp: ${tokenTemp}`);

  // verify a token symmetric
  jwt.verify(token, secret, function(err, decoded) {
    if (err) {
      res.status(401).send('UnauthorizedError');
      return;
    }
    req.userInfo = decoded;
    req.userInfo.userId = userId;

    // check recource and originalUrl is matched
    if (req.originalUrl !== req.userInfo.resource) {
      logger.info(`req.originalUrl=${req.originalUrl}`);
      logger.info(`req.userInfo.resource=${req.userInfo.resource}`);
      res.status(401).send('UnauthorizedError');
      return;
    }

    next();
  });
};

router.get('/assets', authorize, async (req, res) => {
  try {
    const assets = await dbHandler.queryAssetsByUserId(req.userInfo.userId);

    const sttw = await dbHandler.querySttwByUserId(req.userInfo.userId);

    const data = {
      assets: {
        pvList: assets,
        sttw: sttw
      }
    };
    // logger.info(`data = ${JSON.stringify(data)}`);
    res.send(data);
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send();
  }
});

router.get('/assets/valueHistory', authorize, async (req, res) => {
  try {
    const rows = await dbHandler.queryValueHistoryByUserId(req.userInfo.userId);

    const data = {
      valueHistory: rows
    };
    // logger.info(`data = ${JSON.stringify(data)}`);
    res.send(data);
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send();
  }
});

router.get('/assets/unrealizedProfit', authorize, async (req, res) => {
  try {
    const rows = await dbHandler.queryUnrealizedProfitByUserId(
      req.userInfo.userId,
      req.query.pvs
    );
    const data = {
      unrealizedProfit: rows
    };
    // logger.info(`data = ${JSON.stringify(data)}`);
    res.send(data);
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send();
  }
});

router.get('/assets/realizedProfit', authorize, async (req, res) => {
  try {
    const rows = await dbHandler.queryRealizedProfitByUserId(
      req.userInfo.userId,
      req.query.pvs
    );
    const data = {
      realizedProfit: rows
    };
    // logger.info(`data = ${JSON.stringify(data)}`);
    res.send(data);
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send();
  }
});

// event
// /summary
router.get('/event/summary', authorize, async (req, res) => {
  try {
    const rows = await dbHandler.queryEventSummary(
      req.userInfo.userId,
      req.query.begin,
      req.query.end
    );
    const data = {
      eventSummary: rows
    };
    res.send(data);
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send();
  }
});

// /pv
router.get('/event/pv', authorize, async (req, res) => {
  try {
    const rows = await dbHandler.queryEventPv(req.query.begin, req.query.end);
    const data = {
      events: rows
    };
    res.send(data);
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send();
  }
});

// /simulate
router.get('/event/simulate', authorize, async (req, res) => {
  try {
    const rows = await dbHandler.queryEventSimulate(
      req.userInfo.userId,
      req.query.begin,
      req.query.end
    );
    const data = {
      events: rows
    };
    res.send(data);
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send();
  }
});

// /simulateDetail
router.get('/event/simulateDetail', authorize, async (req, res) => {
  try {
    const rows = await dbHandler.queryEventSimulateDetail(
      req.userInfo.userId,
      req.query.date
    );
    const data = {
      events: rows
    };
    res.send(data);
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send();
  }
});

// /bill
router.get('/event/bill', authorize, async (req, res) => {
  try {
    const rows = await dbHandler.queryEventBill(
      req.userInfo.userId,
      req.query.begin,
      req.query.end
    );
    const data = {
      events: rows
    };
    res.send(data);
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send();
  }
});

// /billDetail
router.get('/event/billDetail', authorize, async (req, res) => {
  try {
    const rows = await dbHandler.queryEventSimulateDetail(
      req.userInfo.userId,
      req.query.date
    );
    const data = {
      events: rows
    };
    res.send(data);
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send();
  }
});

// /repay
router.get('/event/repay', authorize, async (req, res) => {
  try {
    const rows = await dbHandler.queryEventRepay(
      req.userInfo.userId,
      req.query.begin,
      req.query.end
    );
    const data = {
      events: rows
    };
    res.send(data);
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send();
  }
});

// /repayDetail
router.get('/event/repayDetail', authorize, async (req, res) => {
  try {
    const rows = await dbHandler.queryEventRepayDetail(
      req.userInfo.userId,
      req.query.date
    );
    const data = {
      events: rows
    };
    res.send(data);
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send();
  }
});

// userPv
router.get('/userPv', authorize, async (req, res) => {
  try {
    const rows = await dbHandler.queryUserPv(req.userInfo.userId);
    const data = {
      userPv: rows
    };
    res.send(data);
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send();
  }
});

// /achievementRate
router.get('/userPv/achievementRate', authorize, async (req, res) => {
  try {
    const rows = await dbHandler.queryUserPvAchievementRate(
      req.userInfo.userId
    );
    const data = {
      achievementRate: rows
    };
    res.send(data);
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send();
  }
});

// pv
// data
router.get('/pv/data', authorize, async (req, res) => {
  try {
    const rows = await dbHandler.queryPvData(req.query.pvId);
    const data = {
      pvData: rows
    };
    res.send(data);
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send();
  }
});

// /values
router.get('/pv/values', authorize, async (req, res) => {
  try {
    const rows = await dbHandler.queryPvValues(req.query.pvId);
    const data = {
      pvValues: rows
    };
    res.send(data);
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send();
  }
});

// /img
router.get('/pv/img', authorize, async (req, res) => {
  try {
    const rows = await dbHandler.queryPvImg(req.query.pvId);
    const data = {
      pvImg: rows
    };
    res.send(data);
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send();
  }
});

// /coord
router.get('/pv/coord', authorize, async (req, res) => {
  try {
    const rows = await dbHandler.queryPvCoord(req.query.pvId);
    const data = {
      pvCoord: rows
    };
    res.send(data);
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send();
  }
});

// /stage
router.get('/pv/stage', authorize, async (req, res) => {
  try {
    const rows = await dbHandler.queryPvStage(req.query.pvId);
    const data = {
      pvStage: rows
    };
    res.send(data);
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send();
  }
});

// userBill
router.get('/userBill', authorize, async (req, res) => {
  try {
    const rows = await dbHandler.queryUserBill(req.query.pvId);
    const data = {
      userBill: rows
    };
    res.send(data);
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send();
  }
});

module.exports = router;
