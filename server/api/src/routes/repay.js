const express = require('express');
const router = express.Router();
const moment = require('moment');
const sprintf = require('sprintf-js').sprintf;
const db = require('../db');
const path = require('path');
const log4js = require('log4js');
const axios = require('axios');
const BN = require('bignumber.js').clone({ DECIMAL_PLACES: 16 });

const url = require('../config/urls');

// 以filename設定logger分類
const logger = log4js.getLogger(path.basename(__filename));
logger.level = 'debug';

router.get('/test/', async (req, res) => {
  if (req.query.begin === undefined || req.query.end === undefined)
    return res.send({
      error: 'invalid params. e.g. v1/repay/test?begin=2016-10-12&end=2036-10-13'
    });

  let day = moment(req.query.begin, 'YYYY-MM-DD');
  try {
    while (day.isBefore(req.query.end)) {
      let response = await axios.get(`${url.schedulerURL}/api/v1/repay?date=${day.format('YYYY-MM-DD')}`);
      day.add(1, 'days');
      logger.info(day.format('YYYY-MM-DD'));
    }
    res.send({ msg: 'ok' });
  } catch (e) {
    logger.error(e);
  }
});

router.get('/', (req, res) => {
  if (req.query.date === undefined)
    return res.send({
      error: 'invalid params. e.g. v1/repay?date=2018-01-1'
    });

  const date = req.query.date;

  (async () => {
    try {
      await db.query('BEGIN');

      //初始發行量
      const allSntwResult = await db.query(
        'SELECT id, "ownedCapital"/10.0 as origin, "validityDateBegin", "validityDuration" FROM pvs'
      );

      //計算還本後的結果
      var result = {
        events: []
      };
      for (let i = 0; i < allSntwResult.rows.length; i++) {
        const pvId = allSntwResult.rows[i].id;
        const origin = allSntwResult.rows[i].origin;
        const dateBegin = allSntwResult.rows[i].validityDateBegin;
        const dateEnd = moment(allSntwResult.rows[i].validityDateBegin).add(
          allSntwResult.rows[i].validityDuration,
          'years'
        );

        const repayDay = moment(dateEnd, 'YYYY-MM-DD');
        repayDay.subtract(1, 'days');

        //最後五年還本, 金流問題還未解決(暫時關閉)
        const NeedToDiscuss = true;
        if (NeedToDiscuss) {
          break;
        }
        let isRepayDay = false;
        let lastYearCount = 5;
        while (lastYearCount--) {
          if (repayDay.isSame(date, 'day')) {
            isRepayDay = true;
            break;
          }
          repayDay.subtract(1, 'years');
        }

        if (!isRepayDay) {
          continue;
        }

        //現有流通量
        const allAssetResult = await db.query(
          `SELECT "userId", amount FROM "userAssets" WHERE "pvId"=$1 AND amount > 0`,
          [pvId]
        );

        //還原比例
        const now = allAssetResult.rows.reduce((a, b) => BN(a).plus(b.amount), 0);
        const originScale = BN(origin).div(now);

        //當日應縮小比例
        const totalDays = moment(dateEnd, 'YYYY-MM-DD').diff(moment(dateBegin, 'YYYY-MM-DD'), 'days');
        const remainDays = moment(dateEnd, 'YYYY-MM-DD').diff(moment(date, 'YYYY-MM-DD'), 'days');

        if (totalDays < 0 || remainDays < 0) {
          continue;
        }

        for (let j = 0; j < allAssetResult.rows.length; j++) {
          const userId = allAssetResult.rows[j].userId;

          let x = BN(allAssetResult.rows[j].amount, 10);
          let y = x
            .times(originScale.toString())
            .times(remainDays)
            .div(totalDays);
          let repay = x.minus(y).times(10);

          result.events.push(sprintf(`%s_user%s %s => %s, repay %s sttw`, pvId, userId, x, y, repay));

          //更新1.userAssets & 2.users's sttw
          const updateAssetResult = await db.query(
            'UPDATE "userAssets" SET amount=$1 WHERE "userId"=$2 AND "pvId"=$3',
            [y.toString(), userId, pvId]
          );

          const oldSttwResult = await db.query(`SELECT sttw FROM users WHERE id=$1`, [userId]);
          const oldSttw = oldSttwResult.rows[0].sttw;

          const updateSttwResult = await db.query(
            'UPDATE users SET sttw=sttw+$1 WHERE id=$2 RETURNING sttw',
            [repay.toString(), userId]
          );
          const newSttw = updateSttwResult.rows[0].sttw;

          //產生event-還本狀況(原持有, 新持有, 退還本金)
          const insertRepayEventResult = await db.query(
            `INSERT INTO "eventByRepay" ("pvId", "userId", date, "oldAmount", "newAmount", "oldSttw", "newSttw", repay, "updateDt") 
             VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9) RETURNING id`,
            [
              pvId,
              userId,
              date,
              x.toString(),
              y.toString(),
              oldSttw.toString(),
              newSttw.toString(),
              repay.toString(),
              moment()
            ]
          );

          //記錄區塊交易
          const txType = 'repaySync';
          const txId = insertRepayEventResult.rows[0].id;
          const data = {
            repayId: insertRepayEventResult.rows[0].id,
            userId: userId,
            date: moment(),
            pvId: pvId,
            oldAmount: x
              .times(Math.pow(10, 16))
              .integerValue()
              .toString(10),
            newAmount: y
              .times(Math.pow(10, 16))
              .integerValue()
              .toString(10),
            repay: repay
              .times(Math.pow(10, 16))
              .integerValue()
              .toString(10)
          };

          const insertBcTxResult = await db.query(
            'INSERT INTO "bcTransation" ("txType", "txId", data) VALUES ($1,$2,$3)',
            [txType, txId, data]
          );
        }
      }

      await db.query('COMMIT');
      return result;
    } catch (e) {
      await db.query('ROLLBACK');
      throw e;
    }
  })()
    .then(result => {
      res.send(result);
    })
    .catch(e => logger.error(e.stack));
});

module.exports = router;
