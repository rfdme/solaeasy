var express = require('express');
var router = express.Router();
const moment = require('moment');
const BN = require('bignumber.js').clone({ DECIMAL_PLACES: 16 });
const db = require('../db');
const { canAccessUser } = require('../modules/accessController');
const response = require('../modules/rsps');
const log4js = require('../modules/logger');
const logger = log4js.getLogger();
router.use(function(req, res, next) {
  logger.addContext('reqId', req.reqId);
  next();
});

// TODO: double check while these api is going to release
router.get('/userAsset/:id', async (req, res) => {
  let rsps = new response();
  logger.info('req params user id: ' + req.params.id);
  try {
    if (!canAccessUser(req.user, parseInt(req.params.id, 10))) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access user id= ${
          req.params.id
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    const query = {
      text: `
        SELECT A."pvId" as id, B.name, (A.amount - A."lockAmount") as remained FROM "userAssets" A
        LEFT JOIN pvs B ON B.id = A."pvId"
        WHERE A."userId" = $1
        ORDER BY id`,
      values: [req.params.id]
    };
    const { rows } = await db.query(query);
    rsps.result = { userAsset: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/ngoAsset', async (req, res) => {
  let rsps = new response();
  try {
    const query = {
      text: `
        SELECT id, "sellPrice", SUM(amount) as amount, SUM(income) as income FROM (
          SELECT A.id, B.amount, C."sellPrice", COALESCE(SUM(D.profit), 0) as income FROM users A
            RIGHT JOIN "userAssets" B ON B."userId"=A.id
            LEFT JOIN pvs C ON C.id=B."pvId"
            LEFT JOIN "eventBySimulate" D ON D."pvId" = B."pvId" AND D."userId" = A.id
            WHERE A.id >=1 AND A.id <= 1 AND B.amount > 0
            GROUP BY A.id, B."pvId", B.amount, C."sellPrice"
          ) T
        GROUP BY T.id, T."sellPrice"`
    };
    const { rows } = await db.query(query);
    rsps.result = { ngoAsset: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.post('/donate', async (req, res) => {
  let rsps = new response();
  const { giver, receiver, pvId } = req.body;
  logger.info('req body: ' + JSON.stringify(req.body));
  const amount = BN(parseInt(req.body.amount, 10));

  try {
    if (!canAccessUser(req.user, parseInt(req.body.giver, 10))) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access user id= ${
          req.body.giver
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    await db.query('BEGIN');

    const oldSntwGiverResult = await db.query(
      `SELECT amount, (amount - "lockAmount") AS remained FROM "userAssets" WHERE "userId"=$1 AND "pvId"=$2`,
      [giver, pvId]
    );
    if (oldSntwGiverResult.rowCount == 0 || amount.gt(oldSntwGiverResult.rows[0].remained)) {
      await db.query('ROLLBACK');
      logger.warn("donation failed, giver doesn't have enough sntw");
      rsps.result = { response: 'fail' };
      rsps.msg = '捐贈失敗: 贈與者的SNTW餘額不足';
      rsps.status = true;
      res.send(rsps.getResult());
      return;
    }
    const oldSntwGiver = oldSntwGiverResult.rows[0].amount;

    const paySntwResult = await db.query(
      `UPDATE "userAssets" SET amount=amount-$1 WHERE "userId"=$2 AND "pvId"=$3 RETURNING amount`,
      [amount.toString(), giver, pvId]
    );

    if (paySntwResult.rowCount !== 1) {
      await db.query('ROLLBACK');
      logger.warn("donation failed, giver's sntw cannot be deducted");
      rsps.result = { response: 'fail' };
      rsps.msg = '捐贈失敗: 贈與者無法扣除SNTW';
      rsps.status = true;
      res.send(rsps.getResult());
      return;
    }

    const newSntwGiver = paySntwResult.rows[0].amount;

    const oldSntwReceiverResult = await db.query(
      `SELECT amount FROM "userAssets" WHERE "userId"=$1 AND "pvId"=$2`,
      [receiver, pvId]
    );
    const oldSntwReceiver = oldSntwReceiverResult.rowCount > 0 ? oldSntwReceiverResult.rows[0].amount : BN(0);

    const getSntwResult = await db.query(
      `INSERT INTO "userAssets" as A ("userId", "pvId", amount) VALUES ($1,$2,$3)
       ON CONFLICT ("userId", "pvId") DO UPDATE SET amount=A.amount+$3 RETURNING amount`,
      [receiver, pvId, amount.toString()]
    );

    if (getSntwResult.rowCount !== 1) {
      await db.query('ROLLBACK');
      logger.warn('donation failed, receiver cannot receive the sntw');
      rsps.result = { response: 'fail' };
      rsps.msg = '捐贈失敗: 受贈者無法得到SNTW';
      rsps.status = true;
      res.send(rsps.getResult());
      return;
    }

    const newSntwReceiver = getSntwResult.rows[0].amount;

    //insert donation
    const createDonationRecordResult = await db.query(
      `INSERT INTO donations(
        receiver,
        giver,
        datetime,
        amount,
        "oldAmountReceiver",
        "newAmountReceiver",
        "oldAmountGiver",
        "newAmountGiver")
      VALUES ($1,$2,$3,$4,$5,$6,$7,$8) RETURNING id`,
      [
        receiver,
        giver,
        moment(),
        amount.toString(),
        oldSntwReceiver.toString(),
        newSntwReceiver.toString(),
        oldSntwGiver.toString(),
        newSntwGiver.toString()
      ]
    );

    if (createDonationRecordResult.rowCount !== 1) {
      await db.query('ROLLBACK');
      logger.warn('donation failed, unable to create record');
      rsps.result = { response: 'fail' };
      rsps.msg = '捐贈失敗: 無法建立捐贈紀錄';
      rsps.status = true;
      res.send(rsps.getResult());
      return;
    }

    /* TODO: 待saint加上donateSync
    //記錄區塊交易
    const txType = 'donateSync';
    const txId = createDonationRecordResult.rows[0].id;
    const data = {
      giver: giver,
      receiver: receiver,
      pvId: pvId,
      amount: amount
        .times(Math.pow(10, 16))
        .integerValue()
        .toString(10),
      date: moment().format('YYYY-MM-DD HH:mm:ss')
    };

    const insertBcTxResult = await db.query(
      'INSERT INTO "bcTransation" ("txType", "txId", data) VALUES ($1,$2,$3)',
      [txType, txId, data]
    );

    if (insertBcTxResult.rowCount !== 1) {
      await db.query('ROLLBACK');
      res.send({ result: 'fail', msg: '捐贈失敗: 無法新增區塊交易' });
      return;
    }
    */

    await db.query('COMMIT');
    rsps.result = { response: 'ok' };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    await db.query('ROLLBACK');
    rsps.msg = '捐贈失敗: 未知的錯誤';
    rsps.result = { response: 'fail' };
    res.send(rsps.getResult());
  }
});

module.exports = router;
