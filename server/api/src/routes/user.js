const express = require('express');
const router = express.Router();
const { findManagerById, addGoogle2faSecretById } = require('../sqliteDb');
const _ = require('lodash');
const speakeasy = require('speakeasy');
const QRCode = require('qrcode');
const db = require('../db');
const response = require('../modules/rsps');
const log4js = require('../modules/logger');
const logger = log4js.getLogger();
router.use(function(req, res, next) {
  logger.addContext('reqId', req.reqId);
  next();
});

router.get('/google2fa/:id', async (req, res) => {
  let rsps = new response();
  logger.info('req params manager id: ' + req.params.id);
  try {
    if (req.user.id !== parseInt(req.params.id)) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access manager id= ${
          req.params.id
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    let google2FASecretBase32 = '';
    const managers = await findManagerById(req.params.id);
    if (!_.isEmpty(managers)) {
      if (managers[0].google2FASecretBase32 === undefined || managers[0].google2FASecretBase32 === null) {
        // generate new secret
        const secret = speakeasy.generateSecret({
          length: 20,
          name: 'Sola Exchange'
        });

        await addGoogle2faSecretById(req.params.id, secret.base32);
        google2FASecretBase32 = secret.base32;
      } else {
        google2FASecretBase32 = managers[0].google2FASecretBase32;
      }

      const optauthURL = speakeasy.otpauthURL({
        secret: google2FASecretBase32,
        label: managers[0].email,
        issuer: 'Sola Exchange'
      });
      QRCode.toDataURL(optauthURL, (err, data_url) => {
        rsps.result = {
          google2FASecretBase32: google2FASecretBase32,
          data_url: data_url
        };
        rsps.status = true;
        res.send(rsps.getResult());
      });
    } else {
      rsps.msg = 'manager is not exist';
      logger.info('manager is not exist');
      res.send(rsps.getResult());
    }
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/google2fa/status/:id', async (req, res) => {
  let rsps = new response();
  logger.info('req params manager id: ' + req.params.id);
  try {
    if (req.user.id !== parseInt(req.params.id)) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access manager id= ${
          req.params.id
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    const managers = await findManagerById(req.params.id);
    if (!_.isEmpty(managers)) {
      rsps.status = true;
      rsps.result = {
        enable: managers[0].google2FAStatus
      };
      res.send(rsps.getResult());
    } else {
      rsps.msg = 'manager is not exist';
      logger.info('manager is not exist');
      res.send(rsps.getResult());
    }
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.post('/google2fa/toggle/:id', async (req, res) => {
  /*
   * {
   *  token: string (6 digits)
   *  enable: bool
   * }
   */
  let rsps = new response();
  logger.info(`req enable status=${req.body.enable}`);
  try {
    if (req.user.id !== parseInt(req.params.id)) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access manager id= ${
          req.params.id
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    const managers = await findManagerById(req.params.id);
    if (!_.isEmpty(managers)) {
      const tokenValidates = speakeasy.totp.verify({
        secret: managers[0].google2FASecretBase32,
        encodeing: 'base32',
        token: req.body.token
      });
      if (tokenValidates) {
        const query = {
          text: req.body.enable
            ? 'UPDATE managers SET "google2FAStatus"=TRUE WHERE id=$1'
            : 'UPDATE managers SET "google2FAStatus"=FALSE, "google2FASecretBase32"=NULL WHERE id=$1',
          values: [req.params.id]
        };
        await db.query(query);
      }
      rsps.status = true;
      rsps.result = { tokenValidates };
      res.send(rsps.getResult());
    } else {
      rsps.msg = 'manager is not exist';
      logger.info('manager is not exist');
      res.send(rsps.getResult());
    }
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.post('/google2fa/verify/:id', async (req, res) => {
  /*
   * {
   *  token: string (6 digits)
   * }
   */
  let rsps = new response();
  logger.debug(req.body);
  try {
    if (req.user.id !== parseInt(req.params.id)) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access manager id= ${
          req.params.id
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    const managers = await findManagerById(req.params.id);
    if (!_.isEmpty(managers)) {
      const tokenValidates = speakeasy.totp.verify({
        secret: managers[0].google2FASecretBase32,
        encodeing: 'base32',
        token: req.body.token
      });
      rsps.status = true;
      rsps.result = { tokenValidates };
      res.send(rsps.getResult());
    } else {
      rsps.msg = 'manager is not exist';
      logger.info('manager is not exist');
      res.send(rsps.getResult());
    }
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

module.exports = router;
