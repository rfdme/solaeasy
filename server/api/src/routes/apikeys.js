const express = require('express');
const router = express.Router();
const db = require('../modules/db');
const crypto = require('crypto');
const { canAccessUser } = require('../modules/accessController');
const response = require('../modules/rsps');
const log4js = require('../modules/logger');
const logger = log4js.getLogger();
router.use(function(req, res, next) {
  logger.addContext('reqId', req.reqId);
  next();
});

const dbHandler = new db();

router.get('/:id', async (req, res) => {
  let rsps = new response();
  logger.info('req params user id: ' + req.params.id);
  try {
    if (!canAccessUser(req.user, parseInt(req.params.id, 10))) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access user id= ${
          req.params.id
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    const apikeys = await dbHandler.queryApiKeys(req.params.id);
    rsps.status = true;
    rsps.result = { apikeys: apikeys };
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

// options = {
//   readInfo: true,
//   trade: false,
//   withdraw: false
// }

// You MUST have 2FA enabled to create an API key for your own safety.
router.post('/', async (req, res) => {
  let rsps = new response();
  logger.info('req options: ' + req.body.options);
  const { authToken, options } = req.body;

  if (authToken === undefined || authToken === '') {
    logger.info('invalid parameters, missing authToken');
    rsps.msg = 'invalid parameters, missing authToken';
    res.send(rsps.getResult());
    return;
  }

  if (options === undefined) {
    logger.info('invalid parameters, missing options');
    rsps.msg = 'invalid parameters, missing options';
    res.send(rsps.getResult());
    return;
  }
  if (options.readInfo === undefined) {
    logger.info('invalid parameters, missing options.readInfo');
    rsps.msg = 'invalid parameters, missing options.readInfo';
    res.send(rsps.getResult());
    return;
  }
  if (options.trade === undefined) {
    logger.info('invalid parameters, missing options.trade');
    rsps.msg = 'invalid parameters, missing options.trade';
    res.send(rsps.getResult());
    return;
  }
  if (options.withdraw === undefined) {
    logger.info('invalid parameters, missing options.withdraw');
    rsps.msg = 'invalid parameters, missing options.withdraw';
    res.send(rsps.getResult());
    return;
  }

  // 必須先啟用2FA才能創造api key
  const isEnabled = await dbHandler.check2FAenable(req.user.id);
  if (!isEnabled) {
    logger.info("user didn't active google2fa before create the api key");
    rsps.msg = '請先啟用Google Authenticator兩步驗證功能.';
    res.send(rsps.getResult());
    return;
  }

  const isPassed = await dbHandler.verify2FAToken(req.user.id, authToken);
  if (!isPassed) {
    logger.info('google2fa verification failed');
    rsps.msg = '認證失敗';
    res.send(rsps.getResult());
    return;
  }

  // now we can generate api key for access control
  const apiKey = crypto.randomBytes(16).toString('hex');

  // generate api secret for sign url
  const apiSecret = crypto.randomBytes(32).toString('hex');

  // insert to db
  const data = {
    key: apiKey,
    secret: apiSecret,
    readInfo: options.readInfo,
    trade: options.trade,
    withdraw: options.withdraw,
    userId: req.user.id
  };
  await dbHandler.insertApiKey(data);

  rsps.status = true;
  rsps.result = {
    success: true,
    apiKey,
    apiSecret
  };
  res.send(rsps.getResult());
});

module.exports = router;
