var express = require('express');
var router = express.Router();
const axios = require('axios');
const moment = require('moment');
const path = require('path');
const log4js = require('log4js');
const db = require('../db');

// 以filename設定logger分類
const logger = log4js.getLogger(path.basename(__filename));
logger.level = 'debug';

const url = require('../config/urls');

//測試:每日結算流程
router.get('/', async (req, res) => {
  if (req.query.begin === undefined || req.query.end === undefined)
    return res.send({
      error: 'invalid params. e.g. v1/test?begin=2017-02-02&end=2017-12-31'
    });

  let day = moment(req.query.begin, 'YYYY-MM-DD');
  try {
    while (day.isBefore(req.query.end)) {
      await simulateDaily(day.format('YYYY-MM-DD'));
      //await cleananceDaily(day.format('YYYY-MM-DD'));
      //await billDaily(day.format('YYYY-MM-DD'));
      //await repayDaily(day.format('YYYY-MM-DD'));
      day.add(1, 'days');
    }
    res.send({ msg: 'ok' });
  } catch (e) {
    logger.error(e);
  }
});

const simulateDaily = async date => {
  logger.info(`${url.schedulerURL}/api/v1/simulateDaily?date=${date}`);
  await axios
    .get(`${url.schedulerURL}/api/v1/simulateDaily?date=${date}`)
    .then(response => {
      logger.info(response.data);
    })
    .catch(err => {
      logger.error(err);
    });
};

const cleananceDaily = async date => {
  logger.info(`${url.schedulerURL}/api/v1/clearance/asset?date=${date}`);
  await axios
    .get(`${url.schedulerURL}/api/v1/clearance/asset?date=${date}`)
    .then(response => {
      logger.info(response.data);
    })
    .catch(err => {
      logger.error(err);
    });
};

const billDaily = async date => {
  logger.info(`${url.schedulerURL}/api/v1/bill/dispatch?date=${date}`);
  await axios
    .get(`${url.schedulerURL}/api/v1/bill/dispatch?date=${date}`)
    .then(response => {
      logger.info(response.data);
    })
    .catch(err => {
      //可能會有超時問題
      if (err.code === 'ECONNRESET') {
        logger.error('billDaily timeout');
      } else {
        logger.error(err);
      }
    });
};

const repayDaily = async date => {
  logger.info(`${url.schedulerURL}/api/v1/repay?date=${date}`);
  await axios
    .get(`${url.schedulerURL}/api/v1/repay?date=${date}`)
    .then(response => {
      logger.info(response.data);
    })
    .catch(err => {
      logger.error(err);
    });
};

module.exports = router;
