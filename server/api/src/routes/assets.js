const express = require('express');
const router = express.Router();
const axios = require('axios');
const db = require('../db');
const { canAccessUser } = require('../modules/accessController');
const response = require('../modules/rsps');
const log4js = require('../modules/logger');
const logger = log4js.getLogger();
router.use(function(req, res, next) {
  logger.addContext('reqId', req.reqId);
  next();
});

router.get('/:user', async (req, res) => {
  let rsps = new response();
  logger.info('req params user id: ' + req.params.user);
  try {
    if (!canAccessUser(req.user, parseInt(req.params.user, 10))) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access user id= ${
          req.params.user
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    const queryAsset = {
      text: `
        SELECT id, "holdAmount", name, status, capacity, "totalAmount", "initialPrice", price, "validityDateBegin", "validityDuration", MAX(date) as "lastRepayDate" FROM (SELECT
          A."pvId" as id,
          A.amount as "holdAmount",
          B.name,
          B.status,
          B.capacity,
          B."ownedCapital"/10.0 as "totalAmount",
          B."initialPrice",
          COALESCE(C.price, B."initialPrice") as price,
          B."validityDateBegin",
          B."validityDuration",
          D.date
        FROM "userAssets" A
        LEFT JOIN pvs B ON B.id = A."pvId"
        LEFT JOIN "marketInfo" C ON C."pvId" = A."pvId"
        LEFT JOIN "eventByRepay" D ON D."pvId" = A."pvId"
        WHERE A."userId"=$1) AS T
        GROUP BY id, "holdAmount", name, status, capacity, "totalAmount", "initialPrice", price, "validityDateBegin", "validityDuration"
        ORDER BY id`,
      values: [req.params.user]
    };
    const { rows } = await db.query(queryAsset);

    const querySttw = {
      text: `SELECT sttw FROM users WHERE id=$1`,
      values: [req.params.user]
    };
    const result = await db.query(querySttw);

    const data = {
      assets: {
        pvList: rows,
        sttw: result.rows[0].sttw
      }
    };
    rsps.status = true;
    rsps.result = data;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/valueHistory/:user', async (req, res) => {
  let rsps = new response();
  logger.info('req params user id: ' + req.params.user);
  try {
    if (!canAccessUser(req.user, parseInt(req.params.user, 10))) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access user id= ${
          req.params.user
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    const query = {
      text: `
        SELECT date, SUM("presentValue") AS value FROM "dailyClearance"
        WHERE "userId"=$1
        GROUP BY date
        ORDER BY date`,
      values: [req.params.user]
    };
    const { rows } = await db.query(query);

    const data = {
      valueHistory: rows
    };
    rsps.status = true;
    rsps.result = data;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/unrealizedProfit/:user', async (req, res) => {
  let rsps = new response();
  logger.info('req params user id: ' + req.params.user);
  try {
    if (!canAccessUser(req.user, parseInt(req.params.user, 10))) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access user id= ${
          req.params.user
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    const query = {
      text: `
        SELECT date, SUM(profit) AS value FROM "eventBySimulate"
        WHERE "userId"=$1
        GROUP BY date
        ORDER BY date`,
      values: [req.params.user]
    };
    const { rows } = await db.query(query);

    const data = {
      unrealizedProfit: rows
    };
    rsps.status = true;
    rsps.result = data;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/realizedProfit/:user', async (req, res) => {
  let rsps = new response();
  logger.info('req params user id: ' + req.params.user);
  logger.info(`req.params = ${JSON.stringify(req.params)}`);
  try {
    if (!canAccessUser(req.user, parseInt(req.params.user, 10))) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access user id= ${
          req.params.user
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    const query = {
      text: `
        SELECT date, SUM(profit) AS value FROM "eventByBill"
        WHERE "userId"=$1
        GROUP BY date
        ORDER BY date`,
      values: [req.params.user]
    };
    const { rows } = await db.query(query);

    const data = {
      realizedProfit: rows
    };
    rsps.status = true;
    rsps.result = data;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.post('/forMock', async (req, res) => {
  logger.info(`req.params = ${JSON.stringify(req.params)}`);
  try {
    const query = {
      text: `SELECT
          A.id,
          0 as "holdAmount",
          A.name,
          A.status,
          A.capacity,
          A."ownedCapital"/10.0 as "totalAmount",
          COALESCE(B.price, A."initialPrice") as price
        FROM pvs A
        LEFT JOIN "marketInfo" B ON B."pvId" = A.Id
        WHERE A.id = ANY ($1)`,
      values: [req.body.ids]
    };
    const { rows } = await db.query(query);

    const data = {
      assets: {
        pvList: rows,
        sttw: 0
      }
    };
    logger.info(`data = ${JSON.stringify(data)}`);
    res.send(data);
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send();
  }
});

router.post('/etherscanLink', async (req, res) => {
  let rsps = new response();
  logger.info('user: ' + req.body.user, 'token: ' + req.body.token);
  if (req.body.user === undefined || req.body.token === undefined) {
    logger.info(`invalid parameters, user/token = ${req.body.user}/${req.body.token}`);
    rsps.msg = 'invalid parameters';
    res.send(rsps.getResult());
    return;
  }
  try {
    if (!canAccessUser(req.user, parseInt(req.body.user, 10))) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access user id= ${
          req.body.user
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    let response = await axios.get(
      `http://54.95.137.247:3000/bc/public/tokenBalanceUrl?userId=${req.body.user}&pvId=${req.body.token}`
    );
    rsps.result = response.data;
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

module.exports = router;
