var express = require('express');
var router = express.Router();
const moment = require('moment');
const _ = require('lodash');
const axios = require('axios');
const { findPv, getInsuranceList, getLoanFeeList, getLoanList } = require('../sqliteDb');
const { simulatedDailyProfit } = require('../finance');
const path = require('path');
const log4js = require('log4js');
const keys = require('../config/keys');

// 以filename設定logger分類
const logger = log4js.getLogger(path.basename(__filename));
logger.level = 'debug';

router.get('/', async function(req, res, next) {
  logger.info(`req.query = ${JSON.stringify(req.query)}`);
  if (req.query.token === undefined || req.query.date === undefined) {
    res.send({
      error: 'invalid params. e.g. /api/v1/profit?token=SNTW&date=2018-01-22'
    });
    logger.info('invalid params');
    return;
  }

  const token = req.query.token;
  let pvId = '';
  let projectCode = '';
  let validityDateBegin = '';
  let validityDuration = '';
  let sellPrice = 0.0;
  let totalCapital = 0;
  let operationRateByCapital = 0.0;
  let operationRateByIncome = 0.0;
  let serviceRateByIncome = 0.0;
  let rentRateByIncome = 0.0;
  let rentYearCost = 0;
  let rentWithTax = false;

  // check token
  try {
    let rows = await findPv(token);

    if (_.isEmpty(rows)) return res.send({ error: `there is no ${token}` });
    pvId = rows[0].id;
    projectCode = rows[0].projectCode;
    validityDateBegin = rows[0].validityDateBegin;
    validityDuration = rows[0].validityDuration;
    sellPrice = rows[0].sellPrice;
    totalCapital = rows[0].totalCapital;
    operationRateByCapital = rows[0].operationRateByCapital;
    operationRateByIncome = rows[0].operationRateByIncome;
    serviceRateByIncome = rows[0].serviceRateByIncome;
    rentRateByIncome = rows[0].rentRateByIncome;
    rentYearCost = rows[0].rentYearCost;
    rentWithTax = rows[0].rentWithTax;
  } catch (e) {
    logger.error(e);
    return res.send({ error: 'something wrong, please try again later.' });
  }

  // check date
  const date = req.query.date;
  const m = moment(req.query.date, 'YYYY-MM-DD');
  if (!m.isValid()) {
    logger.info('invalid params');
    res.send({
      error: 'invalid date format. e.g. YYYY-MM-DD 2018-01-22'
    });
    return;
  }

  if (!m.isBetween(validityDateBegin, moment(validityDateBegin).add(validityDuration, 'years'), null, '[)')) {
    return res.send({
      hasData: false
    });
  }

  // fetch power value
  let value = 0.0;
  let hasData = true;
  try {
    let response = await axios.get(`${keys.itsURL}/v1/values/${projectCode}?date=${date}`);
    if (_.isEmpty(response.data.result.pvdata)) {
      value = 0.0;
      hasData = false;
    } else {
      value = response.data.result.pvdata[0].value;
    }
  } catch (e) {
    logger.error(e);
    res.status(500).send();
    return;
  }

  const insuranceList = await getInsuranceList(pvId);
  const loanFeeList = await getLoanFeeList(pvId);
  const loanList = await getLoanList(pvId);

  let result = simulatedDailyProfit({
    date,
    power: value,
    sellPrice,
    totalCapital,
    operationRateByCapital,
    operationRateByIncome,
    serviceRateByIncome,
    rentRateByIncome,
    rentYearCost,
    rentWithTax,
    insuranceList,
    loanFeeList,
    loanList
  });

  return res.send({
    result,
    hasData: hasData
  });
});

module.exports = router;
