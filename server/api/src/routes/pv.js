var express = require('express');
var router = express.Router();
const axios = require('axios');
const db = require('../db');
const moment = require('moment');
const { simulatedDailyProfit } = require('../finance');
//const { findAllPv, getInsuranceList, getLoanFeeList, getLoanList } = require('../sqliteDb');
const _ = require('lodash');
const path = require('path');
const log4js = require('log4js');

const achievement = require('../modules/achievement.js');
const response = require('../modules/rsps');
const keys = require('../config/keys');

// 以filename設定logger分類
const logger = log4js.getLogger(path.basename(__filename));
logger.level = 'debug';
/*
router.get('/allPvUnitProfit', async (req, res) => {
  let rsps = new response();
  try {
    let result = {};

    let rows = await findAllPv();

    //匯集所有結果才送出
    let fetchCounter = 0;
    const fetchHelper = function(cmd) {
      switch (cmd) {
        case 'incr':
          fetchCounter++;
          break;
        case 'decr':
          fetchCounter--;
          break;
      }

      if (fetchCounter === 0) {
        rsps.result = result;
        rsps.status = true;
        res.send(rsps.getResult());
      }
    };

    rows.forEach(async pv => {
      const date = moment().format('YYYY-MM-DD');
      const insuranceList = await getInsuranceList(pv.pvId);
      const loanFeeList = await getLoanFeeList(pv.pvId);
      const loanList = await getLoanList(pv.pvId);

      fetchHelper('incr');
      axios
        .get('https://sola-api.herokuapp.com/api/v1/values/' + pv.projectCode)
        .then(response => {
          let profitData = {};
          response.data.forEach((item, index) => {
            const date = moment(item.dt).format('YYYY-MM-DD');
            let v = profitData[date];
            if (v) {
              v += item.value;
              profitData[date] = v;
            } else {
              profitData[date] = item.value;
            }
          });

          let {
            rentDayCostFixed,
            rentDayCostFloat,
            operationDayCostFixed,
            operationDayCostFloat,
            serviceDayCostFloat,
            meterRentDayCost,
            insuranceDayCost,
            loanDayCost,
            loanFeeDayCost,
            dayBusinessTax
          } = simulatedDailyProfit({
            date,
            power: 0,
            sellPrice: pv.sellPrice,
            totalCapital: pv.totalCapital,
            operationRateByCapital: pv.operationRateByCapital,
            operationRateByIncome: pv.operationRateByIncome,
            serviceRateByIncome: pv.serviceRateByIncome,
            rentRateByIncome: pv.rentRateByIncome,
            rentYearCost: pv.rentYearCost,
            rentWithTax: pv.rentWithTax,
            insuranceList,
            loanFeeList,
            loanList
          });

          const baseCost =
            rentDayCostFixed +
            rentDayCostFloat +
            operationDayCostFixed +
            operationDayCostFloat +
            serviceDayCostFloat +
            meterRentDayCost +
            insuranceDayCost +
            loanDayCost +
            loanFeeDayCost +
            dayBusinessTax;

          profitData['unitCost'] = baseCost / (pv.ownedCapital / 10.0);

          for (k in profitData) {
            if (profitData.hasOwnProperty(k)) {
              const { dayProfit } = simulatedDailyProfit({
                date: k,
                power: profitData[k],
                sellPrice: pv.sellPrice,
                totalCapital: pv.totalCapital,
                operationRateByCapital: pv.operationRateByCapital,
                operationRateByIncome: pv.operationRateByIncome,
                serviceRateByIncome: pv.serviceRateByIncome,
                rentRateByIncome: pv.rentRateByIncome,
                rentYearCost: pv.rentYearCost,
                rentWithTax: pv.rentWithTax,
                insuranceDayCost: insuranceDayCost,
                loanFeeDayCost: loanFeeDayCost,
                loanAmount: pv.loanAmount,
                loanRate: pv.loanRate,
                loanPeriod: pv.loanPeriod,
                loanBeginDate: pv.loanBeginDate
              });

              const unitProfit = dayProfit / (pv.ownedCapital / 10.0);
              profitData[k] = unitProfit;
            }
          }

          result[pv.id] = {
            profitData
          };

          fetchHelper('decr');
        })
        .catch(err => {
          logger.error(err);
          fetchHelper('decr');
        });
    });
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});
*/
router.get('/project', async (req, res) => {
  let rsps = new response();
  try {
    const query = {
      text: 'SELECT id, name FROM pvs ORDER BY id'
    };
    const { rows } = await db.query(query);
    rsps.result = { pvList: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    console.trace(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/project/:id', async (req, res) => {
  let rsps = new response();
  try {
    const query = {
      text: `SELECT
          A.id,
          A.name,
          A."projectCode",
          A.location,
          A."coordLat",
          A."coordLng",
          A.capacity,
          A."ownedCapital",
          A."initialPrice",
          A."sellPrice",
          A.status,
          A.stage,
          A.owner,
          COALESCE(B.price, A."initialPrice") as price
        FROM pvs A
        LEFT JOIN "marketInfo" B ON B."pvId" = A.id
        WHERE id=$1`,
      values: [req.params.id]
    };
    const { rows } = await db.query(query);
    rsps.result = { pvData: rows[0] };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    console.trace(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/values/:id', async (req, res) => {
  let rsps = new response();
  try {
    const query = {
      text: `SELECT "projectCode" FROM pvs WHERE id=$1`,
      values: [req.params.id]
    };
    const { rows } = await db.query(query);

    if (rows.length === 0) {
      rsps.status = true;
      rsps.result = {
        powerData: null
      };
      res.send(rsps.getResult());
      return;
    }

    axios
      .get(keys.itsURL + '/v1/values/' + rows[0].projectCode + '?pagesize=10000&pagenum=1')
      .then(response => {
        let labels = [];
        let values = [];
        response.data.result.pvdata.forEach((item, index) => {
          labels.push(item.dt);
          values.push(item.value);
        });

        const data = {
          powerData: { labels, values }
        };
        rsps.status = true;
        rsps.result = data;
        res.send(rsps.getResult());
      })
      .catch(err => {
        logger.error(err);
        res.status(500).send(rsps.getResult());
      });
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/img/:id', async (req, res) => {
  let rsps = new response();
  try {
    const query = {
      text: 'SELECT img FROM "pvImg" WHERE "pvId"=$1',
      values: [req.params.id]
    };
    const { rows } = await db.query(query);
    if (rows.length > 0) {
      rsps.result = { pvImg: rows[0].img };
    } else {
      rsps.result = { pvImg: null };
    }
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/stage/:id', async (req, res) => {
  let rsps = new response();
  try {
    const query = {
      text: 'SELECT stage, content, file FROM "pvStage" WHERE "pvId"=$1',
      values: [req.params.id]
    };
    const { rows } = await db.query(query);
    rsps.result = { pvStage: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/achievementRate', async (req, res) => {
  let rsps = new response();
  try {
    const { rows } = await db.query(
      `SELECT id, "projectCode", capacity, decay, "annualAmount", "validityDateBegin" FROM pvs`
    );

    const data = await achievement.getRate(rows);
    rsps.result = { data: data };
    rsps.status = true;
    res.send(rsps.getResult());
    // res.send(data);
  } catch (error) {
    logger.error(error.stack);
    res.status(500).send(rsps.getResult());
  }
});

// it looks like, this api is not used by this repo itself
router.get('/achievementRate/:id', async (req, res) => {
  let rsps = new response();
  try {
    const { rows } = await db.query(
      `SELECT id, "projectCode", capacity, decay, "annualAmount", "validityDateBegin" FROM pvs WHERE id=$1`,
      [req.params.id]
    );

    const data = await achievement.getRate(rows);
    rsps.result = { data: data };
    rsps.status = true;
    res.send(rsps.getResult());

    logger.info(data);
  } catch (error) {
    logger.error(error.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/summaryToday', async (req, res) => {
  let rsps = new response();
  try {
    const { rows } = await db.query(`SELECT id, "projectCode", "sellPrice" from pvs`);

    const now = moment();
    const today = now.format('YYYY-MM-DD');
    const tomorrow = now.add(1, 'days').format('YYYY-MM-DD');
    const powerQuery = await axios.get(keys.itsURL + `/v1/values/?start=${today}&end=${tomorrow}`);

    // 過濾未上架的電廠發電資料，並加上income = value * sellPrice
    let values =
      powerQuery.data.result == undefined || powerQuery.data.result.pvdata == undefined
        ? []
        : powerQuery.data.result.pvdata.map(v => {
            const pv = _.find(rows, o => {
              return o.projectCode === v.id;
            });
            if (pv === undefined) {
              return;
            }
            const income = parseFloat(v.value, 10) * pv.sellPrice;
            return {
              id: v.id,
              value: v.value,
              income: income
            };
          });

    values = _.filter(values, o => {
      return o !== undefined;
    });

    const totalValue = values.reduce((accumulator, x) => {
      return accumulator + x.value;
    }, 0);

    // 計算公式 = values * 電力排放係數 (0.529 kg)
    const co2 = totalValue * 0.529;

    const totalIncome = values.reduce((accumulator, x) => {
      return accumulator + x.income;
    }, 0);
    rsps.result = {
      values: totalValue,
      co2: co2,
      income: totalIncome
    };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (error) {
    logger.error(error.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/summary', async (req, res) => {
  let rsps = new response();
  try {
    const { rows } = await db.query(`SELECT id, "projectCode", "sellPrice" from pvs`);

    //查詢最新資料
    const previousSummary = await db.query(`SELECT values, co2, income, "lastDate" FROM "pvSummary"`);

    //最新定義為昨日
    const today = moment().format('YYYY-MM-DD');
    const yesterday = moment()
      .add(-1, 'days')
      .format('YYYY-MM-DD');

    let summary;
    let powerQuery;

    //有資料時僅需更新累計至昨日之發電資料
    if (previousSummary.rows.length > 0) {
      summary = {
        values: parseFloat(previousSummary.rows[0].values, 10),
        co2: parseFloat(previousSummary.rows[0].co2, 10),
        income: parseFloat(previousSummary.rows[0].income, 10)
      };

      const lastDate = moment(previousSummary.rows[0].lastDate);
      if (lastDate.isBefore(yesterday)) {
        //需更新至昨日
        const start = lastDate.add(1, 'days').format('YYYY-MM-DD');

        logger.info(`start fetching ${keys.itsURL}/v1/values?start=${start}&end=${today}`);
        powerQuery = await axios.get(`${keys.itsURL}/v1/values?start=${start}&end=${today}`);
      } else {
        //不需更新
        logger.info('Just Query PvSummary');
        rsps.status = true;
        rsps.result = summary;
        res.send(rsps.getResult());
        return;
      }
    } else {
      //無資料時重新建立
      logger.info('recover pvSummary');
      const start = '2016-01-01'; //足夠久遠
      summary = {
        values: 0,
        co2: 0,
        income: 0
      };

      logger.info(`start fetching ${keys.itsURL}/v1/values?start=${start}&end=${today}`);
      powerQuery = await axios.get(`${keys.itsURL}/v1/values?start=${start}&end=${today}`);
    }
    // 過濾未上架的電廠發電資料，並加上income = value * sellPrice
    let values =
      powerQuery.data.result == undefined || powerQuery.data.result.pvdata == undefined
        ? []
        : powerQuery.data.result.pvdata.map(v => {
            const pv = _.find(rows, o => {
              return o.projectCode === v.id;
            });
            if (pv === undefined) {
              return;
            }
            const income = parseFloat(v.value, 10) * pv.sellPrice;
            return {
              id: v.id,
              value: v.value,
              income: income
            };
          });

    values = _.filter(values, o => {
      return o !== undefined;
    });

    const totalValue = values.reduce((accumulator, x) => {
      return accumulator + x.value;
    }, 0);

    // 計算公式 = values * 電力排放係數 (0.529 kg)
    const co2 = totalValue * 0.529;

    const totalIncome = values.reduce((accumulator, x) => {
      return accumulator + x.income;
    }, 0);

    summary.values += totalValue;
    summary.co2 += co2;
    summary.income += totalIncome;

    if (previousSummary.rows.length > 0) {
      logger.info('Query and Update PvSummary');
      await db.query(`UPDATE "pvSummary" SET values=$1, co2=$2, income=$3, "lastDate"=$4, "updateAt"=$5`, [
        summary.values,
        summary.co2,
        summary.income,
        yesterday,
        moment()
      ]);
    } else {
      logger.info('Query and Init PvSummary');
      await db.query(
        `INSERT INTO "pvSummary" as A (values, co2, income, "lastDate", "updateAt") VALUES ($1,$2,$3,$4,$5)`,
        [summary.values, summary.co2, summary.income, yesterday, moment()]
      );
    }
    rsps.status = true;
    rsps.result = summary;
    res.send(rsps.getResult());
  } catch (error) {
    logger.error(error.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/running', async (req, res) => {
  let rsps = new response();
  try {
    const query = {
      text: `SELECT COUNT(id) FROM pvs WHERE status=0
             UNION ALL
             SELECT COUNT(id) FROM pvs`
    };
    const { rows } = await db.query(query);
    rsps.result = { running: rows[0].count, total: rows[1].count };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    console.trace(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

module.exports = router;
