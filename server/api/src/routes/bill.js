const express = require('express');
const router = express.Router();
const moment = require('moment');
const sprintf = require('sprintf-js').sprintf;
const axios = require('axios');
const BN = require('bignumber.js').clone({ DECIMAL_PLACES: 16 });

const db = require('../db');

const url = require('../config/urls');

const path = require('path');
const log4js = require('log4js');
const keys = require('../config/keys');
// 以filename設定logger分類
const logger = log4js.getLogger(path.basename(__filename));
logger.level = 'debug';

router.get('/dispatch', (req, res) => {
  if (req.query.date === undefined) {
    res.send({
      error: 'invalid params. e.g. api/v1/bill/dispatch?date=2018-01-1'
    });
    logger.info('invalid params');
    return;
  }

  const date = req.query.date;

  (async () => {
    try {
      //查詢未處理的帳單
      const allBillResult = await db.query(
        `SELECT
           A.id, A."pvId", A."rangeBegin", A."rangeEnd", A."incomeWithTax",
           A."profit", A."meterRentCost", A."rentCostFloat", A."rentCostFixed",
           A."operateCostFloat", A."operateCostFixed", A."serviceCostFloat",
           A."loanCost", A."insuranceCost", A."loanFeeCost", A."businessTax", A."inTrust",
           B."projectCode", B."ownedCapital" FROM "bills" A
         LEFT JOIN pvs B ON B.id = A."pvId"
         WHERE A.cleared IS NULL AND A."rangeEnd"<=$1`,
        [date]
      );

      //標記帳單處理中
      for (let i = 0; i < allBillResult.rows.length; i++) {
        const bill = allBillResult.rows[i];
        await db.query(
          `UPDATE bills SET
                cleared=false,
                "updateDt"=$2
             WHERE id=$1`,
          [bill.id, moment()]
        );
      }

      for (let i = 0; i < allBillResult.rows.length; i++) {
        await db.query('BEGIN');

        const bill = allBillResult.rows[i];
        const end = moment(bill.rangeEnd, 'YYYY-MM-DD').format('YYYY-MM-DD');

        //取得帳單期間的發電數據
        const powerData = await getPowerByDateRange(bill.projectCode, bill.rangeBegin, end);

        const totalPower = Object.keys(powerData).reduce((previous, key) => previous + powerData[key], 0);

        //無發電資料時, 電費相關成本則平均分配
        let byAvg = true;
        if (Object.keys(powerData).length > 0) {
          byAvg = false;
        }

        let day = moment(bill.rangeBegin);

        const dayPercentage = 1 / moment(bill.rangeEnd).diff(bill.rangeBegin, 'days');

        let cashoutList = {};

        while (day.isBefore(bill.rangeEnd)) {
          const date = day.format('YYYY-MM-DD');
          const powerPercentage = byAvg ? dayPercentage : powerData[date] ? powerData[date] / totalPower : 0;

          //依據實際帳單費用、每日發電數據、每日各帳戶的持有比利, 計算出各帳戶的實際收益
          /*
          const totalAmountResult = await db.query(
            `SELECT SUM(amount) FROM "dailyClearance"
             WHERE date=$1 AND item=$2`,
            [date, bill.pvId]
          );

          const totalAmount = totalAmountResult.rows[0].sum;
          if (totalAmount === null) {
            logger.error('%s no dailyClearance in %s', bill.pvId, date);
            await db.query('ROLLBACK');
            return;
          }
          */
          logger.info('process bill(%s) for %s on:', bill.id, bill.pvId, date);

          const dayRentCost = bill.rentCostFloat * powerPercentage + bill.rentCostFixed * dayPercentage;
          const dayMeterRentCost = bill.meterRentCost * dayPercentage;
          const dayOperateCost =
            bill.operateCostFloat * powerPercentage + bill.operateCostFixed * dayPercentage;
          const dayServiceCost = bill.serviceCostFloat * powerPercentage;
          const dayLoanCost = bill.loanCost * dayPercentage;
          const dayInsuranceCost = bill.insuranceCost * dayPercentage;
          const dayLoanFeeCost = bill.loanFeeCost * dayPercentage;
          const dayBusinessTax = bill.businessTax * powerPercentage;
          const dayProfit =
            bill.incomeWithTax * powerPercentage -
            dayRentCost -
            dayMeterRentCost -
            dayOperateCost -
            dayServiceCost -
            dayLoanCost -
            dayInsuranceCost -
            dayLoanFeeCost -
            dayBusinessTax;

          const checkClearanceResult = await db.query(
            `SELECT COUNT(amount) FROM "dailyClearance" WHERE date=$1 AND item=$2`,
            [date, bill.pvId]
          );

          //產生eventByBill
          let insertEventResult;
          if (checkClearanceResult.rows[0].count > 0) {
            //從每日結算資料紀錄
            insertEventResult = await db.query(
              `INSERT INTO "eventByBill" ("billId", "userId", date, profit, "rentCost", "meterRentCost", "insuranceCost", "operateCost", "serviceCost", "loanCost", "loanFeeCost", "businessTax", "oldSttw", "newSttw", "updateDt")
                 SELECT $1, A."userId", A.date,
                   A.amount*$2*10.0/B."ownedCapital", A.amount*$3*10.0/B."ownedCapital",
                   A.amount*$4*10.0/B."ownedCapital", A.amount*$5*10.0/B."ownedCapital",
                   A.amount*$6*10.0/B."ownedCapital", A.amount*$7*10.0/B."ownedCapital",
                   A.amount*$8*10.0/B."ownedCapital", A.amount*$9*10.0/B."ownedCapital",
                   A.amount*$10*10.0/B."ownedCapital", $11, $12, $13 FROM "dailyClearance" A
                 LEFT JOIN pvs B ON B.id=A.item
                 WHERE A.date=$14 AND A.item=$15 RETURNING *`,
              [
                bill.id,
                dayProfit,
                dayRentCost,
                dayMeterRentCost,
                dayInsuranceCost,
                dayOperateCost,
                dayServiceCost,
                dayLoanCost,
                dayLoanFeeCost,
                dayBusinessTax, //TODO借款還完後, 應屬於profit
                0,
                0,
                moment(),
                date,
                bill.pvId
              ]
            );
          } else {
            //沒有結算資料紀錄, 直接使用當前資產資料
            insertEventResult = await db.query(
              `INSERT INTO "eventByBill" ("billId", "userId", date, profit, "rentCost", "meterRentCost", "insuranceCost", "operateCost", "serviceCost", "loanCost", "loanFeeCost", "businessTax", "oldSttw", "newSttw", "updateDt")
                 SELECT $1, A."userId", $14,
                  A.amount*$2*10.0/B."ownedCapital", A.amount*$3*10.0/B."ownedCapital",
                  A.amount*$4*10.0/B."ownedCapital", A.amount*$5*10.0/B."ownedCapital",
                  A.amount*$6*10.0/B."ownedCapital", A.amount*$7*10.0/B."ownedCapital",
                  A.amount*$8*10.0/B."ownedCapital", A.amount*$9*10.0/B."ownedCapital",
                  A.amount*$10*10.0/B."ownedCapital", $11, $12, $13 FROM "userAssets" A
                 LEFT JOIN pvs B ON B.id=A."pvId"
                 WHERE A."pvId"=$15 RETURNING *`,
              [
                bill.id,
                dayProfit,
                dayRentCost,
                dayMeterRentCost,
                dayInsuranceCost,
                dayOperateCost,
                dayServiceCost,
                dayLoanCost,
                dayLoanFeeCost,
                dayBusinessTax, //TODO借款還完後, 應屬於profit
                0,
                0,
                moment(),
                date,
                bill.pvId
              ]
            );
          }

          if (bill.inTrust) {
            //信託內則產生token
            //發放收益
            for (let j = 0; j < insertEventResult.rows.length; j++) {
              const row = insertEventResult.rows[j];
              const profit = BN(row.profit, 10);

              const oldSttwResult = await db.query(`SELECT sttw FROM users WHERE id=$1`, [row.userId]);
              const oldSttw = oldSttwResult.rows[0].sttw;

              const profitResult = await db.query(
                `UPDATE users SET sttw=sttw+$1 WHERE id=$2 RETURNING sttw`,
                [profit.toString(), row.userId]
              );
              const newSttw = profitResult.rows[0].sttw;

              await db.query('UPDATE "eventByBill" SET "oldSttw"=$1, "newSttw"=$2 WHERE id=$3', [
                oldSttw.toString(),
                newSttw.toString(),
                row.id
              ]);

              //記錄區塊交易
              const txType = 'profitSync';
              const txId = row.id;
              const data = {
                billId: row.billId,
                userId: row.userId,
                date: row.date,
                pvId: bill.pvId,
                rangeBegin: bill.rangeBegin,
                rangeEnd: bill.rangeEnd,
                income: BN(bill.incomeWithTax.toString())
                  .times(Math.pow(10, 16))
                  .integerValue()
                  .toString(10),
                userProfit: profit
                  .times(Math.pow(10, 16))
                  .integerValue()
                  .toString(10),
                totalCost: BN(row.rentCost.toString())
                  .plus(row.meterRentCost.toString())
                  .plus(row.insuranceCost.toString())
                  .plus(row.operateCost.toString())
                  .plus(row.serviceCost.toString())
                  .plus(row.loanCost.toString())
                  .plus(row.loanFeeCost.toString())
                  .plus(row.businessTax.toString())
                  .times(Math.pow(10, 16))
                  .integerValue()
                  .toString(10)
              };

              const insertBcTxResult = await db.query(
                'INSERT INTO "bcTransation" ("txType", "txId", data) VALUES ($1,$2,$3)',
                [txType, txId, data]
              );
            }
          } else {
            //直接出金
            for (let j = 0; j < insertEventResult.rows.length; j++) {
              const row = insertEventResult.rows[j];

              const oldSttwResult = await db.query(`SELECT sttw FROM users WHERE id=$1`, [row.userId]);
              const oldSttw = oldSttwResult.rows[0].sttw;

              await db.query('UPDATE "eventByBill" SET "oldSttw"=$1, "newSttw"=$2 WHERE id=$3', [
                oldSttw.toString(),
                oldSttw.toString(),
                row.id
              ]);

              const keyUserId = row.userId.toString();
              if (!(keyUserId in cashoutList)) {
                cashoutList[keyUserId] = { profit: BN(0), oldSttw: oldSttw };
              }

              cashoutList[keyUserId].profit = cashoutList[keyUserId].profit.plus(row.profit);
            }
          }

          day.add(1, 'days');
        }

        for (var key in cashoutList) {
          if (cashoutList.hasOwnProperty(key)) {
            const userId = parseInt(key, 10);
            const profit = BN(cashoutList[key].profit.toFixed(0));
            const sttw = cashoutList[key].oldSttw;
            const comment = '尚未信託，直接分潤';

            const cashoutRecord = {
              text:
                'INSERT INTO "cashoutRecords" ("userId", date, amount, status, "oldSttw", "newSttw", "updateDt", comment) VALUES($1,$2,$3,$4,$5,$6,$7,$8)',
              values: [
                userId,
                moment().format('YYYY-MM-DD'),
                profit.toString(),
                1,
                sttw.toString(),
                sttw.toString(),
                moment(),
                comment
              ]
            };
            await db.query(cashoutRecord);
          }
        }

        //標記帳單已發放
        const updateBillResult = await db.query(
          `UPDATE bills
           SET cleared=$1, "updateDt"=$2
           WHERE id=$3 AND cleared=FALSE`,
          [true, moment(), bill.id]
        );

        if (updateBillResult.rowCount !== 1) {
          await db.query('ROLLBACK');
        } else {
          await db.query('COMMIT');
        }
      }

      return sprintf('結算帳單完成(%s筆)', allBillResult.rowCount);
    } catch (e) {
      console.log(e.stack);
      await db.query('ROLLBACK');
      throw e;
    }
  })()
    .then(result => {
      res.send(result);
      logger.info(`result = ${result}`);
    })
    .catch(e => {
      logger.error(e.stack);
      res.status(500).send();
    });
});

const getPowerByDateRange = async (projectCode, begin, end) => {
  let powerData = {};
  try {
    let response = await axios.get(`${keys.itsURL}/v1/values/${projectCode}?start=${begin}&end=${end}`);
    response.data.result.pvdata.forEach((item, index) => {
      powerData[item.dt] = item.value;
    });
  } catch (e) {
    logger.error(e);
  } finally {
    return powerData;
  }
};

module.exports = router;
