var express = require('express');
var router = express.Router();
const db = require('../db');
const response = require('../modules/rsps');
const log4js = require('../modules/logger');
const logger = log4js.getLogger();
router.use(function(req, res, next) {
  logger.addContext('reqId', req.reqId);
  next();
});

router.get('/allPv', async (req, res) => {
  let rsps = new response();
  try {
    const query = {
      text: `
        SELECT id, name, "ownedCapital"/10.0 as "totalAmount", COALESCE(B.open, A."initialPrice") as open, COALESCE(B.price, A."initialPrice") as price FROM pvs A
        LEFT JOIN "marketInfo" B ON B."pvId" = A.id
        ORDER BY id`
    };
    const { rows } = await db.query(query);
    rsps.result = { allPv: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/userPv/:id', async (req, res) => {
  let rsps = new response();
  logger.info('req params user id: ' + req.params.id);
  try {
    const query = {
      text: `
        SELECT id, name, COALESCE(B.open, A."initialPrice") as open, COALESCE(B.price, A."initialPrice") as price, C.amount, D."totalAmount" FROM pvs A
        LEFT JOIN "marketInfo" B ON B."pvId" = A.id
        LEFT JOIN "userAssets" C ON C."pvId" = A.id
        LEFT JOIN (
            SELECT "pvId", SUM(amount) as "totalAmount" FROM "userAssets"
            GROUP BY "pvId") D ON D."pvId" = A.id
        WHERE C."userId" = $1
        ORDER BY id`,
      values: [req.params.id]
    };
    const { rows } = await db.query(query);
    rsps.status = true;
    rsps.result = { userPv: rows };
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.post('/timeline', async (req, res) => {
  let rsps = new response();
  logger.info('req.body: ' + JSON.stringify(req.body));
  const { dateBegin, dateEnd, userId } = req.body;
  try {
    const query = {
      text: `
        SELECT * FROM (
          SELECT 'pv' as event, date FROM "eventByPV" WHERE date >=$1 AND date <=$2
          UNION ALL
          SELECT 'cashin' as event, date FROM "cashinRecords" WHERE date >=$1 AND date <=$2 AND "userId"=$3
          UNION ALL
          SELECT 'cashout' as event, date FROM "cashoutRecords" WHERE date >=$1 AND date <=$2 AND "userId"=$3
          UNION ALL
          SELECT 'bill' as event, date FROM "eventByBill" WHERE date >=$1 AND date <=$2 AND "userId"=$3
          UNION ALL
          SELECT 'repay' as event, date FROM "eventByRepay" WHERE date >=$1 AND date <=$2 AND "userId"=$3
          UNION ALL
          SELECT 'buy' as event, A.datetime::date as date FROM trades A
            LEFT JOIN "orderBuys" B ON B.id = A."orderBuyId"
            WHERE A.datetime >=$1 AND A.datetime <=$2 AND B."userId"=$3
          UNION ALL
          SELECT 'sell' as event, A.datetime::date as date FROM trades A
            LEFT JOIN "orderSells" B ON B.id = A."orderSellId"
            WHERE A.datetime >=$1 AND A.datetime <=$2 AND B."userId"=$3
        ) T GROUP BY event, date`,
      values: [dateBegin, dateEnd, userId]
    };
    const { rows } = await db.query(query);
    rsps.result = { timeline: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.post('/dateEvent', async (req, res) => {
  let rsps = new response();
  logger.info('req.body: ' + JSON.stringify(req.body));
  const {
    date,
    userId,
    checkPv,
    checkBuy,
    checkSell,
    checkBill,
    checkRepay,
    checkCashout,
    checkCashin
  } = req.body;
  try {
    const query = {
      text: `
        SELECT * FROM (
          SELECT 'pv' as event, date FROM "eventByPV" WHERE date=$1 AND $3
          UNION ALL
          SELECT 'buy' as event, A.datetime::date as date FROM trades A
            LEFT JOIN "orderBuys" B ON B.id = A."orderBuyId"
            WHERE A.datetime::date=$1 AND B."userId"=$2 AND $4
          UNION ALL
          SELECT 'sell' as event, A.datetime::date as date FROM trades A
            LEFT JOIN "orderSells" B ON B.id = A."orderSellId"
            WHERE A.datetime::date=$1 AND B."userId"=$2 AND $5
          UNION ALL
          SELECT 'bill' as event, date FROM "eventByBill" WHERE date=$1 AND "userId"=$2 AND $6
          UNION ALL
          SELECT 'repay' as event, date FROM "eventByRepay" WHERE date=$1 AND "userId"=$2 AND $7
          UNION ALL
          SELECT 'cashout' as event, date FROM "cashoutRecords" WHERE date=$1 AND "userId"=$2 AND $8
          UNION ALL
          SELECT 'cashin' as event, date FROM "cashinRecords" WHERE date=$1 AND "userId"=$2 AND $9
        ) T GROUP BY event, date`,
      values: [
        date,
        userId,
        checkPv,
        checkBuy,
        checkSell,
        checkBill,
        checkRepay,
        checkCashout,
        checkCashin
      ]
    };
    const { rows } = await db.query(query);
    rsps.result = { dateEvent: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err, stack);
    res.status(500).send(rsps.getResult());
  }
});

router.post('/eventData/pv', async (req, res) => {
  let rsps = new response();
  logger.info('req.body: ' + JSON.stringify(req.body));
  const { date } = req.body;
  try {
    const query = {
      text: `
      SELECT A."pvId", B.name AS "pvName", A.name, A.content FROM "eventByPV" A
      LEFT JOIN pvs B ON B.id=A."pvId"
      WHERE date = $1`,
      values: [date]
    };
    const { rows } = await db.query(query);
    rsps.result = { eventData: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.post('/eventData/cashin', async (req, res) => {
  let rsps = new response();
  logger.info('req.body: ' + JSON.stringify(req.body));
  const { date, userId } = req.body;
  try {
    const query = {
      text: `SELECT amount FROM "cashinRecords" WHERE date=$1 AND "userId"=$2`,
      values: [date, userId]
    };
    const { rows } = await db.query(query);
    rsps.result = { eventData: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.post('/eventData/cashout', async (req, res) => {
  let rsps = new response();
  logger.info('req.body: ' + JSON.stringify(req.body));
  const { date, userId } = req.body;
  try {
    const query = {
      text: `SELECT amount, status FROM "cashoutRecords" WHERE date=$1 AND "userId"=$2`,
      values: [date, userId]
    };
    const { rows } = await db.query(query);
    rsps.result = { eventData: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.post('/eventData/bill', async (req, res) => {
  let rsps = new response();
  logger.info('req.body: ' + JSON.stringify(req.body));
  const { date, userId } = req.body;
  try {
    const query = {
      text: `SELECT SUM(profit) as profit FROM "eventByBill" WHERE date=$1 AND "userId"=$2 GROUP BY date`,
      values: [date, userId]
    };
    const { rows } = await db.query(query);
    rsps.result = { eventData: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.post('/eventData/repay', async (req, res) => {
  let rsps = new response();
  logger.info('req.body: ' + JSON.stringify(req.body));
  const { date, userId } = req.body;
  try {
    const query = {
      text: `SELECT SUM(repay) as repay FROM "eventByRepay" WHERE date=$1 AND "userId"=$2 GROUP BY date`,
      values: [date, userId]
    };
    const { rows } = await db.query(query);
    rsps.result = { eventData: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.post('/eventData/buy', async (req, res) => {
  let rsps = new response();
  logger.info('req.body: ' + JSON.stringify(req.body));
  const { date, userId } = req.body;
  try {
    const query = {
      text: `
          SELECT B."pvId", C.name, A.datetime, A.price, A.amount FROM trades A
          LEFT JOIN "orderBuys" B ON B.id = A."orderBuyId"
          LEFT JOIN pvs C ON B."pvId" = C.id
          WHERE A.datetime::date=$1 AND B."userId"=$2`,
      values: [date, userId]
    };
    const { rows } = await db.query(query);
    rsps.result = { eventData: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.post('/eventData/sell', async (req, res) => {
  let rsps = new response();
  logger.info('req.body: ' + JSON.stringify(req.body));
  const { date, userId } = req.body;
  try {
    const query = {
      text: `
          SELECT B."pvId", C.name, A.datetime, A.price, A.amount FROM trades A
          LEFT JOIN "orderSells" B ON B.id = A."orderSellId"
          LEFT JOIN pvs C ON B."pvId" = C.id
          WHERE A.datetime::date=$1 AND B."userId"=$2`,
      values: [date, userId]
    };
    const { rows } = await db.query(query);
    rsps.result = { eventData: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/lastTrades/:userId', async (req, res) => {
  let rsps = new response();
  logger.info('req params user id: ' + req.params.userId);
  try {
    const query = {
      text: `
          SELECT A.*, B.name FROM
            (SELECT '買' as side, B."pvId", A.datetime, A.price, A.amount FROM trades A
            LEFT JOIN "orderBuys" B ON B.id = A."orderBuyId"
            WHERE B."userId"=$1
            UNION ALL
            SELECT '賣' as side, B."pvId", A.datetime, A.price, A.amount FROM trades A
            LEFT JOIN "orderSells" B ON B.id = A."orderSellId"
            WHERE B."userId"=$1) A
          LEFT JOIN pvs B ON B.id=A."pvId"
          ORDER BY datetime DESC LIMIT 20`,
      values: [req.params.userId]
    };
    const { rows } = await db.query(query);
    rsps.result = { lastTrades: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/lastMarketTrades', async (req, res) => {
  let rsps = new response();
  try {
    const query = {
      text: `
          SELECT B."pvId", C.name, A.price, A.amount FROM trades A
          LEFT JOIN "orderBuys" B ON B.id = A."orderBuyId"
          LEFT JOIN pvs C ON C.id=B."pvId"
          ORDER BY A.datetime DESC LIMIT 20`
    };
    const { rows } = await db.query(query);
    rsps.result = { lastTrades: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/favoritePv/:userId', async (req, res) => {
  let rsps = new response();
  logger.info('req params user id: ' + req.params.userId);
  try {
    const query = {
      text: `
          SELECT A."pvId", B.name, COALESCE(C.price, B."initialPrice") as price FROM "userAssets" A
          LEFT JOIN pvs B ON B.id=A."pvId"
          LEFT JOIN "marketInfo" C ON C."pvId"=A."pvId"
          WHERE A."userId"=$1
          ORDER BY A."pvId"`,
      values: [req.params.userId]
    };
    const { rows } = await db.query(query);
    rsps.result = { favoritePv: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/hotPv', async (req, res) => {
  let rsps = new response();
  try {
    const query = {
      text: `
      SELECT A.id as "pvId", A.name, COALESCE(B.price, A."initialPrice") as price  FROM pvs A
      LEFT JOIN "marketInfo" B ON B."pvId"=A.id
      ORDER BY A.id`
    };
    const { rows } = await db.query(query);
    rsps.result = { hotPv: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

module.exports = router;
