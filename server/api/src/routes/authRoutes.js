var express = require('express');
var router = express.Router();
const passport = require('passport');
const urls = require('../config/urls');
const path = require('path');
const { sendResetPwdMail } = require('../modules/mail');
const response = require('../modules/rsps');
const log4js = require('../modules/logger');
const logger = log4js.getLogger();
router.use(function(req, res, next) {
  logger.addContext('reqId', req.reqId);
  next();
});
const { findManagerByEmail, findUsersByManager, verifyPassword, resetPassword } = require('../sqliteDb');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const keys = require('../config/keys');

router.post('/local', async (req, res) => {
  let rsps = new response();
  try {
    if (req.body.email === undefined || req.body.pwd === undefined) {
      rsps.msg = 'invalid parameters';
      logger.warn(`invalid parameters, email|pwd is undefined`);
    } else {
      logger.info(`req manager's email = ${req.body.email}`);
      const existingManager = await findManagerByEmail(req.body.email);
      if (!_.isEmpty(existingManager)) {
        const pass = await verifyPassword(req.body.email, req.body.pwd);
        if (!pass) {
          rsps.msg = '帳號或密碼不正確';
          logger.warn('email exist, but pwd is incorrect');
        } else {
          const users = await findUsersByManager(existingManager[0].id);
          rsps.status = true;
          rsps.result = {
            manager: existingManager[0],
            users: users,
            token: jwt.sign({ id: existingManager[0].id, users: users }, keys.jwtSecret)
          };
        }
      } else {
        rsps.msg = '帳號或密碼不正確';
        logger.info('unregister manager');
      }
    }
    res.send(rsps.getResult());
  } catch (error) {
    logger.error(error.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.post('/resetPwd', async (req, res) => {
  let rsps = new response();
  try {
    if (req.body.email === undefined) {
      rsps.msg = 'invalid parameters';
      logger.warn(`invalid parameters, email undefined`);
    } else {
      logger.info(`req manager's email = ${req.body.email}`);
      const existingManager = await findManagerByEmail(req.body.email);
      if (!_.isEmpty(existingManager)) {
        const pwd = await resetPassword(existingManager[0].email);
        if (pwd) {
          rsps.status = true;
          await sendResetPwdMail(existingManager[0].email, existingManager[0].name, pwd);
        } else {
          rsps.msg = '重設密碼失敗';
          logger.info('reset password fail');
        }
      } else {
        rsps.msg = '無此帳號';
        logger.info('unregister manager');
      }
    }
    res.send(rsps.getResult());
  } catch (error) {
    logger.error(error.stack);
    res.status(500).send(rsps.getResult());
  }
});

module.exports = router;
