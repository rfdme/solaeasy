const express = require('express');
const router = express.Router();
const db = require('../db');
const { canAccessUser } = require('../modules/accessController');
const response = require('../modules/rsps');
const log4js = require('../modules/logger');
const logger = log4js.getLogger();
router.use(function(req, res, next) {
  logger.addContext('reqId', req.reqId);
  next();
});

router.get('/order/:id', async (req, res) => {
  let rsps = new response();
  logger.info('req params user id: ' + req.params.id);
  try {
    if (!canAccessUser(req.user, parseInt(req.params.id, 10))) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access user id= ${
          req.params.id
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    const query = {
      text: `
          SELECT * FROM
            (SELECT '買' as side, id, "pvId", datetime, price, filled, amount, status FROM "orderBuys"
             WHERE "userId"=$1
             UNION ALL
             SELECT '賣' as side, id, "pvId", datetime, price, filled, amount, status FROM "orderSells"
             WHERE "userId"=$1) A
          ORDER BY datetime DESC`,
      values: [req.params.id]
    };
    const { rows } = await db.query(query);
    rsps.result = { orderList: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/trade/:id', async (req, res) => {
  let rsps = new response();
  logger.info('req params user id: ' + req.params.id);
  try {
    if (!canAccessUser(req.user, parseInt(req.params.id, 10))) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access user id= ${
          req.params.id
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    const query = {
      text: `
          SELECT * FROM
            (SELECT '買' as side, B."pvId", A.datetime, A.price, A.amount, A.fee, A."oldAmountBuyer" AS "oldAmount", A."newAmountBuyer" AS "newAmount", A."oldSttwBuyer" AS "oldSttw", A."newSttwBuyer" AS "newSttw", A."blockInfo" FROM trades A
            LEFT JOIN "orderBuys" B ON B.id = A."orderBuyId"
            WHERE B."userId"=$1
            UNION ALL
            SELECT '賣' as side, B."pvId", A.datetime, A.price, A.amount, A.fee, A."oldAmountSeller" AS "oldAmount", A."newAmountSeller" AS "newAmount", A."oldSttwSeller" AS "oldSttw", A."newSttwSeller" AS "newSttw", A."blockInfo" FROM trades A
            LEFT JOIN "orderSells" B ON B.id = A."orderSellId"
            WHERE B."userId"=$1) A
          ORDER BY datetime DESC`,
      values: [req.params.id]
    };
    const { rows } = await db.query(query);
    rsps.result = { tradeList: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/cashin/:id', async (req, res) => {
  let rsps = new response();
  logger.info('req params user id: ' + req.params.id);
  try {
    if (!canAccessUser(req.user, parseInt(req.params.id, 10))) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access user id= ${
          req.params.id
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    const query = {
      text: `SELECT date, amount, "oldSttw", "newSttw", "blockInfo" FROM "cashinRecords" WHERE "userId"=$1 ORDER BY date DESC`,
      values: [req.params.id]
    };
    const { rows } = await db.query(query);
    rsps.result = { cashinList: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/cashout/:id', async (req, res) => {
  let rsps = new response();
  logger.info('req params user id: ' + req.params.id);
  try {
    if (!canAccessUser(req.user, parseInt(req.params.id, 10))) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access user id= ${
          req.params.id
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    const query = {
      text: `SELECT date, amount, status, "oldSttw", "newSttw", "blockInfo" FROM "cashoutRecords" WHERE "userId"=$1 ORDER BY date DESC`,
      values: [req.params.id]
    };
    const { rows } = await db.query(query);
    rsps.result = { cashoutList: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/profit/:id', async (req, res) => {
  let rsps = new response();
  logger.info('req params user id: ' + req.params.id);
  try {
    if (!canAccessUser(req.user, parseInt(req.params.id, 10))) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access user id= ${
          req.params.id
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    const query = {
      text: `
        SELECT A.date, B."pvId", B."rangeBegin", B."rangeEnd", A.profit, A."oldSttw", A."newSttw", A."blockInfo" FROM "eventByBill" A
        LEFT JOIN bills B ON B.id = A."billId"
        WHERE "userId"=$1 ORDER BY date DESC, "pvId"`,
      values: [req.params.id]
    };
    const { rows } = await db.query(query);
    rsps.result = { profitList: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

router.get('/repay/:id', async (req, res) => {
  let rsps = new response();
  logger.info('req params user id: ' + req.params.id);
  try {
    if (!canAccessUser(req.user, parseInt(req.params.id, 10))) {
      logger.warn(
        `403 Forbidden. current manager id= ${req.user.id}, trying to access user id= ${
          req.params.id
        } 's account data`
      );
      res.status(403).send(rsps.getResult());
      return;
    }
    const query = {
      text: `
        SELECT date, "pvId", "oldAmount", "newAmount", "oldSttw", "newSttw", repay, "blockInfo" FROM "eventByRepay"
        WHERE "userId"=$1 ORDER BY date DESC, "pvId"`,
      values: [req.params.id]
    };
    const { rows } = await db.query(query);
    rsps.result = { repayList: rows };
    rsps.status = true;
    res.send(rsps.getResult());
  } catch (err) {
    logger.error(err.stack);
    res.status(500).send(rsps.getResult());
  }
});

module.exports = router;
