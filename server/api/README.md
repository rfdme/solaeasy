# SOLA EASY - 太陽能交易平台

 提供太陽能資產管理與交易服務。

## 環境參數

可參考 server/api/src/config/prod.js

- COOKIEKEY: 加密 Cookie 的金鑰
- JWT_SECRET: 加密 JWT 的金鑰
- POSTGRESQL: 資料庫 URI
- SALT_ROUNDS: 使用者密碼使用的參數，預設為 10
- LOG_FILE: log 檔名, default: spv.log
- LOG_PATH: log 檔案路徑, default: ./logs
- LOG_LEVEL: log 的 level, default: INFO
- LOG_TYPE: log 的 type, file - 寫進檔案, stdout - 螢幕輸出, default: file
- ASSET_URL: SOLA Exchange 網址(含 port)。(e.g. http://localhost:8081)
- ITS_URL: Internal Service 網址(含 port)。(e.g. http://localhost:8080)
- API_URL: api server ip
- PORT: api server port

## 指令

執行 app

```
node src/server.js
```
