#!/bin/bash
########################################################
# $1: image tag name
# $2: docker registry name
########################################################

[ "$#" -lt 2 ] && echo "2 parameters are required" && exit 256

source prepare-b4-deploy.sh
ansible-vault decrypt --vault-password-file=ansible.vault.se inventories/prod/vault_files/prod.solase

IMAGE_TAG="$1"
DOCKER_REGISTRY="$2"

TMP_ENV={\"__docker_registry\":\"$DOCKER_REGISTRY\",\"__docker_registry_user\":\"$DOCKER_USERNAME\",\"__docker_registry_pwd\":\"$DOCKER_PASSWORD\",\"__image_tag\":\"$IMAGE_TAG\"}

IS_AUTO=1
[[ "$IMAGE_TAG" == stable-man-* ]] && \
echo "Expect to deploy manually." && \
echo "" && echo "" && echo "" && echo "" && \
IS_AUTO=0

ansible-galaxy install nickhammond.logrotate
ansible-playbook chmod_key.yml --vault-id ansible.vault.se
ansible-playbook prerequisite.yml -i inventories/prod/tokyo --vault-id ansible.vault.se
ansible-playbook fetch-env.yml -i inventories/prod/tokyo --vault-id ansible.vault.se
# ansible-playbook provision-maintenance.yml -i inventories/prod/tokyo -e "$TMP_ENV" --vault-id ansible.vault.se
# ansible-playbook provision-nginx-mt.yml -i inventories/prod/tokyo --vault-id ansible.vault.se
# sleep 3
# ansible-playbook provision-schema.yml -i inventories/prod/tokyo -e "$TMP_ENV" --vault-id ansible.vault.se
# ansible-playbook provision-websocket.yml -i inventories/prod/tokyo -e "$TMP_ENV" --vault-id ansible.vault.se
ansible-playbook provision-regular.yml -i inventories/prod/tokyo -e "$TMP_ENV" --vault-id ansible.vault.se

# if [ "$IS_AUTO" == "1" ]; then 
#     echo "Sleep until npm build finished"
#     sleep 180
#     ansible-playbook provision-letsencrypt.yml -i inventories/prod/tokyo --vault-id ansible.vault.se
#     ansible-playbook provision-nginx.yml -i inventories/prod/tokyo --vault-id ansible.vault.se
# fi