import React from 'react';
import styled from 'styled-components';
import LineSvg from '../../svg/line_step.png';
import DefaultPoint from '../../svg/icon_menu_default.svg';
import YellowPoint from '../../svg/icon_menu_yellow.svg';
import YellowHoverPoint from '../../svg/icon_menu_yellow_hover.svg';
import { Lightgrey, White, Black } from '../CommonColor';

const PvStage = ({ stage, handleClick }) => {
  console.log(window);
  const Height = window.innerHeight - 130;
  const StepCount = 8;
  const MarginTop = 30;
  const MarginBottom = 30;
  const LineHeight = Height - MarginTop - MarginBottom;
  const StepHeight = LineHeight / (StepCount - 1);

  const stages = [
    '簽訂租約',
    '簽訂工程合約',
    '取得併聯審查意見書',
    '取得同意備案函',
    '完工併聯',
    '取得銀行貸款撥款',
    '能源局設備登記',
    '建置完成'
  ];

  let points = [];
  let steps = [];
  const currentStep = stage - 1;
  let imgSrc, textStyle, pointStyle;
  for (let i = 0; i < StepCount; i++) {
    if (i < currentStep) {
      imgSrc = YellowPoint;
      textStyle = {
        color: Lightgrey
      };
      pointStyle = {
        width: '20px',
        height: '20px'
      };
    } else if (i === currentStep) {
      imgSrc = YellowHoverPoint;
      textStyle = {
        color: White
      };
      pointStyle = {
        width: '40px',
        height: '40px'
      };
    } else {
      imgSrc = DefaultPoint;
      textStyle = {
        color: Black,
        fontWeight: 'bold'
      };
      pointStyle = {
        width: '20px',
        height: '20px'
      };
    }
    points.push(
      <Point
        key={i}
        src={imgSrc}
        y={i * StepHeight + MarginTop}
        style={pointStyle}
      />
    );

    if (i % 2 === 0) {
      steps.push(
        <RightStep
          key={i}
          y={i * StepHeight + MarginTop}
          style={textStyle}
          onClick={i <= currentStep ? () => handleClick(i) : null}
          clickable={i <= currentStep ? true : false}
        >
          <StepLabel>Step {i + 1}</StepLabel>
          {stages[i]}
        </RightStep>
      );
    } else {
      steps.push(
        <LeftStep
          key={i}
          y={i * StepHeight + MarginTop}
          style={textStyle}
          onClick={i <= currentStep ? () => handleClick(i) : null}
          clickable={i <= currentStep ? true : false}
        >
          <StepLabel>Step {i + 1}</StepLabel>
          {stages[i]}
        </LeftStep>
      );
    }
  }
  return (
    <Container height={Height}>
      <DefalutLine top={MarginTop} height={LineHeight} />
      <Line
        src={LineSvg}
        top={MarginTop}
        y={currentStep > 0 ? currentStep * StepHeight : 0}
      />
      {points}
      {steps}
    </Container>
  );
};

const Container = styled.div`
  position: relative;
  width: 100%;
  height: ${props => props.height + 'px'};
`;

const DefalutLine = styled.div`
  position: absolute;
  top: ${props => props.top + 'px'};
  height: ${props => props.height + 'px'};
  left: 50%;
  width: 5px;
  background-color: ${Black};
  transform: translateX(-50%);
`;

const Line = styled.img`
  position: absolute;
  top: ${props => props.top + 'px'};
  height: ${props => props.y + 'px'};
  left: 50%;
  width: 5px;
  transform: translateX(-50%);
`;

const Point = styled.img`
  position: absolute;
  top: ${props => props.y + 'px'};
  left: 50%;
  transform: translate(-50%, -50%);
`;

const RightStep = styled.div`
  position: absolute;
  width: 40%;
  left: 58%;
  top: ${props => props.y + 'px'};
  transform: translateY(-10px);
  font-size: 18px;
  text-align: left;
  cursor: ${props => (props.clickable ? 'pointer' : null)};
`;

const LeftStep = styled.div`
  position: absolute;
  width: 40%;
  right: 58%;
  top: ${props => props.y + 'px'};
  transform: translateY(-10px);
  font-size: 18px;
  text-align: right;
  cursor: ${props => (props.clickable ? 'pointer' : null)};
`;

const StepLabel = styled.div`
  margin-bottom: 0px;
`;

export default PvStage;
