import React from 'react';
import styled from 'styled-components';

import { Darkgrey, Lightgrey, HrColor } from '../CommonColor';

const PvData = () => (
  <Table>
    <Row>
      <Col1>電廠代號</Col1>
      <Col2>SNTW1</Col2>
    </Row>
    <Row>
      <Col1>專案名稱</Col1>
      <Col2>鹽水牛舍</Col2>
    </Row>
    <Row>
      <Col1>裝置容量</Col1>
      <Col2>0.275瓩</Col2>
    </Row>
    <Row>
      <Col1>建置日期</Col1>
      <Col2>2016-12-31</Col2>
    </Row>
    <Row>
      <Col1>裝置數量</Col1>
      <Col2>360</Col2>
    </Row>
    <Row>
      <Col1>狀態</Col1>
      <Col2>發電中</Col2>
    </Row>
    <Row>
      <Col1>地號</Col1>
      <Col2>TODO</Col2>
    </Row>
    <Row>
      <Col1>總裝置容量</Col1>
      <Col2>99瓩</Col2>
    </Row>
    <Row>
      <Col1>使用能源</Col1>
      <Col2>多晶矽太陽能板</Col2>
    </Row>
    <Row>
      <Col1>發電年衰減率</Col1>
      <Col2>0.007</Col2>
    </Row>
  </Table>
);

const Table = styled.div`
  padding: 0 20px;
`;

const Row = styled.div`
  height: 50px;
  border-bottom: 1px solid ${HrColor};
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-size: 15px;
`;

const Col1 = styled.div`
  color: ${Darkgrey};
`;

const Col2 = styled.div`
  color: ${Lightgrey};
`;

export default PvData;
