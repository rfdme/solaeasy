import React from 'react';
import styled from 'styled-components';
import { compose, withProps } from 'recompose';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker
} from 'react-google-maps';
import testView from './testView.jpg';

const MapComponent = compose(
  withProps({
    googleMapURL:
      'https://maps.google.com/maps/api/js?key=AIzaSyCQ3UTayHzAcoPXJUM3Ag_-FBxII1o6fNg',
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: window.innerHeight / 2 }} />,
    mapElement: <div style={{ height: `100%` }} />
  }),
  withScriptjs,
  withGoogleMap
)(props => (
  <GoogleMap
    defaultZoom={7}
    defaultCenter={{ lat: 23.6, lng: 121 }}
    mapTypeId="satellite"
    options={{
      mapTypeControl: false,
      fullscreenControl: false,
      streetViewControl: false,
      gestureHandling: 'cooperative'
    }}
    center={props.position}
    zoom={19}
  >
    {props.isMarkerShown && <Marker position={props.position} />}
  </GoogleMap>
));

const PvMap = props => (
  <div>
    <MapComponent {...props} />
    <Img src={testView} />
  </div>
);

const Img = styled.img`
  margin-top: 10px;
  width: 100%;
`;

export default PvMap;
