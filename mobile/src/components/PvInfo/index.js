import React, { Component } from 'react';
import styled from 'styled-components';
import moment from 'moment';

import { Container, Paper } from '../CommonLayout';
import {
  Darkgrey,
  Lightgrey,
  Yellow,
  Green,
  BgColor,
  TabColor
} from '../CommonColor';
import SttwSvg from '../../svg/icon_sttw.svg';
import PvData from './PvData';
import PvStage from './PvStage';
import PvChart from './PvChart';
import PvView from './PvView';

class PvInfo extends Component {
  constructor(props) {
    super(props);

    this.state = {
      seletedTab: 'data'
    };
  }

  selectPv = id => {
    this.props.selectPv(id);
  };

  selectTab = tab => {
    this.setState({ seletedTab: tab });
  };

  openStage = index => {
    console.log('openStage', index);
  };

  render() {
    const { seletedTab } = this.state;
    let tabContent = null;
    switch (this.state.seletedTab) {
      case 'data':
        tabContent = <PvData />;
        break;
      case 'stage':
        tabContent = <PvStage stage={4} handleClick={this.openStage} />;
        break;
      case 'chart':
        let powerData = {
          labels: [],
          values: []
        };
        let date = moment();
        for (let i = 0; i < 100; i++) {
          powerData.labels.push(date.format('YYYY-MM-DD'));
          powerData.values.push(parseInt(Math.random() * 1000, 10));
          date = date.subtract(1, 'days');
        }
        tabContent = <PvChart powerData={powerData} />;
        break;
      case 'view':
        tabContent = (
          <PvView
            position={{ lat: 23.25017, lng: 120.246349 }}
            isMarkerShown={true}
          />
        );
        break;
      default:
        break;
    }

    return (
      <Container>
        <Paper>
          <Header>
            <Name>SNTW1</Name>
            <Price>
              <Unit src={SttwSvg} />
              <Value>13.7</Value>
            </Price>
          </Header>
          <Tabs>
            <Tab>
              <Button
                selected={seletedTab === 'data' ? true : false}
                onClick={() => this.selectTab('data')}
              >
                電廠資料
              </Button>
            </Tab>
            <Tab>
              <Button
                selected={seletedTab === 'stage' ? true : false}
                onClick={() => this.selectTab('stage')}
              >
                建置時程
              </Button>
            </Tab>
            <Tab>
              <Button
                selected={seletedTab === 'chart' ? true : false}
                onClick={() => this.selectTab('chart')}
              >
                發電數據
              </Button>
            </Tab>
            <Tab>
              <Button
                selected={seletedTab === 'view' ? true : false}
                onClick={() => this.selectTab('view')}
              >
                電廠實景
              </Button>
            </Tab>
          </Tabs>
          {tabContent}
        </Paper>
      </Container>
    );
  }
}

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 5px 10px;
`;

const Name = styled.div`
  font-size: 20px;
  color: ${Lightgrey};
`;

const Price = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
`;

const Unit = styled.img`
  width: 45px;
  margin-top: 10px;
  margin-right: 10px;
`;

const Value = styled.span`
  font-size: 35px;
  color: ${Green};
  font-family: Montserrat;
`;

const Tabs = styled.div`
  display: flex;
  align-items: center;
  height: 50px;
  border-top: 2px solid ${BgColor};
  border-bottom: 2px solid ${BgColor};
  padding: 0 5px;
  font-size: 15px;
  background-color: ${TabColor};
`;

const Tab = styled.div`
  width: 25%;
  display: flex;
  justify-content: center;
`;

const Button = styled.div`
  color: ${props => (props.selected ? Yellow : Darkgrey)};
  border: 1px solid ${props => (props.selected ? Yellow : Darkgrey)};
  border-radius: 20px;
  padding: 0 10px;
`;

export default PvInfo;
