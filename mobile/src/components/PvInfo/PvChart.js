import React from 'react';
import { HorizontalBar } from 'react-chartjs-2';

const PvChart = ({ powerData }) => {
  const data = {
    labels: powerData.labels,
    datasets: [
      {
        label: '發電量(瓩)',
        backgroundColor: '#FFC107',
        borderColor: '#FFF9C4',
        borderWidth: 1,
        hoverBackgroundColor: '#FFF9C4',
        hoverBorderColor: '#FFC107',
        data: powerData.values
      }
    ]
  };

  return (
    <div>
      <HorizontalBar
        data={data}
        height={powerData.labels.length * 15}
        options={{
          maintainAspectRatio: false,
          scales: {
            xAxes: [
              {
                position: 'top'
              }
            ]
          }
        }}
      />
    </div>
  );
};

export default PvChart;
