import React from 'react';
import styled from 'styled-components';

import { Lightgrey, HrColor } from '../CommonColor';

const Item = ({ id, name, price, diff, onClick }) => (
  <Div onClick={onClick}>
    <Title>{id + ' ' + name}</Title>
    <Row style={{ margin: '22px 0 19px' }}>
      <Value>{price}</Value>
    </Row>
    <Row>
      <Value>{diff}</Value>
    </Row>
  </Div>
);

const Div = styled.div`
  margin: 15px 15px 30px 15px;
  border-bottom: 1px solid ${HrColor};
`;

const Title = styled.div`
  font-size: 20px;
  color: ${Lightgrey};
  margin-bottom: 22px;
`;

const Row = styled.div`
  font-size: 15px;
  display: flex;
  justify-content: space-between;
`;

const Value = styled.div`
  color: ${Lightgrey};
`;

export default Item;
