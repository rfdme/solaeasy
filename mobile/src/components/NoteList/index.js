import React, { Component } from 'react';
import styled from 'styled-components';

import { Container, Paper } from '../CommonLayout';
import { Yellow, Darkgrey, BgColor } from '../CommonColor';
import Item from './Item';

class NoteList extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  selectPv = id => {
    this.props.onSelect(id);
  };

  render() {
    return (
      <Container>
        <Paper>
          <Tabs>
            <Tab selected={true}>您關注的電廠</Tab>
            <Tab>所有電廠</Tab>
          </Tabs>
          <Item
            id="SNTW1"
            name="金耘鋼鐵"
            price={10.2}
            diff={3.867}
            onClick={() => this.selectPv('SNTW1')}
          />
          <Item
            id="SNTW2"
            name="鹽水牛舍"
            price={13.7}
            diff={-1.1425}
            onClick={() => this.selectPv('SNTW2')}
          />
          <Item
            id="SNTW3"
            name="光田養護中心"
            price={10.5}
            diff={-0.8425}
            onClick={() => this.selectPv('SNTW3')}
          />
        </Paper>
      </Container>
    );
  }
}

const Tabs = styled.div`
  display: flex;
  align-items: center;
  height: 50px;
  border-bottom: 3px solid ${BgColor};
  font-size: 15px;
`;

const Tab = styled.div`
  color: ${props => (props.selected ? Yellow : Darkgrey)};
  border: 1px solid ${props => (props.selected ? Yellow : Darkgrey)};
  border-radius: 20px;
  padding: 0 15px;
  margin-left: 15px;
`;

export default NoteList;
