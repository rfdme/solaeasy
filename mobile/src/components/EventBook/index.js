import React, { Component, Fragment } from 'react';
import styled from 'styled-components';
import moment from 'moment';
import MuiPickersUtilsProvider from 'material-ui-pickers/utils/MuiPickersUtilsProvider';
import MomentUtils from 'material-ui-pickers/utils/moment-utils';
import DatePicker from 'material-ui-pickers/DatePicker';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core';
import 'moment/locale/zh-tw';

import { Container, Paper } from '../CommonLayout';
import { Lightgrey, Darkgrey, BgColor, Green } from '../CommonColor';
import Day from './Day';
import Detail from './Detail';
import DateIcon from '../../svg/icon_calendar.svg';

class EventBook extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedDate: '2018-08-03'
    };
  }

  handleDateClick = date => {
    this.setState({ selectedDate: date });
  };

  handlePrevMonth = () => {
    let d = moment(this.state.selectedDate, 'YYYY-MM-DD');
    d.subtract(1, 'months');
    this.setState({ selectedDate: d.format('YYYY-MM-DD') });
  };

  handleNextMonth = () => {
    let d = moment(this.state.selectedDate, 'YYYY-MM-DD');
    d.add(1, 'months');
    this.setState({ selectedDate: d.format('YYYY-MM-DD') });
  };

  handleSelectDate = date => {
    this.setState({ selectedDate: date.format('YYYY-MM-DD') });
  };

  openPicker = () => {
    this.picker.open();
  };

  render() {
    const { selectedDate } = this.state;
    let beginDate = moment(selectedDate, 'YYYY-MM-DD').startOf('month');
    let endDate = moment(selectedDate, 'YYYY-MM-DD').endOf('month');
    const prevMonthDayCount = beginDate.isoWeekday() - 1;
    const nextMonthDayCount = 7 - endDate.isoWeekday();
    beginDate.subtract(prevMonthDayCount, 'days');
    endDate.add(nextMonthDayCount, 'days');

    const count = endDate.diff(beginDate, 'days') + 1;
    let i = 0;
    let lastWeek = -1;
    let weeks = [];
    let days;
    let day = beginDate;

    weeks.push(
      <Row key={-1}>
        <Weekday>MON</Weekday>
        <Weekday>TUE</Weekday>
        <Weekday>WED</Weekday>
        <Weekday>THU</Weekday>
        <Weekday>FRI</Weekday>
        <Weekday>SAT</Weekday>
        <Weekday>SUN</Weekday>
      </Row>
    );

    const points = ['red', 'green', 'blue', 'double'];
    while (i < count) {
      const curWeek = Math.floor(i / 7);
      if (curWeek !== lastWeek) {
        lastWeek = curWeek;
        days = [];
        weeks.push(<Row key={curWeek}>{days}</Row>);
      }

      days.push(
        <Day
          key={i}
          date={day.format('YYYY-MM-DD')}
          selected={day.format('YYYY-MM-DD') === selectedDate}
          point={points[Math.floor(Math.random() * points.length)]}
          sameMonth={day.format('YYYY-MM') === selectedDate.slice(0, 7)}
          onClick={this.handleDateClick}
        />
      );
      day.add(1, 'days');
      i++;
    }

    const theme = createMuiTheme({
      palette: {
        type: 'dark'
      }
    });
    return (
      <Container>
        <Paper>
          <Bar>
            <Button onClick={this.handlePrevMonth}>上個月</Button>
            <PickContainer>
              <MuiThemeProvider theme={theme}>
                <MuiPickersUtilsProvider utils={MomentUtils}>
                  <DatePicker
                    showTodayButton
                    ref={node => {
                      this.picker = node;
                    }}
                    autoOk={true}
                    value={this.state.selectedDate}
                    onChange={this.handleSelectDate}
                    format="YYYY-MM-DD"
                    okLabel="確認"
                    cancelLabel="取消"
                    todayLabel="今天"
                    TextFieldComponent={() => (
                      <Fragment>
                        <Icon src={DateIcon} />
                        <span onClick={this.openPicker}>
                          {this.state.selectedDate}
                        </span>
                      </Fragment>
                    )}
                  />
                </MuiPickersUtilsProvider>
              </MuiThemeProvider>
            </PickContainer>
            <Button onClick={this.handleNextMonth}>下個月</Button>
          </Bar>
          <Content>
            <Calendar>{weeks}</Calendar>
            <Detail time="2018-05-11" msg="返回本金11,064.4元" color={Green} />
          </Content>
        </Paper>
      </Container>
    );
  }
}

const Bar = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 50px;
  border-bottom: 3px solid ${BgColor};
  padding: 0 20px;
`;

const Button = styled.div`
  border: 1px solid ${Darkgrey};
  border-radius: 30px;
  padding: 2px 15px;
  line-height: 1.2;
  color: ${Darkgrey};
`;

const Icon = styled.img`
  height: 20px;
  margin-right: 5px;
`;

const PickContainer = styled.div`
  width: 50%;
  display: flex;
  justify-content: center;
  line-height: 20px;
  font-size: 18px;
  color: ${Darkgrey};
`;

const Content = styled.div`
  padding: 0 20px;
`;

const Calendar = styled.div`
  padding: 15px 0;
  font-family: Montserrat;
`;

const Row = styled.div`
  display: flex;
`;

const Weekday = styled.div`
  flex: 1;
  height: 50px;
  display: flex;
  justify-content: center;
  align-items: center;
  color: ${Lightgrey};
  font-size: 13px;
`;

export default EventBook;
