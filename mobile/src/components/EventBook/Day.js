import React from 'react';
import styled from 'styled-components';

import { Lightgrey } from '../CommonColor';

import GP from '../../svg/icon_calendar_green.svg';
import GPA from '../../svg/icon_calendar_green_a.svg';
import RP from '../../svg/icon_calendar_red.svg';
import RPA from '../../svg/icon_calendar_red_a.svg';
import BP from '../../svg/icon_calendar_blue.svg';
import BPA from '../../svg/icon_calendar_blue_a.svg';
import DP from '../../svg/icon_calendar_double.svg';
import DPA from '../../svg/icon_calendar_double_a.svg';

const Day = ({ date, selected, point, sameMonth, onClick }) => {
  let p = null;
  let pw = '6px';
  let c = null;

  switch (point) {
    case 'red':
      p = RP;
      c = RPA;
      break;
    case 'green':
      p = GP;
      c = GPA;
      break;
    case 'blue':
      p = BP;
      c = BPA;
      break;
    case 'double':
      p = DP;
      pw = '12px';
      c = DPA;
      break;
    default:
      break;
  }

  return (
    <Div sameMonth={sameMonth} onClick={() => onClick(date)}>
      <Value>{date.slice(8, 11)}</Value>
      {selected ? <Circle src={c} /> : <Point src={p} style={{ width: pw }} />}
    </Div>
  );
};

const Div = styled.div`
  flex: 1;
  height: 50px;
  position: relative;
  opacity: ${props => (props.sameMonth ? 1 : 0.3)};
`;

const Value = styled.div`
  position: absolute;
  color: ${Lightgrey};
  font-size: 15px;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

const Point = styled.img`
  position: absolute;
  top: 82%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

const Circle = styled.img`
  position: absolute;
  height: 87%;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

export default Day;
