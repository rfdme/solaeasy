import React from 'react';
import styled from 'styled-components';

import { Darkgrey, Lightgrey } from '../CommonColor';

const Detail = ({ time, msg, color }) => (
  <Div color={color}>
    <Time>{time}</Time>
    <Msg>{msg}</Msg>
  </Div>
);

const Div = styled.div`
  border-left: 2px solid ${props => props.color};
  min-height: 55px;
  background-color: #2b2e34;
  padding: 10px 0 10px 15px;
  margin-top: 10px;
`;

const Time = styled.div`
  font-size: 15px;
  color: ${Darkgrey};
  margin-bottom: 5px;
`;

const Msg = styled.div`
  font-size: 18px;
  color: ${Lightgrey};
`;

export default Detail;
