import React from 'react';
import styled from 'styled-components';

import back from '../../svg/icon_back.svg';
import logo from '../../svg/logo.svg';
import menu from '../../svg/icon_menu.svg';
import { Yellow, PaperColor } from '../CommonColor';

const Header = ({ menuClick, title, backClick }) => (
  <Div>
    <Left>{title ? <Back src={back} onClick={backClick} /> : null}</Left>
    {title ? <Center>{title}</Center> : <Logo src={logo} />}
    <Right>
      <Menu src={menu} onClick={menuClick} />
    </Right>
  </Div>
);

const Div = styled.div`
  height: 50px;
  background-color: ${PaperColor};
  display: flex;
  justify-content: space-between;
  align-items: center;
  color: ${Yellow};
`;

const Left = styled.div`
  width: 10%;
`;
const Back = styled.img`
  width: 12px;
  float: left;
  padding-left: 15px;
`;

const Center = styled.div`
  width: 80%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Logo = styled.img`
  width: 156px;
`;

const Right = styled.div`
  width: 10%;
`;

const Menu = styled.img`
  width: 18px;
  float: right;
  padding-right: 10px;
`;

export default Header;
