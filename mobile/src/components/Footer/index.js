import React from 'react';
import styled from 'styled-components';

import logo from '../../svg/logo_dw.svg';

const Footer = () => (
  <Div className="site-footer">
    <Content>
      <Logo src={logo} />
    </Content>
  </Div>
);

const Div = styled.div`
  min-height: 40px;
  height: 40px;
  display: flex;
  flex-direction: column;
`;

const Content = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Logo = styled.img`
  width: 126px;
`;

export default Footer;
