const Darkgrey = '#58626f';
const Lightgrey = '#929dac';
const Yellow = '#f7b100';
const Green = '#2bbc8f';
const White = '#ffffff';
const Black = '#000000';
const BgColor = '#151515';
const PaperColor = '#23262b';
const TabColor = '#1d1f23';
const HrColor = '#444649';

export {
  Darkgrey,
  Lightgrey,
  Yellow,
  Green,
  White,
  Black,
  BgColor,
  PaperColor,
  TabColor,
  HrColor
};
