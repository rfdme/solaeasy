import React, { Component } from 'react';
import styled from 'styled-components';

import { Container, Paper } from '../CommonLayout';
import { BgColor } from '../CommonColor';
import { Droplist } from '../Widget';
import Simulation from './Simulation';
import Realization from './Realization';

class Profit extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedIndex: 0
    };
  }

  handleSelect = index => {
    this.setState({ selectedIndex: index });
  };

  render() {
    const list = ['預估累積收益', '已實現累積收益'];
    let content = null;
    switch (this.state.selectedIndex) {
      case 0:
        content = <Simulation />;
        break;
      case 1:
        content = <Realization />;
        break;
      default:
        break;
    }

    return (
      <Container>
        <Paper>
          <Bar>
            <Droplist list={list} onSelect={this.handleSelect} />
          </Bar>
          {content}
        </Paper>
      </Container>
    );
  }
}

const Bar = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 50px;
  border-bottom: 3px solid ${BgColor};
  padding: 0 20px;
`;

export default Profit;
