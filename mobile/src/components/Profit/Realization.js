import React from 'react';

import { LineChart } from '../Chart';

const Realization = () => {
  const data = [
    {
      date: '2018-01-01',
      value: 100
    },
    {
      date: '2018-01-02',
      value: 105
    },
    {
      date: '2018-01-03',
      value: 120
    },
    {
      date: '2018-01-04',
      value: 130
    },
    {
      date: '2018-01-05',
      value: 125
    },
    {
      date: '2018-01-06',
      value: 130
    },
    {
      date: '2018-01-07',
      value: 110
    },
    {
      date: '2018-01-08',
      value: 120
    },
    {
      date: '2018-01-09',
      value: 125
    },
    {
      date: '2018-01-10',
      value: 110
    }
  ];
  const tick = Math.round(data.length / 4, 1);

  let labels = [];
  let values = [];
  let tickLabels = [];

  let accumulated = 0;
  for (let i = 0; i < data.length; i++) {
    const d = data[i];
    labels.push(d.date);
    accumulated = accumulated + d.value;

    values.push(accumulated.toFixed(1));
    if (i % tick === 0) {
      tickLabels.push(d.date);
    }
  }
  return (
    <div>
      {values.length > 0 ? (
        <LineChart
          height={'50vh'}
          width={'100%'}
          labels={labels}
          values={values}
          tickLabels={tickLabels}
        />
      ) : (
        <div>
          <label>尚無資料</label>
        </div>
      )}
    </div>
  );
};

export default Realization;
