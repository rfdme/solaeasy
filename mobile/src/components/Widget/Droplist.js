import React, { Component } from 'react';
import styled from 'styled-components';
import ContainerDimensions from 'react-container-dimensions';

import { Darkgrey, Yellow, Black } from '../CommonColor';
import AR from '../../svg/icon_filter_arrow_right.png';
import AD from '../../svg/icon_filter_arrow_down.png';

class Droplist extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedIndex: 0,
      open: false
    };
  }

  handleToggle = () => {
    this.setState({ open: !this.state.open });
  };

  handleSelect = index => {
    this.setState({ selectedIndex: index, open: false });
    this.props.onSelect(index);
  };

  render() {
    const { list } = this.props;

    let items = [];
    for (let i = 0; i < list.length; i++) {
      items.push(
        <Item
          key={i}
          first={i === 0}
          selected={i === this.state.selectedIndex}
          onClick={() => this.handleSelect(i)}
        >
          {list[i]}
        </Item>
      );
    }

    return (
      <Div>
        <SelectBox onClick={this.handleToggle}>
          {list[this.state.selectedIndex]}
          <Icon src={this.state.open ? AD : AR} />
        </SelectBox>
        {this.state.open ? (
          <ContainerDimensions>
            {({ width }) => <ListBox minWidth={width}>{items}</ListBox>}
          </ContainerDimensions>
        ) : null}
      </Div>
    );
  }
}

const Div = styled.div`
  position: relative;
  font-size: 15px;
`;

const SelectBox = styled.div`
  border: 1px solid ${Yellow};
  border-radius: 30px;
  color: ${Yellow};
  padding: 3px 10px 3px 20px;
  display: flex;
  align-items: center;
`;

const Icon = styled.img`
  margin-left: 20px;
`;

const ListBox = styled.div`
  position: absolute;
  top: 35px;
  border: 1px solid #474c55;
  border-radius: 10px;
  background-color: ${Black};
  min-width: ${props => props.minWidth + 'px'};
  z-index: 1;
`;

const Item = styled.div`
  height: 40px;
  border-top: ${props => (props.first ? '0px' : '1px')} solid #474c55;
  color: ${props => (props.selected ? Yellow : Darkgrey)};
  margin: 0px 10px;
  padding: 0px 15px;
  display: flex;
  align-items: center;
`;

export default Droplist;
