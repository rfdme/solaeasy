import React from 'react';
import styled from 'styled-components';

import { Yellow } from '../CommonColor';

const ProgressBar = ({ value }) => {
  const percentage = (value * 100).toFixed(2) + '%';
  return (
    <BarContainer>
      <Bar width={percentage} />
    </BarContainer>
  );
};

const BarContainer = styled.div`
  background-color: #1f1f22;
  border-radius: 10px;
  padding: 3px;
  width: 100%;
`;
const Bar = styled.div`
  background-color: ${Yellow};
  width: ${props => props.width};
  height: 10px;
  border-radius: 10px;
`;

export default ProgressBar;
