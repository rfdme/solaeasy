import React, { Component } from 'react';
import styled from 'styled-components';

import { Darkgrey, Lightgrey, HrColor } from '../CommonColor';
import icon from '../../svg/icon_power_title.svg';
import AR from '../../svg/icon_list_arrow_right.png';
import AD from '../../svg/icon_list_arrow_down.png';

class Card extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false
    };
  }

  handleToggle = () => {
    this.setState({ open: !this.state.open });
  };

  render() {
    const { name, date, data } = this.props.data;

    let rows = [];
    let cols = [];
    let i = 0;
    while (i < data.length) {
      cols.push(
        <Col key={i % 2}>
          <Field>{data[i].field}</Field>
          <Value>{data[i].value}</Value>
        </Col>
      );
      if (i % 2 === 1) {
        rows.push(<Row key={i}>{cols}</Row>);
        cols = [];
      }
      i++;
    }
    if (cols.length > 0) {
      rows.push(<Row key={i}>{cols}</Row>);
    }

    return (
      <div>
        <Header onClick={this.handleToggle}>
          <HeaderContent>
            <Img
              src={icon}
              style={{
                width: '29px',
                marginRight: '10px'
              }}
            />
            <span style={{ marginRight: '30px' }}>{name}</span>
            <span>{date}</span>
          </HeaderContent>
          <Img src={this.state.open ? AD : AR} />
        </Header>
        {this.state.open ? rows : null}
      </div>
    );
  }
}

const Img = styled.img``;

const Div = styled.div`
  height: 50px;
  margin: 0 15px;
  border-bottom: 1px solid ${HrColor};
  display: flex;
  align-items: center;
  font-size: 15px;
`;

const Header = styled(Div)`
  color: ${Darkgrey};
  justify-content: space-between;
`;

const HeaderContent = styled.div`
  display: flex;
  align-items: center;
`;

const Row = styled(Div)`
  color: ${Lightgrey};
  justify-content: space-between;
`;

const Col = styled.div`
  width: 45%;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const Field = styled.div`
  color: ${Darkgrey};
`;

const Value = styled.div`
  color: ${Lightgrey};
`;

export default Card;
