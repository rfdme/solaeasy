import styled from 'styled-components';

import { PaperColor } from '../CommonColor';

const Container = styled.div`
  flex: 1;
  display: flex;
  flex-wrap: wrap;
  align-content: flex-start;
  margin: 1vw;
  overflow: auto;
`;

const Paper = styled.div`
  width: 98vw;
  height: 100vh;
  background-color: ${PaperColor};
  border-radius: 4px;
  margin: 1vw;
  overflow: auto;
`;

export { Container, Paper };
