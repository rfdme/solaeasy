import React from 'react';
import styled from 'styled-components';

import { ProgressBar } from '../Widget';
import { Darkgrey, Lightgrey, HrColor } from '../CommonColor';

const Item = ({ id, name, price, achievingRate, onClick }) => (
  <Div onClick={onClick}>
    <Title>{id + ' ' + name}</Title>
    <Row style={{ margin: '22px 0 19px' }}>
      <Label>價格</Label>
      <Value>{price}</Value>
    </Row>
    <Row>
      <Label>發電達成率</Label>
      <Value>{(achievingRate * 100).toFixed(0) + '%'}</Value>
    </Row>
    <Row style={{ margin: '12px 0 18px' }}>
      <ProgressBar value={achievingRate} />
    </Row>
  </Div>
);

const Div = styled.div`
  margin: 15px 15px 30px 15px;
  border-bottom: 1px solid ${HrColor};
`;

const Title = styled.div`
  font-size: 20px;
  color: ${Lightgrey};
  margin-bottom: 22px;
`;

const Row = styled.div`
  font-size: 15px;
  display: flex;
  justify-content: space-between;
`;

const Label = styled.div`
  color: ${Darkgrey};
`;

const Value = styled.div`
  color: ${Lightgrey};
`;

export default Item;
