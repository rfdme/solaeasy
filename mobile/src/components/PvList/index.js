import React, { Component } from 'react';
import styled from 'styled-components';

import { Container, Paper } from '../CommonLayout';
import { Yellow, Darkgrey, BgColor } from '../CommonColor';
import Item from './Item';

class PvList extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  selectPv = id => {
    this.props.onSelect(id);
  };

  render() {
    return (
      <Container>
        <Paper>
          <Tabs>
            <Tab selected={true}>您關注的電廠</Tab>
            <Tab>所有電廠</Tab>
          </Tabs>
          <Item
            id="SNTW1"
            name="金耘鋼鐵"
            price="32.4070456"
            achievingRate={0.85}
            onClick={() => this.selectPv('SNTW1')}
          />
          <Item
            id="SNTW2"
            name="金耘鋼鐵"
            price="32.4070456"
            achievingRate={0.85}
            onClick={() => this.selectPv('SNTW2')}
          />
          <Item
            id="SNTW3"
            name="金耘鋼鐵"
            price="32.4070456"
            achievingRate={0.85}
            onClick={() => this.selectPv('SNTW3')}
          />
          <Item
            id="SNTW4"
            name="金耘鋼鐵"
            price="32.4070456"
            achievingRate={0.85}
            onClick={() => this.selectPv('SNTW4')}
          />
          <Item
            id="SNTW5"
            name="金耘鋼鐵"
            price="32.4070456"
            achievingRate={0.85}
            onClick={() => this.selectPv('SNTW5')}
          />
        </Paper>
      </Container>
    );
  }
}

const Tabs = styled.div`
  display: flex;
  align-items: center;
  height: 50px;
  border-bottom: 3px solid ${BgColor};
  font-size: 15px;
`;

const Tab = styled.div`
  color: ${props => (props.selected ? Yellow : Darkgrey)};
  border: 1px solid ${props => (props.selected ? Yellow : Darkgrey)};
  border-radius: 20px;
  padding: 0 15px;
  margin-left: 15px;
`;

export default PvList;
