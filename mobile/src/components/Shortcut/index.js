import React from 'react';
import styled from 'styled-components';

import mask from '../../svg/bk_icon_path.svg';
import { Yellow, PaperColor } from '../CommonColor';

const Shortcut = ({ icon, name, onClick }) => (
  <Div onClick={onClick}>
    <Mask src={mask} />
    <Icon src={icon} />
    <Name>{name}</Name>
  </Div>
);

const Div = styled.div`
  width: 47vw;
  height: 47vw;
  background-color: ${PaperColor};
  border-radius: 4px;
  margin: 1vw;
  position: relative;
`;

const Mask = styled.img`
  position: absolute;
  width: 100%;
`;

const Icon = styled.img`
  position: absolute;
  width: 20vw;
  left: 50%;
  top: 45%;
  transform: translate(-50%, -50%);
`;

const Name = styled.div`
  position: absolute;
  left: 50%;
  top: 80%;
  transform: translate(-50%, -50%);
  font-size: 17px;
  color: ${Yellow};
`;

export default Shortcut;
