import React from 'react';

import Shortcut from '../Shortcut';
import { Container } from '../CommonLayout';

import icon2 from '../../svg/icon_menu_nav_2.svg';
//import icon3 from '../../svg/icon_menu_nav_3.svg';
import icon4 from '../../svg/icon_menu_nav_4.svg';
import icon5 from '../../svg/icon_menu_nav_5.svg';
import icon6 from '../../svg/icon_menu_nav_6.svg';
//import icon7 from '../../svg/icon_menu_nav_7.svg';
import icon8 from '../../svg/icon_menu_nav_3.svg';

const Home = ({ selectPage }) => (
  <Container>
    <Shortcut
      icon={icon2}
      name="電廠資料"
      onClick={() => selectPage('pvList')}
    />
    {/*}
    <Shortcut
      icon={icon3}
      name="交易中心"
      onClick={() => selectPage('noteList')}
    />*/}
    <Shortcut
      icon={icon4}
      name="事件日曆"
      onClick={() => selectPage('eventBook')}
    />
    <Shortcut
      icon={icon5}
      name="明細查詢"
      onClick={() => selectPage('detail')}
    />
    <Shortcut
      icon={icon6}
      name="資產總覽"
      onClick={() => selectPage('testPage')}
    />
    {/*}
    <Shortcut
      icon={icon7}
      name="NGO捐贈"
      onClick={() => selectPage('testPage')}
    />
    />*/}
    <Shortcut
      icon={icon8}
      name="累積收益"
      onClick={() => selectPage('profit')}
    />
  </Container>
);

export default Home;
