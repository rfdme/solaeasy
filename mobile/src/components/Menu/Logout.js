import React from 'react';

import { Item, Button } from './Layout';

const Logout = ({ logined }) => {
  if (logined) {
    return (
      <Item>
        <Button>登出</Button>
      </Item>
    );
  } else {
    return null;
  }
};

export default Logout;
