import React from 'react';
import styled from 'styled-components';

import Account from './Account';
import Logout from './Logout';
import Link from './Link';

import icon1 from '../../svg/icon_menu_nav_1a.svg';
import icon2 from '../../svg/icon_menu_nav_2a.svg';
//import icon3 from '../../svg/icon_menu_nav_3a.svg';
import icon4 from '../../svg/icon_menu_nav_4a.svg';
import icon5 from '../../svg/icon_menu_nav_5a.svg';
import icon6 from '../../svg/icon_menu_nav_6a.svg';
//import icon7 from '../../svg/icon_menu_nav_7a.svg';
import icon8 from '../../svg/icon_menu_nav_3a.svg';

const Menu = ({ opened, logined, name, selectPage }) => (
  <Div shift={opened}>
    <Account logined={logined} name={name} />
    <Link icon={icon1} name="首頁" onClick={() => selectPage('home')} />
    <Link icon={icon2} name="電廠資料" onClick={() => selectPage('pvList')} />
    {/*<Link icon={icon3} name="交易中心" onClick={() => selectPage('noteList')} />*/}
    <Link
      icon={icon4}
      name="事件日曆"
      onClick={() => selectPage('eventBook')}
    />
    <Link icon={icon5} name="明細查詢" onClick={() => selectPage('detail')} />
    <Link icon={icon6} name="資產總覽" onClick={() => selectPage('testPage')} />
    {/*<Link icon={icon7} name="NGO捐贈" onClick={() => selectPage('testPage')} />*/}
    <Link icon={icon8} name="累積收益" onClick={() => selectPage('profit')} />
    <Logout logined={logined} />
  </Div>
);

const Div = styled.div`
  position: absolute;
  width: 180px;
  height: 100vh;
  right: 0px;
  display: ${props => (props.shift ? 'block' : 'none')};
  background-color: #202125;
`;

export default Menu;
