import styled from 'styled-components';

import { Darkgrey } from '../CommonColor';

const Item = styled.div`
  height: 39px;
  border-top: 1px solid #27282d;
  margin: 0 10px;
  padding: 5px 0;
  display: flex;
  align-items: center;
  color: ${Darkgrey};
`;

const Icon = styled.img`
  width: 22px;
  margin-right: 10px;
`;

const Name = styled.div`
  line-height: 1.2;
  display: flex;
  align-items: center;
`;

const Button = styled.div`
  height: 20px;
  padding: 0 10px;
  border: 1px solid ${Darkgrey};
  border-radius: 3px;
  line-height: 1.2;
  display: flex;
  align-items: center;
  font-size: 14px;
`;

export { Item, Icon, Name, Button };
