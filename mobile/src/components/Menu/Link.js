import React from 'react';

import { Item, Icon, Name } from './Layout';

const Link = ({ icon, name, onClick }) => (
  <Item onClick={onClick}>
    <Icon src={icon} />
    <Name>{name}</Name>
  </Item>
);

export default Link;
