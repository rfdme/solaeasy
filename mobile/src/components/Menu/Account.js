import React from 'react';

import { Item, Icon, Name, Button } from './Layout';

import icon from '../../svg/icon_menu_nav_member.svg';

const Account = ({ logined, name }) => {
  let items = [];
  if (logined) {
    items.push(<Icon key={1} src={icon} />);
    items.push(<Name key={2}>{name}</Name>);
  } else {
    items.push(<Button>登入</Button>);
  }

  return <Item style={{ border: 'none' }}>{items}</Item>;
};

export default Account;
