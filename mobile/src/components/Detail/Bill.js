import React from 'react';

import { Card } from '../Widget';

const Bill = () => {
  const list = [
    {
      name: 'SNTW1',
      date: '2018-08-08',
      data: [
        {
          field: '帳單起始日',
          value: '2017-09-08'
        },
        {
          field: '帳單結束日',
          value: '2017-11-09'
        },
        {
          field: '分配收益',
          value: '3460元'
        }
      ]
    },
    {
      name: 'SNTW1',
      date: '2018-08-09',
      data: [
        {
          field: '帳單起始日',
          value: '2017-08-08'
        },
        {
          field: '帳單結束日',
          value: '2017-10-09'
        },
        {
          field: '分配收益',
          value: '11064元'
        }
      ]
    }
  ];

  return (
    <div>
      <Card data={list[0]} />
      <Card data={list[1]} />
    </div>
  );
};

export default Bill;
