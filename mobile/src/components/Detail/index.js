import React, { Component } from 'react';
import styled from 'styled-components';

import { Container, Paper } from '../CommonLayout';
import { BgColor } from '../CommonColor';
import { Droplist } from '../Widget';
import Deposit from './Deposit';
import Invest from './Invest';
import Bill from './Bill';

class Detail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedIndex: 0
    };
  }

  handleSelect = index => {
    this.setState({ selectedIndex: index });
  };

  render() {
    const list = ['增資明細', '投資明細', '收益明細'];
    let content = null;
    switch (this.state.selectedIndex) {
      case 0:
        content = <Deposit />;
        break;
      case 1:
        content = <Invest />;
        break;
      case 2:
        content = <Bill />;
        break;
      default:
        break;
    }

    return (
      <Container>
        <Paper>
          <Bar>
            <Droplist list={list} onSelect={this.handleSelect} />
          </Bar>
          {content}
        </Paper>
      </Container>
    );
  }
}

const Bar = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 50px;
  border-bottom: 3px solid ${BgColor};
  padding: 0 20px;
`;

export default Detail;
