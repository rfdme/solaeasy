import React from 'react';
import styled from 'styled-components';

import { Darkgrey, Lightgrey, HrColor } from '../CommonColor';

const Deposit = () => {
  const data = [
    { date: '2018-08-08', sttw: 200000 },
    { date: '2018-08-01', sttw: 100000 }
  ];

  let rows = [];
  for (let i = 0; i < data.length; i++) {
    rows.push(
      <Row key={i}>
        <Col>{data[i].date}</Col>
        <Col>{data[i].sttw}</Col>
      </Row>
    );
  }

  return (
    <div>
      <Header>
        <Col>日期</Col>
        <Col>金額</Col>
      </Header>
      {rows}
    </div>
  );
};

const Div = styled.div`
  height: 50px;
  margin: 0 15px;
  border-bottom: 1px solid ${HrColor};
  display: flex;
  font-size: 15px;
`;

const Header = styled(Div)`
  color: ${Darkgrey};
`;

const Row = styled(Div)`
  color: ${Lightgrey};
`;

const Col = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
`;

export default Deposit;
