import React from 'react';
import { Card } from '../Widget';

const Invest = () => {
  const list = [
    {
      name: 'SNTW1',
      date: '2018-08-08',
      data: [
        {
          field: '數量',
          value: 10000
        },
        {
          field: '金額',
          value: 100000
        },
        {
          field: 'test',
          value: 'test'
        }
      ]
    },
    {
      name: 'SNTW2',
      date: '2018-08-09',
      data: [
        {
          field: '數量',
          value: 15000
        },
        {
          field: '金額',
          value: 150000
        },
        {
          field: 'test',
          value: 'test'
        }
      ]
    }
  ];

  return (
    <div>
      <Card data={list[0]} />
      <Card data={list[1]} />
    </div>
  );
};

export default Invest;
