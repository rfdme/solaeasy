import React, { Component } from 'react';
import styled, { injectGlobal } from 'styled-components';

import { BgColor } from './components/CommonColor';
import Header from './components/Header';
import Footer from './components/Footer';
import Menu from './components/Menu';
import Home from './components/Home';
import PvList from './components/PvList';
import PvInfo from './components/PvInfo';
import NoteList from './components/NoteList';
import Exchange from './components/Exchange';
import EventBook from './components/EventBook';
import Detail from './components/Detail';
import Profit from './components/Profit';
import TestPage from './components/TestPage';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menuOpened: false,
      showPage: 'home',
      pageStack: [],
      selectedPv: null,
      selectNote: null
    };
  }

  componentDidMount() {}

  toggleMenu = () => {
    this.setState({ menuOpened: !this.state.menuOpened });
  };

  selectPage = newPage => {
    let { pageStack } = this.state;
    pageStack.push(this.state.showPage);
    this.setState({
      menuOpened: false,
      showPage: newPage,
      pageStack: pageStack
    });
  };

  backPage = () => {
    let { pageStack } = this.state;
    let page = pageStack.pop();
    this.setState({
      showPage: page,
      pageStack: pageStack
    });
  };

  selectPv = (id, name) => {
    this.setState({ selectedPv: { id, name } });
    this.selectPage('pvInfo');
  };

  selectNote = (id, name) => {
    this.setState({ selectedNote: { id, name } });
    this.selectPage('exchange');
  };

  render() {
    let main = null;
    switch (this.state.showPage) {
      case 'home':
        main = (
          <Main shift={this.state.menuOpened}>
            <Page className="page-wrap">
              <Header menuClick={this.toggleMenu} />
              <Home selectPage={this.selectPage} />
            </Page>
            <Footer />
          </Main>
        );
        break;
      case 'pvList':
        main = (
          <Main shift={this.state.menuOpened}>
            <Page>
              <Header
                menuClick={this.toggleMenu}
                title="電廠資料"
                backClick={this.backPage}
              />
              <PvList onSelect={this.selectPv} />
            </Page>
          </Main>
        );
        break;
      case 'pvInfo':
        main = (
          <Main shift={this.state.menuOpened}>
            <Page>
              <Header
                menuClick={this.toggleMenu}
                title={this.state.selectedPv.id}
                backClick={this.backPage}
              />
              <PvInfo />
            </Page>
          </Main>
        );
        break;
      case 'noteList':
        main = (
          <Main shift={this.state.menuOpened}>
            <Page>
              <Header
                menuClick={this.toggleMenu}
                title="交易中心"
                backClick={this.backPage}
              />
              <NoteList onSelect={this.selectNote} />
            </Page>
          </Main>
        );
        break;
      case 'exchange':
        main = (
          <Main shift={this.state.menuOpened}>
            <Page>
              <Header
                menuClick={this.toggleMenu}
                title={this.state.selectedNote.id}
                backClick={this.backPage}
              />
              <Exchange />
            </Page>
          </Main>
        );
        break;
      case 'eventBook':
        main = (
          <Main shift={this.state.menuOpened}>
            <Page>
              <Header
                menuClick={this.toggleMenu}
                title="事件日曆"
                backClick={this.backPage}
              />
              <EventBook />
            </Page>
          </Main>
        );
        break;
      case 'detail':
        main = (
          <Main shift={this.state.menuOpened}>
            <Page>
              <Header
                menuClick={this.toggleMenu}
                title="明細查詢"
                backClick={this.backPage}
              />
              <Detail />
            </Page>
          </Main>
        );
        break;
      case 'profit':
        main = (
          <Main shift={this.state.menuOpened}>
            <Page>
              <Header
                menuClick={this.toggleMenu}
                title="累積收益"
                backClick={this.backPage}
              />
              <Profit />
            </Page>
          </Main>
        );
        break;
      default:
        main = (
          <Main shift={this.state.menuOpened}>
            <Page>
              <Header
                menuClick={this.toggleMenu}
                title="Test"
                backClick={this.backPage}
              />
              <TestPage />
            </Page>
          </Main>
        );
        break;
    }

    return (
      <BG>
        {main}
        <Menu
          opened={this.state.menuOpened}
          logined={true}
          name={'alan@sola.com'}
          selectPage={this.selectPage}
        />
      </BG>
    );
  }
}

const BG = styled.div`
  width: 100vw;
  height: 100vh;
  background-color: ${BgColor};
`;

const Main = styled.div`
  position: absolute;
  width: 100vw;
  height: 100vh;
  left: ${props => (props.shift ? '-180px' : '0px')};
`;

const Page = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
`;

injectGlobal`
  @import url(https://fonts.googleapis.com/earlyaccess/notosanstc.css);
  @import url(https://fonts.googleapis.com/css?family=Montserrat:300);
  
  body {
    margin: 0;
        font-family: Arial, "Noto Sans", "Noto Sans TC", "Microsoft Yahei" !important;
        font-weight: 300;
  }

  //Sticky Footer
  .page-wrap {
    min-height: 100%;
    /* equal to footer height */
    margin-top: -1px;
    margin-bottom: -41px;
  }
  .page-wrap:after {
    content: "";
    display: block;
  }
  .site-footer, .page-wrap:after {
    height: 40px; 
  }

  //scrollbar style
  ::-webkit-scrollbar
  {
    width: 0px;
  }
`;

export default App;
